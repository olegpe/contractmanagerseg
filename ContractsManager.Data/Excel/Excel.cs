﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Xps.Packaging;
using ContractsManager.Data.Repository;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using _Excel = Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace ContractsManager.Data.Excel
{
    public class Excel
    {
        private string _path = "";
        //int _sheet;
        Application _excel = new _Excel.Application();
        Workbook _wb;

        public Excel(string path)
        {
            this._path = path;
            this._wb = _excel.Workbooks.Open(path);
        }

        public IList<BoqSheet> GetSheets()
        {
            List<BoqSheet> sheets = new List<BoqSheet>();
            foreach (Worksheet sheet in _wb.Sheets)
            {
                sheets.Add(new BoqSheet { Name = sheet.Name });
            }
            return sheets;
        }
        public IList<BoqItem> GetItems(BoqSheet boqSheet, ref double orderBoq, UnitOfWork unitOfWork)
        {
            List<BoqItem> boqItems = new List<BoqItem>();
            var isExist = _wb.Worksheets.OfType<Worksheet>().FirstOrDefault(ws => ws.Name == boqSheet.Name);
            if (isExist == null)
            {
                //boqSheet.IsActive = false;
                return null;
            }
            else
                boqSheet.IsActive = true;
            object[,] array = _wb.Sheets[boqSheet.Name].UsedRange.Value2;

            Range range = _wb.Sheets[boqSheet.Name].UsedRange;
            int rowCount = range.Rows.Count;
            int colCount = range.Columns.Count;

            var costCodes = unitOfWork.CostCodeLibraryRepository.Get();


            if (array == null)
                return null;
            for (int i = 1; i <= rowCount; i++)
            {
                if (array[i, 1] == null)
                    continue;
                string val = array[i, 1].ToString().Trim();

                if (Regex.IsMatch(val, "(([0-9a-zA-Z]+.?)|-.?)+"))

                {
                    var qte = array[i, 5];
                    var pu = array[i, 6];
                    BoqItem boqParent = new BoqItem();
                    boqParent.No = val == "-" ? 0.ToString() : val;
                    boqParent.Key = Convert.ToString(array[i, 2]);
                    var unit = Convert.ToString(array[i, 3]);
                    if (unit.Trim() == "u")
                        unit = "U";
                    else if (unit.Trim() == "FT")
                        unit = "ft";
                    else if (unit.Trim() == "d")
                        unit = "D";
                    else if (unit.Trim() == "ENS")
                        unit = "ens";
                    else if (unit.Trim() == "ML")
                        unit = "ml";
                    else if (unit.Trim() == "M²")
                        unit = "m2";
                    else if (unit.Trim() == "m²")
                        unit = "m2";
                    else if (unit.Trim() == "Kg")
                        unit = "kg";
                    else if (unit.Trim() == "m³")
                        unit = "m3";

                    else if (unit.Trim() == "M³")
                        unit = "m3";
                    boqParent.Unite = unit != null ? unit.Trim() : null;
                    boqParent.CostCode = costCodes.Where(x => x.Code == Convert.ToString(array[i, 4])).FirstOrDefault();
                    if (qte is string)
                        boqParent.Qte = ((string)qte).Trim() == "-" ? 0 : (double)Convert.ToDouble(qte);
                    else
                    {
                        if (qte != null)
                            boqParent.Qte = (double)Convert.ToDouble(qte);
                    }
                    if (pu is string)
                        boqParent.Pu = ((string)pu).Trim() == "-" ? 0 : (double)Convert.ToDouble(pu);
                    else
                    {
                        if (pu != null)
                            boqParent.Pu = (double)Convert.ToDouble(pu);
                    };
                    boqParent.BOQType = BOQType.Line;
                    boqParent.OrderBoq = orderBoq + 1.0f;

                    boqItems.Add(boqParent);
                }
                orderBoq += 1.0f;
            }
            Marshal.ReleaseComObject(range);

            return boqItems;

        }
        public IList<Vo> GetVoItems(BoqSheet boqSheet, ref double orderBoq, UnitOfWork unitOfWork, int level)
        {
            List<Vo> boqItems = new List<Vo>();
            var isExist = _wb.Worksheets.OfType<Worksheet>().FirstOrDefault(ws => ws.Name == boqSheet.Name);
            if (isExist == null)
            {
                //boqSheet.IsActive = false;
                return null;
            }
            else
                boqSheet.IsActive = true;
            object[,] array = _wb.Sheets[boqSheet.Name].UsedRange.Value2;

            Range range = _wb.Sheets[boqSheet.Name].UsedRange;
            int rowCount = range.Rows.Count;
            int colCount = range.Columns.Count;

            var costCodes = unitOfWork.CostCodeLibraryRepository.Get();


            if (array == null)
                return null;
            for (int i = 1; i <= rowCount; i++)
            {
                if (array[i, 1] == null)
                    continue;
                string val = array[i, 1].ToString().Trim();

                if (Regex.IsMatch(val, "(([0-9a-zA-Z]+.?)|-.?)+"))

                {
                    var qte = array[i, 5];
                    var pu = array[i, 6];
                    Vo boqParent = new Vo();
                    boqParent.No = val == "-" ? 0.ToString() : val;
                    boqParent.Key = Convert.ToString(array[i, 2]);
                    var unit = Convert.ToString(array[i, 3]);
                    if (unit.Trim() == "u")
                        unit = "U";
                    else if (unit.Trim() == "FT")
                        unit = "ft";
                    else if (unit.Trim() == "d")
                        unit = "D";
                    else if (unit.Trim() == "ENS")
                        unit = "ens";
                    else if (unit.Trim() == "ML")
                        unit = "ml";
                    else if (unit.Trim() == "M²")
                        unit = "m2";
                    else if (unit.Trim() == "m²")
                        unit = "m2";
                    else if (unit.Trim() == "Kg")
                        unit = "kg";
                    else if (unit.Trim() == "m³")
                        unit = "m3";

                    else if (unit.Trim() == "M³")
                        unit = "m3";
                    boqParent.Unite = unit != null ? unit.Trim() : null;
                    boqParent.CostCode = costCodes.Where(x => x.Code == Convert.ToString(array[i, 4])).FirstOrDefault();
                    if (qte is string)
                        boqParent.Qte = ((string)qte).Trim() == "-" ? 0 : (double)Convert.ToDouble(qte);
                    else
                    {
                        if (qte != null)
                            boqParent.Qte = (double)Convert.ToDouble(qte);
                    }
                    if (pu is string)
                        boqParent.Pu = ((string)pu).Trim() == "-" ? 0 : (double)Convert.ToDouble(pu);
                    else
                    {
                        if (pu != null)
                            boqParent.Pu = (double)Convert.ToDouble(pu);
                    };
                    boqParent.Level = level;
                    boqParent.BOQType = BOQType.Line;
                    boqParent.OrderVo = orderBoq + 1.0f;

                    boqItems.Add(boqParent);
                }
                orderBoq += 1.0f;
            }
            Marshal.ReleaseComObject(range);

            return boqItems;

        }
        public IList<ContractBoqItem> GetContractBoqItems(BoqSheet boqSheet, ContractsDataset contractdataset, ref double orderBoq, UnitOfWork unitOfWork)
        {
            List<ContractBoqItem> boqItems = new List<ContractBoqItem>();
            var isExist = _wb.Worksheets.OfType<Worksheet>().FirstOrDefault(ws => ws.Name == boqSheet.Name);
            if (isExist == null)
            {
                boqSheet.IsActive = false;
                return null;
            }
            else
                boqSheet.IsActive = true;

            Range range = _wb.Sheets[boqSheet.Name].UsedRange;
            object[,] array = _wb.Sheets[boqSheet.Name].UsedRange.Value2;

            int rowCount = range.Rows.Count;
            int colCount = range.Columns.Count;
            var costCodes = unitOfWork.CostCodeLibraryRepository.Get();

            //ContractBoqItem boqParent = null;
            //ContractBoqItem boqItem = null;

            for (int i = 1; i <= rowCount; i++)
            {
                if (array[i, 1] == null || array[i, 1] == null)
                    continue;
                string val = range.Cells[i, 1].Value2.ToString();
                if (Regex.IsMatch(val, "(([0-9a-zA-Z]+.?)|-.?)+"))
                {
                    var qte = array[i, 5];
                    var pu = array[i, 6];
                    var unit = Convert.ToString(array[i, 3]);
                    if (unit == "u")
                        unit = "U";
                    else if (unit == "FT")
                        unit = "ft";
                    else if (unit == "d")
                        unit = "D";
                    else if (unit == "ENS")
                        unit = "ens";
                    else if (unit == "ML")
                        unit = "ml";
                    else if (unit == "M²")
                        unit = "m2";
                    else if (unit == "m²")
                        unit = "m2";

                    else if (unit == "m³")
                        unit = "m3";
                    else if (unit == "Kg")
                        unit = "kg";

                    else if (unit == "M³")
                        unit = "m3";

                    ContractBoqItem boqParent = new ContractBoqItem();
                    boqParent.No = Convert.ToString(array[i, 1]);
                    boqParent.Key = Convert.ToString(array[i, 2]);
                    boqParent.Unite = unit != null ? unit.Trim() : null;
                    boqParent.CostCode = costCodes.Where(x => x.Code == Convert.ToString(array[i, 4]).Trim()).FirstOrDefault();
                    if (qte is string)
                        boqParent.Qte = ((string)qte).Trim() == "-" ? 0 : (double)Convert.ToDouble(qte);
                    else
                    {
                        if (qte != null)
                            boqParent.Qte = (double)Convert.ToDouble(qte);
                    }
                    if (pu is string)
                        boqParent.Pu = ((string)pu).Trim() == "-" ? 0 : (double)Convert.ToDouble(pu);
                    else
                    {
                        if (pu != null)
                            boqParent.Pu = (double)Convert.ToDouble(pu);
                    };
                    boqParent.BOQType = BOQType.Line;
                    boqParent.OrderBoq = orderBoq + 1.0f;
                    boqParent.ContractsDataset = contractdataset;
                    boqItems.Add(boqParent);
                }
                orderBoq += 1.0f;
            }
            Marshal.ReleaseComObject(range);

            return boqItems;

        }
        public IList<ContractVo> GetContractVoItems(BoqSheet boqSheet, VoDataset contractdataset, ref double orderBoq, UnitOfWork unitOfWork)
        {
            List<ContractVo> boqItems = new List<ContractVo>();
            var isExist = _wb.Worksheets.OfType<Worksheet>().FirstOrDefault(ws => ws.Name == boqSheet.Name);
            if (isExist == null)
            {
                boqSheet.IsActive = false;
                return null;
            }
            else
                boqSheet.IsActive = true;

            Range range = _wb.Sheets[boqSheet.Name].UsedRange;
            object[,] array = _wb.Sheets[boqSheet.Name].UsedRange.Value2;

            int rowCount = range.Rows.Count;
            int colCount = range.Columns.Count;
            var costCodes = unitOfWork.CostCodeLibraryRepository.Get();

            //ContractBoqItem boqParent = null;
            //ContractBoqItem boqItem = null;

            for (int i = 1; i <= rowCount; i++)
            {
                if (array[i, 1] == null || array[i, 1] == null)
                    continue;
                string val = range.Cells[i, 1].Value2.ToString();
                if (Regex.IsMatch(val, "(([0-9a-zA-Z]+.?)|-.?)+"))
                {
                    var qte = array[i, 5];
                    var pu = array[i, 6];
                    var unit = Convert.ToString(array[i, 3]);
                    if (unit == "u")
                        unit = "U";
                    else if (unit == "FT")
                        unit = "ft";
                    else if (unit == "d")
                        unit = "D";
                    else if (unit == "ENS")
                        unit = "ens";
                    else if (unit == "ML")
                        unit = "ml";
                    else if (unit == "M²")
                        unit = "m2";
                    else if (unit == "m²")
                        unit = "m2";

                    else if (unit == "m³")
                        unit = "m3";
                    else if (unit == "Kg")
                        unit = "kg";

                    else if (unit == "M³")
                        unit = "m3";

                    ContractVo boqParent = new ContractVo();
                    boqParent.No = Convert.ToString(array[i, 1]);
                    boqParent.Key = Convert.ToString(array[i, 2]);
                    boqParent.Unite = unit != null ? unit.Trim() : null;
                    boqParent.CostCode = costCodes.Where(x => x.Code == Convert.ToString(array[i, 4]).Trim()).FirstOrDefault();
                    if (qte is string)
                        boqParent.Qte = ((string)qte).Trim() == "-" ? 0 : (double)Convert.ToDouble(qte);
                    else
                    {
                        if (qte != null)
                            boqParent.Qte = (double)Convert.ToDouble(qte);
                    }
                    if (pu is string)
                        boqParent.Pu = ((string)pu).Trim() == "-" ? 0 : (double)Convert.ToDouble(pu);
                    else
                    {
                        if (pu != null)
                            boqParent.Pu = (double)Convert.ToDouble(pu);
                    };
                    boqParent.BOQType = BOQType.Line;
                    boqParent.OrderVo = orderBoq + 1.0f;
                    boqItems.Add(boqParent);
                }
                orderBoq += 1.0f;
            }
            Marshal.ReleaseComObject(range);

            return boqItems;

        }

        public IList<Po> GetVenteLocal()
        {
            Range range = _wb.Sheets[1].UsedRange;
            int rowCount = range.Rows.Count;
            int colCount = range.Columns.Count;
            List<Po> lvente = new List<Po>();

            for (int i = 2; i <= rowCount; i++)
            {
                Po v = new Po();
                v.PoRef = Convert.ToString(range.Cells[i, 1].Value2);
                v.Item = Convert.ToString(range.Cells[i, 2].Value2);
                //v.Acronym = Convert.ToString(range.Cells[i, 3].Value2);
                //v.Subcontractor = Convert.ToString(range.Cells[i, 4].Value2);
                //v.Contract = Convert.ToString(range.Cells[i, 5].Value2);
                v.Unit = Convert.ToString(range.Cells[i, 3].Value2);
                v.OrderdQte = (double)Convert.ToDouble(range.Cells[i, 4].Value2);
                //var val = range.Cells[i, 8].Value2.ToString();

                v.UnitPrice = (double)Convert.ToDouble(range.Cells[i, 5].Value2);


                //v.TotalSale = (double)Convert.ToDouble(range.Cells[i, 9].Value2);
                v.DeliveredQte = (double)Convert.ToDouble(range.Cells[i, 7].Value2);
                //v.Consumes = (double)Convert.ToDouble(range.Cells[i, 11].Value2);
                lvente.Add(v);
            }
            Marshal.ReleaseComObject(range);

            return lvente;

        }
        public IList<CostCodeLibrary> GetCostCode()
        {
            Range range = _wb.Sheets[1].UsedRange;

            try
            {
                int rowCount = range.Rows.Count;
                int colCount = range.Columns.Count;
                List<CostCodeLibrary> costcode = new List<CostCodeLibrary>();

                for (int i = 2; i <= rowCount; i++)
                {
                    CostCodeLibrary v = new CostCodeLibrary();
                    v.En = Convert.ToString(range.Cells[i, 1].Value2);
                    v.Fr = Convert.ToString(range.Cells[i, 2].Value2);
                    v.Code = Convert.ToString(range.Cells[i, 3].Value2);
                    v.Bold = Convert.ToBoolean(range.Cells[i, 3].Font.Bold);
                    Range r = range.Cells[i, 3] as Range;
                    int colorNumber = System.Convert.ToInt32(r.Font.Color);
                    Color color = ColorTranslator.FromOle(colorNumber);
                    v.Color = color.Name;
                    costcode.Add(v);
                }
                Marshal.ReleaseComObject(range);
                return costcode;

            }
            catch (Exception ex)
            {
                Marshal.ReleaseComObject(range);
                System.Windows.MessageBox.Show("File format not correct", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return null;
            }



        }

        public string GetBoqAttachment(string fileName, ContractsDataset contractsDataset)
        {
            Worksheet xlSht = _wb.Sheets[1];
            //_excel.Visible = true;
            var boqItems = contractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).OrderBy(x => x.OrderBoq).ToList();
            Range xrange = xlSht.UsedRange;
            var tax = new UnitOfWork().ConfigurationRepository.Get(x => x.Key == "TAX").FirstOrDefault();
            var sum = boqItems.Sum(x => x.Pt);

            xrange.Cells[1, 1].Value2 = contractsDataset.Project.Name;
            xrange.Cells[2, 1].Value2 = contractsDataset.BoqSheet.Name;
            xrange.Cells[7, 1].Value2 = "TOTAL TRAVAUX DU " + contractsDataset.BoqSheet.Name;
            xrange.Cells[7, 7].Value2 = sum;

            xrange.Cells[8, 6].Value2 = tax.Value+"%";
            xrange.Cells[8, 7].Value2 = (Convert.ToDouble(tax.Value) / 100) * sum;
            xrange.Cells[9, 7].Value2 = sum +((Convert.ToDouble(tax.Value) / 100) * sum);
            xrange.Cells[9, 1].Value2 = "TOTAL GENERAL DE TRAVAUX "+ contractsDataset.BoqSheet.Name + "   - TTC";

            int i = 7;
            foreach (var item in boqItems)
            {
                xrange.Rows[i].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                if(item.Unite == null || item.Unite == "")
                {
                   // Range range = (Range)xlSht.Cells[5,7];
                    Range range = (Range)xlSht.get_Range("A5:G5");
                    range.Copy();
                    Range dest = xlSht.get_Range("A"+i+":G"+i);
                    dest.PasteSpecial(XlPasteType.xlPasteFormats);

                  
                    xrange.Cells[i, 2].Value2 = item.Key;
                }
                else
                {
                    Range range = (Range)xlSht.get_Range("A6:G6");
                    range.Copy();
                    Range dest = xlSht.get_Range("A" + i + ":G" + i);
                    dest.PasteSpecial(XlPasteType.xlPasteFormats);

                    xrange.Cells[i, 1].Value2 = item.No;
                    xrange.Cells[i, 2].Value2 = item.Key;
                    xrange.Cells[i, 3].Value2 = item.Unite;
                    xrange.Cells[i, 4].Value2 = item.CostCode.Code;
                    xrange.Cells[i, 5].Value2 = item.Qte;
                    xrange.Cells[i, 6].Value2 = item.Pu;
                    xrange.Cells[i, 7].Value2 = item.Pt;
                }
                i++;
            }
            xrange.Rows[6].Delete();
            xrange.Rows[5].Delete();
            _wb.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, fileName);
            _excel.DisplayAlerts = false;
            Close();

            return "";
        }
        public void ExportSiteReports(List<ContractsDataset> contracts, Project project, ProgressDialogController controller, double progress, double Tax, List<Currency> Currencies)
        {
            Range range = _wb.Sheets[2].UsedRange;
            _wb.Sheets[1].Name = "Recap " + project.Name;
            _wb.Sheets[2].Name = project.Name;
            double USD = Currencies.FirstOrDefault(x => x.Currencies == project.Currency.Currencies).ConversionRate;
            double EUR = USD / Currencies.FirstOrDefault(x => x.Currencies == "EUR").ConversionRate;
            double count = (double)((50.0 / project.ContractDatasets.Count) / 100.0);
            //var firstRow = range.Rows[15];
            foreach (var item in contracts)
            {
                range.Rows[3].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                range.Cells[3, 1].Value2 = item.ContractNumber;
                range.Cells[3, 2].Value2 = item.Contract.Type;
                range.Cells[3, 3].Value2 = item.Created.ToString("dd-MMM-yy");
                range.Cells[3, 4].Value2 = item.SubName;
                range.Cells[3, 5].Value2 = item.BoqSheet.Name;
                range.Cells[3, 6].Value2 = item.Currency.Currencies;
                range.Cells[3, 7].Value2 = item.Rate;
                range.Cells[3, 8].Value2 = item.TotalBoq;
                range.Cells[3, 9].Value2 = item.Additions;
                range.Cells[3, 10].Value2 = -item.Ommissions;
                range.Cells[3, 11].Value2 = item.TotalBAO;
                range.Cells[3, 12].Value2 = item.AdvancePayment;
                range.Cells[3, 13].Value2 = item.AdvancePaymentPercentage;
                range.Cells[3, 14].Value2 = item.TotalCumul;
                range.Cells[3, 15].Value2 = item.TotalCumulPercentage;
                range.Cells[3, 16].Value2 = -item.TotalProrataCumul;
                range.Cells[3, 17].Value2 = -item.TotalProrataCumulPercentage;
                range.Cells[3, 18].Value2 = -item.TotalCumulDeduction;
                range.Cells[3, 19].Value2 = -item.TotalCumulDeductionPercentage;
                range.Cells[3, 20].Value2 = item.NetExecuted;
                range.Cells[3, 21].Value2 = item.NetExecutedPercentage;
                range.Cells[3, 22].Value2 = -item.Retention;
                range.Cells[3, 23].Value2 = -item.RetentionPercentage;
                range.Cells[3, 24].Value2 = -item.ApRecovery;
                range.Cells[3, 25].Value2 = -item.ApRecoveryPercentage;
                range.Cells[3, 26].Value2 = item.Perceived;
                range.Cells[3, 27].Value2 = item.PerceivedTTC;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            progress += count;
                            controller.SetProgress(progress);
                            controller.SetMessage("Generating Excel File ...");

                        }));

            }
            range.Rows[2].Delete();
            Range range2 = _wb.Sheets[1].UsedRange;
            range2.Cells[2, 2].Value2 = project.Name;
            range2.Cells[5, 2].Value2 = project.Currency.Currencies;
            range2.Cells[7, 3].Value2 = Tax+"%";
            range2.Cells[5, 6].Value2 = USD;
            range2.Cells[6, 6].Value2 = EUR;
            System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                       new ThreadStart(async () =>
                       {
                           controller.SetProgress(1);
                          
                           SaveFileDialog sfd = new SaveFileDialog();
                           sfd.FileName = "Report-" + project.Name;
                           var result = sfd.ShowDialog();
                           if (result == true)
                           {
                               _wb.SaveAs(sfd.FileName);

                           }
                           await controller.CloseAsync();
                       }));
           
            Close();

        }
        public void ExportRecapReports()
        {
            //Range range = _wb.Sheets[1].UsedRange;
            //var firstRow = range.Rows[15];
            //rangeSecond.Rows[16].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
        }
        public XpsDocument CreateIpc(Ipc ipc, bool preview, out double totalAmount, UnitOfWork unitOfWork, List<VenteLocal> vente, out string outFilePath, bool save = false)
        {
            outFilePath = "";
            try
            {

                _excel.DisplayAlerts = false;

                //_excel.Visible = true;
                Range range = _wb.Sheets[1].UsedRange;
                int rowCount = range.Rows.Count;
                int colCount = range.Columns.Count;
                double precedRetention = 0.0F, precedAdvancePayment = 0.0F, precedRetentionCumul = 0.0F, precedApRecovery = 0.0F,
                    precedAdvancePaymentCumul = 0.0F, previousPenalty = 0.0F, LastRetention = 0.0F, LastRetentionReleaseAmount = 0.0F;

                var sheet = unitOfWork.SheetsRepository.Get(x => x.Name == ipc.ContractsDataset.BoqSheet.Name).FirstOrDefault();

                List<VenteLocal> venteLocal;
                List<Labor> labor;
                List<Machine> machine;
                // var unitOfWork = new UnitOfWork();
                if (vente == null)
                    venteLocal = unitOfWork.VenteLocalRepository.Get(x => x.Contract == ipc.ContractsDataset.ContractNumber, orderBy: x=>x.OrderByDescending(y => y.Id)).ToList();
                else
                    venteLocal = vente;
                labor = ipc.ContractsDataset.Labors.ToList();
                machine = ipc.ContractsDataset.Machines.ToList();
                if (ipc.Number > 1)
                {
                    var ipcs = unitOfWork.IpcRepository.Get(x => x.ContractsDatasetId == ipc.ContractsDataset.Id).ToList();
                    Ipc lastIpc;
                    if (ipc.Id > 0)
                    {
                        if (ipcs.Count >= 2)
                            lastIpc = ipcs[ipcs.Count - 2];
                        else
                            lastIpc = ipcs.Last();
                        precedRetention = lastIpc.RetentionAmount;
                        precedAdvancePayment = lastIpc.AdvancePaymentAmount;
                        precedAdvancePaymentCumul = lastIpc.AdvancePaymentAmountCumul;
                        precedRetentionCumul = lastIpc.RetentionAmountCumul;
                        previousPenalty = lastIpc.PreviousPenalty;
                        LastRetention = lastIpc.Retention;
                        LastRetentionReleaseAmount = lastIpc.RetentionAmount;
                        precedApRecovery = lastIpc.ApRecovery;
                    }
                    else
                    {
                        lastIpc = ipcs.Last();
                        precedRetention = lastIpc.RetentionAmount;
                        precedAdvancePaymentCumul = lastIpc.AdvancePaymentAmountCumul;
                        precedAdvancePayment = lastIpc.AdvancePaymentAmount;
                        previousPenalty = lastIpc.PreviousPenalty;
                        precedRetentionCumul = lastIpc.RetentionAmountCumul;
                        LastRetention = lastIpc.Retention;
                        LastRetentionReleaseAmount = lastIpc.RetentionAmount;
                        precedApRecovery = lastIpc.ApRecovery;
                    }

                }

                Range rangeSecond = (Range)_wb.Sheets[4].UsedRange;
                int i = 16;
                foreach (var item in machine)
                {
                    var dedcutionPercentage = item.Deduction / 100;
                    var deductionAmount = item.UnitPrice * item.Quantity;
                    var firstRow = rangeSecond.Rows[15];
                    rangeSecond.Rows[16].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                    var secondRow = rangeSecond.Rows[16];
                    secondRow.Value2 = firstRow.Value2;

                    rangeSecond.Cells[16, 1].Value2 = item.Id;
                    rangeSecond.Cells[16, 2].Value2 = item.MachineType;
                    //rangeSecond.Cells[16, 5].Value2 = item.MachineCode;
                    rangeSecond.Cells[16, 4].Value2 = item.Unit;
                    rangeSecond.Cells[16, 6].Value2 = item.Amount;
                    rangeSecond.Cells[16, 9].Value2 = "=H16+G16";

                    if (ipc.Number == 1)
                    {
                        if (deductionAmount != 0)
                        {
                            rangeSecond.Cells[16, 9].Value2 = (double)(dedcutionPercentage * item.Amount) / deductionAmount;
                            rangeSecond.Cells[16, 8].Value2 = (double)(dedcutionPercentage * item.Amount) / deductionAmount;
                        }
                        else
                        {
                            rangeSecond.Cells[16, 9].Value2 = 0;
                            rangeSecond.Cells[16, 8].Value2 = 0;
                        }
                        item.PrecedentAmount = (double)(dedcutionPercentage * item.Amount);
                    }
                    else
                    {
                        if (deductionAmount != 0)
                        {
                            rangeSecond.Cells[16, 9].Value2 = (double)(dedcutionPercentage * item.Amount) / deductionAmount;
                            rangeSecond.Cells[16, 8].Value2 = (double)((dedcutionPercentage * item.Amount) / deductionAmount) - item.PrecedentAmount / deductionAmount;
                            rangeSecond.Cells[16, 7].Value2 = (double)item.PrecedentAmount / deductionAmount;
                        }
                        else
                        {
                            rangeSecond.Cells[16, 9].Value2 = 0;
                            rangeSecond.Cells[16, 8].Value2 = 0;
                            rangeSecond.Cells[16, 7].Value2 = 0;
                        }

                        if (preview == false)
                        {
                            item.PrecedentAmountOld = item.PrecedentAmount;
                            item.PrecedentAmount = (double)(dedcutionPercentage * item.Amount);
                        }
                    }
                    rangeSecond.Cells[16, 10].Value2 = "=G16*F16";
                    rangeSecond.Cells[16, 11].Value2 = "=H16*F16";
                    rangeSecond.Cells[16, 12].Value2 = "=I16*F16";
                    i++;
                    //rangeSecond.Cells[16, 13].Value2 = item.ActualAmount;
                    //rangeSecond.Cells[16, 14].Value2 = item.Deduction;
                }
                if (i > 16)
                    rangeSecond.Rows[15].Delete();
                rangeSecond.Rows[i - 1].Delete();
                i = 13;
                foreach (var item in venteLocal)
                {
                    //var po = unitOfWork.PoRepository.Get(x => x.PoRef == item.Bc).FirstOrDefault();
                    var deductionTotal = item.SaleUnit * (item.Po != null ? item.Po.DeliveredQte : 0);
                    var decutionPercentage = (item.Deduction / 100);
                    var firstRow = rangeSecond.Rows[12]; ;
                    rangeSecond.Rows[13].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                    var secondRow = rangeSecond.Rows[13];
                    secondRow.Value2 = firstRow.Value2;

                    rangeSecond.Cells[13, 1].Value2 = item.Id;
                    rangeSecond.Cells[13, 2].Value2 = item.Designation;
                    //rangeSecond.Cells[26, 5].Value2 = item.cost;
                    rangeSecond.Cells[13, 4].Value2 = item.Unit;
                    rangeSecond.Cells[13, 5].Value2 = item.Bc;
                    rangeSecond.Cells[13, 6].Value2 = deductionTotal;
                    rangeSecond.Cells[13, 9].Value2 = "=H13+G13";

                    if (ipc.Number == 1)
                    {
                        if(deductionTotal != 0)
                        {
                        rangeSecond.Cells[13, 9].Value2 = (double)((decutionPercentage * item.ConsumedAmount) / deductionTotal);
                        rangeSecond.Cells[13, 8].Value2 = (double)((decutionPercentage * item.ConsumedAmount) / deductionTotal);
                        }
                        else
                        {
                            rangeSecond.Cells[13, 9].Value2 = 0;
                            rangeSecond.Cells[13, 8].Value2 = 0;
                        }
                        item.PrecedentAmount = (double)(decutionPercentage * item.ConsumedAmount);
                    }
                    else
                    {

                        if (deductionTotal != 0)
                        {
                            rangeSecond.Cells[13, 9].Value2 = (double)((decutionPercentage * item.ConsumedAmount) / deductionTotal);
                            rangeSecond.Cells[13, 8].Value2 = (double)((decutionPercentage * item.ConsumedAmount) / deductionTotal) - item.PrecedentAmount / deductionTotal;
                            rangeSecond.Cells[13, 7].Value2 = item.PrecedentAmount / deductionTotal;
                        }
                        else
                        {
                            rangeSecond.Cells[13, 9].Value2 = 0;
                            rangeSecond.Cells[13, 8].Value2 = 0;
                        }
                       

                        if (preview == false)
                        {
                            item.PrecedentAmountOld = item.PrecedentAmount;
                            item.PrecedentAmount = (double)(decutionPercentage * item.ConsumedAmount);
                        }
                    }
                    unitOfWork.VenteLocalRepository.Update(item);
                    rangeSecond.Cells[13, 10].Value2 = "=G13*F13";
                    rangeSecond.Cells[13, 11].Value2 = "=H13*F13";
                    rangeSecond.Cells[13, 12].Value2 = "=I13*F13";
                    i++;
                    //rangeSecond.Cells[12, 12].Value2 = item.ConsumedAmount;
                    //rangeSecond.Cells[12, 13].Value2 = item.ActualAmount;
                    //rangeSecond.Cells[12, 14].Value2 = item.Deduction;
                }
                // unitOfWork.Save();
                if (i > 13)
                    rangeSecond.Rows[12].Delete();
                rangeSecond.Rows[i - 1].Delete();
                i = 10;
                foreach (var item in labor)
                {
                    var firstRow = rangeSecond.Rows[9];
                    var deductionPercentage = item.Deduction / 100;
                    var deductionTotal = item.UnitPrice * item.Quantity;
                    rangeSecond.Rows[10].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                    var secondRow = rangeSecond.Rows[10];
                    secondRow.Value2 = firstRow.Value2;

                    rangeSecond.Cells[10, 1].Value2 = item.Id;
                    rangeSecond.Cells[10, 2].Value2 = item.ActivityDescription;
                    //rangeSecond.Cells[26, 5].Value2 = item.cost;
                    rangeSecond.Cells[10, 4].Value2 = item.Unit;//4
                    //rangeSecond.Cells[10, 7].Value2 = item.TypeOfWorker;
                    rangeSecond.Cells[10, 6].Value2 = item.Amount;//6

                    rangeSecond.Cells[10, 9].Value2 = "=H10+G10";
                    if (ipc.Number == 1)
                    {

                        if (deductionTotal != 0)
                        {
                            rangeSecond.Cells[10, 9].Value2 = (double)deductionPercentage * item.Amount / deductionTotal;
                            rangeSecond.Cells[10, 8].Value2 = (double)deductionPercentage * item.Amount / deductionTotal;
                        }
                        else
                        {
                            rangeSecond.Cells[10, 9].Value2 = 0;
                            rangeSecond.Cells[10, 8].Value2 = 0;
                        }
                        item.PrecedentAmount = (double)(deductionPercentage * item.Amount);
                    }
                    else
                    {

                        if (deductionTotal != 0)
                        {
                            rangeSecond.Cells[10, 9].Value2 = (double)deductionPercentage * item.Amount / deductionTotal;
                            rangeSecond.Cells[10, 8].Value2 = (double)(deductionPercentage * item.Amount / deductionTotal) - (item.PrecedentAmount / deductionTotal);
                            rangeSecond.Cells[10, 7].Value2 = item.PrecedentAmount / deductionTotal;
                        }
                        else
                        {
                            rangeSecond.Cells[10, 9].Value2 = 0;
                            rangeSecond.Cells[10, 8].Value2 = 0;
                            rangeSecond.Cells[10, 7].Value2 = 0;
                        }
                        if (preview == false)
                        {
                            item.PrecedentAmountOld = item.PrecedentAmount;
                            item.PrecedentAmount = (double)(deductionPercentage * item.Amount);
                        }

                    }
                    rangeSecond.Cells[10, 10].Value2 = "=G10*F10";
                    rangeSecond.Cells[10, 11].Value2 = "=H10*F10";
                    rangeSecond.Cells[10, 12].Value2 = "=I10*F10";
                    i++;
                    //rangeSecond.Cells[10, 12].Value2 = item.ConsumedAmount;
                    //rangeSecond.Cells[10, 13].Value2 = item.ActualAmount;
                    //rangeSecond.Cells[10, 14].Value2 = item.Deduction;
                }
                if (i > 10)
                    rangeSecond.Rows[9].Delete();
                rangeSecond.Rows[i - 1].Delete();
                i = 0;
                string sheets = "";

                if (ipc.ContractsDataset.Contract.Language == "EN")
                {
                    sheets = sheet.Name;
                }
                else if (ipc.ContractsDataset.Contract.Language == "FR")
                {
                    sheets = sheet.NameFr;
                }

                var boqitems = ipc.ContractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).ToList();
                var sum = boqitems.Sum(x => x.Pt);
                sum += ipc.ContractsDataset.VoDatasets.Where( x=>x.Type != "Omission").SelectMany(x => x.VoItems).Sum(x => x.Pt);
                sum -= ipc.ContractsDataset.VoDatasets.Where( x=>x.Type == "Omission").SelectMany(x => x.VoItems).Sum(x => x.Pt);
                //sum -= ipc.ContractsDataset.VoDatasets.Where(x => x.Type == "Ommissions").SelectMany(x => x.VoItems).Sum(x => x.Pt);

                var contractType = ipc.ContractsDataset.Contract.Type;
                if (contractType == "Lump Sum")
                    contractType = "Forfait / " + contractType;
                if (contractType == "Remeasured")
                    contractType = "Métré / " + contractType;
                if (contractType == "Cost Plus")
                    contractType = "Régie / " + contractType;
                var con = unitOfWork.ConfigurationRepository.Get(x => x.Key == "Tax").FirstOrDefault();


                range.Cells[1, 4].Value2 = ipc.ContractsDataset.Project.Name;
                range.Cells[1, 9].Value2 = ipc.ContractsDataset.Project.Code;

                range.Cells[2, 4].Value2 = sheets;
                if (sheet != null)
                    if (sheet.CostCode != null)
                        range.Cells[2, 9].Value2 = ipc.ContractsDataset.Project.Code + sheet.CostCode.Code;

                range.Cells[3, 4].Value2 = ipc.ContractsDataset.Subcontractor.Name;
                range.Cells[3, 9].Value2 = ipc.ContractsDataset.ContractNumber;

                range.Cells[4, 4].Value2 = sum;
                range.Cells[4, 9].Value2 = contractType;

                range.Cells[5, 4].Value2 = Convert.ToDouble(ipc.ContractsDataset.ManagementFees) / 100;
                range.Cells[5, 9].Value2 = Convert.ToDouble(ipc.ContractsDataset.Termination) / 100;

                range.Cells[6, 4].Value2 = ipc.AdvancePayment;
                range.Cells[6, 9].Value2 = Convert.ToDouble(ipc.ContractsDataset.RecoverAdvance) / 100;
             

                range.Cells[7, 4].Value2 = (Convert.ToDouble(ipc.ContractsDataset.ProrataAccount) + Convert.ToDouble(ipc.ContractsDataset.PlansExecution)) / 100;
                range.Cells[7, 9].Value2 = Convert.ToDouble(ipc.ContractsDataset.HoldWarranty) / 100;

                range.Cells[8, 4].Value2 = ipc.Number.ToString("00");
                range.Cells[8, 9].Value2 = ipc.Type;
                if (ipc.DateIpc != null)
                {
                    var date1 = ipc.DateIpc.Value;
                    range.Cells[9, 4].Value2 = date1;
                    range.Cells[9, 9].Value2 = date1.AddDays(Convert.ToInt32(ipc.ContractsDataset.DaysNumber));
                }
                   
                if(ipc.FromDate != null)
                    range.Cells[10, 4].Value2 = ipc.FromDate.Value;
                if (ipc.ToDate != null)
                    range.Cells[10, 9].Value2 = ipc.ToDate.Value;
               

                //if (ipc.Number == 1)
                //    range.Cells[10, 4].Value2 = DateTime.Now;
                //else
                //{
                //    range.Cells[10, 4].Value2 = ipc.ContractsDataset.Ipcs.Where(x => x.Number == ipc.Number - 1).FirstOrDefault().Created;
                //
                //}
                //range.Cells[10, 9].Value2 = DateTime.Now;

                range.Cells[40, 8].Value2 = (double)Convert.ToDouble(con.Value) / 100;
                var AllContBoqItems = new List<ContractBoqItem>();

                //range.Cells[25, 5].Value2 = ipc.Retention - ipc.RetentionAmount;
                range.Cells[25, 7].Value2 = LastRetention;
                //range.Cells[25, 9].Value2 = ipc.Retention - ipc.RetentionAmount - LastRetention - LastRetentionReleaseAmount;


                if (ipc.Penalty == 0)
                {
                    range.Cells[27, 5].Value2 = previousPenalty;
                    range.Cells[27, 7].Value2 = previousPenalty;
                    range.Cells[27, 9].Value2 = 0;
                }
                else
                {
                    range.Cells[27, 7].Value2 = previousPenalty;
                    range.Cells[27, 9].Value2 = ipc.Penalty;
                }

                if (ipc.Number == 1)
                {
                    range.Cells[15, 9].Value2 = ipc.AdvancePaymentAmount;
                    if ((precedAdvancePaymentCumul - precedApRecovery) == 0)
                    {
                        range.Cells[26, 9].Value2 = 0;
                        ipc.ApRecovery = 0;
                    }
                    else if (precedAdvancePaymentCumul > (precedApRecovery + ipc.ApRecovery))
                    {
                        range.Cells[26, 9].Value2 = ipc.ApRecovery;

                    }
                    else
                    {
                        range.Cells[26, 9].Value2 = precedAdvancePaymentCumul - precedApRecovery;
                        ipc.ApRecovery = precedAdvancePaymentCumul - precedApRecovery;
                    }
                }
                else
                {
                    if ((precedAdvancePaymentCumul - precedApRecovery) == 0)
                    {
                        range.Cells[26, 9].Value2 = 0;
                        ipc.ApRecovery = 0;

                    }
                    else if (precedAdvancePaymentCumul > (precedApRecovery + ipc.ApRecovery))
                    {
                        range.Cells[26, 9].Value2 = ipc.ApRecovery;

                    }
                    else
                    {
                        range.Cells[26, 9].Value2 = precedAdvancePaymentCumul - precedApRecovery;
                        ipc.ApRecovery = precedAdvancePaymentCumul - precedApRecovery;
                    }
                    if (ipc.Type == "Avance / Advance Payment")
                    {
                        range.Cells[15, 9].Value2 = ipc.AdvancePaymentAmount;
                        range.Cells[15, 7].Value2 = precedAdvancePaymentCumul;
                        ipc.AdvancePaymentAmountCumul = ipc.AdvancePaymentAmount + precedAdvancePayment;
                    }
                    else
                    {
                        ipc.AdvancePaymentAmount = precedAdvancePayment;
                        ipc.AdvancePaymentAmountCumul = precedAdvancePaymentCumul;
                        range.Cells[15, 5].Value2 = precedAdvancePaymentCumul;
                        range.Cells[15, 7].Value2 = precedAdvancePaymentCumul;
                    }
                    if (ipc.Type == "RG / Retention")
                    {
                        range.Cells[34, 9].Value2 = ipc.RetentionAmount;
                        range.Cells[34, 7].Value2 = precedRetentionCumul;
                        ipc.RetentionAmountCumul = ipc.RetentionAmount + precedRetention;
                    }
                    else
                    {
                        ipc.RetentionAmount = precedRetention;
                        ipc.RetentionAmountCumul = precedRetentionCumul;
                        range.Cells[34, 5].Value2 = precedRetentionCumul;
                        range.Cells[34, 7].Value2 = precedRetentionCumul;
                    }

                }

                Worksheet xlSht = _wb.Sheets[2];
                int indexVo = 0;
                var additions = 0.0;
                var PrevAdditions = 0.0;
                var ommissions = 0.0;
                var PrevOmmissions = 0.0;
                if(ipc.ContractsDataset.VoDatasets.Where(x => x.Type == "Omission").Any())
                {
                    range.Cells[22, 5].Formula = "=";
                    range.Cells[22, 7].Formula = "=";
                    range.Cells[22, 9].Formula = "=";
                }
                else
                {
                    range.Cells[22, 5].Value2 = "0";
                    range.Cells[22, 7].Value2 = "0";
                    range.Cells[22, 9].Value2 = "0";
                }
                if (ipc.ContractsDataset.VoDatasets.Where(x => x.Type == "Addition").Any())
                {
                    range.Cells[21, 5].Formula = "=";
                    range.Cells[21, 7].Formula = "=";
                    range.Cells[21, 9].Formula = "=";
                }
                else
                {
                    range.Cells[21, 5].Value2 = "0";
                    range.Cells[21, 7].Value2 = "0";
                    range.Cells[21, 9].Value2 = "0";
                }
               
                foreach (var vodataset in ipc.ContractsDataset.VoDatasets)
                {


                    indexVo++;
                    xlSht.Copy(Type.Missing, _wb.Sheets[1]);
                    _wb.Sheets[2].Name = vodataset.Building.Name + "-VO" + indexVo;
                    Range yrange = (Range)_wb.Sheets[2].UsedRange;
                    yrange.Cells[3, 3].Value2 = vodataset.ContractsDataset.ContractNumber + "-" + vodataset.VoNumber;
                    yrange.Cells[5, 3].Value2 = vodataset.Building.Name;
                    if (vodataset.Type == "Omission")
                    {
                        range.Cells[22, 5].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!N13";
                        range.Cells[22, 7].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!L13";
                        range.Cells[22, 9].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!M13";
                    }
                    else
                    {
                        range.Cells[21, 5].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!N13";
                        range.Cells[21, 7].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!L13";
                        range.Cells[21, 9].Formula += "+'" + vodataset.Building.Name + "-VO" + indexVo + "'!M13";
                    }
                    int ivo = 12;
                    var val = 0.0;
                    var prevVal = 0.0;
                    var sortedVo = vodataset.VoItems.OrderBy(x => x.OrderVo).ToList();
                    foreach (var item in sortedVo)
                    {
                        var firstRow = yrange.Rows[10];
                        val += item.CumulAmount;
                        prevVal += item.PrecedAmount;

                        //Range line = xrange.Rows[10].EntireRow;
                        bool line = (bool)yrange.Rows[ivo].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);

                        yrange.Rows[ivo].Value2 = firstRow.Value2;
                        //line.Insert( XlInsertFormatOrigin.xlFormatFromRightOrBelow);
                        yrange.Cells[ivo, 1].Value2 = item.No;
                        Borders border = yrange.Cells[10, 1].Borders;
                        //xrange.Cells[11, 1].Borders = border;
                        Borders border1 = yrange.Cells[ivo, 1].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        yrange.Cells[ivo, 2].Value2 = item.Key;
                        border = yrange.Cells[10, 2].Borders;
                        border1 = yrange.Cells[ivo, 2].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 3].Borders;
                        yrange.Cells[ivo, 3].Value2 = item.CostCode != null ? item.CostCode.Code : "";
                        border1 = yrange.Cells[ivo, 3].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 4].Borders;
                        yrange.Cells[ivo, 4].Value2 = item.Unite;
                        border1 = yrange.Cells[ivo, 4].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 5].Borders;
                        yrange.Cells[ivo, 5].Value2 = item.Qte;
                        border1 = yrange.Cells[ivo, 5].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 6].Borders;
                        yrange.Cells[ivo, 6].Value2 = item.Pu;
                        border1 = yrange.Cells[ivo, 6].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 7].Borders;
                        yrange.Cells[ivo, 7].Value2 = item.Pt;
                        border1 = yrange.Cells[ivo, 7].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;


                        border = yrange.Cells[10, 8].Borders;
                        if (item.Qte != 0)
                            yrange.Cells[ivo, 8].Value2 = item.PrecedQte / item.Qte;
                        border1 = yrange.Cells[ivo, 8].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 9].Borders;
                         if(item.Qte != 0)
                            yrange.Cells[ivo, 9].Value2 = item.ActualQte / item.Qte;
                        border1 = yrange.Cells[ivo, 9].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;

                        border = yrange.Cells[10, 10].Borders;
                        if (item.Qte != 0)
                            yrange.Cells[ivo, 10].Value2 = item.CumulQte / item.Qte;
                        border1 = yrange.Cells[ivo, 10].Borders;
                        border1[XlBordersIndex.xlEdgeLeft].LineStyle = border[XlBordersIndex.xlEdgeLeft].LineStyle;
                        border1[XlBordersIndex.xlEdgeTop].LineStyle = border[XlBordersIndex.xlEdgeTop].LineStyle;
                        border1[XlBordersIndex.xlEdgeBottom].LineStyle = border[XlBordersIndex.xlEdgeBottom].LineStyle;
                        border1[XlBordersIndex.xlEdgeRight].LineStyle = border[XlBordersIndex.xlEdgeRight].LineStyle;


                        if (ipc.ContractsDataset.Contract.Type == "Lump Sum")
                        {
                            yrange.Cells[ivo, 11].Value2 = "=" + 1 + "-J" + ivo;

                            yrange.Cells[ivo, 12].Value2 = "=H" + ivo + "*G" + ivo;
                            yrange.Cells[ivo, 13].Value2 = "=I" + ivo + "*G" + ivo;
                            yrange.Cells[ivo, 14].Value2 = "=J" + ivo + "*G" + ivo;
                        }
                        else
                        {
                            yrange.Cells[ivo, 11].Value2 = "=E" + ivo + "-J" + ivo;

                            yrange.Cells[ivo, 12].Value2 = "=H" + ivo + "*F" + ivo;
                            yrange.Cells[ivo, 13].Value2 = "=I" + ivo + "*F" + ivo;
                            yrange.Cells[ivo, 14].Value2 = "=J" + ivo + "*F" + ivo;
                        }
                        ivo++;


                    }
                    yrange.Rows[10].Delete();
                    yrange.Rows[ivo - 1].Delete();
                    if (vodataset.Type == "Omission")
                    {
                        ommissions += val;
                        PrevOmmissions = prevVal;
                    }
                    else
                    {
                        additions += val;
                        PrevAdditions += prevVal;
                    }
                }
                //range.Cells[21, 5].Value2 = additions;
                //range.Cells[21, 7].Value2 = PrevAdditions;
                //range.Cells[21, 9].Value2 = additions - PrevAdditions;
                //range.Cells[22, 5].Value2 = ommissions;
                //range.Cells[22, 7].Value2 = PrevOmmissions;
                //range.Cells[22, 9].Value2 = ommissions - PrevOmmissions;
                 
                i = 12;
                Range xrange = xlSht.UsedRange;
                string formula1 = "=", formula2 = "=", formula3 = "=";
                range.Cells[18, 5].Value2 = "=";
                range.Cells[18, 7].Value2 = "=";
                range.Cells[18, 9].Value2 = "=";
                foreach (var building in ipc.ContractsDataset.Buildings)
                {
                    i = 12;
                    var boq = ipc.ContractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).Where(x => x.BoqSheet.Boq.Building.Id == building.Id).OrderBy(x => x.OrderBoq).ToList();
                    if (boq.Count == 0)
                        continue;
                    xlSht.Copy(Type.Missing, _wb.Sheets[1]);
                    _wb.Sheets[2].Name = building.Name + "-BOQ";

                    range.Cells[18, 5].Formula += "+'" + building.Name + "-BOQ'!N13";
                    range.Cells[18, 7].Formula += "+'" + building.Name + "-BOQ'!L13";
                    range.Cells[18, 9].Formula += "+'" + building.Name + "-BOQ'!M13";
                    Range yrange = (Range)_wb.Sheets[2].UsedRange;
                    yrange.Cells[5, 3].Value2 = building.Name;
                    foreach (var item in boq)
                    {
                        var firstRow = yrange.Rows[10];
                        bool line = (bool)yrange.Rows[i].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        yrange.Rows[i].Value2 = firstRow.Value2;
                        yrange.Cells[i, 1].Value2 = item.No;
                        Borders border = yrange.Cells[10, 1].Borders;
                        Borders border1 = yrange.Cells[i, 1].Borders;

                        yrange.Cells[i, 2].Value2 = item.Key;
                        border = yrange.Cells[10, 2].Borders;
                        border1 = yrange.Cells[i, 2].Borders;

                        border = yrange.Cells[10, 3].Borders;
                        yrange.Cells[i, 3].Value2 = item.CostCode != null ? item.CostCode.Code : "";
                        border1 = yrange.Cells[i, 3].Borders;

                        border = yrange.Cells[10, 4].Borders;
                        yrange.Cells[i, 4].Value2 = item.Unite;
                        border1 = yrange.Cells[i, 4].Borders; 

                        border = yrange.Cells[10, 5].Borders;
                        yrange.Cells[i, 5].Value2 = item.Qte;
                        border1 = yrange.Cells[i, 5].Borders; 

                        border = yrange.Cells[10, 6].Borders;
                        yrange.Cells[i, 6].Value2 = item.Pu;
                        border1 = yrange.Cells[i, 6].Borders; 

                        border = yrange.Cells[10, 7].Borders;
                        yrange.Cells[i, 7].Value2 = item.Pt;
                        border1 = yrange.Cells[i, 7].Borders;

                        border = yrange.Cells[10, 8].Borders;
                        if (item.Qte != 0)
                            yrange.Cells[i, 8].Value2 = item.PrecedQte / item.Qte;
                        else
                            yrange.Cells[i, 8].Value2 = item.PrecedQte;
                        border = yrange.Cells[10, 9].Borders;
                        if (item.Qte != 0)
                            yrange.Cells[i, 9].Value2 = item.ActualQte / item.Qte;
                        else
                            yrange.Cells[i, 8].Value2 = item.ActualQte;
                        border1 = yrange.Cells[i, 9].Borders; 

                        border = yrange.Cells[10, 10].Borders;

                        if (item.Qte != 0)
                            yrange.Cells[i, 10].Value2 = item.CumulQte / item.Qte;
                        else
                            yrange.Cells[i, 8].Value2 = item.CumulQte;
                        border1 = yrange.Cells[i, 10].Borders; 
                         
                        if (ipc.ContractsDataset.Contract.Type == "Lump Sum")
                        {
                            yrange.Cells[i, 11].Value2 = "=" + 1 + "-J" + i;
                            yrange.Cells[i, 12].Value2 = "=H" + i + "*G" + i;
                            yrange.Cells[i, 13].Value2 = "=I" + i + "*G" + i;
                            yrange.Cells[i, 14].Value2 = "=J" + i + "*G" + i;
                        }
                        else
                        {
                            yrange.Cells[i, 11].Value2 = "=E" + i + "-J" + i;
                            yrange.Cells[i, 12].Value2 = "=H" + i + "*F" + i;
                            yrange.Cells[i, 13].Value2 = "=I" + i + "*F" + i;
                            yrange.Cells[i, 14].Value2 = "=J" + i + "*F" + i;
                        }

                        i++;
                    }
                    yrange.Rows[10].Delete();
                    yrange.Rows[i - 1].Delete();
                }

                _excel.DisplayAlerts = false;
                xlSht.Delete();
                Worksheet sommSht = _wb.Sheets[_wb.Sheets.Count-1];
                Range someRange = sommSht.UsedRange;
                //someRange.Cells[1, 3].Value2 = ipc.ContractsDataset.Project.Name;
                //someRange.Cells[2, 3].Value2 = ipc.ContractsDataset.Subcontractor.Name;
                //someRange.Cells[3, 3].Value2 = ipc.ContractsDataset.ContractNumber;
                //someRange.Cells[4, 3].Value2 = ipc.Number;
                if (ipc.ContractsDataset.Buildings.Count > 1)
                {
                    int bIndex = 12, bNumber = 1;
                    double total = ipc.ContractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                    someRange[1, 3].Value2 = ipc.ContractsDataset.Project.Name;
                    foreach (var building in ipc.ContractsDataset.Buildings)
                    {
                        var boq = ipc.ContractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null)
                            .Where(x => x.BoqSheet.Boq.Building.Id == building.Id).OrderBy(x => x.OrderBoq).ToList();

                        bool line = (bool)someRange.Rows[bIndex].Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        var totalSum = boq.Sum(x => x.Pt);
                        someRange.Cells[bIndex, 1].Value2 = bNumber;
                        someRange.Cells[bIndex, 2].Value2 = building.Name;
                        someRange.Cells[bIndex, 3].Value2 = totalSum;
                        if (totalSum == 0)
                            continue;
                        if (ipc.ContractsDataset.Contract.Type == "Lump Sum")
                        {
                            someRange.Cells[bIndex, 4].Value2 = (boq.Sum(x => x.Pt) / total);
                            someRange.Cells[bIndex, 5].Value2 = boq.Sum(x => x.PrecedAmount) / totalSum;
                            someRange.Cells[bIndex, 6].Value2 = boq.Sum(x => x.ActualAmount) / totalSum;
                            someRange.Cells[bIndex, 7].Value2 = boq.Sum(x => x.CumulAmount) / totalSum;
                            someRange.Cells[bIndex, 8].Value2 = 1 - boq.Sum(x => x.CumulAmount) / totalSum;
                        }
                        else
                        {
                            someRange.Cells[bIndex, 4].Value2 = boq.Sum(x => x.Pt) / total;
                            someRange.Cells[bIndex, 5].Value2 = boq.Sum(x => x.PrecedQte);
                            someRange.Cells[bIndex, 6].Value2 = boq.Sum(x => x.ActualQte);
                            someRange.Cells[bIndex, 7].Value2 = boq.Sum(x => x.CumulQte);
                            someRange.Cells[bIndex, 8].Value2 = 1 - boq.Sum(x => x.CumulQte);

                        }


                        someRange.Cells[bIndex, 9].Value2 = boq.Sum(x => x.PrecedAmount);
                        someRange.Cells[bIndex, 10].Value2 = boq.Sum(x => x.ActualAmount);
                        someRange.Cells[bIndex, 11].Value2 = boq.Sum(x => x.CumulAmount);
                        bIndex++;
                        bNumber++;
                    }
                    someRange.Rows[11].Delete();
                    someRange.Rows[bIndex].Delete();
                }
                else
                    sommSht.Delete();
                var value = range.Cells[41, 9].Value2;
                totalAmount = (double)value;
                ipc.TotalAmount = totalAmount;
                ipc.Prorata = (double)range.Cells[28, 5].Value2;
                if (save == true)
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = ipc.ContractsDataset.Subcontractor.Name + "-" + ipc.ContractsDataset.ContractNumber + "-" + "IPC#" + ipc.Number.ToString("000");
                    var result = sfd.ShowDialog();
                    if (result == true)
                    {
                        _wb.SaveAs(sfd.FileName);

                    }
                    Close();
                    return null;
                }
                if (preview == false)
                {
                    if (ipc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                    {
                        var messageResultContract = MessageBox.Show("Do you want to save the contract", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (messageResultContract == MessageBoxResult.Yes)
                        {
                            SaveFileDialog sf = new SaveFileDialog();
                            sf.Title = "Save Contract First";

                            sf.FileName = ipc.ContractsDataset.ContractNumber;
                            if (sf.ShowDialog() == true)
                            {
                                ipc.ContractsDataset.IsGenerated = true;
                                var contract3 = unitOfWork.ContractsDatasetRepository.Get(
                                      x => x.Id == ipc.ContractsDataset.Id,
                                      includeProperties: "PrescriptionTechniques,Plans,Contract").FirstOrDefault();
                                var contract4 = unitOfWork.ContractsDatasetRepository.Get(
                                      x => x.Id == ipc.ContractsDataset.Id,
                                      includeProperties: "Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();
                                var contract1 = unitOfWork.ContractsDatasetRepository.Get(
                                          x => x.Id == ipc.ContractsDataset.Id,
                                          includeProperties: "ContractFile,Planning").FirstOrDefault();
                                var contract2 = unitOfWork.ContractsDatasetRepository.Get(
                                      x => x.Id == ipc.ContractsDataset.Id,
                                      includeProperties: "UnitePrice,BoqAtt").FirstOrDefault();
                                ipc.ContractsDataset.Contract = contract1.Contract;
                                ipc.ContractsDataset.ContractFile = contract1.ContractFile;
                                ipc.ContractsDataset.Planning = contract1.Planning;
                                ipc.ContractsDataset.UnitePrice = contract2.UnitePrice;
                                ipc.ContractsDataset.BoqAtt = contract2.BoqAtt;
                                ipc.ContractsDataset.PrescriptionTechniques = contract3.PrescriptionTechniques;
                                ipc.ContractsDataset.Plans = contract3.Plans;
                                ipc.ContractsDataset.Other = contract4.Other;
                                ipc.ContractsDataset.PlansHSE = contract4.PlansHSE;
                                ipc.ContractsDataset.DocumentsJuridiques = contract4.DocumentsJuridiques;
                                Word w = new Word();
                                string filePath;
                                w.GenerateContract(ipc.ContractsDataset, out filePath, sf.FileName);
                                ipc.ContractsDataset.ContractFile = new ContractFile();
                                ipc.ContractsDataset.ContractFile.Content = ReadFile(sf.FileName + ".docx");
                            }
                            else
                            {
                                MessageBox.Show("IPC Generation canceled", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                                Close();
                                return null;
                            }
                        }
                        else
                        {
                            //TODO: FIX filepath parameter and delete unnecessary files;
                            string FileName = Path.GetTempFileName();
                            ipc.ContractsDataset.IsGenerated = true;

                            Word w = new Word();
                            string filePath;
                            w.GenerateContract(ipc.ContractsDataset, out filePath, FileName);
                            ipc.ContractsDataset.ContractFile = new ContractFile();
                            ipc.ContractsDataset.ContractFile.Content = ReadFile(FileName + ".docx");
                            File.Delete(FileName);
                            File.Delete(FileName + ".docx");
                        }
                    }
                    var messageResult = MessageBox.Show("Do you want to save the IPC", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (messageResult == MessageBoxResult.Yes)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = ipc.ContractsDataset.Subcontractor.Name + "-" + ipc.ContractsDataset.ContractNumber + "-" + "IPC#" + ipc.Number.ToString("000");
                        var result = sfd.ShowDialog();
                        if (result == true)
                        {
                            _wb.SaveAs(sfd.FileName);

                        }
                        Close();
                        if (result == true)
                        {
                            byte[] bytes = ReadFile(sfd.FileName + ".xlsx");
                            IpcFile ipcFile = new IpcFile { Content = bytes };
                            ipc.IpcFile = ipcFile;
                        }

                        return null;
                    }
                    else
                    {
                        string FileName = Path.GetTempFileName() + ".xlsx";
                        _wb.SaveCopyAs(FileName);
                        byte[] bytes = ReadFile(FileName);
                        IpcFile ipcFile = new IpcFile { Content = bytes };
                        ipc.IpcFile = ipcFile;
                        File.Delete(FileName);
                        Close();
                        return null;
                    }

                }
                else
                {

                    string FileName = Path.GetTempFileName() + ".xlsx";
                    _wb.SaveCopyAs(FileName);
                    outFilePath = FileName;
                    // xpsFileName = (new DirectoryInfo(path)).FullName;

                    var nFileName = FileName.Replace(new FileInfo(FileName).Extension, "") + ".xps";

                    _wb.ExportAsFixedFormat(XlFixedFormatType.xlTypeXPS,

                    Filename: nFileName,

                    OpenAfterPublish: false);

                    XpsDocument xpsDoc = new XpsDocument(nFileName, System.IO.FileAccess.Read, CompressionOption.NotCompressed);
                    _wb.Close(false, null, null);
                    Marshal.ReleaseComObject(_wb);
                    _excel.Quit();
                    Marshal.ReleaseComObject(_excel);
                    return xpsDoc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error While Generating IPC", MessageBoxButton.OK, MessageBoxImage.Error);
                MessageBox.Show(ex.StackTrace, "Error While Generating IPC", MessageBoxButton.OK, MessageBoxImage.Error);
               
                totalAmount = 0;
                _wb.Close(false, null, null);
                Marshal.ReleaseComObject(_wb);
                _excel.Quit();
                Marshal.ReleaseComObject(_excel);
                return null;
            }
            // TODO:: TEMP FILE DELETE HERE AND IN WORD
            //for (int i = 0; i < length; i++)
            //{
            //
            //}
        }
        public XpsDocument PreviewExcel()
        {
            // xpsFileName = (new DirectoryInfo(path)).FullName;

            _path = _path.Replace(new FileInfo(_path).Extension, "") + ".xps";

            _wb.ExportAsFixedFormat(XlFixedFormatType.xlTypeXPS,

            Filename: _path,

            OpenAfterPublish: false);

            XpsDocument xpsDoc = new XpsDocument(_path, System.IO.FileAccess.Read, CompressionOption.NotCompressed);
            _wb.Close(false, null, null);
            Marshal.ReleaseComObject(_wb);
            _excel.Quit();
            Marshal.ReleaseComObject(_excel);
            return xpsDoc;
        }
        public void Close()
        {

            GC.Collect();
            GC.WaitForPendingFinalizers();

            _wb.Close();
            Marshal.ReleaseComObject(_wb);
            _excel.Quit();
            Marshal.ReleaseComObject(_excel);
        }

        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }

  
    }
}
