﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using System.Windows.Xps.Packaging;
using ContractsManager.Data.utils;
using ExporterObjects;
using Application = Microsoft.Office.Interop.Word.Application;
using ContractsManager.Data.Repository;
using System.Threading;
using System.Windows.Media;

namespace ContractsManager.Data.Excel
{
    public class Word
    {
        public XpsDocument OpenContract(object contractsDataset, string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".docx", ReadOnly: false);
            document.Activate();
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }
        public XpsDocument OpenContractDoc(object contractsDataset, string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".doc", ReadOnly: false);
            document.Activate();
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }

        public XpsDocument OpenVo(object contractsDataset, string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".docx", ReadOnly: false);
            document.Activate();
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }
        public XpsDocument SaveContract(ContractsDataset contractsDataset, string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".docx", ReadOnly: false);
            document.Activate();
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }
        public XpsDocument GenerateVoContract(VoDataset voDataset, out string _fileOutPath, string filePath = "")
        {
            string fileName = Path.GetTempFileName() + ".docx";
            _fileOutPath = fileName;
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                fs.Write(voDataset.Contract.Content, 0, voDataset.Contract.Content.Length);
                fs.Close();
            }

            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName, ReadOnly: false);

            //fileOpen.Visible = true;
            document.Activate();
            var sum = voDataset.VoItems.Sum(x => x.Pt);
            var words = utils.Converter.ConvertNumberToWords(sum, utils.Language.French);
            words = words.First().ToString().ToUpper() + words.Substring(1);
            var sheetName = "";
            UnitOfWork unitOfWork = new UnitOfWork();
            var sheet = unitOfWork.SheetsRepository.Get(x => x.Name == voDataset.ContractsDataset.BoqSheet.Name).FirstOrDefault();

            if (voDataset.Contract.Language == "EN")
            {
                sheetName = sheet.Name;
                words = utils.Converter.ConvertNumberToWords(sum, utils.Language.English);
                words = words.First().ToString().ToUpper() + words.Substring(1);
            }
            else if (voDataset.Contract.Language == "FR")
            {
                words = utils.Converter.ConvertNumberToWords(sum, utils.Language.French);
                words = words.First().ToString().ToUpper() + words.Substring(1);
                sheetName = sheet.NameFr;
            }
            else if (voDataset.Contract.Language == "AR")
            {
                var toWord = new ToWord((Decimal)sum, new CurrencyInfo(CurrencyInfo.Currencies.Syria));
                words = toWord.ConvertToArabic();
            }


            FindAndReplace(fileOpen, "{Type}", voDataset.Type);
            FindAndReplace(fileOpen, "{VoNumber}", voDataset.VoNumber);
            FindAndReplace(fileOpen, "{ContractNumber}", voDataset.ContractsDataset.ContractNumber);
            FindAndReplace(fileOpen, "{Date}", voDataset.Date.Value.ToString("dd-MMM-yyyy"));
            FindAndReplace(fileOpen, "{Sheet}", sheetName);
            FindAndReplace(fileOpen, "{SubcontractorName}", voDataset.Subcontractor.Name);
            FindAndReplace(fileOpen, "{RepresentedBy}", voDataset.Subcontractor.RepresentedBy);
            FindAndReplace(fileOpen, "{Gerant}", voDataset.Subcontractor.QualityRepresentive);
            FindAndReplace(fileOpen, "{SumWord}", words);
            var formattedSum = String.Format("{0:n2}", sum);
            FindAndReplace(fileOpen, "{Sum}", formattedSum);
            FindAndReplace(fileOpen, "{Currency}", voDataset.ContractsDataset.Currency.Currencies);
            FindAndReplace(fileOpen, "{ProjectName}", voDataset.Project.Name);
            if (voDataset.Type == "Addition")
                FindAndReplace(fileOpen, "{IFAO}", "exécuter");
            else
                FindAndReplace(fileOpen, "{IFAO}", "omettre");

            foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
            {
                document.TrackRevisions = false;//Disable Tracking for the Field replacement operation
                                                //Get all Headers
                Microsoft.Office.Interop.Word.HeadersFooters headers = section.Headers;
                //Section headerfooter loop for all types enum WdHeaderFooterIndex. wdHeaderFooterEvenPages/wdHeaderFooterFirstPage/wdHeaderFooterPrimary;                          
                foreach (Microsoft.Office.Interop.Word.HeaderFooter header in headers)
                {
                    Microsoft.Office.Interop.Word.Fields fields = header.Range.Fields;
                    foreach (Microsoft.Office.Interop.Word.Field field in fields)
                    {
                        field.Select();
                        field.Delete();
                        fileOpen.Selection.TypeText(voDataset.ContractsDataset.ContractNumber+"-"+ voDataset.VoNumber);
                    }
                }
            }
            if (voDataset.BOQAtt != null)
                RasterizerSample1.Create(document, voDataset.BOQAtt.Content, "BoqAttachment");
            else if (voDataset.BOQAtt == null)
            {
                var tax = new UnitOfWork().ConfigurationRepository.Get(x => x.Key == "TAX").FirstOrDefault();
                int i = 4;
                    document.Tables[2].Rows[1].Cells[1].Range.Text = voDataset.Project.Name;
                    document.Tables[2].Rows[2].Cells[1].Range.Text = sheetName;
                    document.Tables[2].Rows[2].Cells[1].Range.Font.Color = WdColor.wdColorWhite;
                    var sum1 = voDataset.VoItems.Sum(x => x.Pt);
                    foreach (var item in voDataset.VoItems.OrderBy(x => x.OrderVo).ToList())
                    {


                        document.Tables[2].Rows.Add(document.Tables[2].Rows[i]);

                        document.Tables[2].Rows[i].Cells[1].Range.Text = item.No;
                        document.Tables[2].Rows[i].Cells[2].Range.Text = item.Key;
                        document.Tables[2].Rows[i].Cells[3].Range.Text = item.Unite;
                        if (item.CostCode != null)
                            document.Tables[2].Rows[i].Cells[4].Range.Text = item.CostCode.Code;
                        if (string.IsNullOrWhiteSpace(item.Unite))
                        {
                            document.Tables[2].Rows[i].Cells[5].Range.Text = "";
                            document.Tables[2].Rows[i].Cells[6].Range.Text = "";
                            document.Tables[2].Rows[i].Cells[7].Range.Text = "";
                        }
                    else
                    {
                        document.Tables[2].Rows[i].Cells[5].Range.Text = item.Qte.ToString("N2");
                        document.Tables[2].Rows[i].Cells[6].Range.Text = item.Pu.ToString("N2");
                        document.Tables[2].Rows[i].Cells[7].Range.Text = item.Pt.ToString("N2");
                    }
                    
                    
                    
                        i++;
                    }
                    
                    document.Tables[2].Rows[i + 1].Cells[1].Range.Text = "TOTAL TRAVAUX DU " + sheetName;
                    document.Tables[2].Rows[i + 3].Cells[1].Range.Text = "TOTAL GENERAL DE TRAVAUX " + sheetName + "   - TTC";

                    document.Tables[2].Rows[i + 1].Cells[2].Range.Text = sum1.ToString("N2");

                    document.Tables[2].Rows[i + 2].Cells[1].Range.Text = "TVA " + tax.Value + "%";
                    document.Tables[2].Rows[i + 2].Cells[2].Range.Text = ((Convert.ToDouble(tax.Value) / 100) * sum1).ToString("N2");
                    document.Tables[2].Rows[i + 3].Cells[2].Range.Text = (sum1 + ((Convert.ToDouble(tax.Value) / 100) * sum1)).ToString("N2");

                
                /*ExportList<BoqTable> exp = new ExportList<BoqTable>();

                exp.PathTemplateFolder = Path.Combine(Environment.CurrentDirectory, "templates/electronics");
                var BoqItems = new List<ContractVo>();

                // BoqItems.Add(new ContractBoqItem { Key = "Building: " + build.Name });
                BoqItems.AddRange(voDataset.VoItems.OrderBy(x => x.OrderVo).ToList());
                
                
                var totalSum = new ContractVo { BOQType = BOQType.TotalSum, Key = "Total Sum", Pt = sum };
                
                BoqItems.Add(totalSum);
                var obj = BoqItems.Select(x => new BoqTable { No = x.No, Item = x.Key, Unit = x.Unite, CostCode = x.CostCode != null ? x.CostCode.Code : null, Quantity = String.Format("{0:n0}", x.Qte), Pu = String.Format("{0:n0}", x.Pu), Pt = String.Format("{0:n0}", x.Pt) }).ToList();
                
                exp.ExportTo(obj, ExportToFormat.PDFtextSharpXML, "a.pdf");
                var boqfile = File.ReadAllBytes("a.pdf");
                RasterizerSample1.Create(document, boqfile, "BoqAttachment");
                
         https://prod.liveshare.vsengsaas.visualstudio.com/join?E2E52E6116326F5340C4A0B7BABC4940C3FC
                File.Delete("a.pdf");
                */

            }
            if (filePath != "")
            {
                document.SaveAs2(filePath);
                document.Close(WdSaveOptions.wdDoNotSaveChanges);
                fileOpen.Quit();
                //contractsDataset.Contract = new Contract { Content = ReadFile(filePath+".docx") };
                return null;
            }
            string FileName = Path.GetTempFileName();
            document.Save();
            document.SaveAs2(FileName, WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(FileName, System.IO.FileAccess.Read);
            return xpsDoc;
            return null;
        }

        public string GenerateTerminationFile(string fileName, ContractsDataset contractsDataset)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName, ReadOnly: false);
            document.Activate();

            FindAndReplace(fileOpen, "{Date}", DateTime.Today.ToString("dd/MM/yyyy"));
            FindAndReplace(fileOpen, "{SubcontractorName}", contractsDataset.Subcontractor.Name);
            FindAndReplace(fileOpen, "{RepresentedBy}", contractsDataset.Subcontractor.RepresentedBy);
            FindAndReplace(fileOpen, "{ContractNumber}", contractsDataset.ContractNumber);
            FindAndReplace(fileOpen, "{Gerant}", contractsDataset.Subcontractor.QualityRepresentive);

            foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
            {
                document.TrackRevisions = false;//Disable Tracking for the Field replacement operation
                                                //Get all Headers
                Microsoft.Office.Interop.Word.HeadersFooters headers = section.Headers;
                //Section headerfooter loop for all types enum WdHeaderFooterIndex. wdHeaderFooterEvenPages/wdHeaderFooterFirstPage/wdHeaderFooterPrimary;                          
                foreach (Microsoft.Office.Interop.Word.HeaderFooter header in headers)
                {
                    Microsoft.Office.Interop.Word.Fields fields = header.Range.Fields;
                    foreach (Microsoft.Office.Interop.Word.Field field in fields)
                    {
                        field.Select();
                        field.Delete();
                        fileOpen.Selection.TypeText(contractsDataset.ContractNumber);
                    }
                }
            }

            string filePath = Path.GetTempFileName();
            document.SaveAs2(filePath+".doc");
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            return filePath;
        }
        public XpsDocument GenerateContract(ContractsDataset contractsDataset, out string _fileOutPath, string filePath = "") 
        {

            string fileName = Path.GetTempFileName() + ".doc";
            _fileOutPath = fileName;
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                fs.Write(contractsDataset.Contract.Content, 0, contractsDataset.Contract.Content.Length);
                fs.Close();
            }

            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName, ReadOnly: false);

            //fileOpen.Visible = true;
            document.Activate();
            //TimeSpan ts = (Convert.ToDateTime(contractsDataset.ContractDate) - Convert.ToDateTime(contractsDataset.CompletionDate));
            int year, months, days, weeks = 0;
            GetYearsMonthsDays(contractsDataset.CompletionDate.Value, contractsDataset.ContractDate.Value, out year, out months, out days);
            weeks = days / 7;
            days = days % 7;
            //FindAndReplace(fileOpen, "useless", "very useful");
            UnitOfWork unitOfWork = new UnitOfWork();
            var sheet = unitOfWork.SheetsRepository.Get(x => x.Name == contractsDataset.BoqSheet.Name).FirstOrDefault();
            string sheetName = "";
            var sum = contractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            string words = "";
            if (contractsDataset.Contract.Language == "EN")
            {
                sheetName = sheet.Name;
                words = utils.Converter.ConvertNumberToWords(sum, utils.Language.English);
                words = words.First().ToString().ToUpper() + words.Substring(1);
            }
            else if (contractsDataset.Contract.Language == "FR")
            {
                words = utils.Converter.ConvertNumberToWords(sum, utils.Language.French);
                words = words.First().ToString().ToUpper() + words.Substring(1);
                sheetName = sheet.NameFr;
            }
            else if (contractsDataset.Contract.Language == "AR")
            {
                var toWord = new ToWord((Decimal)sum, new CurrencyInfo(CurrencyInfo.Currencies.Syria));
                words = toWord.ConvertToArabic();
            }

            var formattedSum = String.Format("{0:n2}", sum);
            if (string.IsNullOrWhiteSpace(contractsDataset.SubTrade))
            {
                

            }
            else
            {
                sheetName += " (" + contractsDataset.SubTrade + ")";
            }
            FindAndReplace(fileOpen, "{Sheets}", sheetName);
            FindAndReplace(fileOpen, "{ProjectName}", contractsDataset.Project.Name);
            FindAndReplace(fileOpen, "{SubcontractorName}", contractsDataset.Subcontractor.Name);
            FindAndReplace(fileOpen, "{SiegeSocial}", contractsDataset.Subcontractor.SiegeSocial);
            FindAndReplace(fileOpen, "{CommerceRegister}", contractsDataset.Subcontractor.CommerceRegistrar);
            FindAndReplace(fileOpen, "{SubcontractorTel}", contractsDataset.Subcontractor.SubcontractorTel);
            FindAndReplace(fileOpen, "{CommerceNumber}", contractsDataset.Subcontractor.CommerceNumber);
            FindAndReplace(fileOpen, "{TaxNumber}", contractsDataset.Subcontractor.TaxNumber);
            FindAndReplace(fileOpen, "{RepresentedBy}", contractsDataset.Subcontractor.RepresentedBy);
            FindAndReplace(fileOpen, "{Gérant}", contractsDataset.Subcontractor.QualityRepresentive);

            FindAndReplace(fileOpen, "{ProjectCity}", contractsDataset.Project.City);
            FindAndReplace(fileOpen, "{TotalAmount}", formattedSum);
            FindAndReplace(fileOpen, "{Currency}", contractsDataset.Currency.Currencies);
            FindAndReplace(fileOpen, "{TotalAmountWords}", words);

            FindAndReplace(fileOpen, "{DateduContract}", contractsDataset.ContractDate.Value.ToString("dd/MM/yyyy"));
            FindAndReplace(fileOpen, "{Penalitederetard}", contractsDataset.LatePenalties.Trim());
            FindAndReplace(fileOpen, "{PlafonddePenalitederetard}", contractsDataset.LatePenaliteCeiling.Trim());
            FindAndReplace(fileOpen, "{RetenuedeGarantie}", contractsDataset.HoldWarranty.Trim());
            FindAndReplace(fileOpen, "{PeriodedeMaintenance}", contractsDataset.MintenancePeriod.Trim());
            FindAndReplace(fileOpen, "{GarantiesurTravaux}", contractsDataset.WorkWarranty.Trim());
            FindAndReplace(fileOpen, "{CautiondeBonneFin}", contractsDataset.Termination.Trim());
            FindAndReplace(fileOpen, "{NombredeJours}", contractsDataset.DaysNumber.Trim());
            FindAndReplace(fileOpen, "{SurAvancement}", contractsDataset.Progress);

            FindAndReplace(fileOpen, "{Years}", year);
            FindAndReplace(fileOpen, "{Months}", months);
            FindAndReplace(fileOpen, "{Weeks}", weeks);
            FindAndReplace(fileOpen, "{Dayes}", days);

            FindAndReplace(fileOpen, "{AvancePayeeausous-traitant}", contractsDataset.SubcontractorAdvancePayee.Trim());
            if (contractsDataset.RecoverAdvance.Trim() == "0")
                FindAndReplace(fileOpen, "{Recuperationdel'Avance}", contractsDataset.RecoverAdvance.Trim() + "%");
            else
            {
                FindAndReplace(fileOpen, "{Recuperationdel'Avance}", contractsDataset.RecoverAdvance.Trim() + "% déduite de chaque décompte jusqu'à récupération du montant total de l'Avance");
            }

            if (contractsDataset.ProcurementConstruction.Trim() == "0")
                FindAndReplace(fileOpen, "{ApprovisionnementsurChantier}", contractsDataset.ProcurementConstruction.Trim() + "%");
            else
            {
                FindAndReplace(fileOpen, "{ApprovisionnementsurChantier}", contractsDataset.ProcurementConstruction.Trim() + "% du prix unitaire sur livraison du matériel sur chantier");
            }

            FindAndReplace(fileOpen, "{ApprovisionnementsurChantier}", (contractsDataset.ProcurementConstruction != null? contractsDataset.ProcurementConstruction.Trim() : 0.ToString()));
            FindAndReplace(fileOpen, "{CompteProrata}", contractsDataset.ProrataAccount.Trim());
            FindAndReplace(fileOpen, "{majo}", contractsDataset.ManagementFees.Trim());//majo FraisdeGestion
            FindAndReplace(fileOpen, "{Plansd'exce}", (contractsDataset.PlansExecution != null ? contractsDataset.PlansExecution.Trim() : 0.ToString()));//majo FraisdeGestion
            FindAndReplace(fileOpen, "{Dated'achevementdestravaux}", contractsDataset.CompletionDate.Value.ToString("dd/MM/yyyy"));
            FindAndReplace(fileOpen, "{modedepaiement}", contractsDataset.PaymentsTerm);
            if (string.IsNullOrWhiteSpace(contractsDataset.RemarkCP))
            {
                FindAndReplace(fileOpen, "{RemarkCP}", "-");

            }
            else
            {
                document.Tables[1].Rows[13].Cells[1].Range.Text = contractsDataset.RemarkCP;
                document.Tables[1].Rows[13].Cells[1].Range.Font.Name = "Arial";
                document.Tables[1].Rows[13].Cells[1].Range.Font.Size = 12;
                document.Tables[1].Rows[13].Cells[1].Range.Font.Color = WdColor.wdColorBlack;
             //   FindAndReplace(fileOpen, "{RemarkCP}", contractsDataset.RemarkCP);
            }
            if (string.IsNullOrWhiteSpace(contractsDataset.SubTrade))
            {
                FindAndReplace(fileOpen, "{SubTrade}", "-");

            }
            else
            {
                FindAndReplace(fileOpen, "{SubTrade}", "(" + contractsDataset.SubTrade + ")");
            }
            // FindAndReplace(fileOpen, "{Remark}", contractsDataset.Remark);
            document.Tables[2].Rows[2].Cells[1].Range.Text = "Les Travaux Comprennent: "+ contractsDataset.Remark;
            ExportList<BoqTable> exp = new ExportList<BoqTable>();

            exp.PathTemplateFolder = Path.Combine(Environment.CurrentDirectory, "templates/electronics");
            var withBoqItems = contractsDataset.ContractBoqItem.Where(x => x.BoqSheet != null).ToList();
            var buildings = contractsDataset.Buildings;
            var tax = new UnitOfWork().ConfigurationRepository.Get(x => x.Key == "TAX").FirstOrDefault();
            int bNumber = 3;
           
            if (contractsDataset.BoqAtt == null)
            {
               
                int countBuild = buildings.Count;
                int current = 0;

                foreach (var build in buildings)
                {
                    object unit = WdUnits.wdLine;
                    object count = 3;
                    if (current < (countBuild - 1))
                    {
                        fileOpen.Selection.Document.Select();
                        fileOpen.Selection.Tables[bNumber].Select();
                        fileOpen.Selection.Copy();
                        fileOpen.Selection.MoveDown(ref unit, ref count);
                        fileOpen.Selection.Paste();
                        fileOpen.Selection.Range.InsertAfter("\n\n");
                        current++;
                        Thread.Sleep(200);
                    }
                    bNumber++;
                }
                bNumber = 3;
                foreach (var build in buildings)
                {
                    int i = 4;
                  
                    document.Tables[bNumber].Rows[1].Cells[1].Range.Text = contractsDataset.Project.Name;
                    document.Tables[bNumber].Rows[2].Cells[1].Range.Text = build.Name + "-"+sheetName;
                    document.Tables[bNumber].Rows[2].Cells[1].Range.Font.Color = WdColor.wdColorWhite;
                    var boqItems = withBoqItems.Where(x => x.BoqSheet.Boq.Building.Name == build.Name).ToList();
                    var sum1 = boqItems.Sum(x => x.Pt);
                    foreach (var item in boqItems)
                     {
                       
                      
                        document.Tables[bNumber].Rows.Add(document.Tables[bNumber].Rows[i]);

                        document.Tables[bNumber].Rows[i].Cells[1].Range.Text = item.No;
                        document.Tables[bNumber].Rows[i].Cells[2].Range.Text = item.Key;
                        document.Tables[bNumber].Rows[i].Cells[3].Range.Text = item.Unite;
                        if (item.CostCode != null)
                            document.Tables[bNumber].Rows[i].Cells[4].Range.Text = item.CostCode.Code;
                        if (string.IsNullOrWhiteSpace(item.Unite))
                        {
                            document.Tables[bNumber].Rows[i].Cells[5].Range.Text = "";
                            document.Tables[bNumber].Rows[i].Cells[6].Range.Text = "";
                            document.Tables[bNumber].Rows[i].Cells[7].Range.Text = "";
                        }
                        else
                        {
                            document.Tables[bNumber].Rows[i].Cells[5].Range.Text = item.Qte.ToString("N2");
                            document.Tables[bNumber].Rows[i].Cells[6].Range.Text = item.Pu.ToString("N2");
                            document.Tables[bNumber].Rows[i].Cells[7].Range.Text = item.Pt.ToString("N2");
                        }
                        i++;
                    }

                    document.Tables[bNumber].Rows[i + 1].Cells[1].Range.Text = "TOTAL TRAVAUX DU " + sheetName;
                    document.Tables[bNumber].Rows[i + 3].Cells[1].Range.Text = "TOTAL GENERAL DE TRAVAUX " + sheetName + "   - TTC";

                    document.Tables[bNumber].Rows[i + 1].Cells[2].Range.Text = sum1.ToString("N2");

                    document.Tables[bNumber].Rows[i + 2].Cells[1].Range.Text = "TVA "+tax.Value + "%";
                    document.Tables[bNumber].Rows[i + 2].Cells[2].Range.Text = ((Convert.ToDouble(tax.Value) / 100) * sum1).ToString("N2");
                    document.Tables[bNumber].Rows[i + 3].Cells[2].Range.Text = (sum1 + ((Convert.ToDouble(tax.Value) / 100) * sum1)).ToString("N2");
                    bNumber++;
                }
            }
            else
            {
                RasterizerSample1.Create(document, contractsDataset.BoqAtt.Content, "BordeauxDesPrixUnitaires");
            }

            if (buildings.Count > 1)
            {
                var indexRow = 1;
                double total1 = 0.0, total2 = 0.0;
                object oMissing = System.Reflection.Missing.Value;
                Table newTable;
                Range wrdRng = document.Bookmarks.get_Item("summary").Range;
                newTable = document.Tables.Add(wrdRng, buildings.Count + 2, 4, ref oMissing, ref oMissing);
                newTable.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                newTable.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                newTable.AllowAutoFit = true;
                newTable.Rows[indexRow].Cells[1].Range.Text = "Designation";
                newTable.Rows[indexRow].Cells[1].Range.Text = "Designation";
                newTable.Rows[indexRow].Cells[2].Range.Text = "Montant HT";
                newTable.Rows[indexRow].Cells[3].Range.Text = "TAV " + tax.Value + "%";
                newTable.Rows[indexRow].Cells[4].Range.Text = "Montant TTC";

                Color c = Color.FromArgb(0, 128, 128, 128);
                var wdc = (Microsoft.Office.Interop.Word.WdColor)(c.R + 0x100 * c.G + 0x10000 * c.B);

                    newTable.Rows[indexRow].Range.Shading.BackgroundPatternColor = wdc;
                newTable.Rows[indexRow].Range.Font.Color = WdColor.wdColorWhite;
                newTable.Rows[indexRow].Alignment = WdRowAlignment.wdAlignRowCenter;

                indexRow++;
                foreach (var item in buildings)
                {
                    var boqItems = withBoqItems.Where(x => x.BoqSheet.Boq.Building.Name == item.Name).ToList();
                    var sum1 = boqItems.Sum(x => x.Pt);
                    total1 += sum1;
                    total2 += sum1 * (Convert.ToDouble(tax.Value) / 100);
                    newTable.Rows[indexRow].Cells[1].Range.Text = item.Name;
                    newTable.Rows[indexRow].Cells[2].Range.Text = sum1.ToString("N2");
                    newTable.Rows[indexRow].Cells[3].Range.Text = (sum1 * (Convert.ToDouble(tax.Value) / 100)).ToString("N2");
                    newTable.Rows[indexRow].Cells[4].Range.Text = (sum1 + sum1 * (Convert.ToDouble(tax.Value) / 100)).ToString("N2");


                    indexRow++;
                }
                newTable.Rows[indexRow].Cells[1].Range.Text = "Total";
                newTable.Rows[indexRow].Cells[2].Range.Text = total1.ToString("N2");
                newTable.Rows[indexRow].Cells[3].Range.Text = total2.ToString("N2");
                newTable.Rows[indexRow].Cells[4].Range.Text = (total1 + total2).ToString("N2");
            }
           
            //var BoqItems = new List<ContractBoqItem>();
            // var obj = contractsDataset.ContractBoqItem.Select(x => x.BoqItem).Select(x => new BoqTable{ No = x.No, Item = x.Key, Unit = x.Unite, CostCode = x.CostCode!=null?x.CostCode.Code:null, Quantity = String.Format("{0:n0}", x.Qte), Pu = String.Format("{0:n0}", x.Pu), Pt = String.Format("{0:n0}", x.Pt) }).ToList();

            


            //Object oIconLabel = new FileInfo("C:\\Users\\Abdurrahman\\Desktop\\cnet.pdf").Name;
            if (contractsDataset.Planning != null)
                RasterizerSample1.Create(document, contractsDataset.Planning.Content, "Planning");
            if (contractsDataset.PlansHSE != null)
                RasterizerSample1.Create(document, contractsDataset.PlansHSE.Content, "PlansHSE");
            if (contractsDataset.DocumentsJuridiques != null)
                RasterizerSample1.Create(document, contractsDataset.DocumentsJuridiques.Content, "DocumentsJuridiques");
            if (contractsDataset.Plans != null)
                RasterizerSample1.Create(document, contractsDataset.Plans.Content, "Plans");
            if (contractsDataset.PrescriptionTechniques != null)
                RasterizerSample1.Create(document, contractsDataset.PrescriptionTechniques.Content, "PrescriptionTechniques");
            if (contractsDataset.UnitePrice != null)
                RasterizerSample1.Create(document, contractsDataset.UnitePrice.Content, "PrixUnitairesdeResiliation");
            if (contractsDataset.Other != null)
                RasterizerSample1.Create(document, contractsDataset.Other.Content, "Annex1");
            else if(contractsDataset.UnitePricePercentage > 0)
            { 
                foreach (var build in buildings)
                {
                    var BoqItems = new List<ContractBoqItem>();
                    BoqItems.AddRange(withBoqItems.Where(x => x.BoqSheet != null).Where(x => x.BoqSheet.Boq.Building.Name == build.Name).ToList());

                    var Sum = BoqItems.Sum(x => x.Pt);

                    var totalSum = new ContractBoqItem { BOQType = BOQType.TotalSum, Key = "Total Sum", Pu = Sum };

                    BoqItems.Add(totalSum);
                    var obj = BoqItems.Select(x => 
                    new BoqTable {
                        No = x.No,
                        Item = x.Key,
                        Unit = x.Unite,
                        CostCode = x.CostCode != null ? x.CostCode.Code : null,
                        Quantity = String.Format("{0:n0}", x.Qte),
                        Pu = String.Format("{0:n0}", x.Pu * (contractsDataset.UnitePricePercentage / 100)),
                       // Pt = String.Format("{0:n0}", x.Pt * (contractsDataset.UnitePricePercentage / 100))
                    }).ToList();

                    exp.ExportTo(obj, ExportToFormat.PDFtextSharpXML, "a.pdf");
                    var boqfile = File.ReadAllBytes("a.pdf");
                    RasterizerSample1.Create(document, boqfile, "PrixUnitairesdeResiliation");

                    Bookmark bm = document.Bookmarks["PrixUnitairesdeResiliation"];
                    bm.Range.InsertAfter("Building: " + build.Name);
                }

            }



            File.Delete("a.pdf");
            System.IO.DirectoryInfo di = new DirectoryInfo(exp.PathTemplateFolder);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
            {
                document.TrackRevisions = false;//Disable Tracking for the Field replacement operation
                                            //Get all Headers
                Microsoft.Office.Interop.Word.HeadersFooters headers = section.Headers;
                //Section headerfooter loop for all types enum WdHeaderFooterIndex. wdHeaderFooterEvenPages/wdHeaderFooterFirstPage/wdHeaderFooterPrimary;                          
                foreach (Microsoft.Office.Interop.Word.HeaderFooter header in headers)
                {
                    Microsoft.Office.Interop.Word.Fields fields = header.Range.Fields;
                    foreach (Microsoft.Office.Interop.Word.Field field in fields)
                    {
                        field.Select();
                        field.Delete();
                        fileOpen.Selection.TypeText(contractsDataset.ContractNumber);
                    }
                }
            }
            if (filePath != "")
            {
                document.SaveAs2(filePath + ".docx");
                document.Close(WdSaveOptions.wdDoNotSaveChanges);
                fileOpen.Quit();
                //contractsDataset.Contract = new Contract { Content = ReadFile(filePath+".docx") };
                return null;
            }
            
            string FileName = Path.GetTempFileName();

            document.Save();
            document.SaveAs2(FileName, WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();
            XpsDocument xpsDoc = new XpsDocument(FileName, System.IO.FileAccess.Read);
            return xpsDoc;
            //Close the file out
            //fileOpen.Quit();
        }
       
        public XpsDocument PreviewContract(dynamic contract)
        {
            string fileName = Path.GetTempFileName() + ".doc";
            string fileName1 = Path.GetTempFileName();

            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                fs.Write(contract.Content, 0, contract.Content.Length);
                fs.Close();
            }
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName, ReadOnly: false);
            document.SaveAs2(fileName1, WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();

            XpsDocument xpsDoc = new XpsDocument(fileName1, System.IO.FileAccess.Read);
            return xpsDoc;

        }

        public XpsDocument PreviewContractDataset(string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".docx", ReadOnly: false);
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();

            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }

        public XpsDocument PreviewVoDataset(string fileName)
        {
            Application fileOpen = new Application();
            Document document = fileOpen.Documents.Open(fileName + ".docx", ReadOnly: false);
            document.SaveAs2(fileName + ".xps", WdSaveFormat.wdFormatXPS);
            document.Close(WdSaveOptions.wdDoNotSaveChanges);
            fileOpen.Quit();

            XpsDocument xpsDoc = new XpsDocument(fileName + ".xps", System.IO.FileAccess.Read);
            return xpsDoc;

        }
        //Method to find and replace the text in the word document. Replaces all instances of it
        private static void FindAndReplace(Microsoft.Office.Interop.Word.Application fileOpen, object findText, object replaceWithText)
        {
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = true;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //ADD FONT SET HERE
            fileOpen.Selection.Find.ClearFormatting();
            fileOpen.Selection.Find.Replacement.ClearFormatting();
            fileOpen.Selection.Find.Replacement.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;

            //execute find and replace
            fileOpen.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        private static void GetYearsMonthsDays(DateTime EndDate, DateTime BeginDate, out int years, out int months, out int days)
        {
            var beginMonthDays = DateTime.DaysInMonth(BeginDate.Year, BeginDate.Month);
            var endMonthDays = DateTime.DaysInMonth(EndDate.Year, EndDate.Month);
            // get the full years
            years = EndDate.Year - BeginDate.Year - 1;
            // how many full months in the first year
            var firstYearMonths = 12 - BeginDate.Month;
            // how many full months in the last year
            var endYearMonths = EndDate.Month - 1;
            // full months
            months = firstYearMonths + endYearMonths;
            days = 0;
            // Particular end of month cases
            if (beginMonthDays == BeginDate.Day && endMonthDays == EndDate.Day)
            {
                months++;
            }
            else if (beginMonthDays == BeginDate.Day)
            {
                days += EndDate.Day;
            }
            else if (endMonthDays == EndDate.Day)
            {
                days += beginMonthDays - BeginDate.Day;
            }
            // For all the other cases
            else if (EndDate.Day > BeginDate.Day)
            {
                months++;
                days += EndDate.Day - BeginDate.Day;
            }
            else if (EndDate.Day < BeginDate.Day)
            {
                days += beginMonthDays - BeginDate.Day;
                days += EndDate.Day;
            }
            else
            {
                months++;
            }
            if (months >= 12 )
            {
                years++;
                months = months - 12;
            }
            if (years < 0)
            {
                years = 0;
                if (EndDate.Month == BeginDate.Month)
                    if (months == 11)
                        months = 0;
            }
        }

        /// <summary>
        /// This method takes a Word document full path and new XPS document full path and name
        /// and returns the new XpsDocument
        /// </summary>
        /// <param name="wordDocName"></param>
        /// <param name="xpsDocName"></param>
        /// <returns></returns>
        public static XpsDocument ConvertWordDocToXPSDoc(string wordDocName, string xpsDocName)
        {
            // Create a WordApplication and add Document to it
            Microsoft.Office.Interop.Word.Application
                wordApplication = new Microsoft.Office.Interop.Word.Application();
            wordApplication.Documents.Add(wordDocName);


            Document doc = wordApplication.ActiveDocument;
            // You must make sure you have Microsoft.Office.Interop.Word.Dll version 12.
            // Version 11 or previous versions do not have WdSaveFormat.wdFormatXPS option
            try
            {
                doc.SaveAs(xpsDocName, WdSaveFormat.wdFormatXPS);
                wordApplication.Quit();

                XpsDocument xpsDoc = new XpsDocument(xpsDocName, System.IO.FileAccess.Read);
                return xpsDoc;
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }
            return null;
        }

        public string converti(System.Int64 chiffre)
        {
            System.Int64 centaine, dizaine, unite, reste, y;
            bool dix = false;
            bool soixanteDix = false;
            string lettre = "";
            //strcpy(lettre, "");

            reste = chiffre / 1;

            for (int i = 1000000000; i >= 1; i /= 1000)
            {
                y = reste / i;
                if (y != 0)
                {
                    centaine = y / 100;
                    dizaine = (y - centaine * 100) / 10;
                    unite = y - (centaine * 100) - (dizaine * 10);
                    switch (centaine)
                    {
                        case 0:
                            break;
                        case 1:
                            lettre += "cent ";
                            break;
                        case 2:
                            if ((dizaine == 0) && (unite == 0)) lettre += "deux cents ";
                            else lettre += "deux cent ";
                            break;
                        case 3:
                            if ((dizaine == 0) && (unite == 0)) lettre += "trois cents ";
                            else lettre += "trois cent ";
                            break;
                        case 4:
                            if ((dizaine == 0) && (unite == 0)) lettre += "quatre cents ";
                            else lettre += "quatre cent ";
                            break;
                        case 5:
                            if ((dizaine == 0) && (unite == 0)) lettre += "cinq cents ";
                            else lettre += "cinq cent ";
                            break;
                        case 6:
                            if ((dizaine == 0) && (unite == 0)) lettre += "six cents ";
                            else lettre += "six cent ";
                            break;
                        case 7:
                            if ((dizaine == 0) && (unite == 0)) lettre += "sept cents ";
                            else lettre += "sept cent ";
                            break;
                        case 8:
                            if ((dizaine == 0) && (unite == 0)) lettre += "huit cents ";
                            else lettre += "huit cent ";
                            break;
                        case 9:
                            if ((dizaine == 0) && (unite == 0)) lettre += "neuf cents ";
                            else lettre += "neuf cent ";
                            break;
                    }// endSwitch(centaine)

                    switch (dizaine)
                    {
                        case 0:
                            break;
                        case 1:
                            dix = true;
                            break;
                        case 2:
                            lettre += "vingt ";
                            break;
                        case 3:
                            lettre += "trente ";
                            break;
                        case 4:
                            lettre += "quarante ";
                            break;
                        case 5:
                            lettre += "cinquante ";
                            break;
                        case 6:
                            lettre += "soixante ";
                            break;
                        case 7:
                            dix = true;
                            soixanteDix = true;
                            lettre += "soixante ";
                            break;
                        case 8:
                            lettre += "quatre-vingt ";
                            break;
                        case 9:
                            dix = true;
                            lettre += "quatre-vingt ";
                            break;
                    } // endSwitch(dizaine)

                    switch (unite)
                    {
                        case 0:
                            if (dix) lettre += "dix ";
                            break;
                        case 1:
                            if (soixanteDix) lettre += "et onze ";
                            else
                                if (dix) lettre += "onze ";
                            else if ((dizaine != 1 && dizaine != 0)) lettre += "et un ";
                            else lettre += "un ";
                            break;
                        case 2:
                            if (dix) lettre += "douze ";
                            else lettre += "deux ";
                            break;
                        case 3:
                            if (dix) lettre += "treize ";
                            else lettre += "trois ";
                            break;
                        case 4:
                            if (dix) lettre += "quatorze ";
                            else lettre += "quatre ";
                            break;
                        case 5:
                            if (dix) lettre += "quinze ";
                            else lettre += "cinq ";
                            break;
                        case 6:
                            if (dix) lettre += "seize ";
                            else lettre += "six ";
                            break;
                        case 7:
                            if (dix) lettre += "dix-sept ";
                            else lettre += "sept ";
                            break;
                        case 8:
                            if (dix) lettre += "dix-huit ";
                            else lettre += "huit ";
                            break;
                        case 9:
                            if (dix) lettre += "dix-neuf ";
                            else lettre += "neuf ";
                            break;
                    } // endSwitch(unite)

                    switch (i)
                    {
                        case 1000000000:
                            if (y > 1) lettre += "milliards ";
                            else lettre += "milliard ";
                            break;
                        case 1000000:
                            if (y > 1) lettre += "millions ";
                            else lettre += "million ";
                            break;
                        case 1000:
                            lettre += "mille ";
                            break;
                    }
                } // end if(y!=0)
                reste -= y * i;
                dix = false;
                soixanteDix = false;
            } // end for
            if (lettre.Length == 0) lettre += "zero";

            // pour les chiffres apres la virgule :
            Decimal chiffre3;
            chiffre3 = (Decimal)(chiffre * 100) % 100;
            Console.WriteLine(chiffre3);

            //int chiffre2 = (int)((chiffre - (int)(chiffre/1))*100);
            //Console.WriteLine(chiffre2);
            dizaine = (int)(chiffre3) / 10;
            unite = (int)chiffre3 - (dizaine * 10);

            string lettre2 = "";
            switch (dizaine)
            {
                case 0:
                    break;
                case 1:
                    dix = true;
                    break;
                case 2:
                    lettre2 += "vingt ";
                    break;
                case 3:
                    lettre2 += "trente ";
                    break;
                case 4:
                    lettre2 += "quarante ";
                    break;
                case 5:
                    lettre2 += "cinquante ";
                    break;
                case 6:
                    lettre2 += "soixante ";
                    break;
                case 7:
                    dix = true;
                    soixanteDix = true;
                    lettre2 += "soixante ";
                    break;
                case 8:
                    lettre2 += "quatre-vingt ";
                    break;
                case 9:
                    dix = true;
                    lettre2 += "quatre-vingt ";
                    break;
            } // endSwitch(dizaine)

            switch (unite)
            {
                case 0:
                    if (dix) lettre2 += "dix ";
                    break;
                case 1:
                    if (soixanteDix) lettre2 += "et onze ";
                    else
                        if (dix) lettre2 += "onze ";
                    else if ((dizaine != 1 && dizaine != 0)) lettre2 += "et un ";
                    else lettre2 += "un ";
                    break;
                case 2:
                    if (dix) lettre2 += "douze ";
                    else lettre2 += "deux ";
                    break;
                case 3:
                    if (dix) lettre2 += "treize ";
                    else lettre2 += "trois ";
                    break;
                case 4:
                    if (dix) lettre2 += "quatorze ";
                    else lettre2 += "quatre ";
                    break;
                case 5:
                    if (dix) lettre2 += "quinze ";
                    else lettre2 += "cinq ";
                    break;
                case 6:
                    if (dix) lettre2 += "seize ";
                    else lettre2 += "six ";
                    break;
                case 7:
                    if (dix) lettre2 += "dix-sept ";
                    else lettre2 += "sept ";
                    break;
                case 8:
                    if (dix) lettre2 += "dix-huit ";
                    else lettre2 += "huit ";
                    break;
                case 9:
                    if (dix) lettre2 += "dix-neuf ";
                    else lettre2 += "neuf ";
                    break;
            }

            // on enleve le un devant le mille :
            if (lettre.StartsWith("un mille"))
            {
                //Console.WriteLine("on enleve le un devant le mille");
                lettre = lettre.Remove(0, 3);
            }

            if (lettre2.Equals(""))
                return lettre + "";
            else if (dizaine.Equals(0) && unite.Equals(1))
                return lettre + " virgule " + lettre2 + "";
            else
                return lettre + " virgule " + lettre2 + "";
        }
        private string ones(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        private string tens(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private string ConvertWholeNumber(string Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        private string ConvertDecimals(string number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        private string ConvertToWords(string numb)
        {
            string val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            string endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }
        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }
    }
}
