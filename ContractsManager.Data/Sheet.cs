﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class Sheet : ModelBase, INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string NameFr { get; set; }
        private CostCodeLibrary _costCode;
 
        public virtual CostCodeLibrary CostCode
        {
            get { return _costCode; }
            set
            {
                _costCode = value;
                NotifyPropertyChanged("CostCode");
            }
        }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
