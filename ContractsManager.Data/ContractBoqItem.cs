﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class ContractBoqItem : ModelBase, INotifyPropertyChanged
    {
        [StringLength(255)]
        public string No { get; set; }

        [StringLength(255)]
        public string Key { get; set; }

        [StringLength(255)]
        public string Unite { get; set; }

        public int? CostCodeId { get; set; }
        [ForeignKey("CostCodeId")]
        private CostCodeLibrary _costCode;
        public virtual CostCodeLibrary CostCode
        {
            get { return _costCode; }
            set
            {
                _costCode = value;
                NotifyPropertyChanged("CostCode");
            }
        }

        private double _qte;
        public double Qte
        {
            get { return _qte; }
            set
            {
                _qte = value;
              // _pt = _qte * _pu;

                NotifyPropertyChanged("Qte");
                NotifyPropertyChanged("Pt");
            }
        }

        private double _pu;
        public double Pu
        {
            get { return _pu; }
            set
            {
                _pu = value;
                //_pt = _qte * _pu;
                NotifyPropertyChanged("Pu");
                NotifyPropertyChanged("Pt");
            }
        }

        private double _pt;
        public double Pt
        {
            get
            {
                //if (_qte == 0 && _pu == 0)
                //    return _pt;
            
                    return _qte * _pu;
            }
            set
            {

                if (value != 0)
                    _pt = value;
                else
                {
                    if (_qte != 0 && _pu != 0)
                        _pt = _qte * _pu;
                }

                NotifyPropertyChanged("Pt");
            }
        }

        private double _orderBoq;
        public double OrderBoq
        {
            get { return _orderBoq; }
            set
            {
                _orderBoq = value;
                NotifyPropertyChanged("OrderBoq");
            }
        }
        [Required]
        public BOQType BOQType { get; set; }

        public int? BoqSheetId { get; set; }
        [ForeignKey("BoqSheetId")]
        public virtual BoqSheet BoqSheet { get; set; }

        public int? ParentId { get; set; }
        public ContractBoqItem Parent { get; set; }

        public ICollection<ContractBoqItem> Children { get; set; }

        public int ContractsDatasetId { get; set; }

        public ContractsDataset ContractsDataset { get; set; }

        //Additional Fields
        private double _cumulQte;
        public double CumulQte
        {
            get { return _cumulQte; }
            set {
                _cumulQte = value;
                if (_qte != 0)
                {
                    _cumulPercent = (_cumulQte / _qte) * 100;
                }
                else
                    _cumulPercent = 0;
                _actualQte = _cumulQte - _precedQte;
                _cumulAmount = Pu * _cumulQte;
                _actualAmount = Pu * _actualQte;
                _precedAmount = Pu * _precedQte;

                NotifyPropertyChanged("CumulQte");
                NotifyPropertyChanged("ActualQte");
                NotifyPropertyChanged("PrecedQte");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
                NotifyPropertyChanged("CumulPercent");
            }
        }

        private double _precedQte;
        public double PrecedQte
        {
            get { return _precedQte; }
            set {
                _precedQte = value;
                // _cumulQte = _actualQte + _precedQte;
                _actualQte = _cumulQte - _precedQte;
                _cumulAmount = Pu * _cumulQte;
                _actualAmount = Pu * _actualQte;
                _precedAmount = Pu * _precedQte;
                NotifyPropertyChanged("CumulQte");
                NotifyPropertyChanged("ActualQte");
                NotifyPropertyChanged("PrecedQte");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
            }
        }

        private double _actualQte;
        public double ActualQte
        {
            get { return _actualQte; }
            set
            {
                _actualQte = value;
                _cumulQte = _actualQte + _precedQte;
                if (_qte != 0)
                {
                    _cumulPercent = (_cumulQte / _qte) * 100;
                }
                else
                    _cumulPercent = 0;
                _cumulAmount = Pu * _cumulQte;
                _actualAmount = Pu * _actualQte;
                _precedAmount = Pu * _precedQte;
                NotifyPropertyChanged("ActualQte");
                NotifyPropertyChanged("CumulQte");
                NotifyPropertyChanged("PrecedQte");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
                NotifyPropertyChanged("CumulPercent");
            }
        }

        private double _cumulQtePer;
        public double CumulQtePer
        {
            get { return _cumulQtePer; }
            set
            {
                _cumulQtePer = value;
                if (_cumulQtePer == 0)
                {
                    if(_qte != 0)
                        _cumulQtePer = _cumulQte / _qte;
                    else
                        _cumulQtePer = 0;
                }
                 _actualQtePer = _cumulQtePer - _precedQtePer;
              
                NotifyPropertyChanged("CumulQtePer");
                NotifyPropertyChanged("ActualQtePer");
                NotifyPropertyChanged("PrecedQtePer");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
            }
        }

        private double _precedQtePer;
        public double PrecedQtePer
        {
            get { return _precedQtePer; }
            set
            {
                _precedQtePer = value;
                if (_precedQtePer == 0)
                {
                    if (_qte != 0)
                        _precedQtePer = _precedQte / _qte;
                    else
                        _precedQtePer = 0;
                }
                _actualQtePer = _cumulQtePer - _precedQtePer;
              

                NotifyPropertyChanged("CumulQtePer");
                NotifyPropertyChanged("ActualQtePer");
                NotifyPropertyChanged("PrecedQtePer");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
            }
        }

        private double _actualQtePer;
        public double ActualQtePer
        {
            get { return _actualQtePer; }
            set
            {
                _actualQtePer = value;
                if (_actualQtePer == 0)
                {
                    if (_qte != 0)
                        _actualQtePer = _actualQte / _qte;
                    else
                        _actualQtePer = 0;
                }
                _cumulQtePer = _actualQtePer + _precedQtePer;
                
                NotifyPropertyChanged("CumulQtePer");
                NotifyPropertyChanged("ActualQtePer");
                NotifyPropertyChanged("PrecedQtePer");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");
            }
        }

        public double LastCumul { get; set; }
        public double LastActual { get; set; }
        public double LastPreced { get; set; }
        private double _cumulPercent;
        [NotMapped]
        public double CumulPercent
        {
            get { return _cumulPercent; }
            set {
                _cumulPercent = value;
                _cumulQte = Qte * (_cumulPercent / 100);
                _actualQte = _cumulQte - _precedQte;
                _cumulAmount = Pu * _cumulQte;
                _actualAmount = Pu * _actualQte;
                _precedAmount = Pu * _precedQte;
                NotifyPropertyChanged("CumulPercent");
                NotifyPropertyChanged("CumulQte");
                NotifyPropertyChanged("ActualQte");
                NotifyPropertyChanged("CumulAmount");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("PrecedAmount");

            }
        }

        private double _cumulAmount;
        [NotMapped]
        public double CumulAmount
        {
            get { return _cumulAmount; }
            set {
                _cumulAmount = value;
                NotifyPropertyChanged("CumulAmount");
            }
        }

        private double _actualAmount;
        [NotMapped]
        public double ActualAmount
        {
            get { return _actualAmount; }
            set {
                _actualAmount = value;
                NotifyPropertyChanged("ActualAmount");
            }
        }

        private double _precedAmount;
        [NotMapped]
        public double PrecedAmount
        {
            get { return _precedAmount; }
            set {
                _precedAmount = value;
                NotifyPropertyChanged("PrecedAmount");
            }
        }


        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
