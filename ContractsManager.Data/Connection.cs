﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContractsManager.Data
{
    public class Connection
    {

        //A transport-level error has occurred when receiving results from the server. (provider: Session Provider, error: 19 - Physical connection is not usable)
        public static String BuildConnectionString(ConnectionModel cmodel)
        {
            // Build the connection string from the provided datasource and database
            String connString = @"data source=" + cmodel.DataSource + ";initial catalog=" + cmodel.Database + ";integrated security=false;User ID =" + cmodel.Username + ";Password=" + cmodel.Password + ";TransparentNetworkIPResolution=False";

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = cmodel.DataSource; 
            builder.InitialCatalog = cmodel.Database;
            builder.UserID = cmodel.Username;
            builder.Password = cmodel.Password;
            builder.MultipleActiveResultSets = true;
            builder.PersistSecurityInfo = false;
            return builder.ConnectionString.ToString();
        }

        public static ObservableCollection<ConnectionModel> GetConnectionStrings()
        {
            ObservableCollection<ConnectionModel> cmodels = new ObservableCollection<ConnectionModel>();
            if (File.Exists("connectionData_32.bin"))
            {
                using (var file = File.OpenRead("connectionData_32.bin"))
                {
                    cmodels = Serializer.Deserialize<ObservableCollection<ConnectionModel>>(file);
                }
            }
            else
                return new ObservableCollection<ConnectionModel>();

            return cmodels;
        }
        public static Boolean SetConnectionString(ObservableCollection<ConnectionModel> cmodels)
        {
            try
            {
                using (var file = File.Create("connectionData_32.bin"))
                    {
                        Serializer.Serialize(file, cmodels);
                    }

                return true;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error In Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            
        }
    }
}
