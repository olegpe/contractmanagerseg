﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class Deduction : ModelBase
    {
        public int? LaborId { get; set; }
        public virtual Labor Labor { get; set; }

        public int? MaterialsId { get; set; }
        public virtual Material Material { get; set; }

        public int? MachinesId { get; set; }
        public virtual Machine Machine { get; set; }
    }
}
