﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data.Repository
{
    public class UnitOfWork : IDisposable
    {
        public DataContext context { get; set; }
        private GenericRepository<Project> projecttRepository;
        private GenericRepository<VOContract> voVontractRepository;
        private GenericRepository<VoDataset> voDatasetRepository;
        private GenericRepository<Configuration> _configurationRepository;
        private GenericRepository<MachineDatabase> _machineDatabaseRepository;
        private GenericRepository<Po> _poRepository;
        private GenericRepository<LaborDatabase> _laborDatabaseRepository;
        private GenericRepository<Country> _countryRepository;
        private GenericRepository<BoqSheet> _boqSheetRepository;
        private GenericRepository<BoqItem> _boqItemRepository;
        private GenericRepository<Vo> _voRepository;
        private GenericRepository<VenteLocal> _venteLocalRepository;
        private GenericRepository<Subcontractor> _subcontractorRepository;
        private GenericRepository<User> _usersRepository;
        private GenericRepository<CostCodeLibrary> _costCodeLibraryRepository;
        private GenericRepository<ContractsDataset> _contractsDatasetRepository;
        private GenericRepository<Contract> _contractRepository;
        private GenericRepository<Material> _materialRepository;
        private GenericRepository<Labor> _laborRepository;
        private GenericRepository<Machine> _machineRepository;
        private GenericRepository<ContractBoqItem> _contractBoqItemRepository;
        private GenericRepository<ContractVo> _contractVoRepository;
        private GenericRepository<Ipc> _ipcRepository;
        private GenericRepository<Contract> _contract;
        private GenericRepository<Currency> _currencies;
        private GenericRepository<Sheet> _sheets;
        private GenericRepository<ContractFile> _contractFile;

        private GenericRepository<Notification> _notifications;
        public UnitOfWork()
        {
            if(Connection.GetConnectionStrings().Count > 0)
            {
                var con = Connection.GetConnectionStrings().Where(x => x.IsCurrent == true).FirstOrDefault();
                if (con == null)
                    return; // TODO :: ERROR MESSAGE
                context = new DataContext(Connection.BuildConnectionString(con));
                context.Database.CommandTimeout = 180;
            }
        }
        public GenericRepository<VOContract> VOContractRepository
        {
            get
            {

                if (this.voVontractRepository == null)
                {
                    this.voVontractRepository = new GenericRepository<VOContract>(context);
                }
                return voVontractRepository;
            }
        }
        public GenericRepository<ContractFile> ContractFileRepository
        {
            get
            {

                if (this._contractFile == null)
                {
                    this._contractFile = new GenericRepository<ContractFile>(context);
                }
                return _contractFile;
            }
        }
        
        public GenericRepository<BoqSheet> BoqSheetRepository
        {
            get
            {

                if (this._boqSheetRepository == null)
                {
                    this._boqSheetRepository = new GenericRepository<BoqSheet>(context);
                }
                return _boqSheetRepository;
            }
        }
        
        public GenericRepository<VoDataset> VoDatasetRepository
        {
            get
            {

                if (this.voDatasetRepository == null)
                {
                    this.voDatasetRepository = new GenericRepository<VoDataset>(context);
                }
                return voDatasetRepository;
            }
        }

        public GenericRepository<Configuration> ConfigurationRepository
        {
            get
            {

                if (this._configurationRepository == null)
                {
                    this._configurationRepository = new GenericRepository<Configuration>(context);
                }
                return _configurationRepository;
            }
        }
        public GenericRepository<BoqItem> BoqItemRepository
        {
            get
            {

                if (this._boqItemRepository == null)
                {
                    this._boqItemRepository = new GenericRepository<BoqItem>(context);
                }
                return _boqItemRepository;
            }
        }
        public GenericRepository<Vo> VoRepository
        {
            get
            {

                if (this._voRepository == null)
                {
                    this._voRepository = new GenericRepository<Vo>(context);
                }
                return _voRepository;
            }
        }
        public GenericRepository<Po> PoRepository
        {
            get
            {

                if (this._poRepository == null)
                {
                    this._poRepository = new GenericRepository<Po>(context);
                }
                return _poRepository;
            }
        }
        public GenericRepository<LaborDatabase> LaborDatabaseRepository
        {
            get
            {

                if (this._laborDatabaseRepository == null)
                {
                    this._laborDatabaseRepository = new GenericRepository<LaborDatabase>(context);
                }
                return _laborDatabaseRepository;
            }
        }
        public GenericRepository<MachineDatabase> MachineDatabaseRepository
        {
            get
            {

                if (this._machineDatabaseRepository == null)
                {
                    this._machineDatabaseRepository = new GenericRepository<MachineDatabase>(context);
                }
                return _machineDatabaseRepository;
            }
        }
        public GenericRepository<Project> ProjectRepository
        {
            get
            {

                if (this.projecttRepository == null)
                {
                    this.projecttRepository = new GenericRepository<Project>(context);
                }
                return projecttRepository;
            }
        }
        public GenericRepository<Country> CountryRepository
        {
            get
            {

                if (this._countryRepository == null)
                {
                    this._countryRepository = new GenericRepository<Country>(context);
                }
                return _countryRepository;
            }
        }
        public GenericRepository<VenteLocal> VenteLocalRepository
        {
            get
            {

                if (this._venteLocalRepository == null)
                {
                    this._venteLocalRepository = new GenericRepository<VenteLocal>(context);
                }
                return _venteLocalRepository;
            }
        }
        public GenericRepository<Subcontractor> SubcontractorRepository
        {
            get
            {

                if (this._subcontractorRepository == null)
                {
                    this._subcontractorRepository = new GenericRepository<Subcontractor>(context);
                }
                return _subcontractorRepository;
            }
        }
        public GenericRepository<User> UsersRepository
        {
            get
            {

                if (this._usersRepository == null)
                {
                    this._usersRepository = new GenericRepository<User>(context);
                }
                return _usersRepository;
            }
        }
        public GenericRepository<CostCodeLibrary> CostCodeLibraryRepository
        {
            get
            {

                if (this._costCodeLibraryRepository == null)
                {
                    this._costCodeLibraryRepository = new GenericRepository<CostCodeLibrary>(context);
                }
                return _costCodeLibraryRepository;
            }
        }
        public GenericRepository<ContractsDataset> ContractsDatasetRepository
        {
            get
            {

                if (this._contractsDatasetRepository == null)
                {
                    this._contractsDatasetRepository = new GenericRepository<ContractsDataset>(context);
                }
                return _contractsDatasetRepository;
            }
        }
        public GenericRepository<Contract> ContractRepository
        {
            get
            {

                if (this._contractRepository == null)
                {
                    this._contractRepository = new GenericRepository<Contract>(context);
                }
                return _contractRepository;
            }
        }
        public GenericRepository<Material> MaterialRepository
        {
            get
            {

                if (this._materialRepository == null)
                {
                    this._materialRepository = new GenericRepository<Material>(context);
                }
                return _materialRepository;
            }
        }
        public GenericRepository<Labor> LaborRepository
        {
            get
            {

                if (this._laborRepository == null)
                {
                    this._laborRepository = new GenericRepository<Labor>(context);
                }
                return _laborRepository;
            }
        }
        public GenericRepository<Machine> MachineRepository
        {
            get
            {

                if (this._machineRepository == null)
                {
                    this._machineRepository = new GenericRepository<Machine>(context);
                }
                return _machineRepository;
            }
        }
        public GenericRepository<ContractBoqItem> ContractBoqItemRepository
        {
            get
            {

                if (this._contractBoqItemRepository == null)
                {
                    this._contractBoqItemRepository = new GenericRepository<ContractBoqItem>(context);
                }
                return _contractBoqItemRepository;
            }
        }
        public GenericRepository<ContractVo> ContractVoRepository
        {
            get
            {

                if (this._contractVoRepository == null)
                {
                    this._contractVoRepository = new GenericRepository<ContractVo>(context);
                }
                return _contractVoRepository;
            }
        }
        public GenericRepository<Notification> Notifications
        {
            get
            {

                if (this._notifications == null)
                {
                    this._notifications = new GenericRepository<Notification>(context);
                }
                return _notifications;
            }
        }
        public GenericRepository<Ipc> IpcRepository
        {
            get
            {

                if (this._ipcRepository == null)
                {
                    this._ipcRepository = new GenericRepository<Ipc>(context);
                }
                return _ipcRepository;
            }
        }
        public GenericRepository<Currency> CurrenciesRepository
        {
            get
            {

                if (this._currencies == null)
                {
                    this._currencies = new GenericRepository<Currency>(context);
                }
                return _currencies;
            }
        }
        public GenericRepository<Sheet> SheetsRepository
        {
            get
            {

                if (this._sheets == null)
                {
                    this._sheets = new GenericRepository<Sheet>(context);
                }
                return _sheets;
            }
        }
        public GenericRepository<Contract> Contracts
        {
            get
            {

                if (this._contract == null)
                {
                    this._contract = new GenericRepository<Contract>(context);
                }
                return _contract;
            }
        }

        public void Save()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    foreach (var entry in ex.Entries)
                    {
                        // Update the values of the entity that failed to save from the store
                        if(entry.State != System.Data.Entity.EntityState.Deleted)
                        {
                             var dv = entry.GetDatabaseValues();
                             var cv = entry.CurrentValues;
                             entry.OriginalValues.SetValues(dv);
                             entry.CurrentValues.SetValues(cv);
                        }
                        
                    }
                }
                catch(Exception ex)
                {

                    while (ex != null)
                    {
                        MessageBox.Show("Message : " + ex.Message, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error);
                        ex = ex.InnerException;
                    }
                }
         

            } while (saveFailed);
            /*try
            {
                context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    if (entry.Entity is Project)
                    {
                        var proposedValues = entry.CurrentValues;
                        var databaseValues = entry.GetDatabaseValues();

                        foreach (var property in proposedValues.PropertyNames)
                        {
                            var proposedValue = proposedValues[property];
                            var databaseValue = databaseValues[property];

                            // TODO: decide which value should be written to database
                             proposedValues[property] = proposedValue;
                        }

                        // Refresh original values to bypass next concurrency check
                        entry.OriginalValues.SetValues(databaseValues);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new NotSupportedException(
                            "Don't know how to handle concurrency conflicts for "
                            + entry);
                    }
                }
            }*/
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
