﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{
    public class Machine : ModelBase, INotifyPropertyChanged
    {

        private MachineDatabase _machineCode;
        public virtual MachineDatabase MachineCode
        {
            get { return _machineCode; }
            set
            {
                _machineCode = value;
                NotifyPropertyChanged("MachineCode");
            }
        }

        private string _machineType;
        public string MachineType
        {
            get { return _machineType; }
            set
            {
                _machineType = value;
                NotifyPropertyChanged("MachineType");

            }
        }
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set
            {
                _unit = value;
                NotifyPropertyChanged("Unit");
            }
        }
        private double _unitPrice;
        public double UnitPrice
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
                    _amount = _quantity * UnitPrice;
                NotifyPropertyChanged("UnitPrice");
                NotifyPropertyChanged("Amount");
            }
        }

        private string _ref;
        public string Ref
        {
            get { return _ref; }
            set {
                _ref = value;
                NotifyPropertyChanged("Ref");
            }
        }

        private double _quantity;
        public double Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                    _amount = _quantity * UnitPrice;
                NotifyPropertyChanged("Quantity");
                NotifyPropertyChanged("Amount");
            }
        }

        private double _amount;
        public double Amount
        {
            get { return _amount; }
            set
            {
                _amount = value;
                _actualAmount =( _deduction * _amount)/100;
                NotifyPropertyChanged("Amount");
                NotifyPropertyChanged("ActualAmount");

            }
        }

        private double _consumedAmount;
        public double ConsumedAmount
        {
            get { return _consumedAmount; }
            set
            {
                _consumedAmount = value;
               // _consumedAmount = Quantity * UnitPrice;
                NotifyPropertyChanged("ConsumedAmount");
                NotifyPropertyChanged("ActualAmount");
            }
        }

        private double _actualAmount;
        public double ActualAmount
        {
            get { return _actualAmount; }
            set
            {
                _actualAmount = value;
                NotifyPropertyChanged("ActualAmount");
            }
        }

        private double _deduction;
        public double Deduction
        {
            get { return _deduction; }
            set
            {
                _deduction = value;
                _actualAmount = (_deduction * _amount)/ 100;
                NotifyPropertyChanged("Deduction");
                NotifyPropertyChanged("ActualAmount");
            }
        }

        public double PrecedentAmount { get; set; }
        public double PrecedentAmountOld { get; set; }

        public int? ContractsDatasetId { get; set; }
        public ContractsDataset ContractsDataset { get; set; }

        public Machine()
        {
            Deduction = 100;
        }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}