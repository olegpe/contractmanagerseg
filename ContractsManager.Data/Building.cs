﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("Buildings")]
    public class Building : ModelBase
    {
        [StringLength(255)]
        public string Name { get; set; }


        
        public int ProjectLevel { get; set; }
        public int SubContractorLevel { get; set; }
        public string Type { get; set; }

        public int? BoqId { get; set; }
        [ForeignKey("BoqId")]
        public virtual Boq Boq { get; set; }

        public int? ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public virtual ICollection<ContractsDataset> ContractsDatasets { get; set; }

    }
}
