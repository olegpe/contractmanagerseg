﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class ContractsDataset: ModelBase, INotifyPropertyChanged
    {
        private string _subName;
        [NotMapped]
        public string SubName
        {
            get { return _subName; }
            set { _subName = value;
                NotifyPropertyChanged("SubName");
            }
        }

        [NotMapped]
        public string Activity { get; set; }
        private double _total;
        [NotMapped]
        public double TotalBoq
        {
            get { return _total; }
            set
            {
                _total = value;
                NotifyPropertyChanged("TotalBoq");
            }
        }
        [NotMapped]
        public double TotalBAO { get; set; }
        [NotMapped]
        public double Additions { get; set; }
        [NotMapped]
        public double Ommissions { get; set; }
        [NotMapped]
        public double NetExecuted { get; set; }
        [NotMapped]
        public double NetExecutedPercentage { get; set; }
        [NotMapped]
        public double PerceivedTTC { get; set; }
        //[NotMapped]
        //public double Rate { get; set; }
        

        public double AdvancePayment { get; set; }
        [NotMapped]
        public double AdvancePaymentPercentage { get; set; }
        [NotMapped]
        public double TotalCumul { get; set; }
        [NotMapped]
        public double TotalCumulPercentage { get; set; }
        [NotMapped]
        public double TotalProrataCumul { get; set; }
        [NotMapped]
        public double TotalProrataCumulPercentage { get; set; }
        [NotMapped]
        public double TotalCumulDeduction { get; set; }
        [NotMapped]
        public double TotalCumulDeductionPercentage { get; set; }
        [NotMapped]
        public double Retention { get; set; }
        [NotMapped]
        public double RetentionPercentage { get; set; }
        [NotMapped]
        public double ApRecovery { get; set; }
        [NotMapped]
        public double ApRecoveryPercentage { get; set; }
        [NotMapped]
        public double Perceived { get; set; }
        [NotMapped]
        public double Rate { get; set; }

        public ContractsDataset()
        {
            ContractDate = null;
            CompletionDate = null;
        }
        public int VoLevel { get; set; }
        public DateTime? ContractDate { get; set; }
        private DateTime? _completionDate;
        public DateTime? CompletionDate
        {
            get { return _completionDate; }
            set
            {
                _completionDate = value;
                NotifyPropertyChanged("CompletionDate");
            }
        }

        public int Order { get; set; }
        private string _purchaseIncrease;
        public string PurchaseIncrease
        {
            get { return _purchaseIncrease; }
            set
            {
                _purchaseIncrease = value;
                NotifyPropertyChanged("PurchaseIncrease");
            }
        }
        private string _LatePenalties;
        public string LatePenalties
        {
            get { return _LatePenalties; }
            set
            {
                _LatePenalties = value;
                NotifyPropertyChanged("LatePenalties");
            }
        }
        private string _LatePenaliteCeiling;
        public string LatePenaliteCeiling
        {
            get { return _LatePenaliteCeiling; }
            set
            {
                _LatePenaliteCeiling = value;
                NotifyPropertyChanged("LatePenaliteCeiling");
            }
        }
        private string _HoldWarranty;
        public string HoldWarranty
        {
            get { return _HoldWarranty; }
            set
            {
                _HoldWarranty = value;
                NotifyPropertyChanged("HoldWarranty");
            }
        }
        private string _MintenancePeriod;
        public string MintenancePeriod
        {
            get { return _MintenancePeriod; }
            set
            {
                _MintenancePeriod = value;
                NotifyPropertyChanged("MintenancePeriod");
            }
        }
        private string _WorkWarranty;
        public string WorkWarranty
        {
            get { return _WorkWarranty; }
            set
            {
                _WorkWarranty = value;
                NotifyPropertyChanged("WorkWarranty");
            }
        }
        private string _Termination;
        public string Termination
        {
            get { return _Termination; }
            set
            {
                _Termination = value;
                NotifyPropertyChanged("Termination");
            }
        }
        private string _DaysNumber;
        public string DaysNumber
        {
            get { return _DaysNumber; }
            set
            {
                _DaysNumber = value;
                NotifyPropertyChanged("DaysNumber");
            }
        }
        private string _Progress;
        public string Progress
        {
            get { return _Progress; }
            set
            {
                _Progress = value;
                NotifyPropertyChanged("Progress");
            }
        }
        private string _HoldBack;
        public string HoldBack
        {
            get { return _HoldBack; }
            set
            {
                _HoldBack = value;
                NotifyPropertyChanged("HoldBack");
            }
        }
        private string _SubcontractorAdvancePayee;
        public string SubcontractorAdvancePayee
        {
            get { return _SubcontractorAdvancePayee; }
            set
            {
                _SubcontractorAdvancePayee = value;
                NotifyPropertyChanged("SubcontractorAdvancePayee");
            }
        }
        private string _RecoverAdvance;
        public string RecoverAdvance
        {
            get { return _RecoverAdvance; }
            set
            {
                _RecoverAdvance = value;
                NotifyPropertyChanged("RecoverAdvance");
            }
        }
        private string _ProcurementConstruction;
        public string ProcurementConstruction
        {
            get { return _ProcurementConstruction; }
            set
            {
                _ProcurementConstruction = value;
                NotifyPropertyChanged("ProcurementConstruction");
            }
        }
        private string _ProrataAccount;
        public string ProrataAccount
        {
            get { return _ProrataAccount; }
            set
            {
                _ProrataAccount = value;
                NotifyPropertyChanged("ProrataAccount");
            }
        }
        private string _ManagementFees;
        public string ManagementFees
        {
            get { return _ManagementFees; }
            set
            {
                _ManagementFees = value;
                NotifyPropertyChanged("ManagementFees");
            }
        }
        private string _plansExecution;
        public string PlansExecution
        {
            get { return _plansExecution; }
            set
            {
                _plansExecution = value;
                NotifyPropertyChanged("PlansExecution");
            }
        }
        private string _paymentsTerm;
        public string PaymentsTerm
        {
            get { return _paymentsTerm; }
            set
            {
                _paymentsTerm = value;
                NotifyPropertyChanged("PaymentsTerm");
            }
        }
        private string _subTrade;

        public string SubTrade
        {
            get { return _subTrade; }
            set { 
                _subTrade = value; 
                NotifyPropertyChanged("SubTrade");
            }
        }
        [Column(TypeName = "text")]
        public string RemarkCP { get; set; }
        public int? Planning_Id { get; set; }
        public int? UnitePrice_Id { get; set; }
        public int? BoqAtt_Id { get; set; }
        public int? PrescriptionTechniques_Id { get; set; }
        public int? Plans_Id { get; set; }
        public int? Other_Id { get; set; }
        public int? PlansHSE_Id { get; set; }
        public int? DocumentsJuridiques_Id { get; set; }
        [ForeignKey("Planning_Id")]
        public ContractFile Planning { get; set; }
        [ForeignKey("UnitePrice_Id")]
        public ContractFile UnitePrice { get; set; }
        [ForeignKey("BoqAtt_Id")]
        public ContractFile BoqAtt { get; set; }
        private double _unitPricePercantage;
        public double UnitePricePercentage
        {
            get { return _unitPricePercantage; }
            set
            {
                _unitPricePercantage = value;
                NotifyPropertyChanged("UnitePricePercentage");
            }
        }
        [ForeignKey("PrescriptionTechniques_Id")]
        public ContractFile PrescriptionTechniques { get; set; }
        [ForeignKey("Plans_Id")]
        public ContractFile Plans { get; set; }
        [ForeignKey("Other_Id")]
        public ContractFile Other { get; set; }
        [ForeignKey("PlansHSE_Id")]
        public ContractFile PlansHSE { get; set; }
        [ForeignKey("DocumentsJuridiques_Id")]
        public ContractFile DocumentsJuridiques { get; set; }

        public int? TerminatedFile_Id { get; set; }

        [ForeignKey("TerminatedFile_Id")]
        public ContractFile TerminatedFile { get; set; }
        public bool IsGenerated { get; set; }

        [Column(TypeName = "text")]
        public string Remark { get; set; }

        private Currency _currency;
        public int? CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public virtual Currency Currency
        {
            get { return _currency; }
            set
            {
                _currency = value;
                NotifyPropertyChanged("Currency");
            }
        }

        private string _contractNumber;
        public string ContractNumber
        {
            get { return _contractNumber; }
            set
            {
                _contractNumber = value;
                NotifyPropertyChanged("ContractNumber");
            }
        }
        private ContractDatasetStatus _contractDatasetStatus;
        public ContractDatasetStatus ContractDatasetStatus
        {
            get { return _contractDatasetStatus; }
            set
            {
                _contractDatasetStatus = value;
                NotifyPropertyChanged("ContractDatasetStatus");
            }
        }

        public int? ProjectId{ get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public int? SubcontractorId{ get; set; }
        private Subcontractor _subcontractor;
        [ForeignKey("SubcontractorId")]
        public virtual Subcontractor Subcontractor
        {
            get { return _subcontractor; }
            set
            {
                _subcontractor = value;
                NotifyPropertyChanged("Subcontractor");
            }
        }

        private Contract _contract;
        public int? ContractId{ get; set; }
        [ForeignKey("ContractId")]
        public Contract Contract
        {
            get { return _contract; }
            set
            {
                _contract = value;
                NotifyPropertyChanged("Contract");
            }
        }

        private BoqSheet _boqSheet;
        public virtual BoqSheet BoqSheet
        {
            get { return _boqSheet; }
            set
            {
                _boqSheet = value;
                NotifyPropertyChanged("BoqSheet");
            }
        }
        public virtual ICollection<Ipc> Ipcs { get; set; }
        public virtual ICollection<Machine> Machines { get; set; }
        public virtual ICollection<Labor> Labors { get; set; }

        public virtual ICollection<ContractBoqItem> ContractBoqItem { get; set; }
        public virtual ICollection<Building> Buildings { get; set; }
        public virtual ICollection<VoDataset> VoDatasets { get; set; }

        public int? ContractFileId { get; set; }
        public ContractFile ContractFile { get; set; }
        //public int? LaborId{ get; set; }
        //[ForeignKey("LaborId")]
        //public virtual Labor Labor { get; set; }

        //public int? MachineId{ get; set; }
        //[ForeignKey("MachineId")]
        //public virtual Machine Machine { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    public enum ContractDatasetStatus
    {
        Editable, Terminated, Active, PendingApproval
    }
}
