﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class Ipc : ModelBase, INotifyPropertyChanged
    {

        public Ipc()
        {
            FromDate = null;
            ToDate = null;
        }
        private DateTime? _fromDate;
        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                NotifyPropertyChanged("FromDate");
            }
        }
        private DateTime? _toDate;
        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                NotifyPropertyChanged("ToDate");
            }
        }
        public DateTime? DateIpc { get; set; }

        public int? ContractsDatasetId { get; set; }
        [ForeignKey("ContractsDatasetId")]
        public virtual ContractsDataset ContractsDataset { get; set; }
        private IpcStatus _ipcStatus;
        public IpcStatus IpcStatus
        {
            get { return _ipcStatus; }
            set
            {
                _ipcStatus = value;
                NotifyPropertyChanged("IpcStatus");
            }
        }
        private double _totalAmount;
        public double TotalAmount
        {
            get { return _totalAmount; }
            set
            {
                _totalAmount = value;
                NotifyPropertyChanged("TotalAmount");
            }
        }
        private double _totalAmountht;
        [NotMapped]
        public double TotalAmountHT
        {
            get { return _totalAmountht; }
            set
            {
                _totalAmountht = value;
                NotifyPropertyChanged("TotalAmountHT");
            }
        }
        public double AdvancePayment { get; set; }
        public double Retention { get; set; }
        public string Type { get; set; }
        public int Number { get; set; }
        public bool IsGenerated { get; set; }
        public double Prorata { get; set; }
        public double ApRecovery { get; set; }
        public double AdvancePaymentAmount { get; set; }
        public double AdvancePaymentAmountCumul { get; set; }
        public double RetentionAmount { get; set; }
        public double RetentionAmountCumul { get; set; }
        private double _retentionPercentage;
        public double RetentionPercentage
        {
            get { return _retentionPercentage; }
            set {
                _retentionPercentage = value;
                NotifyPropertyChanged("RetentionPercentage");
            }
        }
        private double _advancePaymentPercentage;
        public double AdvancePaymentPercentage
        {
            get { return _advancePaymentPercentage; }
            set {
                _advancePaymentPercentage = value;
                NotifyPropertyChanged("AdvancePaymentPercentage");
            }
        }
        private double _paid;

        public double Paid
        {
            get { return _paid; }
            set {
                _paid = value;
                NotifyPropertyChanged("Paid");
            }
        }
        public bool ContractActivated { get; set; }
        public double Penalty { get; set; }
        public double PreviousPenalty { get; set; }
        //public virtual Notification Notification { get; set; }
        public int? IpcFileId { get; set; }
        public IpcFile IpcFile { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    public enum IpcStatus

    {
        Issued, Editable, PendingApproval
    }
}
