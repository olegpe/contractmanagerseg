﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public interface IEntity
    {
        int Id { get; set; }

        DateTime Created { get; set; }
    }
}
