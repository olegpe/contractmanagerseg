﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{
    public class Po : ModelBase, INotifyPropertyChanged
    {
        public string PoRef { get; set; } // REF
        private string _item;
        public string Item
        {
            get { return _item; }
            set
            {
                _item = value;
                NotifyPropertyChanged("Item");
            }
        }
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set
            {
                _unit = value;
                NotifyPropertyChanged("Unit");
            }
        } //Unit
        private double _unitPrice;
        public double UnitPrice // Unit Price
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
                NotifyPropertyChanged("UnitPrice");
            }
        }

        private double _orderdQte;
        public double OrderdQte
        {
            get { return _orderdQte; }
            set
            {
                _orderdQte = value;
                NotifyPropertyChanged("OrderdQte");
            }
        }

        private double _deliveredQte;
        public double DeliveredQte // Delivered Qte
        {
            get { return _deliveredQte; }
            set
            {
                _deliveredQte = value;
                NotifyPropertyChanged("DeliveredQte");
            }
        }
     
        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
