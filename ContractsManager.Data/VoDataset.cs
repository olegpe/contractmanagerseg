﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class VoDataset : ModelBase, INotifyPropertyChanged
    {
        private string type;
        public string Type
        {
            get { return type; }
            set {
                type = value;
                NotifyPropertyChanged("Type");
            }
        }
        private double _totalAmount;
        [NotMapped]
        public double TotalAmount
        {
            get { return _totalAmount; }
            set {
                _totalAmount = value;
                NotifyPropertyChanged("TotalAmount");
            }
        }

        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }
            set
            {
                _date  = value;
                NotifyPropertyChanged("Date");
            }
        }
        private string _voNumber;
        public string VoNumber
        {
            get { return _voNumber; }
            set
            {
                _voNumber = value;
                NotifyPropertyChanged("VoNumber");
            }
        }
        public ContractFile BOQAtt { get; set; }
        public ContractFile ContractFile { get; set; }
        private VOContract _contract;
        public VOContract Contract
        {
            get { return _contract; }
            set
            {
                _contract = value;
                NotifyPropertyChanged("Contract");
            }
        }

        private ContractDatasetStatus _contractDatasetStatus;
        public ContractDatasetStatus ContractDatasetStatus
        {
            get { return _contractDatasetStatus; }
            set
            {
                _contractDatasetStatus = value;
                NotifyPropertyChanged("ContractDatasetStatus");
            }
        }


        public virtual Building Building { get; set; }
        public int? ContractsDataset_Id { get; set; }
        private ContractsDataset _ContractsDataset;
        [ForeignKey("ContractsDataset_Id")]
        public virtual ContractsDataset ContractsDataset
        {
            get
            {
                return _ContractsDataset;
            }
            set
            {
                _ContractsDataset = value;
                NotifyPropertyChanged("ContractsDataset");
            }
        }
        private Project _project;
        public virtual Project Project {
            get { return _project; }
            set { _project = value;
                NotifyPropertyChanged("Project");
            }
            }
        public virtual Subcontractor Subcontractor{ get; set; }
        public virtual ICollection<ContractVo> VoItems { get; set; }


        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
