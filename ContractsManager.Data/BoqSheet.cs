﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{
    [Table("BoqSheets")]
    public class BoqSheet : ModelBase, INotifyPropertyChanged
    {
        private string _name;
        [Required]
        [StringLength(255)]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }
        public int? BoqId { get; set; }
        [ForeignKey("BoqId")]
        public virtual Boq Boq { get; set; }
        public virtual ICollection<Vo> Vos { get; set; }
        public virtual ICollection<ContractVo> ContractVos { get; set; }
        public bool HasVo { get; set; }

        public int? CostCodeId { get; set; }
        
        private CostCodeLibrary _costCode;
        [ForeignKey("CostCodeId")]
        public virtual CostCodeLibrary CostCode
        {
            get { return _costCode; }
            set
            {
                _costCode = value;
                NotifyPropertyChanged("CostCode");
            }
        }
        [NotMapped]
        public bool IsSelected { get; set; }
        public virtual ICollection<BoqItem> BoqItems { get; set; }
        public virtual ICollection<ContractBoqItem> ContractBoqItems { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
