﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{   
    [Table("Boqs")]
    public class Boq : ModelBase, INotifyPropertyChanged
    {
        [ForeignKey("Building")]
        public new int Id { get; set; }
        public byte[] Content { get; set; }
     //   public int? CurrencyId { get; set; }
        private string _currency;
        [StringLength(10)]
     //   [ForeignKey("CurrencyId")]
        public string Currency
        {
            get { return _currency; }
            set
            {
                _currency = value;
                NotifyPropertyChanged("Currency");
            }
        }
        public int? BuildingId { get; set; }
        [ForeignKey("BuildingId")]
        public virtual Building Building { get; set; }
        public virtual ICollection<BoqSheet> BoqSheets { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
 
}