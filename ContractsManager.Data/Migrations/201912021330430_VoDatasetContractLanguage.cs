namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VoDatasetContractLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VOContracts", "Language", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VOContracts", "Language");
        }
    }
}
