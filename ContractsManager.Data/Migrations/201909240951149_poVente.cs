namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class poVente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VenteLocals", "Po_Id", c => c.Int());
            CreateIndex("dbo.VenteLocals", "Po_Id");
            AddForeignKey("dbo.VenteLocals", "Po_Id", "dbo.Poes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VenteLocals", "Po_Id", "dbo.Poes");
            DropIndex("dbo.VenteLocals", new[] { "Po_Id" });
            DropColumn("dbo.VenteLocals", "Po_Id");
        }
    }
}
