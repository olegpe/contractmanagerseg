namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class buildingType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Buildings", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Buildings", "Type");
        }
    }
}
