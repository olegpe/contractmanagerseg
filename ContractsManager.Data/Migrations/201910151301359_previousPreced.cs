namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class previousPreced : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Labors", "PrecedentAmountOld", c => c.Single(nullable: false));
            AddColumn("dbo.Machines", "PrecedentAmountOld", c => c.Single(nullable: false));
            AddColumn("dbo.VenteLocals", "PrecedentAmountOld", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VenteLocals", "PrecedentAmountOld");
            DropColumn("dbo.Machines", "PrecedentAmountOld");
            DropColumn("dbo.Labors", "PrecedentAmountOld");
        }
    }
}
