namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class totalamountDeduction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VenteLocals", "PrecedentAmount", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VenteLocals", "PrecedentAmount");
        }
    }
}
