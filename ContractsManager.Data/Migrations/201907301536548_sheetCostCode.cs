namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sheetCostCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sheets", "CostCode_Id", c => c.Int());
            CreateIndex("dbo.Sheets", "CostCode_Id");
            AddForeignKey("dbo.Sheets", "CostCode_Id", "dbo.CostCodeLibraries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sheets", "CostCode_Id", "dbo.CostCodeLibraries");
            DropIndex("dbo.Sheets", new[] { "CostCode_Id" });
            DropColumn("dbo.Sheets", "CostCode_Id");
        }
    }
}
