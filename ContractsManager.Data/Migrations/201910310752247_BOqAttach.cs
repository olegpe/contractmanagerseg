namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BOqAttach : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "BoqAtt", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "BoqAtt");
        }
    }
}
