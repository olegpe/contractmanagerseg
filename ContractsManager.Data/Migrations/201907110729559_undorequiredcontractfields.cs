namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class undorequiredcontractfields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contracts", "Template", c => c.String(maxLength: 255));
            AlterColumn("dbo.Contracts", "TemplateNumber", c => c.String(maxLength: 255));
            AlterColumn("dbo.Contracts", "Type", c => c.String(maxLength: 255));
            AlterColumn("dbo.Contracts", "SecondType", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contracts", "SecondType", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Contracts", "Type", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Contracts", "TemplateNumber", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Contracts", "Template", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
