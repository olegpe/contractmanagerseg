namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeductionsDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BoqItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        No = c.String(maxLength: 255),
                        Key = c.String(maxLength: 255),
                        Unite = c.String(maxLength: 255),
                        CostCode = c.String(maxLength: 255),
                        Qte = c.String(maxLength: 255),
                        Pu = c.String(maxLength: 255),
                        Pt = c.String(maxLength: 255),
                        BOQType = c.Int(nullable: false),
                        BoqSheetId = c.Int(nullable: false),
                        ParentId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BoqSheets", t => t.BoqSheetId, cascadeDelete: true)
                .ForeignKey("dbo.BoqItems", t => t.ParentId)
                .Index(t => t.BoqSheetId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.BoqSheets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        BoqId = c.Int(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Boqs", t => t.BoqId, cascadeDelete: true)
                .Index(t => t.BoqId);
            
            CreateTable(
                "dbo.Boqs",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Content = c.Binary(),
                        Currency = c.String(maxLength: 10),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Buildings", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Buildings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        ProjectId = c.Int(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        Acronym = c.String(nullable: false, maxLength: 255),
                        City = c.String(nullable: false, maxLength: 255),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContractsDatasets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractDate = c.String(),
                        CompletionDate = c.String(),
                        PurchaseIncrease = c.String(),
                        LatePenalties = c.String(),
                        LatePenaliteCeiling = c.String(),
                        HoldWarranty = c.String(),
                        MintenancePeriod = c.String(),
                        WorkWarranty = c.String(),
                        Termination = c.String(),
                        DaysNumber = c.String(),
                        Progress = c.String(),
                        HoldBack = c.String(),
                        SubcontractorAdvancePayee = c.String(),
                        RecoverAdvance = c.String(),
                        ProcurementConstruction = c.String(),
                        ProrataAccount = c.String(),
                        ManagementFees = c.String(),
                        Planning = c.Binary(),
                        UnitePrice = c.Binary(),
                        PrescriptionTechniques = c.Binary(),
                        Plans = c.Binary(),
                        Other = c.Binary(),
                        ContractNumber = c.String(),
                        ContractDatasetStatus = c.Int(nullable: false),
                        ProjectId = c.Int(),
                        SubcontractorId = c.Int(),
                        ContractId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contracts", t => t.ContractId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .ForeignKey("dbo.Subcontractors", t => t.SubcontractorId)
                .Index(t => t.ProjectId)
                .Index(t => t.SubcontractorId)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Template = c.String(nullable: false, maxLength: 255),
                        TemplateNumber = c.String(nullable: false, maxLength: 255),
                        Type = c.String(nullable: false, maxLength: 255),
                        Content = c.Binary(),
                        FileType = c.Int(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subcontractors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SiegeSocial = c.String(),
                        CommerceRegistrar = c.String(),
                        CommerceNumber = c.String(),
                        TaxNumber = c.String(),
                        RepresentedBy = c.String(),
                        QualityRepresentive = c.String(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CostCodeLibraries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        En = c.String(maxLength: 255),
                        Fr = c.String(maxLength: 255),
                        Code = c.String(maxLength: 255),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        Acronym = c.String(nullable: false, maxLength: 255),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Labors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeOfWorker = c.String(),
                        ActivityDescription = c.String(),
                        Unit = c.String(),
                        UnitPrice = c.Double(nullable: false),
                        Quantity = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        ContractsDatasetId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDatasetId)
                .Index(t => t.ContractsDatasetId);
            
            CreateTable(
                "dbo.Machines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MachineCode = c.String(),
                        MachineType = c.String(),
                        Unit = c.Double(nullable: false),
                        UnitPrice = c.Double(nullable: false),
                        Quantity = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        ContractsDatasetId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDatasetId)
                .Index(t => t.ContractsDatasetId);
            
            CreateTable(
                "dbo.Materials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Item = c.String(),
                        DeliveredQte = c.String(),
                        Unit = c.String(),
                        UnitPrice = c.Double(nullable: false),
                        OrderdQte = c.Double(nullable: false),
                        TransferedQte = c.Double(nullable: false),
                        TransferedTo = c.String(),
                        StockQte = c.Double(nullable: false),
                        Remark = c.String(),
                        ContractsDatasetId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDatasetId)
                .Index(t => t.ContractsDatasetId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        Password = c.String(maxLength: 255),
                        Email = c.String(maxLength: 255),
                        Phone = c.String(maxLength: 255),
                        Username = c.String(maxLength: 255),
                        UserType = c.Int(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VenteLocals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bc = c.String(),
                        Designation = c.String(),
                        Acronym = c.String(),
                        Subcontractor = c.String(),
                        Contract = c.String(),
                        Unit = c.String(),
                        Remark = c.String(),
                        TransferedTo = c.String(),
                        Quantity = c.Single(nullable: false),
                        SaleUnit = c.Single(nullable: false),
                        TransferedQte = c.Single(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        Livree = c.Single(nullable: false),
                        Consumes = c.Single(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContractsDatasetBoqItems",
                c => new
                    {
                        ContractsDataset_Id = c.Int(nullable: false),
                        BoqItem_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContractsDataset_Id, t.BoqItem_Id })
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDataset_Id, cascadeDelete: true)
                .ForeignKey("dbo.BoqItems", t => t.BoqItem_Id, cascadeDelete: true)
                .Index(t => t.ContractsDataset_Id)
                .Index(t => t.BoqItem_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Materials", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropForeignKey("dbo.Machines", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropForeignKey("dbo.Labors", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropForeignKey("dbo.BoqItems", "ParentId", "dbo.BoqItems");
            DropForeignKey("dbo.ContractsDatasets", "SubcontractorId", "dbo.Subcontractors");
            DropForeignKey("dbo.ContractsDatasets", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ContractsDatasets", "ContractId", "dbo.Contracts");
            DropForeignKey("dbo.ContractsDatasetBoqItems", "BoqItem_Id", "dbo.BoqItems");
            DropForeignKey("dbo.ContractsDatasetBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropForeignKey("dbo.BoqItems", "BoqSheetId", "dbo.BoqSheets");
            DropForeignKey("dbo.Boqs", "Id", "dbo.Buildings");
            DropForeignKey("dbo.Buildings", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.BoqSheets", "BoqId", "dbo.Boqs");
            DropIndex("dbo.ContractsDatasetBoqItems", new[] { "BoqItem_Id" });
            DropIndex("dbo.ContractsDatasetBoqItems", new[] { "ContractsDataset_Id" });
            DropIndex("dbo.Materials", new[] { "ContractsDatasetId" });
            DropIndex("dbo.Machines", new[] { "ContractsDatasetId" });
            DropIndex("dbo.Labors", new[] { "ContractsDatasetId" });
            DropIndex("dbo.ContractsDatasets", new[] { "ContractId" });
            DropIndex("dbo.ContractsDatasets", new[] { "SubcontractorId" });
            DropIndex("dbo.ContractsDatasets", new[] { "ProjectId" });
            DropIndex("dbo.Buildings", new[] { "ProjectId" });
            DropIndex("dbo.Boqs", new[] { "Id" });
            DropIndex("dbo.BoqSheets", new[] { "BoqId" });
            DropIndex("dbo.BoqItems", new[] { "ParentId" });
            DropIndex("dbo.BoqItems", new[] { "BoqSheetId" });
            DropTable("dbo.ContractsDatasetBoqItems");
            DropTable("dbo.VenteLocals");
            DropTable("dbo.Users");
            DropTable("dbo.Materials");
            DropTable("dbo.Machines");
            DropTable("dbo.Labors");
            DropTable("dbo.Countries");
            DropTable("dbo.CostCodeLibraries");
            DropTable("dbo.Subcontractors");
            DropTable("dbo.Contracts");
            DropTable("dbo.ContractsDatasets");
            DropTable("dbo.Projects");
            DropTable("dbo.Buildings");
            DropTable("dbo.Boqs");
            DropTable("dbo.BoqSheets");
            DropTable("dbo.BoqItems");
        }
    }
}
