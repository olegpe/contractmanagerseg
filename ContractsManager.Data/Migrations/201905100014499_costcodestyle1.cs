namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class costcodestyle1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CostCodeLibraries", "Bold", c => c.Boolean(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CostCodeLibraries", "Bold", c => c.String(maxLength: 255));
        }
    }
}
