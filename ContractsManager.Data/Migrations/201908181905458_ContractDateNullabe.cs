namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractDateNullabe : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ContractsDatasets", "ContractDate", c => c.DateTime());
            AlterColumn("dbo.ContractsDatasets", "CompletionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ContractsDatasets", "CompletionDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ContractsDatasets", "ContractDate", c => c.DateTime(nullable: false));
        }
    }
}
