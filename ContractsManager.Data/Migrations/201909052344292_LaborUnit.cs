namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LaborUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LaborDatabases", "Unit", c => c.String());
            AlterColumn("dbo.LaborDatabases", "UnitPrice", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LaborDatabases", "UnitPrice", c => c.Double(nullable: false));
            DropColumn("dbo.LaborDatabases", "Unit");
        }
    }
}
