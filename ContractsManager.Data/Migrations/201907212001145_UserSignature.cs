namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserSignature : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Signature", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Signature");
        }
    }
}
