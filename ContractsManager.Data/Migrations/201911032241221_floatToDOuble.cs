namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class floatToDOuble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BoqItems", "Qte", c => c.Double(nullable: false));
            AlterColumn("dbo.BoqItems", "Pu", c => c.Double(nullable: false));
            AlterColumn("dbo.BoqItems", "Pt", c => c.Double(nullable: false));
            AlterColumn("dbo.BoqItems", "OrderBoq", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractsDatasets", "AdvancePayment", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractsDatasets", "UnitePricePercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Qte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Pu", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Pt", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "OrderBoq", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "CumulQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "PrecedQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "ActualQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastCumul", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastActual", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastPreced", c => c.Double(nullable: false));
            AlterColumn("dbo.Currencies", "ConversionRate", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "TotalAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePayment", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "Retention", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "ApRecovery", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentAmountCumul", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionAmountCumul", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionPercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentPercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "Paid", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "Penalty", c => c.Double(nullable: false));
            AlterColumn("dbo.Ipcs", "PreviousPenalty", c => c.Double(nullable: false));
            AlterColumn("dbo.Labors", "PrecedentAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Labors", "PrecedentAmountOld", c => c.Double(nullable: false));
            AlterColumn("dbo.LaborDatabases", "UnitPrice", c => c.Double(nullable: false));
            AlterColumn("dbo.Machines", "PrecedentAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.Machines", "PrecedentAmountOld", c => c.Double(nullable: false));
            AlterColumn("dbo.MachineDatabases", "UnitPrice", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "CumulQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "ActualQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "PrecedQte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastCumul", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastActual", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastPreced", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "Qte", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "Pu", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "Pt", c => c.Double(nullable: false));
            AlterColumn("dbo.ContractVoes", "OrderVo", c => c.Double(nullable: false));
            AlterColumn("dbo.vo", "Qte", c => c.Double(nullable: false));
            AlterColumn("dbo.vo", "Pu", c => c.Double(nullable: false));
            AlterColumn("dbo.vo", "Pt", c => c.Double(nullable: false));
            AlterColumn("dbo.vo", "OrderVo", c => c.Double(nullable: false));
            AlterColumn("dbo.Poes", "UnitPrice", c => c.Double(nullable: false));
            AlterColumn("dbo.Poes", "OrderdQte", c => c.Double(nullable: false));
            AlterColumn("dbo.Poes", "DeliveredQte", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "Allocated", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "Quantity", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "TotalSale", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "Consumes", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "SaleUnit", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "StockQte", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "TransferedQte", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "Livree", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "ActualAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "Deduction", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "ConsumedAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "PrecedentAmount", c => c.Double(nullable: false));
            AlterColumn("dbo.VenteLocals", "PrecedentAmountOld", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VenteLocals", "PrecedentAmountOld", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "PrecedentAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "ConsumedAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "Deduction", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "ActualAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "Livree", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "TransferedQte", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "StockQte", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "SaleUnit", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "Consumes", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "TotalSale", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "Quantity", c => c.Single(nullable: false));
            AlterColumn("dbo.VenteLocals", "Allocated", c => c.Single(nullable: false));
            AlterColumn("dbo.Poes", "DeliveredQte", c => c.Single(nullable: false));
            AlterColumn("dbo.Poes", "OrderdQte", c => c.Single(nullable: false));
            AlterColumn("dbo.Poes", "UnitPrice", c => c.Single(nullable: false));
            AlterColumn("dbo.vo", "OrderVo", c => c.Single(nullable: false));
            AlterColumn("dbo.vo", "Pt", c => c.Single(nullable: false));
            AlterColumn("dbo.vo", "Pu", c => c.Single(nullable: false));
            AlterColumn("dbo.vo", "Qte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "OrderVo", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "Pt", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "Pu", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "Qte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastPreced", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastActual", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "LastCumul", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "PrecedQte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "ActualQte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "CumulQte", c => c.Single(nullable: false));
            AlterColumn("dbo.MachineDatabases", "UnitPrice", c => c.Single(nullable: false));
            AlterColumn("dbo.Machines", "PrecedentAmountOld", c => c.Single(nullable: false));
            AlterColumn("dbo.Machines", "PrecedentAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.LaborDatabases", "UnitPrice", c => c.Single(nullable: false));
            AlterColumn("dbo.Labors", "PrecedentAmountOld", c => c.Single(nullable: false));
            AlterColumn("dbo.Labors", "PrecedentAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "PreviousPenalty", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "Penalty", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "Paid", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentPercentage", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionPercentage", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionAmountCumul", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "RetentionAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentAmountCumul", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePaymentAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "ApRecovery", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "Retention", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "AdvancePayment", c => c.Single(nullable: false));
            AlterColumn("dbo.Ipcs", "TotalAmount", c => c.Single(nullable: false));
            AlterColumn("dbo.Currencies", "ConversionRate", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastPreced", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastActual", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "LastCumul", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "ActualQte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "PrecedQte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "CumulQte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "OrderBoq", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Pt", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Pu", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "Qte", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractsDatasets", "UnitePricePercentage", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractsDatasets", "AdvancePayment", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "OrderBoq", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "Pt", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "Pu", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "Qte", c => c.Single(nullable: false));
        }
    }
}
