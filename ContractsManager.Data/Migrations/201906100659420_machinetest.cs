namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class machinetest : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Machines", "Unit", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Machines", "Unit", c => c.Double(nullable: false));
        }
    }
}
