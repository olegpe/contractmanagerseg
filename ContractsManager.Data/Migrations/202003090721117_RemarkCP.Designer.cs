// <auto-generated />
namespace ContractsManager.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class RemarkCP : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(RemarkCP));
        
        string IMigrationMetadata.Id
        {
            get { return "202003090721117_RemarkCP"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
