namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubTrade : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "SubTrade", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "SubTrade");
        }
    }
}
