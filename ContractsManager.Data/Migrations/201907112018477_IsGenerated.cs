namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsGenerated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "IsGenerated", c => c.Boolean(nullable: false));
            AddColumn("dbo.Ipcs", "IsGenerated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "IsGenerated");
            DropColumn("dbo.ContractsDatasets", "IsGenerated");
        }
    }
}
