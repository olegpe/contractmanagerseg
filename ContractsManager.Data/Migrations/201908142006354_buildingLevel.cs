namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class buildingLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Buildings", "ProjectLevel", c => c.Int(nullable: false));
            AddColumn("dbo.Buildings", "SubContractorLevel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Buildings", "SubContractorLevel");
            DropColumn("dbo.Buildings", "ProjectLevel");
        }
    }
}
