namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tststs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "CurrencyId", c => c.Int());
            CreateIndex("dbo.Projects", "CurrencyId");
            AddForeignKey("dbo.Projects", "CurrencyId", "dbo.Currencies", "Id");
            DropColumn("dbo.Projects", "Currency");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Currency", c => c.String(maxLength: 20));
            DropForeignKey("dbo.Projects", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.Projects", new[] { "CurrencyId" });
            DropColumn("dbo.Projects", "CurrencyId");
        }
    }
}
