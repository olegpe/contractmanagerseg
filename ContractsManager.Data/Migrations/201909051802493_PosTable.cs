namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PosTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Poes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PoRef = c.String(),
                        Item = c.String(),
                        Unit = c.String(),
                        UnitPrice = c.Single(nullable: false),
                        OrderdQte = c.Single(nullable: false),
                        DeliveredQte = c.Single(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Poes");
        }
    }
}
