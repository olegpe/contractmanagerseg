namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IpcDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "DateIpc", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "DateIpc");
        }
    }
}
