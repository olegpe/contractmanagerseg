namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vo44 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BoqItems", "BoqSheetId", "dbo.BoqSheets");
            DropForeignKey("dbo.vo", "BoqSheetId", "dbo.BoqSheets");
            DropIndex("dbo.BoqItems", new[] { "BoqSheetId" });
            DropIndex("dbo.vo", new[] { "BoqSheetId" });
            AlterColumn("dbo.BoqItems", "BoqSheetId", c => c.Int());
            AlterColumn("dbo.vo", "BoqSheetId", c => c.Int());
            CreateIndex("dbo.BoqItems", "BoqSheetId");
            CreateIndex("dbo.vo", "BoqSheetId");
            AddForeignKey("dbo.BoqItems", "BoqSheetId", "dbo.BoqSheets", "Id");
            AddForeignKey("dbo.vo", "BoqSheetId", "dbo.BoqSheets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.vo", "BoqSheetId", "dbo.BoqSheets");
            DropForeignKey("dbo.BoqItems", "BoqSheetId", "dbo.BoqSheets");
            DropIndex("dbo.vo", new[] { "BoqSheetId" });
            DropIndex("dbo.BoqItems", new[] { "BoqSheetId" });
            AlterColumn("dbo.vo", "BoqSheetId", c => c.Int(nullable: false));
            AlterColumn("dbo.BoqItems", "BoqSheetId", c => c.Int(nullable: false));
            CreateIndex("dbo.vo", "BoqSheetId");
            CreateIndex("dbo.BoqItems", "BoqSheetId");
            AddForeignKey("dbo.vo", "BoqSheetId", "dbo.BoqSheets", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BoqItems", "BoqSheetId", "dbo.BoqSheets", "Id", cascadeDelete: true);
        }
    }
}
