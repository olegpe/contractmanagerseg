namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LaborDatabases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LaborType = c.String(),
                        UnitPrice = c.Double(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MachineDatabases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MachineType = c.String(),
                        MachineAcronym = c.String(),
                        Unit = c.String(),
                        UnitPrice = c.Double(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MachineDatabases");
            DropTable("dbo.LaborDatabases");
        }
    }
}
