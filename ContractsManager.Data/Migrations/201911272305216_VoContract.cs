namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VoContract : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VOContracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        TemplateNumber = c.String(maxLength: 255),
                        Content = c.Binary(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VOContracts");
        }
    }
}
