namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationGetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "Body", c => c.String(storeType: "ntext"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "Body");
        }
    }
}
