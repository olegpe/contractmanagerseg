namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentsTerms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "PaymentsTerm", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "PaymentsTerm");
        }
    }
}
