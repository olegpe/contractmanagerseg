namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class machineBinding : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Machines", "MachineType_Id", c => c.Int());
            AlterColumn("dbo.MachineDatabases", "UnitPrice", c => c.Single(nullable: false));
            CreateIndex("dbo.Machines", "MachineType_Id");
            AddForeignKey("dbo.Machines", "MachineType_Id", "dbo.MachineDatabases", "Id");
            DropColumn("dbo.Machines", "MachineType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Machines", "MachineType", c => c.String());
            DropForeignKey("dbo.Machines", "MachineType_Id", "dbo.MachineDatabases");
            DropIndex("dbo.Machines", new[] { "MachineType_Id" });
            AlterColumn("dbo.MachineDatabases", "UnitPrice", c => c.Double(nullable: false));
            DropColumn("dbo.Machines", "MachineType_Id");
        }
    }
}
