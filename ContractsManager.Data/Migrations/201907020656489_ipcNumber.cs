namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipcNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Number", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "Number");
        }
    }
}
