namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IpcFileContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IpcFiles", "Content", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.IpcFiles", "Content");
        }
    }
}
