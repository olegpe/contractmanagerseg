namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipcPaid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Paid", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "Paid");
        }
    }
}
