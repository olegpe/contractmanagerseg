namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class currencyName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Currencies", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Currencies", "Name");
        }
    }
}
