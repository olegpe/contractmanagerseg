namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Attachments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "PlansExecution", c => c.String());
            AddColumn("dbo.ContractsDatasets", "DocumentsJuridiques_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "PlansHSE_Id", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "DocumentsJuridiques_Id");
            CreateIndex("dbo.ContractsDatasets", "PlansHSE_Id");
            AddForeignKey("dbo.ContractsDatasets", "DocumentsJuridiques_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "PlansHSE_Id", "dbo.ContractFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasets", "PlansHSE_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "DocumentsJuridiques_Id", "dbo.ContractFiles");
            DropIndex("dbo.ContractsDatasets", new[] { "PlansHSE_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "DocumentsJuridiques_Id" });
            DropColumn("dbo.ContractsDatasets", "PlansHSE_Id");
            DropColumn("dbo.ContractsDatasets", "DocumentsJuridiques_Id");
            DropColumn("dbo.ContractsDatasets", "PlansExecution");
        }
    }
}
