namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VenteLocalIsTransferred : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VenteLocals", "IsTransferred", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VenteLocals", "IsTransferred");
        }
    }
}
