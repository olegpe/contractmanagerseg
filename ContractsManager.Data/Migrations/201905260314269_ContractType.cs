namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contracts", "SecondType", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contracts", "SecondType");
        }
    }
}
