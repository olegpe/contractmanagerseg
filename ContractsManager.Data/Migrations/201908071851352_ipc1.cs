namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipc1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "RetentionPercentage", c => c.Single(nullable: false));
            AddColumn("dbo.Ipcs", "AdvancePaymentPercentage", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "AdvancePaymentPercentage");
            DropColumn("dbo.Ipcs", "RetentionPercentage");
        }
    }
}
