namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractVoLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "VoLevel", c => c.Int(nullable: false));
            AddColumn("dbo.ContractVoes", "Level", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractVoes", "Level");
            DropColumn("dbo.ContractsDatasets", "VoLevel");
        }
    }
}
