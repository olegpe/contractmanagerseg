namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableForeigenKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BoqSheets", "BoqId", "dbo.Boqs");
            DropIndex("dbo.BoqSheets", new[] { "BoqId" });
            AddColumn("dbo.Boqs", "BuildingId", c => c.Int());
            AddColumn("dbo.Buildings", "BoqId", c => c.Int());
            AlterColumn("dbo.BoqSheets", "BoqId", c => c.Int());
            CreateIndex("dbo.BoqSheets", "BoqId");
            AddForeignKey("dbo.BoqSheets", "BoqId", "dbo.Boqs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BoqSheets", "BoqId", "dbo.Boqs");
            DropIndex("dbo.BoqSheets", new[] { "BoqId" });
            AlterColumn("dbo.BoqSheets", "BoqId", c => c.Int(nullable: false));
            DropColumn("dbo.Buildings", "BoqId");
            DropColumn("dbo.Boqs", "BuildingId");
            CreateIndex("dbo.BoqSheets", "BoqId");
            AddForeignKey("dbo.BoqSheets", "BoqId", "dbo.Boqs", "Id", cascadeDelete: true);
        }
    }
}
