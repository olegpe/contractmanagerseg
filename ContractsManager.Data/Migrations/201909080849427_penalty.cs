namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class penalty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Penalty", c => c.Single(nullable: false));
            AddColumn("dbo.Ipcs", "PreviousPenalty", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "PreviousPenalty");
            DropColumn("dbo.Ipcs", "Penalty");
        }
    }
}
