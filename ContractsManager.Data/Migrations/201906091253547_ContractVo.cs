namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractVo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContractVoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractsDatasetId = c.Int(nullable: false),
                        VoId = c.Int(nullable: false),
                        CumulQte = c.Single(nullable: false),
                        ActualQte = c.Single(nullable: false),
                        PrecedQte = c.Single(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDatasetId, cascadeDelete: true)
                .ForeignKey("dbo.vo", t => t.VoId, cascadeDelete: true)
                .Index(t => t.ContractsDatasetId)
                .Index(t => t.VoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractVoes", "VoId", "dbo.vo");
            DropForeignKey("dbo.ContractVoes", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropIndex("dbo.ContractVoes", new[] { "VoId" });
            DropIndex("dbo.ContractVoes", new[] { "ContractsDatasetId" });
            DropTable("dbo.ContractVoes");
        }
    }
}
