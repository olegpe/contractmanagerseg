namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cumulRetention : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "RetentionAmountCumul", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "RetentionAmountCumul");
        }
    }
}
