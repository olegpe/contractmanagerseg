namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contractSheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "BoqSheet_Id", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "BoqSheet_Id");
            AddForeignKey("dbo.ContractsDatasets", "BoqSheet_Id", "dbo.BoqSheets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasets", "BoqSheet_Id", "dbo.BoqSheets");
            DropIndex("dbo.ContractsDatasets", new[] { "BoqSheet_Id" });
            DropColumn("dbo.ContractsDatasets", "BoqSheet_Id");
        }
    }
}
