namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cumulAdvancePayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "AdvancePaymentAmountCumul", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "AdvancePaymentAmountCumul");
        }
    }
}
