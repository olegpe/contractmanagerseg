namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipcRetentionAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "RetentionAmount", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "RetentionAmount");
        }
    }
}
