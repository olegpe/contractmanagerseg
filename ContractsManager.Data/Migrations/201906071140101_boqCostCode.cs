namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boqCostCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqItems", "CostCodeId", c => c.Int());
            AddColumn("dbo.vo", "CostCodeId", c => c.Int());
            CreateIndex("dbo.BoqItems", "CostCodeId");
            CreateIndex("dbo.vo", "CostCodeId");
            AddForeignKey("dbo.vo", "CostCodeId", "dbo.CostCodeLibraries", "Id");
            AddForeignKey("dbo.BoqItems", "CostCodeId", "dbo.CostCodeLibraries", "Id");
            DropColumn("dbo.BoqItems", "CostCode");
            DropColumn("dbo.vo", "CostCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.vo", "CostCode", c => c.String(maxLength: 255));
            AddColumn("dbo.BoqItems", "CostCode", c => c.String(maxLength: 255));
            DropForeignKey("dbo.BoqItems", "CostCodeId", "dbo.CostCodeLibraries");
            DropForeignKey("dbo.vo", "CostCodeId", "dbo.CostCodeLibraries");
            DropIndex("dbo.vo", new[] { "CostCodeId" });
            DropIndex("dbo.BoqItems", new[] { "CostCodeId" });
            DropColumn("dbo.vo", "CostCodeId");
            DropColumn("dbo.BoqItems", "CostCodeId");
        }
    }
}
