namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContracBoqNamesChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractBoqItems", "CumulQtePer", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "PrecedQtePer", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "ActualQtePer", c => c.Double(nullable: false));
            DropColumn("dbo.ContractBoqItems", "CumulQtePre");
            DropColumn("dbo.ContractBoqItems", "PrecedQtePre");
            DropColumn("dbo.ContractBoqItems", "ActualQtePre");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ContractBoqItems", "ActualQtePre", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "PrecedQtePre", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "CumulQtePre", c => c.Double(nullable: false));
            DropColumn("dbo.ContractBoqItems", "ActualQtePer");
            DropColumn("dbo.ContractBoqItems", "PrecedQtePer");
            DropColumn("dbo.ContractBoqItems", "CumulQtePer");
        }
    }
}
