namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VoDataSet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VoDatasets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Date = c.DateTime(),
                        VoNumber = c.String(),
                        ContractDatasetStatus = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Building_Id = c.Int(),
                        Contract_Id = c.Int(),
                        ContractsDataset_Id = c.Int(),
                        Project_Id = c.Int(),
                        Subcontractor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Buildings", t => t.Building_Id)
                .ForeignKey("dbo.VOContracts", t => t.Contract_Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDataset_Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .ForeignKey("dbo.Subcontractors", t => t.Subcontractor_Id)
                .Index(t => t.Building_Id)
                .Index(t => t.Contract_Id)
                .Index(t => t.ContractsDataset_Id)
                .Index(t => t.Project_Id)
                .Index(t => t.Subcontractor_Id);
            
            AddColumn("dbo.ContractVoes", "VoDataset_Id", c => c.Int());
            CreateIndex("dbo.ContractVoes", "VoDataset_Id");
            AddForeignKey("dbo.ContractVoes", "VoDataset_Id", "dbo.VoDatasets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VoDatasets", "Subcontractor_Id", "dbo.Subcontractors");
            DropForeignKey("dbo.VoDatasets", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.ContractVoes", "VoDataset_Id", "dbo.VoDatasets");
            DropForeignKey("dbo.VoDatasets", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropForeignKey("dbo.VoDatasets", "Contract_Id", "dbo.VOContracts");
            DropForeignKey("dbo.VoDatasets", "Building_Id", "dbo.Buildings");
            DropIndex("dbo.VoDatasets", new[] { "Subcontractor_Id" });
            DropIndex("dbo.VoDatasets", new[] { "Project_Id" });
            DropIndex("dbo.VoDatasets", new[] { "ContractsDataset_Id" });
            DropIndex("dbo.VoDatasets", new[] { "Contract_Id" });
            DropIndex("dbo.VoDatasets", new[] { "Building_Id" });
            DropIndex("dbo.ContractVoes", new[] { "VoDataset_Id" });
            DropColumn("dbo.ContractVoes", "VoDataset_Id");
            DropTable("dbo.VoDatasets");
        }
    }
}
