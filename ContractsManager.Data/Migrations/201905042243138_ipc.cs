namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Labors", "ConsumedAmount", c => c.Double(nullable: false));
            AddColumn("dbo.Labors", "ActualAmount", c => c.Double(nullable: false));
            AddColumn("dbo.Labors", "Deduction", c => c.Double(nullable: false));
            AddColumn("dbo.Machines", "ConsumedAmount", c => c.Double(nullable: false));
            AddColumn("dbo.Machines", "ActualAmount", c => c.Double(nullable: false));
            AddColumn("dbo.Machines", "Deduction", c => c.Double(nullable: false));
            AddColumn("dbo.VenteLocals", "StockQte", c => c.Single(nullable: false));
            AddColumn("dbo.VenteLocals", "ActualAmount", c => c.Single(nullable: false));
            AddColumn("dbo.VenteLocals", "Deduction", c => c.Single(nullable: false));
            AddColumn("dbo.VenteLocals", "ConsumedAmount", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VenteLocals", "ConsumedAmount");
            DropColumn("dbo.VenteLocals", "Deduction");
            DropColumn("dbo.VenteLocals", "ActualAmount");
            DropColumn("dbo.VenteLocals", "StockQte");
            DropColumn("dbo.Machines", "Deduction");
            DropColumn("dbo.Machines", "ActualAmount");
            DropColumn("dbo.Machines", "ConsumedAmount");
            DropColumn("dbo.Labors", "Deduction");
            DropColumn("dbo.Labors", "ActualAmount");
            DropColumn("dbo.Labors", "ConsumedAmount");
        }
    }
}
