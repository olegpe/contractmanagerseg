namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoqItemManytoManyrelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContractsDatasetBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropForeignKey("dbo.ContractsDatasetBoqItems", "BoqItem_Id", "dbo.BoqItems");
            DropIndex("dbo.ContractsDatasetBoqItems", new[] { "ContractsDataset_Id" });
            DropIndex("dbo.ContractsDatasetBoqItems", new[] { "BoqItem_Id" });
            CreateTable(
                "dbo.ContractBoqItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractId = c.Int(nullable: false),
                        BoqITemId = c.Int(nullable: false),
                        CumulQte = c.Single(nullable: false),
                        ActualQte = c.Single(nullable: false),
                        PrecedQte = c.Single(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ContractsDataset_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BoqItems", t => t.BoqITemId, cascadeDelete: true)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDataset_Id)
                .Index(t => t.BoqITemId)
                .Index(t => t.ContractsDataset_Id);
            
            CreateTable(
                "dbo.Ipcs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractsDatasetId = c.Int(),
                        IpcStatus = c.Int(nullable: false),
                        TotalAmount = c.Single(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDatasetId)
                .Index(t => t.ContractsDatasetId);
            
            DropTable("dbo.ContractsDatasetBoqItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ContractsDatasetBoqItems",
                c => new
                    {
                        ContractsDataset_Id = c.Int(nullable: false),
                        BoqItem_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContractsDataset_Id, t.BoqItem_Id });
            
            DropForeignKey("dbo.Ipcs", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropForeignKey("dbo.ContractBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropForeignKey("dbo.ContractBoqItems", "BoqITemId", "dbo.BoqItems");
            DropIndex("dbo.Ipcs", new[] { "ContractsDatasetId" });
            DropIndex("dbo.ContractBoqItems", new[] { "ContractsDataset_Id" });
            DropIndex("dbo.ContractBoqItems", new[] { "BoqITemId" });
            DropTable("dbo.Ipcs");
            DropTable("dbo.ContractBoqItems");
            CreateIndex("dbo.ContractsDatasetBoqItems", "BoqItem_Id");
            CreateIndex("dbo.ContractsDatasetBoqItems", "ContractsDataset_Id");
            AddForeignKey("dbo.ContractsDatasetBoqItems", "BoqItem_Id", "dbo.BoqItems", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ContractsDatasetBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets", "Id", cascadeDelete: true);
        }
    }
}
