namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class voPer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractVoes", "CumulQtePer", c => c.Double(nullable: false));
            AddColumn("dbo.ContractVoes", "PrecedQtePer", c => c.Double(nullable: false));
            AddColumn("dbo.ContractVoes", "ActualQtePer", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractVoes", "ActualQtePer");
            DropColumn("dbo.ContractVoes", "PrecedQtePer");
            DropColumn("dbo.ContractVoes", "CumulQtePer");
        }
    }
}
