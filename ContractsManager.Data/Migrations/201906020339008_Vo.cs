namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.vo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        No = c.String(maxLength: 255),
                        Key = c.String(maxLength: 255),
                        Unite = c.String(maxLength: 255),
                        CostCode = c.String(maxLength: 255),
                        Remark = c.String(maxLength: 255),
                        Qte = c.Single(nullable: false),
                        Pu = c.Single(nullable: false),
                        Pt = c.Single(nullable: false),
                        OrderVo = c.Int(nullable: false),
                        BOQType = c.Int(nullable: false),
                        BoqSheetId = c.Int(nullable: false),
                        ParentId = c.Int(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BoqSheets", t => t.BoqSheetId, cascadeDelete: true)
                .ForeignKey("dbo.vo", t => t.ParentId)
                .Index(t => t.BoqSheetId)
                .Index(t => t.ParentId);
            
            AddColumn("dbo.BoqSheets", "HasVo", c => c.Boolean(nullable: false));
            AddColumn("dbo.ContractBoqItems", "Vo_Id", c => c.Int());
            CreateIndex("dbo.ContractBoqItems", "Vo_Id");
            AddForeignKey("dbo.ContractBoqItems", "Vo_Id", "dbo.vo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractBoqItems", "Vo_Id", "dbo.vo");
            DropForeignKey("dbo.vo", "ParentId", "dbo.vo");
            DropForeignKey("dbo.vo", "BoqSheetId", "dbo.BoqSheets");
            DropIndex("dbo.ContractBoqItems", new[] { "Vo_Id" });
            DropIndex("dbo.vo", new[] { "ParentId" });
            DropIndex("dbo.vo", new[] { "BoqSheetId" });
            DropColumn("dbo.ContractBoqItems", "Vo_Id");
            DropColumn("dbo.BoqSheets", "HasVo");
            DropTable("dbo.vo");
        }
    }
}
