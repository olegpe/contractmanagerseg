namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IpcDateFromTo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "FromDate", c => c.DateTime());
            AddColumn("dbo.Ipcs", "ToDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "ToDate");
            DropColumn("dbo.Ipcs", "FromDate");
        }
    }
}
