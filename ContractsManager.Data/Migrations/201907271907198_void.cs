namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _void : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContractVoes", "VoId", "dbo.vo");
            DropIndex("dbo.ContractVoes", new[] { "VoId" });
            RenameColumn(table: "dbo.ContractVoes", name: "VoId", newName: "Vo_Id");
            AlterColumn("dbo.ContractVoes", "Vo_Id", c => c.Int());
            CreateIndex("dbo.ContractVoes", "Vo_Id");
            AddForeignKey("dbo.ContractVoes", "Vo_Id", "dbo.vo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractVoes", "Vo_Id", "dbo.vo");
            DropIndex("dbo.ContractVoes", new[] { "Vo_Id" });
            AlterColumn("dbo.ContractVoes", "Vo_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.ContractVoes", name: "Vo_Id", newName: "VoId");
            CreateIndex("dbo.ContractVoes", "VoId");
            AddForeignKey("dbo.ContractVoes", "VoId", "dbo.vo", "Id", cascadeDelete: true);
        }
    }
}
