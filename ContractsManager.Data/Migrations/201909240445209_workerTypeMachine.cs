namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class workerTypeMachine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Machines", "MachineCode_Id", c => c.Int());
            CreateIndex("dbo.Machines", "MachineCode_Id");
            AddForeignKey("dbo.Machines", "MachineCode_Id", "dbo.MachineDatabases", "Id");
            DropColumn("dbo.Machines", "MachineCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Machines", "MachineCode", c => c.String());
            DropForeignKey("dbo.Machines", "MachineCode_Id", "dbo.MachineDatabases");
            DropIndex("dbo.Machines", new[] { "MachineCode_Id" });
            DropColumn("dbo.Machines", "MachineCode_Id");
        }
    }
}
