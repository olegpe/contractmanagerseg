namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class frSheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sheets", "NameFr", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sheets", "NameFr");
        }
    }
}
