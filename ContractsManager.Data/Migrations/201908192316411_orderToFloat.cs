namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderToFloat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BoqItems", "OrderBoq", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "OrderBoq", c => c.Single(nullable: false));
            AlterColumn("dbo.ContractVoes", "OrderVo", c => c.Single(nullable: false));
            AlterColumn("dbo.vo", "OrderVo", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.vo", "OrderVo", c => c.Int(nullable: false));
            AlterColumn("dbo.ContractVoes", "OrderVo", c => c.Int(nullable: false));
            AlterColumn("dbo.ContractBoqItems", "OrderBoq", c => c.Int(nullable: false));
            AlterColumn("dbo.BoqItems", "OrderBoq", c => c.Int(nullable: false));
        }
    }
}
