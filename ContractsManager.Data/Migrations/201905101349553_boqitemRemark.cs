namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boqitemRemark : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqItems", "Remark", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BoqItems", "Remark");
        }
    }
}
