namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IpcFile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IpcFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Ipcs", "IpcFileId", c => c.Int());
            CreateIndex("dbo.Ipcs", "IpcFileId");
            AddForeignKey("dbo.Ipcs", "IpcFileId", "dbo.IpcFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ipcs", "IpcFileId", "dbo.IpcFiles");
            DropIndex("dbo.Ipcs", new[] { "IpcFileId" });
            DropColumn("dbo.Ipcs", "IpcFileId");
            DropTable("dbo.IpcFiles");
        }
    }
}
