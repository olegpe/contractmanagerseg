namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VenteLocalMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VenteLocals", "ContractDatasetId", c => c.Int());
            CreateIndex("dbo.VenteLocals", "ContractDatasetId");
            AddForeignKey("dbo.VenteLocals", "ContractDatasetId", "dbo.ContractsDatasets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VenteLocals", "ContractDatasetId", "dbo.ContractsDatasets");
            DropIndex("dbo.VenteLocals", new[] { "ContractDatasetId" });
            DropColumn("dbo.VenteLocals", "ContractDatasetId");
        }
    }
}
