namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class precedentAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Labors", "PrecedentAmount", c => c.Single(nullable: false));
            AddColumn("dbo.Machines", "PrecedentAmount", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Machines", "PrecedentAmount");
            DropColumn("dbo.Labors", "PrecedentAmount");
        }
    }
}
