namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotifContAndipc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "IpcId", c => c.Int());
            AddColumn("dbo.Notifications", "ContractId", c => c.Int());
            CreateIndex("dbo.Notifications", "IpcId");
            CreateIndex("dbo.Notifications", "ContractId");
            AddForeignKey("dbo.Notifications", "ContractId", "dbo.ContractsDatasets", "Id");
            AddForeignKey("dbo.Notifications", "IpcId", "dbo.Ipcs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "IpcId", "dbo.Ipcs");
            DropForeignKey("dbo.Notifications", "ContractId", "dbo.ContractsDatasets");
            DropIndex("dbo.Notifications", new[] { "ContractId" });
            DropIndex("dbo.Notifications", new[] { "IpcId" });
            DropColumn("dbo.Notifications", "ContractId");
            DropColumn("dbo.Notifications", "IpcId");
        }
    }
}
