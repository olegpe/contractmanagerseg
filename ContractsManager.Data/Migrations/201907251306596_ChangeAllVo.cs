namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAllVo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractVoes", "No", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractVoes", "IsContractVo", c => c.Boolean(nullable: false));
            AddColumn("dbo.ContractVoes", "Key", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractVoes", "Unite", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractVoes", "CostCodeId", c => c.Int());
            AddColumn("dbo.ContractVoes", "Remark", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractVoes", "Qte", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "Pu", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "Pt", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "OrderVo", c => c.Int(nullable: false));
            AddColumn("dbo.ContractVoes", "BOQType", c => c.Int(nullable: false));
            AddColumn("dbo.ContractVoes", "BoqSheetId", c => c.Int());
            AddColumn("dbo.ContractVoes", "ParentId", c => c.Int());
            CreateIndex("dbo.ContractVoes", "CostCodeId");
            CreateIndex("dbo.ContractVoes", "BoqSheetId");
            CreateIndex("dbo.ContractVoes", "ParentId");
            AddForeignKey("dbo.ContractVoes", "BoqSheetId", "dbo.BoqSheets", "Id");
            AddForeignKey("dbo.ContractVoes", "ParentId", "dbo.ContractVoes", "Id");
            AddForeignKey("dbo.ContractVoes", "CostCodeId", "dbo.CostCodeLibraries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractVoes", "CostCodeId", "dbo.CostCodeLibraries");
            DropForeignKey("dbo.ContractVoes", "ParentId", "dbo.ContractVoes");
            DropForeignKey("dbo.ContractVoes", "BoqSheetId", "dbo.BoqSheets");
            DropIndex("dbo.ContractVoes", new[] { "ParentId" });
            DropIndex("dbo.ContractVoes", new[] { "BoqSheetId" });
            DropIndex("dbo.ContractVoes", new[] { "CostCodeId" });
            DropColumn("dbo.ContractVoes", "ParentId");
            DropColumn("dbo.ContractVoes", "BoqSheetId");
            DropColumn("dbo.ContractVoes", "BOQType");
            DropColumn("dbo.ContractVoes", "OrderVo");
            DropColumn("dbo.ContractVoes", "Pt");
            DropColumn("dbo.ContractVoes", "Pu");
            DropColumn("dbo.ContractVoes", "Qte");
            DropColumn("dbo.ContractVoes", "Remark");
            DropColumn("dbo.ContractVoes", "CostCodeId");
            DropColumn("dbo.ContractVoes", "Unite");
            DropColumn("dbo.ContractVoes", "Key");
            DropColumn("dbo.ContractVoes", "IsContractVo");
            DropColumn("dbo.ContractVoes", "No");
        }
    }
}
