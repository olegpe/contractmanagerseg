namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class laborAndMachine : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Labors", "TypeOfWorker_Id", "dbo.LaborDatabases");
            DropForeignKey("dbo.Machines", "MachineType_Id", "dbo.MachineDatabases");
            DropIndex("dbo.Labors", new[] { "TypeOfWorker_Id" });
            DropIndex("dbo.Machines", new[] { "MachineType_Id" });
            AddColumn("dbo.Labors", "TypeOfWorker", c => c.String());
            AddColumn("dbo.Machines", "MachineType", c => c.String());
            DropColumn("dbo.Labors", "TypeOfWorker_Id");
            DropColumn("dbo.Machines", "MachineType_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Machines", "MachineType_Id", c => c.Int());
            AddColumn("dbo.Labors", "TypeOfWorker_Id", c => c.Int());
            DropColumn("dbo.Machines", "MachineType");
            DropColumn("dbo.Labors", "TypeOfWorker");
            CreateIndex("dbo.Machines", "MachineType_Id");
            CreateIndex("dbo.Labors", "TypeOfWorker_Id");
            AddForeignKey("dbo.Machines", "MachineType_Id", "dbo.MachineDatabases", "Id");
            AddForeignKey("dbo.Labors", "TypeOfWorker_Id", "dbo.LaborDatabases", "Id");
        }
    }
}
