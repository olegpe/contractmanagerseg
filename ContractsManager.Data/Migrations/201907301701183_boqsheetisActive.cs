namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boqsheetisActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqSheets", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BoqSheets", "IsActive");
        }
    }
}
