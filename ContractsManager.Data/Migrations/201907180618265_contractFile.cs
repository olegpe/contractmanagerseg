namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contractFile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContractFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.Binary(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ContractsDatasets", "ContractFileId", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "ContractFileId");
            AddForeignKey("dbo.ContractsDatasets", "ContractFileId", "dbo.ContractFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasets", "ContractFileId", "dbo.ContractFiles");
            DropIndex("dbo.ContractsDatasets", new[] { "ContractFileId" });
            DropColumn("dbo.ContractsDatasets", "ContractFileId");
            DropTable("dbo.ContractFiles");
        }
    }
}
