namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ContractsDatasets", "ContractDate", c => c.DateTime(nullable: true));
            AlterColumn("dbo.ContractsDatasets", "CompletionDate", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ContractsDatasets", "CompletionDate", c => c.String());
            AlterColumn("dbo.ContractsDatasets", "ContractDate", c => c.String());
        }
    }
}
