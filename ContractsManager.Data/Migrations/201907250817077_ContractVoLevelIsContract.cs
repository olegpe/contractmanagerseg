namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractVoLevelIsContract : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.vo", "IsContractVo", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.vo", "IsContractVo");
        }
    }
}
