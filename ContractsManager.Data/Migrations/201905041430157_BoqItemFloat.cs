namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoqItemFloat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BoqItems", "Qte", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "Pu", c => c.Single(nullable: false));
            AlterColumn("dbo.BoqItems", "Pt", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BoqItems", "Pt", c => c.String(maxLength: 255));
            AlterColumn("dbo.BoqItems", "Pu", c => c.String(maxLength: 255));
            AlterColumn("dbo.BoqItems", "Qte", c => c.String(maxLength: 255));
        }
    }
}
