namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractDatasetRemart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "Remark", c => c.String(unicode: false, storeType: "text"));
            DropColumn("dbo.BoqItems", "Remark");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BoqItems", "Remark", c => c.String(maxLength: 255));
            DropColumn("dbo.ContractsDatasets", "Remark");
        }
    }
}
