namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoqItemFloat1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContractBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropIndex("dbo.ContractBoqItems", new[] { "BoqITemId" });
            DropIndex("dbo.ContractBoqItems", new[] { "ContractsDataset_Id" });
            RenameColumn(table: "dbo.ContractBoqItems", name: "ContractsDataset_Id", newName: "ContractsDatasetId");
            AlterColumn("dbo.ContractBoqItems", "ContractsDatasetId", c => c.Int(nullable: false));
            CreateIndex("dbo.ContractBoqItems", "ContractsDatasetId");
            CreateIndex("dbo.ContractBoqItems", "BoqItemId");
            AddForeignKey("dbo.ContractBoqItems", "ContractsDatasetId", "dbo.ContractsDatasets", "Id", cascadeDelete: true);
            DropColumn("dbo.ContractBoqItems", "ContractId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ContractBoqItems", "ContractId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ContractBoqItems", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropIndex("dbo.ContractBoqItems", new[] { "BoqItemId" });
            DropIndex("dbo.ContractBoqItems", new[] { "ContractsDatasetId" });
            AlterColumn("dbo.ContractBoqItems", "ContractsDatasetId", c => c.Int());
            RenameColumn(table: "dbo.ContractBoqItems", name: "ContractsDatasetId", newName: "ContractsDataset_Id");
            CreateIndex("dbo.ContractBoqItems", "ContractsDataset_Id");
            CreateIndex("dbo.ContractBoqItems", "BoqITemId");
            AddForeignKey("dbo.ContractBoqItems", "ContractsDataset_Id", "dbo.ContractsDatasets", "Id");
        }
    }
}
