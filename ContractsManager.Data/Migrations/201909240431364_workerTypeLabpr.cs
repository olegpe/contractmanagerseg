namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class workerTypeLabpr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Labors", "TypeOfWorker_Id", c => c.Int());
            CreateIndex("dbo.Labors", "TypeOfWorker_Id");
            AddForeignKey("dbo.Labors", "TypeOfWorker_Id", "dbo.LaborDatabases", "Id");
            DropColumn("dbo.Labors", "TypeOfWorker");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Labors", "TypeOfWorker", c => c.String());
            DropForeignKey("dbo.Labors", "TypeOfWorker_Id", "dbo.LaborDatabases");
            DropIndex("dbo.Labors", new[] { "TypeOfWorker_Id" });
            DropColumn("dbo.Labors", "TypeOfWorker_Id");
        }
    }
}
