namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubcontractorTels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subcontractors", "SubcontractorTel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Subcontractors", "SubcontractorTel");
        }
    }
}
