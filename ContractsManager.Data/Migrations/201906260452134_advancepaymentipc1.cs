namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class advancepaymentipc1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "Type");
        }
    }
}
