namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prorata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Prorata", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "Prorata");
        }
    }
}
