namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TerminationFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "TerminatedFile_Id", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "TerminatedFile_Id");
            AddForeignKey("dbo.ContractsDatasets", "TerminatedFile_Id", "dbo.ContractFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasets", "TerminatedFile_Id", "dbo.ContractFiles");
            DropIndex("dbo.ContractsDatasets", new[] { "TerminatedFile_Id" });
            DropColumn("dbo.ContractsDatasets", "TerminatedFile_Id");
        }
    }
}
