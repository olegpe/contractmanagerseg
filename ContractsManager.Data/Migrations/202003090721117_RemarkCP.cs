namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemarkCP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "RemarkCP", c => c.String(unicode: false, storeType: "text"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "RemarkCP");
        }
    }
}
