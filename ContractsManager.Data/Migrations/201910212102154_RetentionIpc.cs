namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RetentionIpc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "Retention", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "Retention");
        }
    }
}
