namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class venteLocalAllocated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VenteLocals", "Allocated", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VenteLocals", "Allocated");
        }
    }
}
