namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipcContractActivated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "ContractActivated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "ContractActivated");
        }
    }
}
