namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LastCumul : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractBoqItems", "LastCumul", c => c.Single(nullable: false));
            AddColumn("dbo.ContractBoqItems", "LastActual", c => c.Single(nullable: false));
            AddColumn("dbo.ContractBoqItems", "LastPreced", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "LastCumul", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "LastActual", c => c.Single(nullable: false));
            AddColumn("dbo.ContractVoes", "LastPreced", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractVoes", "LastPreced");
            DropColumn("dbo.ContractVoes", "LastActual");
            DropColumn("dbo.ContractVoes", "LastCumul");
            DropColumn("dbo.ContractBoqItems", "LastPreced");
            DropColumn("dbo.ContractBoqItems", "LastActual");
            DropColumn("dbo.ContractBoqItems", "LastCumul");
        }
    }
}
