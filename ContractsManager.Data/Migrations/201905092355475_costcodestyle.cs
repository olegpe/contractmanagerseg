namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class costcodestyle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CostCodeLibraries", "Bold", c => c.String(maxLength: 255));
            AddColumn("dbo.CostCodeLibraries", "Color", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CostCodeLibraries", "Color");
            DropColumn("dbo.CostCodeLibraries", "Bold");
        }
    }
}
