namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vo2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContractBoqItems", "Vo_Id", "dbo.vo");
            DropIndex("dbo.ContractBoqItems", new[] { "Vo_Id" });
            DropColumn("dbo.ContractBoqItems", "Vo_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ContractBoqItems", "Vo_Id", c => c.Int());
            CreateIndex("dbo.ContractBoqItems", "Vo_Id");
            AddForeignKey("dbo.ContractBoqItems", "Vo_Id", "dbo.vo", "Id");
        }
    }
}
