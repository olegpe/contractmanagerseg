namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contractunitprice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "UnitePricePercentage", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "UnitePricePercentage");
        }
    }
}
