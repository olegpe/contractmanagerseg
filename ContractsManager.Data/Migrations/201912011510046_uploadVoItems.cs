namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uploadVoItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "VoDataset_Id", c => c.Int());
            CreateIndex("dbo.Notifications", "VoDataset_Id");
            AddForeignKey("dbo.Notifications", "VoDataset_Id", "dbo.VoDatasets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "VoDataset_Id", "dbo.VoDatasets");
            DropIndex("dbo.Notifications", new[] { "VoDataset_Id" });
            DropColumn("dbo.Notifications", "VoDataset_Id");
        }
    }
}
