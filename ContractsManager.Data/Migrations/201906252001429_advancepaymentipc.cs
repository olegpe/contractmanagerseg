namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class advancepaymentipc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "AdvancePayment", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "AdvancePayment");
        }
    }
}
