namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableForeigenKey1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Buildings", "ProjectId", "dbo.Projects");
            DropIndex("dbo.Buildings", new[] { "ProjectId" });
            AlterColumn("dbo.Buildings", "ProjectId", c => c.Int());
            CreateIndex("dbo.Buildings", "ProjectId");
            AddForeignKey("dbo.Buildings", "ProjectId", "dbo.Projects", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Buildings", "ProjectId", "dbo.Projects");
            DropIndex("dbo.Buildings", new[] { "ProjectId" });
            AlterColumn("dbo.Buildings", "ProjectId", c => c.Int(nullable: false));
            CreateIndex("dbo.Buildings", "ProjectId");
            AddForeignKey("dbo.Buildings", "ProjectId", "dbo.Projects", "Id", cascadeDelete: true);
        }
    }
}
