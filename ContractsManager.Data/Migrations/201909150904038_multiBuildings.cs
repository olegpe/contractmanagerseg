namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class multiBuildings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContractsDatasetBuildings",
                c => new
                    {
                        ContractsDataset_Id = c.Int(nullable: false),
                        Building_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContractsDataset_Id, t.Building_Id })
                .ForeignKey("dbo.ContractsDatasets", t => t.ContractsDataset_Id, cascadeDelete: true)
                .ForeignKey("dbo.Buildings", t => t.Building_Id, cascadeDelete: true)
                .Index(t => t.ContractsDataset_Id)
                .Index(t => t.Building_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasetBuildings", "Building_Id", "dbo.Buildings");
            DropForeignKey("dbo.ContractsDatasetBuildings", "ContractsDataset_Id", "dbo.ContractsDatasets");
            DropIndex("dbo.ContractsDatasetBuildings", new[] { "Building_Id" });
            DropIndex("dbo.ContractsDatasetBuildings", new[] { "ContractsDataset_Id" });
            DropTable("dbo.ContractsDatasetBuildings");
        }
    }
}
