namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moveBOQToContract : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContractBoqItems", "BoqItemId", "dbo.BoqItems");
            DropIndex("dbo.ContractBoqItems", new[] { "BoqItemId" });
            RenameColumn(table: "dbo.ContractBoqItems", name: "BoqItemId", newName: "BoqItem_Id");
            AddColumn("dbo.ContractBoqItems", "No", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractBoqItems", "Key", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractBoqItems", "Unite", c => c.String(maxLength: 255));
            AddColumn("dbo.ContractBoqItems", "CostCodeId", c => c.Int());
            AddColumn("dbo.ContractBoqItems", "Qte", c => c.Single(nullable: false));
            AddColumn("dbo.ContractBoqItems", "Pu", c => c.Single(nullable: false));
            AddColumn("dbo.ContractBoqItems", "Pt", c => c.Single(nullable: false));
            AddColumn("dbo.ContractBoqItems", "OrderBoq", c => c.Int(nullable: false));
            AddColumn("dbo.ContractBoqItems", "BOQType", c => c.Int(nullable: false));
            AddColumn("dbo.ContractBoqItems", "BoqSheetId", c => c.Int());
            AddColumn("dbo.ContractBoqItems", "ParentId", c => c.Int());
            AlterColumn("dbo.ContractBoqItems", "BoqItem_Id", c => c.Int());
            CreateIndex("dbo.ContractBoqItems", "CostCodeId");
            CreateIndex("dbo.ContractBoqItems", "BoqSheetId");
            CreateIndex("dbo.ContractBoqItems", "ParentId");
            CreateIndex("dbo.ContractBoqItems", "BoqItem_Id");
            AddForeignKey("dbo.ContractBoqItems", "BoqSheetId", "dbo.BoqSheets", "Id");
            AddForeignKey("dbo.ContractBoqItems", "ParentId", "dbo.ContractBoqItems", "Id");
            AddForeignKey("dbo.ContractBoqItems", "CostCodeId", "dbo.CostCodeLibraries", "Id");
            AddForeignKey("dbo.ContractBoqItems", "BoqItem_Id", "dbo.BoqItems", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractBoqItems", "BoqItem_Id", "dbo.BoqItems");
            DropForeignKey("dbo.ContractBoqItems", "CostCodeId", "dbo.CostCodeLibraries");
            DropForeignKey("dbo.ContractBoqItems", "ParentId", "dbo.ContractBoqItems");
            DropForeignKey("dbo.ContractBoqItems", "BoqSheetId", "dbo.BoqSheets");
            DropIndex("dbo.ContractBoqItems", new[] { "BoqItem_Id" });
            DropIndex("dbo.ContractBoqItems", new[] { "ParentId" });
            DropIndex("dbo.ContractBoqItems", new[] { "BoqSheetId" });
            DropIndex("dbo.ContractBoqItems", new[] { "CostCodeId" });
            AlterColumn("dbo.ContractBoqItems", "BoqItem_Id", c => c.Int(nullable: false));
            DropColumn("dbo.ContractBoqItems", "ParentId");
            DropColumn("dbo.ContractBoqItems", "BoqSheetId");
            DropColumn("dbo.ContractBoqItems", "BOQType");
            DropColumn("dbo.ContractBoqItems", "OrderBoq");
            DropColumn("dbo.ContractBoqItems", "Pt");
            DropColumn("dbo.ContractBoqItems", "Pu");
            DropColumn("dbo.ContractBoqItems", "Qte");
            DropColumn("dbo.ContractBoqItems", "CostCodeId");
            DropColumn("dbo.ContractBoqItems", "Unite");
            DropColumn("dbo.ContractBoqItems", "Key");
            DropColumn("dbo.ContractBoqItems", "No");
            RenameColumn(table: "dbo.ContractBoqItems", name: "BoqItem_Id", newName: "BoqItemId");
            CreateIndex("dbo.ContractBoqItems", "BoqItemId");
            AddForeignKey("dbo.ContractBoqItems", "BoqItemId", "dbo.BoqItems", "Id", cascadeDelete: true);
        }
    }
}
