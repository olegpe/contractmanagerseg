namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqItems", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.BoqSheets", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Boqs", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Buildings", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projects", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.CostCodeLibraries", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.vo", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.ContractVoes", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.ContractsDatasets", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Contracts", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.ContractBoqItems", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Subcontractors", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Countries", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Currencies", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ipcs", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Labors", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Machines", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Materials", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Notifications", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.VenteLocals", "Created", c => c.DateTime(nullable: false));
            DropColumn("dbo.BoqItems", "TimeStamp");
            DropColumn("dbo.BoqSheets", "TimeStamp");
            DropColumn("dbo.Boqs", "TimeStamp");
            DropColumn("dbo.Buildings", "TimeStamp");
            DropColumn("dbo.Projects", "TimeStamp");
            DropColumn("dbo.CostCodeLibraries", "TimeStamp");
            DropColumn("dbo.vo", "TimeStamp");
            DropColumn("dbo.ContractVoes", "TimeStamp");
            DropColumn("dbo.ContractsDatasets", "TimeStamp");
            DropColumn("dbo.Contracts", "TimeStamp");
            DropColumn("dbo.ContractBoqItems", "TimeStamp");
            DropColumn("dbo.Subcontractors", "TimeStamp");
            DropColumn("dbo.Countries", "TimeStamp");
            DropColumn("dbo.Currencies", "TimeStamp");
            DropColumn("dbo.Ipcs", "TimeStamp");
            DropColumn("dbo.Labors", "TimeStamp");
            DropColumn("dbo.Machines", "TimeStamp");
            DropColumn("dbo.Materials", "TimeStamp");
            DropColumn("dbo.Notifications", "TimeStamp");
            DropColumn("dbo.Users", "TimeStamp");
            DropColumn("dbo.VenteLocals", "TimeStamp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VenteLocals", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Users", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Notifications", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Materials", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Machines", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Labors", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Ipcs", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Currencies", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Countries", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Subcontractors", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.ContractBoqItems", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Contracts", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.ContractsDatasets", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.ContractVoes", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.vo", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.CostCodeLibraries", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Projects", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Buildings", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Boqs", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.BoqSheets", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.BoqItems", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            DropColumn("dbo.VenteLocals", "Created");
            DropColumn("dbo.Users", "Created");
            DropColumn("dbo.Notifications", "Created");
            DropColumn("dbo.Materials", "Created");
            DropColumn("dbo.Machines", "Created");
            DropColumn("dbo.Labors", "Created");
            DropColumn("dbo.Ipcs", "Created");
            DropColumn("dbo.Currencies", "Created");
            DropColumn("dbo.Countries", "Created");
            DropColumn("dbo.Subcontractors", "Created");
            DropColumn("dbo.ContractBoqItems", "Created");
            DropColumn("dbo.Contracts", "Created");
            DropColumn("dbo.ContractsDatasets", "Created");
            DropColumn("dbo.ContractVoes", "Created");
            DropColumn("dbo.vo", "Created");
            DropColumn("dbo.CostCodeLibraries", "Created");
            DropColumn("dbo.Projects", "Created");
            DropColumn("dbo.Buildings", "Created");
            DropColumn("dbo.Boqs", "Created");
            DropColumn("dbo.BoqSheets", "Created");
            DropColumn("dbo.BoqItems", "Created");
        }
    }
}
