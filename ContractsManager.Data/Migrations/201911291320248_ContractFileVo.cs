namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractFileVo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VoDatasets", "ContractFile_Id", c => c.Int());
            CreateIndex("dbo.VoDatasets", "ContractFile_Id");
            AddForeignKey("dbo.VoDatasets", "ContractFile_Id", "dbo.ContractFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VoDatasets", "ContractFile_Id", "dbo.ContractFiles");
            DropIndex("dbo.VoDatasets", new[] { "ContractFile_Id" });
            DropColumn("dbo.VoDatasets", "ContractFile_Id");
        }
    }
}
