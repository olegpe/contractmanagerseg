namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractLumpSumIpcType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractBoqItems", "CumulQtePre", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "PrecedQtePre", c => c.Double(nullable: false));
            AddColumn("dbo.ContractBoqItems", "ActualQtePre", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractBoqItems", "ActualQtePre");
            DropColumn("dbo.ContractBoqItems", "PrecedQtePre");
            DropColumn("dbo.ContractBoqItems", "CumulQtePre");
        }
    }
}
