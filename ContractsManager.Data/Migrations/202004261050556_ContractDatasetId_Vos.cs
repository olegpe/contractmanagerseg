namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractDatasetId_Vos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractVoes", "ContractsDatasetId", c => c.Int());
            CreateIndex("dbo.ContractVoes", "ContractsDatasetId");
            AddForeignKey("dbo.ContractVoes", "ContractsDatasetId", "dbo.ContractsDatasets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractVoes", "ContractsDatasetId", "dbo.ContractsDatasets");
            DropIndex("dbo.ContractVoes", new[] { "ContractsDatasetId" });
            DropColumn("dbo.ContractVoes", "ContractsDatasetId");
        }
    }
}
