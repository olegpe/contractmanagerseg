namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractFileVoBoqAtt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VoDatasets", "BOQAtt_Id", c => c.Int());
            CreateIndex("dbo.VoDatasets", "BOQAtt_Id");
            AddForeignKey("dbo.VoDatasets", "BOQAtt_Id", "dbo.ContractFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VoDatasets", "BOQAtt_Id", "dbo.ContractFiles");
            DropIndex("dbo.VoDatasets", new[] { "BOQAtt_Id" });
            DropColumn("dbo.VoDatasets", "BOQAtt_Id");
        }
    }
}
