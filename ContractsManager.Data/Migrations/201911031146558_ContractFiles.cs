namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContractFiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "BoqAtt_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "Other_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "Planning_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "Plans_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "PrescriptionTechniques_Id", c => c.Int());
            AddColumn("dbo.ContractsDatasets", "UnitePrice_Id", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "BoqAtt_Id");
            CreateIndex("dbo.ContractsDatasets", "Other_Id");
            CreateIndex("dbo.ContractsDatasets", "Planning_Id");
            CreateIndex("dbo.ContractsDatasets", "Plans_Id");
            CreateIndex("dbo.ContractsDatasets", "PrescriptionTechniques_Id");
            CreateIndex("dbo.ContractsDatasets", "UnitePrice_Id");
            AddForeignKey("dbo.ContractsDatasets", "BoqAtt_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "Other_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "Planning_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "Plans_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "PrescriptionTechniques_Id", "dbo.ContractFiles", "Id");
            AddForeignKey("dbo.ContractsDatasets", "UnitePrice_Id", "dbo.ContractFiles", "Id");
            DropColumn("dbo.ContractsDatasets", "Planning");
            DropColumn("dbo.ContractsDatasets", "UnitePrice");
            DropColumn("dbo.ContractsDatasets", "BoqAtt");
            DropColumn("dbo.ContractsDatasets", "PrescriptionTechniques");
            DropColumn("dbo.ContractsDatasets", "Plans");
            DropColumn("dbo.ContractsDatasets", "Other");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ContractsDatasets", "Other", c => c.Binary());
            AddColumn("dbo.ContractsDatasets", "Plans", c => c.Binary());
            AddColumn("dbo.ContractsDatasets", "PrescriptionTechniques", c => c.Binary());
            AddColumn("dbo.ContractsDatasets", "BoqAtt", c => c.Binary());
            AddColumn("dbo.ContractsDatasets", "UnitePrice", c => c.Binary());
            AddColumn("dbo.ContractsDatasets", "Planning", c => c.Binary());
            DropForeignKey("dbo.ContractsDatasets", "UnitePrice_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "PrescriptionTechniques_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "Plans_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "Planning_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "Other_Id", "dbo.ContractFiles");
            DropForeignKey("dbo.ContractsDatasets", "BoqAtt_Id", "dbo.ContractFiles");
            DropIndex("dbo.ContractsDatasets", new[] { "UnitePrice_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "PrescriptionTechniques_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "Plans_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "Planning_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "Other_Id" });
            DropIndex("dbo.ContractsDatasets", new[] { "BoqAtt_Id" });
            DropColumn("dbo.ContractsDatasets", "UnitePrice_Id");
            DropColumn("dbo.ContractsDatasets", "PrescriptionTechniques_Id");
            DropColumn("dbo.ContractsDatasets", "Plans_Id");
            DropColumn("dbo.ContractsDatasets", "Planning_Id");
            DropColumn("dbo.ContractsDatasets", "Other_Id");
            DropColumn("dbo.ContractsDatasets", "BoqAtt_Id");
        }
    }
}
