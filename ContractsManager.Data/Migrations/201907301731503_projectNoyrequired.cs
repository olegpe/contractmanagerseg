namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class projectNoyrequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Projects", "Name", c => c.String(maxLength: 255));
            AlterColumn("dbo.Projects", "Acronym", c => c.String(maxLength: 255));
            AlterColumn("dbo.Projects", "City", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "City", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Projects", "Acronym", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Projects", "Name", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
