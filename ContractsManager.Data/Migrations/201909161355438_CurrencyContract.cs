namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CurrencyContract : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "CurrencyId", c => c.Int());
            CreateIndex("dbo.ContractsDatasets", "CurrencyId");
            AddForeignKey("dbo.ContractsDatasets", "CurrencyId", "dbo.Currencies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContractsDatasets", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.ContractsDatasets", new[] { "CurrencyId" });
            DropColumn("dbo.ContractsDatasets", "CurrencyId");
        }
    }
}
