namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoqSheetCostCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqSheets", "CostCodeId", c => c.Int());
            CreateIndex("dbo.BoqSheets", "CostCodeId");
            AddForeignKey("dbo.BoqSheets", "CostCodeId", "dbo.CostCodeLibraries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BoqSheets", "CostCodeId", "dbo.CostCodeLibraries");
            DropIndex("dbo.BoqSheets", new[] { "CostCodeId" });
            DropColumn("dbo.BoqSheets", "CostCodeId");
        }
    }
}
