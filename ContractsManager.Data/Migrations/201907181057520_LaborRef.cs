namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LaborRef : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Labors", "Ref", c => c.String());
            AddColumn("dbo.Machines", "Ref", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Machines", "Ref");
            DropColumn("dbo.Labors", "Ref");
        }
    }
}
