namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aprecovery : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ipcs", "ApRecovery", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ipcs", "ApRecovery");
        }
    }
}
