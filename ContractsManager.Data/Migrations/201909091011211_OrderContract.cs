namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderContract : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContractsDatasets", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContractsDatasets", "Order");
        }
    }
}
