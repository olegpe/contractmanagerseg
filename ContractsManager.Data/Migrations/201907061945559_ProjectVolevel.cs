namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectVolevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "VoLevel", c => c.Int(nullable: false, defaultValue:1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "VoLevel");
        }
    }
}
