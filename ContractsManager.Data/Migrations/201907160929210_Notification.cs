namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        CreatorId = c.Int(),
                        GetterId = c.Int(),
                        Read = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatorId)
                .ForeignKey("dbo.Users", t => t.GetterId)
                .Index(t => t.CreatorId)
                .Index(t => t.GetterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "GetterId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "CreatorId", "dbo.Users");
            DropIndex("dbo.Notifications", new[] { "GetterId" });
            DropIndex("dbo.Notifications", new[] { "CreatorId" });
            DropTable("dbo.Notifications");
        }
    }
}
