namespace ContractsManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boqOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoqItems", "OrderBoq", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BoqItems", "OrderBoq");
        }
    }
}
