﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class CustomIdentity : IIdentity
    {

        public CustomIdentity(int id, string name, string lastname, string email, UserType userType)
        {
            Id = id;
            Name = name;
            LastName = lastname;
            Email = email;
            UserType = userType;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public string LastName { get; private set; }
        public string Username { get; private set; }
        public string Email { get; private set; }
        public UserType UserType { get; private set; }

        #region IIdentity Members
        public string AuthenticationType { get { return "Custom authentication"; } }

        public bool IsAuthenticated { get { return !string.IsNullOrEmpty(Name); } }
        #endregion
    }
}
