﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using WindowsPF.classes;

namespace ContractsManager.Data.utils
{
    [Serializable]
    public static class LoginUser
    {
        public static bool Exist
        {
            get
            {
                if (File.Exists(@"Session.bin"))
                    return true;

                else return false;
            }
        }

        public static void SaveSession(User user)
        {
            if (File.Exists(@"Session.bin"))
                File.Delete(@"Session.bin");
            try
            {
                using (FileStream fs = new FileStream(@"Session.bin", FileMode.Create))
                {
                    Serializer.Serialize(fs, user);
                }
            }
            catch (IOException ex)
            {
                string errormessage = ExceptionHandling.CreateErrorMessage(ex);
                ExceptionHandling.LogFileWrite(errormessage);
                //var metroWindow = (Application.Current.MainWindow as MetroWindow);
                //await metroWindow.ShowMessageAsync("خطأ أثناء فتح ملف", "يرجى اعادة تسجيل الدخول" + ex.Message, MessageDialogStyle.Affirmative);
                // mainForm _f = new mainForm();
                // System.Windows.Forms.MessageBox.Show("Couldn't open file. Error: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                string errormessage = ExceptionHandling.CreateErrorMessage(ex);
                ExceptionHandling.LogFileWrite(errormessage);
                // System.Windows.Forms.MessageBox.Show("Couldn't open file. Error: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                //var metroWindow = (Application.Current.MainWindow as MetroWindow);
                //await metroWindow.ShowMessageAsync("خطأ أثناء فتح ملف", "يرجى اعادة تسجيل الدخول" + ex.Message, MessageDialogStyle.Affirmative);
            }
        }

        public static User LoadSession()
        {
            User user = null;
            if (!Exist) return null;
            try
            {
                using (FileStream fs = new FileStream(@"Session.bin", FileMode.Open))
                {
                    user = Serializer.Deserialize<User>(fs);
                }
            }
            catch (IOException ex)
            {
                string errormessage = ExceptionHandling.CreateErrorMessage(ex);
                ExceptionHandling.LogFileWrite(errormessage);
                //MessageBox.Show("Couldn't open file. Error: " + ex.Message, "Error");
            }
            return user;
        }

        public static void DestroySession()
        {
            try
            {
                File.Delete(@"Session.bin");
            }
            catch (IOException iox)
            {
                string errormessage = ExceptionHandling.CreateErrorMessage(iox);
                ExceptionHandling.LogFileWrite(errormessage);
               // MessageBox.Show("Couldn't open file. Error: " + iox.Message, "Error");
            }
        }
    }
}
