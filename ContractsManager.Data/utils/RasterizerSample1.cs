﻿using Ghostscript.NET;
using Ghostscript.NET.Processor;
using Ghostscript.NET.Rasterizer;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data.utils
{
  
    public static class RasterizerSample1 
    {


        private static string Sample1(string inputFile)
        {
            int desired_x_dpi = 96;
            int desired_y_dpi = 96;


            GhostscriptPngDevice dev = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png16m);
            dev.GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.ResolutionXY = new GhostscriptImageDeviceResolution(desired_x_dpi, desired_y_dpi);
            dev.InputFiles.Add(inputFile);
            dev.Pdf.FirstPage = 1;
            dev.Pdf.LastPage = 50;
            dev.CustomSwitches.Add("-dDOINTERPOLATE"); // custom parameter
            //var path = Path.GetTempFileName();
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);
            //return tempDirectory;
            dev.OutputPath = tempDirectory + "\\test_%03d.png";
            dev.Process();
            return tempDirectory;
        }
        
        public static void Create(Document document, byte[] file, string bookMark)
        {
            //The Bookmark Dosen't exist Exception
            Range r = document.Bookmarks.get_Item(bookMark).Range;

            string planning = Path.GetTempFileName() + ".doc";

            using (FileStream fs = new FileStream(planning, FileMode.Create))
            {
                fs.Write(file, 0, file.Length);
                fs.Close();
            }

            string dir = Sample1(planning);
            foreach (string imageFileName in Directory.GetFiles(dir).OrderByDescending(f => f).ToList())
            {
                using (Bitmap bmp = new Bitmap(imageFileName))
                {
                    r.InlineShapes.AddPicture(imageFileName);
                }
            }
            Directory.Delete(dir, true);

        }
        
    }
    public class DelegateStdIOHandler : GhostscriptStdIO
    {
        readonly Func<int, string> _stdIn;
        readonly Action<string> _stdOut;
        readonly Action<string> _stdErr;

        public DelegateStdIOHandler(
            Func<int, string> stdIn = null,
            Action<string> stdOut = null,
            Action<string> stdErr = null) :
                base(stdIn != null, stdOut != null, stdErr != null)
        {
            _stdIn = stdIn;
            _stdOut = stdOut;
            _stdErr = stdErr;
        }

        public override void StdIn(out string input, int count)
        {
            input = _stdIn == null
                ? null
                : _stdIn(count);
        }

        public override void StdOut(string output)
        {
            if (_stdOut != null)
            {
                _stdOut(output);
            }
        }

        public override void StdError(string error)
        {
            if (_stdErr != null)
            {
                _stdErr(error);
            }
        }
    }
}
