﻿using CurrencyConverter.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContractsManager.Data
{
    public class DataContextDBInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        private CurrencyConverter.Converter convert = new CurrencyConverter.Converter("cc556a7f9d34f553957c");

        protected override void Seed(DataContext context)
        {
            IList<User> defaultUser = new List<User>();

            defaultUser.Add(new User()
            {
                Name = "Marc",
                LastName = "Tabchy",
                Username = "Marc",
                Password = "654321",
                UserType = UserType.GeneralManager,
                Phone = "+212 661-669272",
                Email = "mtabchy@seglb.com",
            });

            defaultUser.Add(new User()
            {
                Name = "Paul",
                LastName = "Labaky",
                Phone = "+212 660-141470",
                Email = "plabaky@seglb.com",
                Username = "Paul",
                Password = "123654",
                UserType = UserType.RegionalOperationsManager,
            });

            defaultUser.Add(new User()
            {
                Name = "Mohamad",
                LastName = "El Ejel",
                Phone = "+961 3 326 541",
                Email = "melejel@seglb.com",
                Username = "Mo",
                Password = "123987",
                UserType = UserType.Admin,
            });
            IList<Currency> DefaultCurrencies = new List<Currency>
            {
                new Currency{Currencies = "USD"},
                new Currency{Currencies = "EUR"},
                new Currency{Currencies = "MAD"},
                new Currency{Currencies = "IQD"},
                new Currency{Currencies = "XAF"},
                new Currency{Currencies = "XOF"},
            };
            
            
           UploadCostCode();
            
            IList<Sheet> DefaultTrades = new List<Sheet>
            {
                new Sheet{Name = "Prelims", NameFr = "Travaux Preliminaires", CostCode = DefaultCostCode.Where(x => x.Code == "0301").FirstOrDefault() },
                new Sheet{Name = "Earth Works", NameFr = "Terrassements", CostCode = DefaultCostCode.Where(x => x.Code == "030101").FirstOrDefault() },
                new Sheet{Name = "Structural Works - Foundations", NameFr = "Travaux en Fondation", CostCode = DefaultCostCode.Where(x => x.Code == "030102").FirstOrDefault() },
                new Sheet{Name = "Underground drainage system", NameFr = "Travaux d'Assainissement", CostCode = DefaultCostCode.Where(x => x.Code == "030103").FirstOrDefault() },
                new Sheet{Name = "Structural Works - Elevation", NameFr = "Travaux en Élévation", CostCode = DefaultCostCode.Where(x => x.Code == "030104").FirstOrDefault() },
                new Sheet{Name = "Masonry", NameFr = "Maçonnerie", CostCode = DefaultCostCode.Where(x => x.Code == "030105").FirstOrDefault() },
                new Sheet{Name = "Plaster", NameFr = "Enduit", CostCode = DefaultCostCode.Where(x => x.Code == "030106").FirstOrDefault() },
                new Sheet{Name = "Waterproofing", NameFr = "Étanchéité", CostCode = DefaultCostCode.Where(x => x.Code == "0302").FirstOrDefault() },
                new Sheet{Name = "False ceiling and False Floors", NameFr = "Plâtrerie", CostCode = DefaultCostCode.Where(x => x.Code == "0303").FirstOrDefault() },
                new Sheet{Name = "Tiling and External Cladding", NameFr = "Revêtement et Carrelage", CostCode = DefaultCostCode.Where(x => x.Code == "0304").FirstOrDefault() },
                new Sheet{Name = "Woodworks", NameFr = "Menuiserie Bois", CostCode = DefaultCostCode.Where(x => x.Code == "0305").FirstOrDefault() },
                new Sheet{Name = "Aluminium Works", NameFr = "Menuiserie Aluminium", CostCode = DefaultCostCode.Where(x => x.Code == "0306").FirstOrDefault() },
                new Sheet{Name = "Metallic Works", NameFr = "Menuiserie Métallique", CostCode = DefaultCostCode.Where(x => x.Code == "0307").FirstOrDefault() },
                new Sheet{Name = "Paint", NameFr = "Peinture", CostCode = DefaultCostCode.Where(x => x.Code == "0308").FirstOrDefault() },
                new Sheet{Name = "Fire Place", NameFr = "Cheminée", CostCode = DefaultCostCode.Where(x => x.Code == "0309").FirstOrDefault() },
                new Sheet{Name = "Kitchen", NameFr = "Cuisine", CostCode = DefaultCostCode.Where(x => x.Code == "0310").FirstOrDefault() },
                new Sheet{Name = "Electrical Works and CCTV", NameFr = "Électricité", CostCode = DefaultCostCode.Where(x => x.Code == "0311").FirstOrDefault() },
                new Sheet{Name = "Plumbing and Fire-Fighting", NameFr = "Plomberie et Incendie", CostCode = DefaultCostCode.Where(x => x.Code == "0312").FirstOrDefault() },
                new Sheet{Name = "HVAC", NameFr = "Climatisation et Ventilation", CostCode = DefaultCostCode.Where(x => x.Code == "0313").FirstOrDefault() },
                new Sheet{Name = "Elevators and Lifts", NameFr = "Ascenseurs", CostCode = DefaultCostCode.Where(x => x.Code == "0314").FirstOrDefault() },
                new Sheet{Name = "Swimming Pool", NameFr = "Piscine", CostCode = DefaultCostCode.Where(x => x.Code == "0315").FirstOrDefault() },
                new Sheet{Name = "Cleaning", NameFr = "Nettoyage", CostCode = DefaultCostCode.Where(x => x.Code == "0316").FirstOrDefault() },
                new Sheet{Name = "External Works", NameFr = "Aménagements Extérieurs", CostCode = DefaultCostCode.Where(x => x.Code == "0317").FirstOrDefault() },
                new Sheet{Name = "Engineering Studies", NameFr = "Etudes et Ingénierie", CostCode = DefaultCostCode.Where(x => x.Code == "0399").FirstOrDefault() },
            };

            var currency = convert.GetAllCurrencies();

            foreach (var item in DefaultCurrencies)
            {
                Enum.TryParse(item.Currencies, out CurrencyType myStatus);
                item.ConversionRate = (double)convert.Convert(1, CurrencyType.USD, myStatus);
                item.Name = currency.Where(x => x.Id == item.Currencies).FirstOrDefault().CurrencyName;

            }

            var path = System.AppDomain.CurrentDomain.BaseDirectory;

            var content = ReadFile(@"D:\Documents\Visual Studio 2017\cmanager\Transition\bin\Debug\defaultContract.docx");
            context.Contracts.Add(new Contract
            {
                TemplateNumber = "C001",
                Template = "Default contract",
                Type = "Remeasured",
                SecondType = "Supply Apply",
                Language = "FR",
                Content = content,
            });
            
            context.Users.AddRange(defaultUser);
            context.CostCodeLibraries.AddRange(DefaultCostCode);
            context.SaveChanges();
            context.Currencies.AddRange(DefaultCurrencies);
            context.Sheets.AddRange(DefaultTrades);
            context.SaveChanges();
           // base.Seed(context);
        }

        IList<CostCodeLibrary> DefaultCostCode = new List<CostCodeLibrary>();

     

     
        private byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }

        private void UploadCostCode()
        {
            var path = System.AppDomain.CurrentDomain.BaseDirectory;
            var name = @"D:\Documents\Visual Studio 2017\cmanager\Transition\bin\Debug\Cost Centers.xlsx";
            var excel = new Excel.Excel(name);
            var costcode = excel.GetCostCode();
            excel.Close();
            DefaultCostCode = costcode;
        }
    }
}
