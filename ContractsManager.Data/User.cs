﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("Users")]
    [ProtoContract]
    public class User : ModelBase , INotifyPropertyChanged//, IIdentity
    {
        private string _name;
        [ProtoMember(1)]
        [Required]
        [StringLength(255)]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        private string _lastname;

        [ProtoMember(2)]
        [StringLength(255)]
        public string LastName
        {
            get { return _lastname; }
            set
            {
                _lastname = value;
                NotifyPropertyChanged("LastName");
            }
        }
        [ProtoMember(3)]
        [StringLength(255)]
        public string Password { get; set; }

        [ProtoMember(4)]
        [StringLength(255)]
        public string Email { get; set; }
        [ProtoMember(5)]
        [StringLength(255)]
        public string Phone { get; set; }
        [ProtoMember(6)]
        [StringLength(255)]
        public string Username { get; set; }
        private byte[] _signature;
        public byte[] Signature
        {
            get { return _signature; }
            set
            {
                _signature = value;
                NotifyPropertyChanged("Signature");
            }
        }
        //[Required]
        // public string[] Roles { get; private set; }
        /*public User(string name,string lastname,string email, string phone, string username, string[] roles)
        {
            Name = name;
            LastName = lastname;
            Email = email;
            Phone = phone;
            Username = username;
            Roles = roles;
        }*/

        /* #region IIdentity Members
         public string AuthenticationType { get { return "Custom authentication"; } }

         public bool IsAuthenticated { get { return !string.IsNullOrEmpty(Name); } }
         #endregion
         */
        [ProtoMember(7)]

        public UserType UserType { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
    public enum UserType
    {
        GeneralManager,
        RegionalOperationsManager,
        OperationsManager,
        ContractsManager,
        QuantitySurveyor,
        Accountant,
        Admin
    }
}
