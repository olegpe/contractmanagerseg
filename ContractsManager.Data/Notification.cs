﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class Notification : ModelBase, INotifyPropertyChanged
    {
        public string Type { get; set; } // IPC, Or Contract, Response

        public int? CreatorId { get; set; }
        public int? GetterId { get; set; }

        public int? IpcId { get; set; }

        public int? ContractId { get; set; }
        [ForeignKey("CreatorId")]
        public virtual User Creator { get; set; }
        [ForeignKey("GetterId")]
        public virtual User Getter { get; set; }

        [ForeignKey("IpcId")]

        public virtual Ipc Ipc { get; set; }

        [ForeignKey("ContractId")]
        public virtual ContractsDataset Contract { get; set; }
        private bool _read;
        public bool Read
        {
            get { return _read; }
            set
            {
                _read = value;
                NotifyPropertyChanged("Read");
            }
        }
        private bool _status;

        public bool Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyPropertyChanged("Status");
            }
        }

        private string _body;
        [Column(TypeName ="ntext")]
        public string Body
        {
            get { return _body; }
            set {
                _body = value;
                NotifyPropertyChanged("Body");
            }
        }
        public int? VoDataset_Id { get; set; }
        [ForeignKey("VoDataset_Id")]

        public virtual VoDataset VoDataset { get; set; }


        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
