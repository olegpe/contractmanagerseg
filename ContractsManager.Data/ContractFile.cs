﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class ContractFile : ModelBase
    {
        public byte[] Content { get; set; }
    }
}
