﻿using System;
using System.Data.Entity;
using System.Linq;

namespace ContractsManager.Data
{
    public class DataContext : DbContext
    {
       

        public DataContext() : base()
        {
        
        }


        public DataContext(String connString) : base(connString)
        {
            Database.SetInitializer<DataContext>(new CreateDatabaseIfNotExists<DataContext>());
            Database.SetInitializer<DataContext>(new DataContextDBInitializer());
            //if (!Database.Exists())
            //{
            //    Database.Initialize(true);
            //}
            // InitializeDatabase();
        }

        public static string getConnection()
        {
            var con = Connection.GetConnectionStrings().FirstOrDefault(x => x.IsCurrent);
            if (con != null)
                return Connection.BuildConnectionString(con);
            else
                return "";
        }
        protected virtual void InitializeDatabase()
        {
           //if (!Database.Exists())
           //{
           //    Database.Initialize(true);
           //    new DataContextDBInitializer().Seed(this);
           //}

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BoqItem>().HasOptional(item => item.Parent).WithMany(item => item.Children).HasForeignKey(item => item.ParentId);
            modelBuilder.Entity<Vo>().HasOptional(item => item.Parent).WithMany(item => item.Children).HasForeignKey(item => item.ParentId);
            modelBuilder.Entity<BoqSheet>().HasOptional<CostCodeLibrary>(s => s.CostCode).WithMany(g => g.BoqSheets).HasForeignKey<int?>(s => s.CostCodeId).WillCascadeOnDelete(false);
        }

        public DbSet<Configuration> Configuration { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Boq> Boqs { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<BoqSheet> BoqSheets { get; set; }
        public DbSet<BoqItem> BoqItems { get; set; }
        public DbSet<VenteLocal> VenteLocals{ get; set; }
        public DbSet<Subcontractor> Subcontractors{ get; set; }
        public DbSet<CostCodeLibrary> CostCodeLibraries{ get; set; }
        public DbSet<ContractsDataset> ContractsDatasets { get; set; }
        public DbSet<Labor> Labors { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Ipc> Ipcs { get; set; }
        public DbSet<ContractBoqItem> ContractBoqItem { get; set; }
        public DbSet<ContractVo> ContractVo { get; set; }
        public DbSet<Vo> Vo { get; set; } 
        public DbSet<Currency> Currencies { get; set; } 
        public DbSet<Notification> Notifications { get; set; } 
        public DbSet<IpcFile> IpcFiles { get; set; } 
        public DbSet<ContractFile> ContractFiles { get; set; } 
        public DbSet<Sheet> Sheets { get; set; } 
        public DbSet<VoDataset> VoDatasets { get; set; } 
        public DbSet<VOContract> VOContract { get; set; } 
        public DbSet<LaborDatabase> LaborDatabases { get; set; } 
        public DbSet<MachineDatabase> MachineDatabases { get; set; } 
        public DbSet<Po> Pos { get; set; } 
    }
}
