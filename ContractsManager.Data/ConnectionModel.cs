﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [ProtoContract]
    public class ConnectionModel
    {
        [ProtoMember(1)]
        public String DataSource { get; set; }
        [ProtoMember(2)]
        public String Database { get; set; }
        [ProtoMember(3)]
        public String Username { get; set; }
        [ProtoMember(4)]
        public String Password { get; set; }
        [ProtoMember(5)]
        public string Code { get; set; }
        [ProtoMember(6)]
        public string Name { get; set; }
        [ProtoMember(7)]
        public string Acronym { get; set; }
        [ProtoMember(8)]
        public bool IsCurrent { get; set; }
        [ProtoMember(9)]
        public double Tax { get; set; }
        [ProtoMember(10)]
        public string Currency { get; set; }
    }
}
