﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("CostCodeLibraries")]
    public class CostCodeLibrary : ModelBase, INotifyPropertyChanged
    {
        [StringLength(255)]
        public string En { get; set; }
        [StringLength(255)]
        public string Fr { get; set; }
        public virtual ICollection<BoqSheet> BoqSheets { get; set; }
        private string _code;
        [StringLength(255)]
        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                NotifyPropertyChanged("Code");
            }
        }

        private Nullable<bool> _bold;
        public Nullable<bool> Bold
        {
            get { return _bold; }
            set
            {
                _bold = value;
                NotifyPropertyChanged("Bold");
            }
        }
        
        private string _color;
        [StringLength(255)]
        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
                NotifyPropertyChanged("Color");
            }
        }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
