﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("vo")]
    public class Vo : ModelBase, INotifyPropertyChanged
    {
        [NotMapped]
        private bool _isSelected = false;
        [NotMapped]
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }
        [StringLength(255)]
        public string No { get; set; }

        public int Level { get; set; }
        public bool IsContractVo { get; set; }

        [StringLength(255)]
        public string Key { get; set; }

        [StringLength(255)]
        public string Unite { get; set; }

        public int? CostCodeId { get; set; }
        private CostCodeLibrary _costCode;
        [ForeignKey("CostCodeId")]
        public virtual CostCodeLibrary CostCode
        {
            get { return _costCode; }
            set
            {
                _costCode = value;
                NotifyPropertyChanged("CostCode");
            }
        }

        private bool _isHide;
        [NotMapped]
        public bool IsHide
        {
            get { return _isHide; }
            set
            {
                _isHide = value;
                NotifyPropertyChanged("IsHide");
            }
        }
        [StringLength(255)]
        public string Remark { get; set; }
        private double _qte;
        public double Qte
        {
            get { return _qte; }
            set
            {
                _qte = value;
                NotifyPropertyChanged("Qte");
                NotifyPropertyChanged("Pt");
            }
        }
        private double _pu;
        public double Pu
        {
            get { return _pu; }
            set
            {
                _pu = value;
                NotifyPropertyChanged("Pu");
                NotifyPropertyChanged("Pt");
            }
        }
        private double _pt;
        public double Pt
        {
            get
            {
                //if (_qte != 0 && _pu != 0)
                return _qte * _pu;
                //else
                //return _pt;
            }
            set
            {

                if (value != 0)
                    _pt = value;
                else
                {
                    if (_qte != 0 && _pu != 0)
                        _pt = _qte * _pu;
                }

                NotifyPropertyChanged("Pt");
            }
        }
        private double _orderVo;
        public double OrderVo
        {
            get { return _orderVo; }
            set
            {
                _orderVo = value;
                NotifyPropertyChanged("OrderVo");
            }
        }
        [Required]
        public BOQType BOQType { get; set; }

        public int? BoqSheetId { get; set; }
        [ForeignKey("BoqSheetId")]
        public virtual BoqSheet BoqSheet { get; set; }

        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual Vo Parent { get; set; }

        //public virtual ICollection<ContractBoqItem> ContractBoqItem { get; set; }

        public virtual ICollection<Vo> Children { get; set; }
        public virtual ICollection<ContractVo> ContractVos { get; set; }


        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
