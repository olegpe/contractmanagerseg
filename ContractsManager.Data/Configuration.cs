﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class Configuration : ModelBase
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
