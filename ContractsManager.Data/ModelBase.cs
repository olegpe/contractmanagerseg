﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{
    public abstract class ModelBase : IEntity
    {
        [Key]
        public int Id { get; set; }
        
        public DateTime Created { get; set; }
        public ModelBase()
        {
            Created = DateTime.Now;
        }
        //public event PropertyChangedEventHandler PropertyChanged;
    }
}
