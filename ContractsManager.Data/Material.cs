﻿namespace ContractsManager.Data
{
    public class Material : ModelBase
    {
        public string Item { get; set; }
        public string DeliveredQte { get; set; }
        public string Unit { get; set; }
        public double UnitPrice { get; set; }
        public double OrderdQte { get; set; }
        public double TransferedQte { get; set; }
        public string TransferedTo { get; set; }
        public double StockQte { get; set; }
        public string Remark { get; set; }
        public int? ContractsDatasetId { get; set; }
        public ContractsDataset ContractsDataset { get; set; }
    }
}