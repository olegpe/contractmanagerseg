﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("Contracts")]
    public class Contract : ModelBase, INotifyPropertyChanged
    {
        private string template;
        private string templateNumber;
        private string type;
        private string secondType;
        private string _language;
        private byte[] content;
        
        [StringLength(255)]
        public string Template
        {
            get
            {
                return template;
            }
            set
            {
                template = value;
                OnPropertyChanged("Template");
            }
        }
        
        [StringLength(255)]
        public string TemplateNumber
        {
            get
            {
                return templateNumber;
            }
            set
            {
                templateNumber = value;
                OnPropertyChanged("TemplateNumber");
            }
        }
        
        [StringLength(255)]
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                OnPropertyChanged("Type");
            }
        }
        
        [StringLength(255)]
        public string SecondType
        {
            get
            {
                return secondType;
            }
            set
            {
                secondType = value;
                OnPropertyChanged("SecondType");
            }
        }

        [StringLength(20)]
        public string Language
        {
            get
            {
                return _language;
            }
            set
            {
                _language = value;
                OnPropertyChanged("Language");
            }
        }

        public byte[] Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }
        public FileType FileType { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
