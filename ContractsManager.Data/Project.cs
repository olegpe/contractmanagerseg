﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("Projects")]
    public class Project : ModelBase, INotifyPropertyChanged
    {
        [Required]
        [StringLength(255)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Acronym { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        //private string _currency;
        //[StringLength(20)]
        //   [ForeignKey("CurrencyId")]
        //public string Currency
        //{
        //    get { return _currency; }
        //    set
        //    {
        //        _currency = value;
        //        NotifyPropertyChanged("Currency");
        //    }
        //}
        public int? CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
        public int VoLevel { get; set; }
        public virtual ICollection<ContractsDataset> ContractDatasets { get; set; }
        public virtual ICollection<Building> Buildings { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
