﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class BoqTable
    {
        public string No { get; set; }
        public string Item { get; set; }
        public string Unit { get; set; }
        [Display(Name = "Cost Code")]
        public string CostCode { get; set; }
        public string Quantity { get; set; }
        public string Pu { get; set; }
        public string Pt { get; set; }
    }
}
