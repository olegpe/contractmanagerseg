﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ContractsManager.Data
{
    public class VenteLocal : ModelBase, INotifyPropertyChanged
    {
        public string Bc { get; set; } // REF
        private Po _po;
        public virtual Po Po
        {
            get { return _po; }
            set
            {
                _po = value;
                NotifyPropertyChanged("Po");
            }
        }

        private string _item;
        public string Designation
        {
            get { return _item; }
            set
            {
                _item = value;
                NotifyPropertyChanged("Designation");
            }
        }
        public string Acronym { get; set; } // PRoject Acronym
        public string Subcontractor { get; set; } // Subcontractor
        public string Contract { get; set; } // ContractDataset
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set
            {
                _unit = value;
                NotifyPropertyChanged("Unit");
            }
        }
        private string _remark;
        public string Remark
        {
            get { return _remark; }
            set
            {
                _remark = value;
                NotifyPropertyChanged("Remark");
            }
        }
        private double _allocated;
        public double Allocated {

            get { return _allocated; }
            set
            {
                _allocated = value;
                _consumedAmount = _saleUnit * (_allocated - _stockQte - _transferedQte);
                NotifyPropertyChanged("Allocated");
                NotifyPropertyChanged("ConsumedAmount");
            }
        }
        private double _quantity;
        public double Quantity {

            get { return _quantity; }
            set
            {
                _quantity = value;
            //    _consumedAmount = _saleUnit * _quantity;
                NotifyPropertyChanged("Quantity");
                NotifyPropertyChanged("ConsumedAmount");
            }
        } //Ordered Quantity
        public string TransferedTo { get; set; } // Remark
        public double TotalSale { get; set; }
       
        public double Consumes { get; set; }

        private double _saleUnit;
        public double SaleUnit // Unit Price
        {
            get { return _saleUnit; }
            set
            {
                _saleUnit = value;
                _consumedAmount = _saleUnit * (_allocated - _stockQte - _transferedQte);

                NotifyPropertyChanged("SaleUnit");
                NotifyPropertyChanged("ConsumedAmount");
            }
        }

        private double _stockQte;
        public double StockQte
        {
            get { return _stockQte; }
            set
            {
                _stockQte = value;
                _consumedAmount = _saleUnit * (_allocated - _stockQte - _transferedQte);

                NotifyPropertyChanged("StockQte");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("ConsumedAmount");
            }
        }

        private double _transferedQte;
        public double TransferedQte //
        {
            get { return _transferedQte; }
            set
            {
                _transferedQte = value;
                _consumedAmount = _saleUnit * (_allocated - _stockQte - _transferedQte);
                NotifyPropertyChanged("TransferedQte");
                NotifyPropertyChanged("ConsumedAmount");
                NotifyPropertyChanged("ActualAmount");
            }
        }

        private double _livree;
        public double Livree // Delivered Qte
        {
            get { return _livree; }
            set
            {
                _livree = value;
                _consumedAmount = _saleUnit * (_allocated - _stockQte - _transferedQte);

                NotifyPropertyChanged("Livree");
                NotifyPropertyChanged("ActualAmount");
                NotifyPropertyChanged("ConsumedAmount");
            }
        }
        private double _actualAmount;
        public double ActualAmount
        {
            get { return _actualAmount;/*(Livree - TransferedQte - StockQte) * SaleUnit;*/ }
            set
            {
                _actualAmount = value;
                NotifyPropertyChanged("ActualAmount");
            }
        }

        private double _deduction;
        public double Deduction
        {
            get { return _deduction; }
            set
            {
                _deduction = value;
                _actualAmount = (_deduction * _consumedAmount) / 100;
                NotifyPropertyChanged("Deduction");
                NotifyPropertyChanged("ActualAmount");
            }
        }

       

        private double _consumedAmount;
        public double ConsumedAmount
        {
            get { return _consumedAmount; }
            set
            {
                _consumedAmount = value;
                _actualAmount = (_deduction * _consumedAmount)/100;
                NotifyPropertyChanged("ConsumedAmount");
                NotifyPropertyChanged("ActualAmount");

            }
        }

        private bool _isTransferred;
        public bool IsTransferred
        {
            get { return _isTransferred; }
            set {
                _isTransferred = value;
                NotifyPropertyChanged("IsTransferred");
            }
        }

        public double PrecedentAmount { get; set; }
        public double PrecedentAmountOld { get; set; }

        public int? ContractDatasetId { get; set; }

        private ContractsDataset _transferedCD;
        [ForeignKey("ContractDatasetId")]
        public ContractsDataset TransferedCD
        {
            get { return _transferedCD; }
            set
            {
                _transferedCD = value;
                NotifyPropertyChanged("TransferedCD");
            }
        }

        public VenteLocal()
        {
            Deduction = 100;
        }
        //public int? ContractDatasetId { get; set; }
        //[ForeignKey("ContractDatasetId")]
        //public ContractsDataset ContractsDataset { get; set; }

        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
