﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    [Table("BoqItems")]
    public class BoqItem : ModelBase, INotifyPropertyChanged
    {
        [NotMapped]
        private bool _isSelected = false;
        [NotMapped]
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }
        [StringLength(255)]
        public string No { get; set; }

        [StringLength(255)]
        public string Key { get; set; }

        [StringLength(255)]
        public string Unite { get; set; }

        public int? CostCodeId { get; set; }
        [ForeignKey("CostCodeId")]
        private CostCodeLibrary _costCode;
        public virtual CostCodeLibrary CostCode
        {
            get { return _costCode; }
            set
            {
                _costCode = value;
                NotifyPropertyChanged("CostCode");
            }
        }

        private bool _isHide;
        [NotMapped]
        public bool IsHide
        {
            get { return _isHide; }
            set
            {
                _isHide = value;
                NotifyPropertyChanged("IsHide");
            }
        }
       
        private double _qte;
        public double Qte
        {
            get { return _qte; }
            set
            {
                _qte = value;
                NotifyPropertyChanged("Qte");
                NotifyPropertyChanged("Pt");
            }
        }
        private double _pu;
        public double Pu
        {
            get { return _pu; }
            set
            {
                _pu = value;
                NotifyPropertyChanged("Pu");
                NotifyPropertyChanged("Pt");
            }
        }
        private double _pt;
        public double Pt
        {
            get
            {
                //if (_qte != 0 && _pu != 0)
                    return _qte * _pu;
                //else
                //return _pt;
            }
            set
            {

                if (value != 0)
                    _pt = value;
                else
                {
                    if(_qte != 0 && _pu != 0)
                        _pt = _qte * _pu;
                }
                    
                NotifyPropertyChanged("Pt");
            }
        }
        private double _orderBoq;
        public double OrderBoq
        {
            get { return _orderBoq; }
            set
            {
                _orderBoq = value;
                NotifyPropertyChanged("OrderBoq");
            }
        }
        private BOQType _boqType;
        [Required]
        public BOQType BOQType
        {
            get { return _boqType; }
            set
            {
                _boqType = value;
                NotifyPropertyChanged("BOQType");
            }
        }

        public int? BoqSheetId { get; set; }
        [ForeignKey("BoqSheetId")]
        public virtual BoqSheet BoqSheet { get; set; }
    
         
        public int? ParentId { get; set; }
        public virtual BoqItem Parent { get; set; }
         
        public virtual ICollection<ContractBoqItem> ContractBoqItem { get; set; }

        public virtual ICollection<BoqItem> Children { get; set; }



        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

    public enum BOQType
    {
        Activity, SubActivity, Line, TotalSum
    }
}
