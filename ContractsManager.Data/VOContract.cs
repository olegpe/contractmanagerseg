﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.Data
{
    public class VOContract : ModelBase, INotifyPropertyChanged
    {
        private string name;
        [StringLength(255)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        private string templateNumber;


        [StringLength(255)]
        public string TemplateNumber
        {
            get
            {
                return templateNumber;
            }
            set
            {
                templateNumber = value;
                OnPropertyChanged("TemplateNumber");
            }
        }
        private byte[] content;
        public byte[] Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }
        private string _language;
        [StringLength(20)]
        public string Language
        {
            get
            {
                return _language;
            }
            set
            {
                _language = value;
                OnPropertyChanged("Language");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
