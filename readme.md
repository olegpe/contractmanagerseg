# ContractManager

Private project for managing contracts and subcontractors for big subcontractors company built by WPF and MSSQL.

# Screenshots

[screenshot](https://bitbucket.org/olegpe/contractmanagerseg/src/master/Transition/Images/Screenshot_1.png)
[screenshot](https://bitbucket.org/olegpe/contractmanagerseg/src/master/Transition/Images/Screenshot_2.png)
[screenshot](https://bitbucket.org/olegpe/contractmanagerseg/src/master/Transition/Images/Screenshot_3.png)