﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf.Transitions;
using MaterialDesignThemes.Wpf;
using ContractsManager.Dialogs;
using ContractsManager.ViewModel;
using ContractsManager.Data;
using ContractsManager.MainSlides;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ContractsManager.Data.utils;

namespace ContractsManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        public static Transitioner TransiSlider { get; set; }

        private ContractsDatasetSlide _userControlDatabaseSlide;
        public ContractsDatasetSlide UserControlDatabaseSlide
        {
            get { return _userControlDatabaseSlide; }
            set
            {
                _userControlDatabaseSlide = value;
                NotifyPropertyChanged("UserControlDatabaseSlide");
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            //this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            TransiSlider = mainTrans;
            UserControlDatabaseSlide = databaseSlide;
            var s = Properties.Settings.Default.Language;

            //RasterizerSample1 ss = new RasterizerSample1();
            //ss.Sample1();
            //List<Item> Items1 = new List<Item>();
            //Items1.Add(new Item() { Name = "First Task", Description = "Solve the problem", IsSelected = true });
            //DataContext = new ListsAndGridsViewModel();


        }

        private void StackPanel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MaterialDesignThemes.Wpf.Transitions.Transitioner.MoveNextCommand.Execute(null, null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mainTrans.SelectedIndex = 2;
        }

        private void PrivBtn1_Click(object sender, RoutedEventArgs e)
        {
            mainTrans.SelectedIndex = 0;
        }

        private void Menu_MouseEnter(object sender, MouseEventArgs e)
        {
            // menuq.Foreground = Brushes.WhiteSmoke;
            Label l = new Label();
            TextBlock tb = new TextBlock();
            MenuItem me = new MenuItem();
            
            
        }

        private void DialogHost_Loaded(object sender, RoutedEventArgs e)
        {
          /*  var cstring = Connection.GetConnectionStrings();
            if(cstring.Count <= 0)
            {
                ConnectionViewModel cv = new ConnectionViewModel();
                cv.RunExtendedDialogCommand.Execute("parameter");
            }
          */
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void MetroWindow_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var menuData = this.mainMenu.DataContext as MainMenuViewModel;
            menuData.IsNotifClicked = false;
        }
    }
}
