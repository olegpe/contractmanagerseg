﻿using ContractsManager.Data;
using ContractsManager.utils;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ContractsManager
{
    /// <summary>
    /// Interaction logic for IntroWindow.xaml
    /// </summary>
    public partial class IntroWindow : MetroWindow
    {
        private MainWindow win;
        public IntroWindow()
        {
            InitializeComponent();
            var videoPath = Directory.GetCurrentDirectory();
            media.Source = new Uri(videoPath + @"\intro.mp4", UriKind.Relative);
            win = new MainWindow();
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            var cstring = Connection.GetConnectionStrings();
            if (cstring.Count <= 0)
            {
               // this.StartupUri = new System.Uri("LoginWindow.xaml", System.UriKind.Relative);
                var win = new LoginWindow();
                win.Show();
                this.Close();
            }
            else
            {
               //this.StartupUri = new System.Uri("MainWindow.xaml", System.UriKind.Relative);
              //  var win = new MainWindow();
                win.Show();
                this.Close();
            }
        }
        
        private void MediaElement_Loaded(object sender, RoutedEventArgs e)
        {
        
           // media.LoadedBehavior = MediaState.Manual;
           // media.Play();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
                var hwndTarget = hwndSource.CompositionTarget;
                hwndTarget.RenderMode = RenderMode.SoftwareOnly;
            }
            catch (Exception ex)
            {
                Logging.Log(ex.Message, ex);
            }
        }
    }
}
