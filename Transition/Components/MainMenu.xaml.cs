﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.MainSlides;
using ContractsManager.ViewModel;
using ContractsManager.Windows;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ContractsManager.Components
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl , INotifyPropertyChanged
    {

       

      
        public int Test
        {
            get; set;
        }
        
        private bool _isApproveVisible;
        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set
            {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }
      
        private bool _isRejectVisible;
        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set
            {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }
       
        public User User
        {
            get; set;
        }


        public MainMenu()
        {
            InitializeComponent();
         
            var s = Properties.Settings.Default.Language;
            language.Items.Add("English");
            language.Items.Add("Français");
            if (s == "en-US")
            {
                language.SelectedItem = "English";
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(s);
            }
            else if(s == "fr-FR")
            {
                language.SelectedItem = "Français";
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(s);
            }
            else
            {
                EnglishSelected = true;
                FrenchSelected = false;
            }
            
        }
        private bool _english;
        public bool EnglishSelected
        {
            get { return _english; }
            set { _english = value;
                NotifyPropertyChanged("EnglishSelected");

            }
        }
        private bool _french;

        public bool FrenchSelected
        {
            get { return _french; }
            set
            {
                _french = value;
                NotifyPropertyChanged("FrenchSelected");

            }
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 0;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            //win.mainWindowSlide.mainMenu = new MainMenu();
            firstMenuBtn.Foreground = Brushes.Black;
            secondtMenuBtn.Foreground = Brushes.Black;
            thirdtMenuBtn.Foreground = Brushes.Black;
            fourthtMenuBtn.Foreground = Brushes.Black;
            SixthMenuBtn.Foreground = Brushes.Black;
            fifthMenuBtn.Foreground = Brushes.Black;
        }

        
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void MenuBtn_MouseEnter(object sender, MouseEventArgs e)
        {
             
        }

        private void MenuBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            
        }

        private void AdminBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 7;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.adminTrans.SelectedIndex = 0;

        }

        private void Language_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((string)language.SelectedItem == "English" && Properties.Settings.Default.Language == "en-US")
                return;
            if ((string)language.SelectedItem == "Français" && Properties.Settings.Default.Language == "fr-FR")
                return;

            if (language.SelectedIndex == 0)
            {
                Properties.Settings.Default.Language = "en-US";
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();

            }
            else if (language.SelectedIndex == 1)
            {
                Properties.Settings.Default.Language = "fr-FR";
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();

            }
            Properties.Settings.Default.Save();
        }

        private void FirstMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 1;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            firstMenuBtn.Foreground = Brushes.Silver;
            secondtMenuBtn.Foreground = Brushes.Black; 
           thirdtMenuBtn.Foreground = Brushes.Black; 
           fourthtMenuBtn.Foreground = Brushes.Black;
           SixthMenuBtn.Foreground = Brushes.Black;
           fifthMenuBtn.Foreground = Brushes.Black;

            if(win.ContractDatabaseSlide.DataContext != null)
            {
                ((ContractsDatasetViewModel)win.ContractDatabaseSlide.DataContext).SelectedProject = null;
                win.ContractDatabaseSlide.DataContext = null;
            }
           
            win.ContractDatabaseSlide.DataContext = new ContractsDatasetViewModel();

            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
 

        }
         private void FifhMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 8;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.ProjectsSlide.DataContext = new ProjectsViewModel();

            fifthMenuBtn.Foreground = Brushes.Silver;
            SixthMenuBtn.Foreground = Brushes.Black;
            fourthtMenuBtn.Foreground = Brushes.Black;
            secondtMenuBtn.Foreground = Brushes.Black;
            firstMenuBtn.Foreground = Brushes.Black;
            thirdtMenuBtn.Foreground = Brushes.Black;





            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
           
        }

        private void SecondtMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 2;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            secondtMenuBtn.Foreground = Brushes.Silver;
            firstMenuBtn.Foreground = Brushes.Black;
           thirdtMenuBtn.Foreground = Brushes.Black; 
           fourthtMenuBtn.Foreground = Brushes.Black;
           SixthMenuBtn.Foreground = Brushes.Black; 
           fifthMenuBtn.Foreground = Brushes.Black;

            win.databaseSlide.DataContext = new ContractsDatabaseViewModel();
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
          
        }

        private void ThirdtMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 3;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            secondtMenuBtn.Foreground = Brushes.Black;
            firstMenuBtn.Foreground = Brushes.Black;
            thirdtMenuBtn.Foreground = Brushes.Silver;
            fourthtMenuBtn.Foreground = Brushes.Black;
            SixthMenuBtn.Foreground = Brushes.Black;  
            fifthMenuBtn.Foreground = Brushes.Black;
            if (win.DeductionDatabaseSlide.DataContext != null)
            {
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedProject = null;
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedSubcontractor = null;
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedContractsDataset = null;
            }
            
            win.DeductionDatabaseSlide.DataContext = new DeducationsViewModel();
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
           
        }

        private void FourthtMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            MainWindow.TransiSlider.SelectedIndex = 5;

            fourthtMenuBtn.Foreground = Brushes.Silver;
            secondtMenuBtn.Foreground = Brushes.Black;
            firstMenuBtn.Foreground = Brushes.Black;
            thirdtMenuBtn.Foreground = Brushes.Black;
            SixthMenuBtn.Foreground = Brushes.Black;
            if (win.IpcDatabaseSlide.DataContext != null)
            {
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedProject = null;
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedSubcontractor = null;
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedContractsDataset = null;
            }
          
            win.IpcDatabaseSlide.DataContext = new IpcViewModel();
            fifthMenuBtn.Foreground = Brushes.Black;CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
            
        }

        private void SixthMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            SixthMenuBtn.Foreground = Brushes.Silver;

            MainWindow.TransiSlider.SelectedIndex = 6;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

            if (win.reportSlide.DataContext != null)
            {
                ((ReportViewModel)win.reportSlide.DataContext).SelectedProject = null;
                ((ReportViewModel)win.reportSlide.DataContext).SelectedSiteProject = null;
            }

            win.reportSlide.DataContext = new ReportViewModel(DialogCoordinator.Instance);
            SixthMenuBtn.Foreground = Brushes.Silver;
            fourthtMenuBtn.Foreground = Brushes.Black;
            secondtMenuBtn.Foreground = Brushes.Black;
            firstMenuBtn.Foreground = Brushes.Black;
            thirdtMenuBtn.Foreground = Brushes.Black;
            fifthMenuBtn.Foreground = Brushes.Black; 
         
        }

        public event PropertyChangedEventHandler PropertyChanged;
        //public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/> for the property whose name matches <see cref="propertyName"/>.
        /// </summary>
        /// <param name="propertyName">Optional. The name of the property whose value has changed.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void MainMenu_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new MainMenuViewModel();
        }

        private void TextBlock_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 9;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.notificationSlide.DataContext = new NotificationViewModel();
        }

        private void min_Click(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.WindowState = WindowState.Minimized;
        }
    }
}
