﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class DeducationsViewModel : ContractsManager.Windows.ViewModel
    {
        private readonly UnitOfWork unitOfWork = new UnitOfWork();
        private double _laborAmount;
        public double LaborAmount
        {
            get { return _laborAmount; }
            set {
                _laborAmount = value;
                NotifyPropertyChanged("LaborAmount");
            }
        }
        public IEnumerable<string> Units
        {
            get
            {
                yield return "ft";
                yield return "U";
                yield return "D";
                yield return "dm";
                yield return "ens";
                yield return "ml";
                yield return "m2";
                yield return "m3";
                yield return "kg";
                yield return "T";
            }
        }
        private double _materialAmount;
        public double MaterialAmount
        {
            get { return _materialAmount; }
            set {
                _materialAmount = value;
                NotifyPropertyChanged("MaterialAmount");
            }
        }

        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject");
            }
        }

        private Subcontractor _selectedSubcontractor;
        public Subcontractor SelectedSubcontractor
        {
            get { return _selectedSubcontractor; }
            set
            {
                _selectedSubcontractor = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSubcontractor");
            }
        }

        private Contract _selectedContract;
        public Contract SelectedContract
        {
            get { return _selectedContract; }
            set
            {
                _selectedContract = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedContract");
            }
        }

        private ContractsDataset _selectedContractsDataset;

        public ContractsDataset SelectedContractsDataset
        {
            get { return _selectedContractsDataset; }
            set {
                _selectedContractsDataset = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedContractsDataset");
            }
        }
        private Labor _selectedLabor;
        public Labor SelectedLabors
        {
            get { return _selectedLabor; }
            set
            {
                _selectedLabor = value;
                NotifyPropertyChanged("SelectedLabors");
            }
        }
        private Machine _selectedMachine;

        public Machine SelectedMachines
        {
            get { return _selectedMachine; }
            set
            {
                _selectedMachine = value;
                NotifyPropertyChanged("SelectedMachines");
            }
        }
        public VenteLocal SelectedMaterials { get; set; }
        private LaborDatabase _selectedLaborDatabase;
        public LaborDatabase SelectedLaborDatabase
        {
            get { return _selectedLaborDatabase; }
            set
            {
                _selectedLaborDatabase = value;
                NotifyPropertyChanged("SelectedLaborDatabase");
            }
        }
        private ObservableCollection<Project> _projects;
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Projects");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors;
        public ObservableCollection<Subcontractor> Subcontractors
        {
            get { return _subcontractors; }
            set
            {
                _subcontractors = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Subcontractors");
            }
        }
        private ObservableCollection<ContractsDataset> _contractDatasets;
        public ObservableCollection<ContractsDataset> ContractDatasets
        {
            get { return _contractDatasets; }
            set
            {
                _contractDatasets = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ContractDatasets");
            }
        }
        private ObservableCollection<LaborDatabase> _laborDatabases;
        public ObservableCollection<LaborDatabase> LaborDatabases
        {
            get { return _laborDatabases; }
            set
            {
                _laborDatabases = value;
                NotifyPropertyChanged("LaborDatabases");
            }
        }
        private ObservableCollection<MachineDatabase> _machineDatabases;
        public ObservableCollection<MachineDatabase> MachineDatabases
        {
            get { return _machineDatabases; }
            set
            {
                _machineDatabases = value;
                NotifyPropertyChanged("MachineDatabases");
            }
        }

        private ObservableCollection<ContractsDataset> _transContractDatasets;
        public ObservableCollection<ContractsDataset> TransContractDatasets
        {
            get { return _transContractDatasets; }
            set
            {
                _transContractDatasets = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("TransContractDatasets");
            }

        }

        private ObservableCollection<Labor> _labors;
        public ObservableCollection<Labor> Labors
        {
            get { return _labors; }
            set
            {
                _labors = value;
                if (_labors != null)
                {
                    _labors.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (Labor item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateLaborTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (Labor item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateLaborTotalSale;
                                item.PropertyChanged += UpdateLaborTotalSale;
                            }
                        }
                    };
                    if (_labors.Count > 0)
                    {
                        LaborAmount = Labors.Sum(x => x.Amount);

                    }
                }
                NotifyPropertyChanged("Labors");

            }
        }
        private void UpdateLaborTotalSale(object sender, PropertyChangedEventArgs e)
        {
            LaborAmount = 0;
            if (Labors.Count == 0)
                return;
            LaborAmount = Labors.Sum(x => x.Amount);
        }

        private Currency _currency;

        public Currency Currency
        {
            get { return _currency; }
            set {
                _currency = value;
                NotifyPropertyChanged("Currency");
            }
        }

        private ObservableCollection<VenteLocal> _venteLocals;
        public ObservableCollection<VenteLocal> VenteLocals
        {
            get { return _venteLocals; }
            set
            {
                _venteLocals = value;
                if (_venteLocals != null)
                {
                    _venteLocals.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (VenteLocal item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateVentTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (VenteLocal item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateVentTotalSale;
                                item.PropertyChanged += UpdateVentTotalSale;
                            }
                        }
                    };
                }
                
                NotifyPropertyChanged("VenteLocals");

            }

        }
        private void UpdateVentTotalSale(object sender, PropertyChangedEventArgs e)
        {
         //We used this method to calculate total sum for venteLocal
        }
        private ObservableCollection<Machine> _machines;
        public ObservableCollection<Machine> Machines
        {
            get { return _machines; }
            set
            {
                _machines = value;
                if (_machines != null)
                {
                    _machines.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (Machine item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateMachineTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (Machine item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateMachineTotalSale;
                                item.PropertyChanged += UpdateMachineTotalSale;
                            }
                        }
                    };
                }

                if (Machines.Count > 0)
                {
                    MaterialAmount = Machines.Sum(x => x.Amount);
                }
                NotifyPropertyChanged("Machines");

            }
        }
        private void UpdateMachineTotalSale(object sender, PropertyChangedEventArgs e)
        {
            MaterialAmount = 0;
            if (Machines.Count == 0)
                return;
            MaterialAmount = Machines.Sum(x => x.Amount);
        }
        private int _deduTrans;
        public int DeduTrans
        {
            get { return _deduTrans; }
            set
            {
                _deduTrans = value;
                NotifyPropertyChanged("DeduTrans");
            }
        }

        private bool _isLabor;
        public bool IsLabor
        {
            get { return _isLabor; }
            set
            {
                _isLabor = value;
                NotifyPropertyChanged("IsLabor");
            }
        }
        public int pro1 { get; set; }
        public int sub1 { get; set; }
        public int cont1 { get; set; }
        public DeducationsViewModel()
        {
            pro1 = 0;
            sub1 = 0;
            cont1 = 0;
            if (Subcontractors == null)
                Subcontractors = new ObservableCollection<Subcontractor>();
            LaborDatabases = new ObservableCollection<LaborDatabase>();
            MachineDatabases = new ObservableCollection<MachineDatabase>();
            IsLabor = true;
            DeduTrans = 0;
            var confg = unitOfWork.ConfigurationRepository.Get(x => x.Key == "Currency").FirstOrDefault();
            Currency = unitOfWork.CurrenciesRepository.Get(x => x.Currencies == confg.Value).FirstOrDefault();
            Task.Factory.StartNew(() =>
            {
                List<ContractsDataset> c = new List<ContractsDataset>();
                if (ContractDatasets == null)
                    ContractDatasets = new ObservableCollection<ContractsDataset>();
                if (unitOfWork.ContractsDatasetRepository.Get() != null)
                    c = unitOfWork.ContractsDatasetRepository.Get().ToList();
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var b = unitOfWork.ProjectRepository.Get();

                var labordatabase = unitOfWork.LaborDatabaseRepository.Get();
                var machinedatabase = unitOfWork.MachineDatabaseRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractDatasets.AddRange(c);
                          Projects.AddRange(b);
                          LaborDatabases.AddRange(labordatabase);
                          MachineDatabases.AddRange(machinedatabase);

                      }));
            });
        }
        public ICommand ProjectSelectedCommand => new ActionCommand(p => ProjectSeleced());
        public ICommand ContractSelectedCommand => new ActionCommand(p => ContractSeleced());
        public ICommand SubcontractorCommand => new ActionCommand(p => SubcontractorSeleced());
        public ICommand OpenVenteLocalCommand => new ActionCommand(p => OpenVenteLocal());

        public ICommand RowEditEndingLaborCommand => new ActionCommand(p => RowEditEndingLabor());
        public ICommand RowEditEndingMaterialCommand => new ActionCommand(p => RowEditEndingMaterial());
        public ICommand CellEditEndingMaterialCommand => new ActionCommand(p => CellEditEndingMaterial(p));
        public ICommand CellEditEndingLaborCommand => new ActionCommand(p => CellEditEndingLabor(p));
        public ICommand CellEditEndingMachineCommand => new ActionCommand(p => CellEditEndingMachine(p));
        public ICommand RowEditEndingMachineCommand => new ActionCommand(p => RowEditEndingMachine());

        public ICommand InsertLaborCommand => new ActionCommand(p => InsertLabor(p));
        public ICommand PasteLaborCommand => new ActionCommand(p => PasteLabor(p));
        public ICommand DeleteLaborCommand => new ActionCommand(p => DeleteLabor(p));
        public ICommand CopyLaborCommand => new ActionCommand(p => CopyLabor(p));
        public ICommand SelectionChangedLaborCommand => new ActionCommand(p => SelectionChangedLabor());

        public ICommand InsertMachineCommand => new ActionCommand(p => InsertMachine(p));
        public ICommand PasteMachineCommand => new ActionCommand(p => PasteMachine(p));
        public ICommand DeleteMachineCommand => new ActionCommand(p => DeleteMachine(p));
        public ICommand CopyMachineCommand => new ActionCommand(p => CopyMachine(p));
        public ICommand SelectionChangedMachineCommand => new ActionCommand(p => SelectionChangedMachine());

        public ICommand InsertVenteCommand => new ActionCommand(p => InsertVente(p));
        public ICommand PasteVenteCommand => new ActionCommand(p => PasteVente(p));
        public ICommand DeleteVenteCommand => new ActionCommand(p => DeleteVente(p));
        public ICommand CopyVenteCommand => new ActionCommand(p => CopyVente(p));
        public ICommand SelectionChangedVenteCommand => new ActionCommand(p => SelectionChangedVente(p));
        public ICommand LaborSelecteionChangedCommand => new ActionCommand(p => LaborSelecteionChanged(p));
        public ICommand SaveCommand => new ActionCommand(p => SaveRecords());

        private void LaborSelecteionChanged(object p)
        {
            //
        }
        private void SelectionChangedLabor()
        {
            if (SelectedLabors == null)
                return;
            if (SelectedContractsDataset == null)
                return;
            if (SelectedLabors.TypeOfWorker == null)
                return;

            var price = SelectedLabors.TypeOfWorker.UnitPrice / Currency.ConversionRate;

            SelectedLabors.Unit = SelectedLabors.TypeOfWorker.Unit;
            SelectedLabors.UnitPrice = price * SelectedContractsDataset.Currency.ConversionRate;
        }

        private void InsertLabor(object p)
        {
            //
        }
    
        private async void DeleteLabor(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                foreach (Labor labor in itemg.SelectedItems.OfType<Labor>().ToList())
                {
                    unitOfWork.LaborRepository.Delete(labor);
                    Labors.Remove(labor);
                }
                UpdateLaborTotalSale(null, null);
                unitOfWork.Save();
            }
        }
        
        private void CopyLabor(object p)
        {
            //
        }
        private void PasteLabor(object p)
        {
            //
        }
        private void SelectionChangedVente(object p)
        {
            //var e = p as SelectionChangedEventArgs;
            //var source = e.Source as DataGrid;
            //SelectedMaterials = source.SelectedItems as List<VenteLocal>;
        }
        private void InsertVente(object p)
        {
            //
        }
        private async void DeleteVente(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                foreach (VenteLocal vente in itemg.SelectedItems.OfType<VenteLocal>().ToList())
                {
                    unitOfWork.VenteLocalRepository.Delete(vente);
                    VenteLocals.Remove(vente);
                }
                UpdateVentTotalSale(null, null);
                unitOfWork.Save();
            }
        }
        private void CopyVente(object p)
        {
            //
        }
        private void PasteVente(object p)
        {
            //
        }
        private void SelectionChangedMachine()
        {
            if (SelectedMachines == null)
                return;
            if (SelectedContractsDataset == null)
                return;
            if (SelectedMachines.MachineCode == null)
                return;

            var price = SelectedMachines.MachineCode.UnitPrice / Currency.ConversionRate;

            SelectedMachines.Unit = SelectedMachines.MachineCode.Unit;
            SelectedMachines.UnitPrice = price * SelectedContractsDataset.Currency.ConversionRate;
            SelectedMachines.MachineType = SelectedMachines.MachineCode.MachineType;
        }
        private void InsertMachine(object p)
        {
            //
        }
        private async void DeleteMachine(object p)
        {
             var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                foreach (Machine machin in itemg.SelectedItems.OfType<Machine>().ToList())
                {
                    unitOfWork.MachineRepository.Delete(machin);
                    Machines.Remove(machin);
                }
                UpdateMachineTotalSale(null, null);
                unitOfWork.Save();
            }
        }
        private void CopyMachine(object p)
        {
            //
        }
        private void PasteMachine(object p)
        {
            //
        }
        private void CellEditEndingMaterial(object p)
        {
            var e = p as DataGridCellEditEndingEventArgs;
            if ((string)e.Column.Header == Properties.Langs.Lang.refNo)
            {
                List<Po> po = unitOfWork.PoRepository.Get(x => x.PoRef == SelectedMaterials.Bc) as List<Po>;
                if (po != null)
                {
                    if(po.Count == 1)
                    {
                        double price = po.FirstOrDefault().UnitPrice / Currency.ConversionRate;
                        SelectedMaterials.Po = po.FirstOrDefault();
                        SelectedMaterials.Unit = po.FirstOrDefault().Unit;
                        SelectedMaterials.SaleUnit = price * SelectedContractsDataset.Currency.ConversionRate;
                        SelectedMaterials.Designation = po.FirstOrDefault().Item;
                        SelectedMaterials.Allocated = po.FirstOrDefault().DeliveredQte;
                    }
                    else if (po.Count > 1)
                    {
                        var flag = true;
                        foreach (var item in po)
                        {
                            double price = item.UnitPrice / Currency.ConversionRate;
                            if(flag)
                            {
                                SelectedMaterials.Po = item;
                                SelectedMaterials.Unit = item.Unit;
                                SelectedMaterials.SaleUnit = price * SelectedContractsDataset.Currency.ConversionRate;
                                SelectedMaterials.Designation = item.Item;
                                SelectedMaterials.Allocated = item.DeliveredQte;
                                flag = false;
                            }
                            else
                            {
                                VenteLocals.Add(new VenteLocal
                                {
                                    Bc = item.PoRef,
                                    Po = item,
                                    Unit = item.Unit,
                                    SaleUnit = price * SelectedContractsDataset.Currency.ConversionRate,
                                    Designation = item.Item,
                                    Allocated = item.DeliveredQte
                                });
                            }
                            
                        }
                    }
                        
                }
            }
            else if ((string)e.Column.Header == Properties.Langs.Lang.allocatedQuantity)
            {
                var po = unitOfWork.PoRepository.Get(x => x.Id == SelectedMaterials.Po.Id).FirstOrDefault();

                var vente = unitOfWork.VenteLocalRepository.Get(x => x.Po.Id == SelectedMaterials.Po.Id).Sum(x => x.Allocated);
                if( SelectedMaterials.Id == 0)
                {
                    vente += SelectedMaterials.Allocated;
                }
                if(vente > po.DeliveredQte)
                {
                    MessageBox.Show("You exceeded the total delivered quantity of this PO by " + (vente - po.DeliveredQte), "Quantity Exceeded!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    SelectedMaterials.Allocated = 0;
                }
            }

        }

        private void CellEditEndingLabor(object p)
        {
            var e = p as DataGridCellEditEndingEventArgs;
            if((string)e.Column.Header == Properties.Langs.Lang.refNo)
            {
                var po = unitOfWork.LaborDatabaseRepository.Get(x => x.LaborType == SelectedLabors.Ref).FirstOrDefault();
                if(po != null)
                {
                    var price = po.UnitPrice / Currency.ConversionRate;

                    SelectedLabors.Unit = po.Unit;
                    SelectedLabors.UnitPrice = price * SelectedContractsDataset.Currency.ConversionRate;
                }
            }
        }
        private void CellEditEndingMachine(object p)
        {
            var e = p as DataGridCellEditEndingEventArgs;
            if((string)e.Column.Header == Properties.Langs.Lang.refNo)
            {
                var po = unitOfWork.MachineDatabaseRepository.Get(x => x.MachineAcronym == SelectedMachines.Ref).FirstOrDefault();
                if(po != null)
                {
                    var price = po.UnitPrice / Currency.ConversionRate;
                    SelectedMachines.Unit = po.Unit;
                    SelectedMachines.UnitPrice = price * SelectedContractsDataset.Currency.ConversionRate;
                    SelectedMachines.MachineType = po.MachineType;
                }
            }
        }
        private void RowEditEndingMaterial()
        {
            foreach (var ventelocal in VenteLocals)
            {
                if (ventelocal.Designation == "-Total-" || ventelocal.Designation == " ")
                    continue;
                if (ventelocal.Id == 0)
                {
                    ventelocal.Contract = SelectedContractsDataset.ContractNumber;
                    unitOfWork.VenteLocalRepository.Insert(ventelocal);
                }
                else
                {
                    unitOfWork.VenteLocalRepository.Update(ventelocal);
                }
                if (ventelocal.TransferedCD != null)
                {
                    //var vv = unitOfWork.VenteLocalRepository.Get(x => x.Bc == ventelocal.Bc && x.Designation == ventelocal.Designation).ToList();
                    //if (vv.Count == 1)
                    //{
                        ventelocal.Remark = (string.IsNullOrWhiteSpace(ventelocal.Remark) ? ventelocal.Remark : ventelocal.Remark + ", ")
                            + ventelocal.TransferedTo + " "+ ventelocal.Unit+ " Transfered To \"" + ventelocal.TransferedCD.Subcontractor.Name + " \""
                            + ventelocal.TransferedCD.ContractNumber + "\"";


                        var dvente = new VenteLocal
                        {
                            Bc = ventelocal.Bc,
                            Po = ventelocal.Po,
                            Designation = ventelocal.Designation,
                            Acronym = ventelocal.Acronym,
                            Contract = ventelocal.TransferedCD.ContractNumber,
                            Unit = ventelocal.Unit,
                            Quantity = ventelocal.TransferedQte,
                            SaleUnit = ventelocal.SaleUnit,
                            Allocated = ventelocal.TransferedQte,
                            IsTransferred = true,
                            Remark = "Transfered From \"" + SelectedContractsDataset.Subcontractor.Name + " \""
                            + SelectedContractsDataset.ContractNumber + "\"",
                        };
                        unitOfWork.VenteLocalRepository.Insert(dvente);
                    if(ventelocal.Id == 0)
                        unitOfWork.VenteLocalRepository.Insert(ventelocal);
                    else if(ventelocal.Id > 0)
                        unitOfWork.VenteLocalRepository.Update(ventelocal);
                    ventelocal.TransferedCD = null;
                    //}
                }
            }
            unitOfWork.Save();
        }
    
        private void RowEditEndingMachine()
        {
            foreach (var machine in Machines)
            {
                if (machine.Id == 0)
                {
                    machine.ContractsDatasetId = SelectedContractsDataset.Id;
                    unitOfWork.MachineRepository.Insert(machine);
                }
                else
                {
                    unitOfWork.MachineRepository.Update(machine);
                }
            }
            unitOfWork.Save();
        }

        private void RowEditEndingLabor()
        {
            foreach (var labor in Labors)
            {
                if (labor.Id == 0)
                {
                    labor.ContractsDatasetId = SelectedContractsDataset.Id;
                    unitOfWork.LaborRepository.Insert(labor);
                }
                else
                {
                    unitOfWork.LaborRepository.Update(labor);
                }
            }
            unitOfWork.Save();
        }

        private void OpenVenteLocal()
        {
            DeduTrans = 3;

        }
        
        private void SaveRecords()
        {
            if (SelectedContractsDataset == null)
            {
                //TODO::ERROR MESSAGE "CHOSE CONTRACT FIRST"
            }
            try
            {
                foreach (var ventelocal in VenteLocals)
                {
                   
                    if (ventelocal.Id == 0)
                    {
                        ventelocal.Contract = SelectedContractsDataset.ContractNumber;
                        unitOfWork.VenteLocalRepository.Insert(ventelocal);
                    }
                    else
                    {
                        unitOfWork.VenteLocalRepository.Update(ventelocal);
                    }
                    if(ventelocal.TransferedCD != null)
                    {
                        var vv = unitOfWork.VenteLocalRepository.Get(x => x.Bc == ventelocal.Bc && x.Designation == ventelocal.Designation).ToList();
                      
                        if (vv.Count == 1)
                        {
                            ventelocal.Remark = (string.IsNullOrWhiteSpace(ventelocal.Remark) ? ventelocal.Remark + ", " : ventelocal.Remark)
                              + " Transfered To \"" + ventelocal.TransferedCD.Subcontractor.Name + " \""
                              + ventelocal.TransferedCD.ContractNumber + "\"";


                            var dvente = new VenteLocal
                            {
                                Bc = ventelocal.Bc,
                                Designation = ventelocal.Designation,
                                Acronym = ventelocal.Acronym,
                                Contract = ventelocal.TransferedCD.ContractNumber,
                                Unit = ventelocal.Unit,
                                Quantity = ventelocal.TransferedQte,
                                SaleUnit = ventelocal.SaleUnit,
                                Allocated = ventelocal.TransferedQte,
                                Remark = "Transfered From \"" + SelectedContractsDataset.Subcontractor.Name + " \"" 
                                + SelectedContractsDataset.ContractNumber + "\"",
                            };
                            unitOfWork.VenteLocalRepository.Insert(dvente);
                            unitOfWork.VenteLocalRepository.Update(ventelocal);
                        }
                    }
                }
                foreach (var labor in Labors)
                {
                    if (labor.Id == 0)
                    {
                        labor.ContractsDatasetId = SelectedContractsDataset.Id;
                        unitOfWork.LaborRepository.Insert(labor);
                    }
                    else
                    {
                        unitOfWork.LaborRepository.Update(labor);
                    }
                }
                foreach (var machine in Machines)
                {
                    if (machine.Id == 0)
                    {
                        machine.ContractsDatasetId = SelectedContractsDataset.Id;
                        unitOfWork.MachineRepository.Insert(machine);
                    }
                    else
                    {
                        unitOfWork.MachineRepository.Update(machine);
                    }
                }
                unitOfWork.Save();
                UpdateLaborTotalSale(null, null);
                UpdateMachineTotalSale(null, null);
                UpdateVentTotalSale(null, null);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }


        }
        
        private void ProjectSeleced()
        {
            pro1++;
            if (pro1 == 1)
            {
                if (SelectedProject == null)
                    return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
                if (SelectedContractsDataset != null)
                {
                    SelectedSubcontractor = null;
                    SelectedContractsDataset = null;
                }

                var tcd = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject.Id);
                var cd = tcd.Select(x => x.Subcontractor).Distinct().ToList();
                Subcontractors.Clear();
                Subcontractors.AddRange(cd);
            }
            else if (pro1 == 3)
                pro1 = 0;
           
        
        }

        private void SubcontractorSeleced()
        {
            sub1++;
            if (sub1 == 1)
            {
                if (SelectedSubcontractor == null)
                return;
                if (SelectedContractsDataset != null)
                {
                    SelectedContractsDataset = null;
                }

                if (TransContractDatasets == null)
                    TransContractDatasets = new ObservableCollection<ContractsDataset>();
                var tcd = unitOfWork.ContractsDatasetRepository.Get();
                TransContractDatasets.Clear();
                TransContractDatasets.AddRange(tcd.ToList());

                var sc = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject.Id && x.SubcontractorId == SelectedSubcontractor.Id).ToList();
                ContractDatasets.Clear();
                ContractDatasets.AddRange(sc);
            }
            else if (sub1 == 3)
                sub1 = 0;
        }

        private void ContractSeleced()
        {
            cont1++;
            if (cont1 == 1)
            {
                if (SelectedContractsDataset == null)
                {
                    return;
                }

                if (VenteLocals == null)
                    VenteLocals = new ObservableCollection<VenteLocal>();
                if (Labors == null)
                    Labors = new ObservableCollection<Labor>();
                if (Machines == null)
                    Machines = new ObservableCollection<Machine>();

                var mt = unitOfWork.VenteLocalRepository.Get(x => x.Contract == SelectedContractsDataset.ContractNumber).ToList();
                VenteLocals.Clear();
                VenteLocals.AddRange(mt);
                var sum = VenteLocals.Sum(x => x.SaleUnit * x.Allocated);
                var ma = unitOfWork.MachineRepository.Get(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();
                Machines.Clear();
                Machines.AddRange(ma);

                var lb = unitOfWork.LaborRepository.Get(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();
                Labors.Clear();
                Labors.AddRange(lb);
                calculateTotal();
            }
            else if (cont1 == 3)
                cont1 = 0;
        }

        private void calculateTotal()
        {
            LaborAmount = 0;
            MaterialAmount = 0;
            if (Labors.Count != 0)
            {
                LaborAmount = (double)Labors.Sum(x => x.Amount);
            }

            if (Machines.Count != 0)
            {
                MaterialAmount = (double)Machines.Sum(x => x.Amount);
            }
        }
    }
}
