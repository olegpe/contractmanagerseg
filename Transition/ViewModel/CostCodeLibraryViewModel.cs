﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using ContractsManager.Data;
using ContractsManager.Data.Repository;
using System.Linq;
using System.Windows.Input;
using ContractsManager.Windows;
using ContractsManager.Dialogs;
using MaterialDesignThemes.Wpf;
using System;
using Microsoft.Win32;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using ContractsManager.Data.Excel;
using System.Windows;
using System.Windows.Controls;
using ContractsManager.Data.utils;

namespace ContractsManager.ViewModel
{
    class CostCodeLibraryViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private bool _isUploading;
        public bool IsUploading
        {
            get
            {
                return _isUploading;
            }
            set
            {
                _isUploading = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsUploading");
            }
        }
        private bool _isEnabled;
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsEnabled");
            }
        }
        private CostCodeLibrary _selectedCostCode;
        public CostCodeLibrary SelectedCostCode
        {
            get { return _selectedCostCode; }
            set
            {
                _selectedCostCode = value;
                NotifyPropertyChanged("SelectedCostCode");
            }
        }
        private ObservableCollection<CostCodeLibrary> _costCodeLibraries;
        public ObservableCollection<CostCodeLibrary> CostCodeLibraries
        {
            get { return _costCodeLibraries; }
            set
            {
                _costCodeLibraries = value;
                NotifyPropertyChanged("CostCodeLibraries");
            }
        }

     
        public CostCodeLibraryViewModel()
        {
            Task.Factory.StartNew(() =>
            {
                CostCodeLibraries = new ObservableCollection<CostCodeLibrary>();
                var costCode = unitOfWork.CostCodeLibraryRepository.Get() ;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          if(costCode != null)
                          CostCodeLibraries.AddRange(costCode.OrderBy(x => x.Code).ToList());
                          if (CostCodeLibraries.Count > 0) IsEnabled = false;
                          else IsEnabled = true;
                      }));
            });
        }
        public ICommand SaveCommand => new ActionCommand(p => SaveCostCode(p));
        public ICommand DeleteCommand => new ActionCommand(p => DeleteCostCode(p));
        public ICommand DeleteKeyCommand => new ActionCommand(p => DeleteKey(p));
        public ICommand UploadCostCodeCommand => new ActionCommand(p => UploadCostCode(p));
        public ICommand InsertCommand => new ActionCommand(p => Insert(p));
        public ICommand CellEditEndingCommand => new ActionCommand(p => CellEditEnding(p));
        public ICommand RowEditEndingCommand => new ActionCommand(p => RowEditEnding(p));

        private void RowEditEnding(object p)
        {
            if (SelectedCostCode == null) return;

            if (SelectedCostCode.Id > 0)
            {
                MessageBox.Show("These changes will affect all the BOQs and VOs in this project", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);

            }

            var e = p as RoutedEventArgs;
            if(SelectedCostCode.Id > 0)
                unitOfWork.CostCodeLibraryRepository.Update(SelectedCostCode);
            else
                unitOfWork.CostCodeLibraryRepository.Insert(SelectedCostCode);
            unitOfWork.Save();
        }

        private void CellEditEnding(object p)
        {
        }

        private void Insert(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGrid)contextMenu.PlacementTarget;
            CostCodeLibrary boq = item.SelectedItem as CostCodeLibrary;

            var index = CostCodeLibraries.IndexOf(boq);
            var newCost = new CostCodeLibrary();
            CostCodeLibraries.Insert(index, newCost);
            //BoqItemsLCV.Refresh();
        }

        private void UploadCostCode(object p)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() ?? false)
            {
                IsUploading = true;
                Task.Factory.StartNew(() =>
                {
                    var excel = new Excel(openFileDialog.FileName);
                    var costcode = excel.GetCostCode();
                    excel.Close();
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                  new ThreadStart(() =>
                  {
                      IsUploading = false;
                      CostCodeLibraries.AddRange(costcode);
                      foreach (var item in costcode)
                      {
                          unitOfWork.CostCodeLibraryRepository.Insert(item);
                      }
                      unitOfWork.Save();
                      IsEnabled = false;
                  }));
                });
            }
        }
        private async void DeleteKey(object p)
        {
            if (SelectedCostCode == null)
                return;
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {
                var ishave = unitOfWork.ProjectRepository.Get();
                var asds = ishave.SelectMany(x => x.Buildings).SelectMany(x => x.Boq.BoqSheets).ToList();
                var asds1 = asds.SelectMany(x => x.BoqItems).Where(x => x.CostCodeId == SelectedCostCode.Id).ToList();
                if (asds1.Count() > 0)
                {
                    MessageBox.Show("Can't delete this Cost Code because it's linked to a BOQ.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                var view = new ConfirmDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                {
                    if (SelectedCostCode.Id != 0)
                    {
                        unitOfWork.CostCodeLibraryRepository.Delete(SelectedCostCode);
                        unitOfWork.Save();
                    }

                    CostCodeLibraries.Remove(SelectedCostCode);
                }
            }
        }
        private async void DeleteCostCode(object p)
        {
            var ishave = unitOfWork.ProjectRepository.Get();
            //  var asd = ishave.Where(x => x.Buildings.SelectMany(y => y.Boq.BoqSheets));
            var asds = ishave.SelectMany(x => x.Buildings).SelectMany(x => x.Boq.BoqSheets).ToList();
            var asds1 = asds.SelectMany(x => x.BoqItems).Where(x => x.CostCodeId == SelectedCostCode.Id).ToList(); 
            if(asds1.Count() > 0)
            {
                MessageBox.Show("Can't delete this Cost Code because it's linked to a BOQ.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                //if(SelectedProject.Buildings != null)
                //{
                //    foreach(var build in SelectedProject.Buildings)
                //    {
                //        if(build.Boq !=)
                //    }
                //}

                //TODO :: EXCEPTION HANDLERS FOR ALL EF OPERATIONS
                //unitOfWork.CostCodeLibraryRepository.
                if(SelectedCostCode.Id != 0)
                {
                    unitOfWork.CostCodeLibraryRepository.Delete(SelectedCostCode);
                    unitOfWork.Save();
                }
                
                CostCodeLibraries.Remove(SelectedCostCode);
            }
        }

        private void SaveCostCode(object p)
        {
            foreach (var costCode in CostCodeLibraries)
            {
                if (costCode.Id == 0)
                {
                    unitOfWork.CostCodeLibraryRepository.Insert(costCode);
                }
                else
                {
                    unitOfWork.CostCodeLibraryRepository.Update(costCode);
                }
            }
            unitOfWork.Save();
        }
    }
}
