﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ContractsDatabaseViewModel : ContractsManager.Windows.ViewModel
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        private ContractsDataset _selectedContractsDataSet;
        public ContractsDataset SelectedContractsDataSet
        {
            get { return _selectedContractsDataSet; }
            set
            {
                _selectedContractsDataSet = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedContractsDataSet");
            }
        }
        
        private ContractsDataset _selectedTerminatedContractsDataSet;
        public ContractsDataset SelectedTerminatedContractsDataset
        {
            get { return _selectedTerminatedContractsDataSet; }
            set
            {
                _selectedTerminatedContractsDataSet = value;
                NotifyPropertyChanged("SelectedTerminatedContractsDataset");
            }
        }
        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject");
            }
        }
        
        private Project _selectedProject3;
        public Project SelectedProject3
        {
            get { return _selectedProject3; }
            set
            {
                _selectedProject3 = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject3");
            }
        }
        private Subcontractor _selectedSubcontractor;
        public Subcontractor SelectedSubcontractor
        {
            get { return _selectedSubcontractor; }
            set
            {
                _selectedSubcontractor = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSubcontractor");
            }
        }
        private Subcontractor _selectedSubcontractor3;
        public Subcontractor SelectedSubcontractor3
        {
            get { return _selectedSubcontractor3; }
            set
            {
                _selectedSubcontractor3 = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSubcontractor3");
            }
        }
        private Sheet _selectedSheet;
        public Sheet SelectedSheet
        {
            get { return _selectedSheet; }
            set
            {
                _selectedSheet = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSheet");
            }
        }
        private Sheet _selectedSheet3;
        public Sheet SelectedSheet3
        {
            get { return _selectedSheet3; }
            set
            {
                _selectedSheet3 = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSheet3");
            }
        }
           
        private VoDataset _selectedVoDataset;
        public VoDataset SelectedVoDataset
        {
            get { return _selectedVoDataset; }
            set
            {
                _selectedVoDataset = value;
                NotifyPropertyChanged("SelectedVoDataset");
            }
        }

        private bool _isApproveVisible;

        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set
            {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }

        private bool _isContractChecked;

        public bool IsContractChecked
        {
            get { return _isContractChecked; }
            set
            {
                _isContractChecked = value;
                NotifyPropertyChanged("IsContractChecked");
            }
        }

        private bool _isTerChecked;

        public bool IsTerChecked
        {
            get { return _isTerChecked; }
            set
            {
                _isTerChecked = value;
                NotifyPropertyChanged("IsTerChecked");
            }
        }
        
        private bool _isVoChecked;

        public bool IsVoChecked
        {
            get { return _isVoChecked; }
            set
            {
                _isVoChecked = value;
                NotifyPropertyChanged("IsVoChecked");
            }
        }
        private bool _isPreview;
        public bool IsPreview
        {
            get { return _isPreview; }
            set
            {
                _isPreview = value;
                NotifyPropertyChanged("IsPreview");
            }
        }

        private bool _isRejectVisible;

        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set
            {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }
        private int _slider;

        public int Slider
        {
            get { return _slider; }
            set { _slider = value;
                NotifyPropertyChanged("Slider");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors2;
        public ObservableCollection<Subcontractor> Subcontractors2
        {
            get { return _subcontractors2; }
            set
            {
                _subcontractors2 = value;
                NotifyPropertyChanged("Subcontractors2");
            }
        }
        private ObservableCollection<BoqSheet> _sheets2;
        public ObservableCollection<BoqSheet> Sheets2
        {
            get { return _sheets2; }
            set
            {
                _sheets2 = value;
                NotifyPropertyChanged("Sheets2");
            }
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged("IsBusy");
            }
        }
        public string FilePath { get; set; }
        private bool _loadMoreButton;
        public bool LoadMoreButton
        {
            get
            {
                return _loadMoreButton;
            }
            set
            {
                _loadMoreButton = value;
                NotifyPropertyChanged("LoadMoreButton");
            }
        }

        private ObservableCollection<ContractsDataset> _contractDatabases;
        public ObservableCollection<ContractsDataset> ContractDatabases
        {
            get { return _contractDatabases; }
            set
            {
                _contractDatabases = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ContractDatabases");
            }
        }
        private ObservableCollection<ContractsDataset> _terminatedContractDatabases;
        public ObservableCollection<ContractsDataset> TerminatedContractDatabases
        {
            get { return _terminatedContractDatabases; }
            set
            {
                _terminatedContractDatabases = value;
                NotifyPropertyChanged("TerminatedContractDatabases");
            }
        }
        private ObservableCollection<Project> _projects;
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Projects");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors;
        public ObservableCollection<Subcontractor> Subcontractors
        {
            get { return _subcontractors; }
            set
            {
                _subcontractors = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Subcontractors");
            }
        }
        private ObservableCollection<Sheet> _sheets;
        public ObservableCollection<Sheet> Sheets
        {
            get { return _sheets; }
            set
            {
                _sheets = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Sheets");
            }
        }
        private ObservableCollection<VoDataset> _voDatasets;
        public ObservableCollection<VoDataset> VoDatasets
        {
            get { return _voDatasets; }
            set
            {
                _voDatasets = value;
                NotifyPropertyChanged("VoDatasets");
            }
        }
        public Subcontractor SelectedSubcontractor2 { get; set; }
        private Project _SelectedProjectFilterVo;
        public Project SelectedProjectFilterVo
        {
            get { return _SelectedProjectFilterVo; }
            set
            {
                _SelectedProjectFilterVo = value;
                NotifyPropertyChanged("SelectedProjectFilterVo");
            }
        }
        public Sheet SelectedSheet2 { get; set; }

        private IDocumentPaginatorSource _document;
        public IDocumentPaginatorSource Document
        {
            get { return _document; }
            set
            {
                _document = value;
                NotifyPropertyChanged("Document");
            }
        }
        private Visibility _isGenerateHidden;
        public Visibility IsGenerateHidden
        {
            get { return _isGenerateHidden; }
            set
            {
                _isGenerateHidden = value;
                NotifyPropertyChanged("IsGenerateHidden");
            }
        }
        public int Skip { get; set; }
        public int SkipTerminate { get; set; }
        public int SkipVo { get; set; }
        public ICommand ProjectSelectedCommand => new ActionCommand(p => ProjectSeleced(p));
        public ICommand ProjectSelected3Command => new ActionCommand(p => ProjectSeleced3(p));
        public ICommand SheetSelectedCommand => new ActionCommand(p => SheetSeleced(p));
        public ICommand SheetSelected3Command => new ActionCommand(p => SheetSeleced3(p));
        public ICommand SubcontractorCommand => new ActionCommand(p => SubcontractorSeleced(p));
        public ICommand Subcontractor3Command => new ActionCommand(p => SubcontractorSeleced3(p));
        public ICommand PreviewCommand => new ActionCommand(p => Preview(p));
        public ICommand PreviewTerminatedCommand => new ActionCommand(p => PreviewTerminated(p));
        public ICommand PreviewVoCommand => new ActionCommand(p => PreviewVo(p));
        public ICommand GenerateCommand => new ActionCommand(p => Generate(p));
        public ICommand GenerateAfterPreviewCommand => new ActionCommand(p => GenerateAfterPreview(p));
        public ICommand GenerateVoCommand => new ActionCommand(p => GenerateVo(p));
        public ICommand GenerateVoAfterPreviewCommand => new ActionCommand(p => GenerateVoAfterPreview(p));
        public ICommand SaveCommand => new ActionCommand(p => Save(p));
        public ICommand SaveVOCommand => new ActionCommand(p => SaveVO(p));
        public ICommand DownloadVoCommand => new ActionCommand(p => SaveVO(p));
        public ICommand DownloadCommand => new ActionCommand(p => Save(p));
        public ICommand VoCommand => new ActionCommand(p => VoSlid(p));
        public ICommand TerminatedCommand => new ActionCommand(p => TerminatedSlid(p));
        public ICommand ContractCommand => new ActionCommand(p => ContractSlid(p));
        public ICommand SubcontractorSelected2Command => new ActionCommand(p => SubcontractorSelected2(p));
        public ICommand ProjectSelected2Command => new ActionCommand(p => ProjectSelected2(p));
        public ICommand SheetSelected2Command => new ActionCommand(p => SheetSelected2(p));
        public ICommand LoadVoCommand => new ActionCommand(p => LoadVo(p));
        public ICommand LoadContractsCommand => new ActionCommand(p => LoadContracts(p));
        public ICommand LoadTerminatedContractsCommand => new ActionCommand(p => LoadTerminatedContracts(p));
        public ICommand ClearVoFilterCommand => new ActionCommand(p => ClearVoFilter(p));
        public ICommand ClearFilterCommand => new ActionCommand(p => ClearFilter(p));
        public ICommand ClearFilterTerminatedCommand => new ActionCommand(p => ClearFilterTerminated(p));


        private void ClearFilterTerminated(object p)
        {
            SkipTerminate = 0;
            SelectedProject3 = null;
            SelectedSubcontractor3= null;
            SelectedSheet3 = null;
            Task.Factory.StartNew(() =>
            {

                //  var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();
                var cdatasets = unitOfWork.ContractsDatasetRepository.Get(x => x.ContractDatasetStatus == ContractDatasetStatus.Terminated ,includeProperties: "ContractBoqItem,Project,Project.Buildings", orderBy: y => y.OrderByDescending(x => x.Created), skip: SkipTerminate, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          TerminatedContractDatabases = new ObservableCollection<ContractsDataset>();
                          TerminatedContractDatabases.AddRange(cdatasets);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in cdatasets)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void ClearFilter(object p)
        {
            Skip = 0;
            SelectedProject = null;
            SelectedSubcontractor = null;
            SelectedSheet = null;
            Task.Factory.StartNew(() =>
            {

                //  var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();
                var cdatasets = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem,Project,Project.Buildings", orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractDatabases = new ObservableCollection<ContractsDataset>();
                          ContractDatabases.AddRange(cdatasets);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in cdatasets)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void ClearVoFilter(object p)
        {
            SkipVo = 0;
            SelectedProjectFilterVo = null;
            SelectedSubcontractor2 = null;
            SelectedSheet2 = null;
            Task.Factory.StartNew(() =>
            {

                var vos = unitOfWork.VoDatasetRepository.Get(orderBy: x => x.OrderByDescending(y => y.Created), skip: SkipVo, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          VoDatasets = new ObservableCollection<VoDataset>();
                          VoDatasets.AddRange(vos);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in vos)
                              {
                                  item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void SubcontractorSelected2(object p)
        {
            if (SelectedSubcontractor2 == null)
                return;

            var cont = unitOfWork.VoDatasetRepository.Get(x => x.Subcontractor.Id == SelectedSubcontractor2.Id).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(cont);
            var sheets = VoDatasets.Select(x => x.ContractsDataset.BoqSheet).Distinct().ToList();
            Sheets2 = new ObservableCollection<BoqSheet>();
            Sheets2.AddRange(sheets);
            SkipVo = 0;

        }
        private void ProjectSelected2(object p)
        {
            if (SelectedProjectFilterVo == null) return;
            var sub = unitOfWork.VoDatasetRepository.Get(x => x.Project.Id == SelectedProjectFilterVo.Id).Select(x => x.Subcontractor).Distinct().ToList();
            Subcontractors = new ObservableCollection<Subcontractor>();
            Subcontractors.AddRange(sub);
            var vos = unitOfWork.VoDatasetRepository.Get(x => x.Project.Id == SelectedProjectFilterVo.Id).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(vos);
            SkipVo = 0;

        }
        private void SheetSelected2(object p)
        {
            if (SelectedSheet2 == null)
                return;
            var cont = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.BoqSheet.Name == SelectedSheet2.Name).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(cont);
            SkipVo = 0;
        }
        private void ContractSlid(object p)
        {
            Slider = 0;
        }

        private void VoSlid(object p)
        {
            Slider = 1;
        }
        private void TerminatedSlid(object p)
        {
            Slider = 2;
        }
        private void Save(object p)
        {
           if(SelectedContractsDataSet == null)
            {
                if(SelectedTerminatedContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Terminated)
                {
                    SaveFileDialog sf = new SaveFileDialog();
                    sf.FileName = SelectedTerminatedContractsDataset.ContractNumber;
                    if (sf.ShowDialog() == true)
                    {
                        System.IO.File.Copy(FilePath, sf.FileName + ".doc", true);
                    }
                    File.Delete(FilePath);
                    FilePath = "";
                }
                return;
            }
            if (SelectedContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Editable)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedContractsDataSet.ContractNumber;
                if (sf.ShowDialog() == true)
                {
                    System.IO.File.Copy(FilePath, sf.FileName+".docx", true);
                }
            }
            else
            {
                IsBusy = true;
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedContractsDataSet.ContractNumber;
                if (sf.ShowDialog() == true)
                {
                    Task.Factory.StartNew(() =>
                    {
                         
                        using (FileStream fs = new FileStream(sf.FileName + ".docx", FileMode.Create))
                        {
                            fs.Write(SelectedContractsDataSet.ContractFile.Content, 0, SelectedContractsDataSet.ContractFile.Content.Length);
                            fs.Close();
                        }
                         
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            IsBusy = false;
                        }));
                    });
                }
            }
            var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
            if (win != null)
                win.Close();
        }

        private void SaveVO(object p)
        {
            IsBusy = true;
            if (SelectedVoDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedVoDataset.ContractsDataset.ContractNumber + "-" + SelectedVoDataset.VoNumber;
                if (sf.ShowDialog() == true)
                {
                    System.IO.File.Copy(FilePath, sf.FileName + ".docx", true);
                    IsBusy = false;
                }
            }
            else
            {
                IsBusy = true;
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedVoDataset.VoNumber;
                if (sf.ShowDialog() == true)
                {
                    Task.Factory.StartNew(() =>
                    {
                        var contract = unitOfWork.VoDatasetRepository.Get(
                            x => x.Id == SelectedVoDataset.Id, includeProperties: "Contract,BoqAtt").FirstOrDefault();
                        using (FileStream fs = new FileStream(sf.FileName + ".docx", FileMode.Create))
                        {
                            fs.Write(contract.ContractFile.Content, 0, contract.ContractFile.Content.Length);
                            fs.Close();
                        }
                         
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            IsBusy = false;
                        }));
                    });
                }
            }
            var win = Application.Current.Windows.OfType<PreviewVo>().FirstOrDefault();
            if (win != null)
                win.Close();
        }
        private void Generate(object p)
        {
            if (SelectedContractsDataSet == null)
                return;

            var messageResultContract = MessageBox.Show("Do you want to save the contract", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageResultContract == MessageBoxResult.Yes)
            {
                Word w = new Word();
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedContractsDataSet.ContractNumber;
                SelectedContractsDataSet.IsGenerated = true;
                var contract = unitOfWork.ContractsDatasetRepository.Get(
                            x => x.Id == SelectedContractsDataSet.Id,
                            includeProperties: "Contract,ContractFile,Planning,UnitePrice,BoqAtt,PrescriptionTechniques,Plans,Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();

                IsBusy = true;
                if (sf.ShowDialog() == true)
                {
                    //TODO: Fix filePath Parameter when not needed
                    string filePath;
                    w.GenerateContract(contract, out filePath, sf.FileName);
                    contract.ContractFile = new ContractFile();
                    contract.ContractFile.Content = ReadFile(sf.FileName + ".docx");

                    CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                    if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                    {
                        contract.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                        var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                        var not = new Notification { Contract = contract, Type = "Contract", Creator = user, Status = false };
                        unitOfWork.Notifications.Insert(not);
                    }
                    else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                        || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                    {
                        contract.ContractDatasetStatus = ContractDatasetStatus.Active;

                    }
                    unitOfWork.ContractsDatasetRepository.Update(contract);
                    unitOfWork.Save();
                }
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
            else
            {
                Word w = new Word();
                string FileName = Path.GetTempFileName();
                SelectedContractsDataSet.IsGenerated = true;
                var contract = unitOfWork.ContractsDatasetRepository.Get(
                            x => x.Id == SelectedContractsDataSet.Id,
                            includeProperties: "Contract,ContractFile,Planning,UnitePrice,BoqAtt,PrescriptionTechniques,Plans,Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();

                IsBusy = true;
                string filePath;
                w.GenerateContract(contract,out filePath, FileName+".docx");
                contract.ContractFile = new ContractFile();
                contract.ContractFile.Content = ReadFile(FileName + ".docx");

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                {
                    contract.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    var not = new Notification { Contract = contract, Type = "Contract", Creator = user, Status = false };
                    unitOfWork.Notifications.Insert(not);
                }
                else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                    || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                {
                    contract.ContractDatasetStatus = ContractDatasetStatus.Active;

                }
                unitOfWork.ContractsDatasetRepository.Update(contract);
                unitOfWork.Save();
                
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
        }
         private void GenerateAfterPreview(object p)
        {
            if (SelectedContractsDataSet == null)
                return;

            var messageResultContract = MessageBox.Show("Do you want to save the contract", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageResultContract == MessageBoxResult.Yes)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedContractsDataSet.ContractNumber;
                

                if (sf.ShowDialog() == true)
                {
                    System.IO.File.Copy(FilePath, sf.FileName + ".docx", true);
                    SelectedContractsDataSet.ContractFile = new ContractFile();
                    SelectedContractsDataSet.ContractFile.Content = ReadFile(sf.FileName + ".docx");

                    CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                    if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                    {
                        SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                        var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                        var not = new Notification { Contract = SelectedContractsDataSet, Type = "Contract", Creator = user, Status = false };
                        unitOfWork.Notifications.Insert(not);
                    }
                    else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                        || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                    {
                        SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Active;

                    }
                    unitOfWork.ContractsDatasetRepository.Update(SelectedContractsDataSet);
                    unitOfWork.Save();
                }
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
            else
            {
                SelectedContractsDataSet.IsGenerated = true;
                SelectedContractsDataSet.ContractFile = new ContractFile();
                SelectedContractsDataSet.ContractFile.Content = ReadFile(FilePath);

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                {
                    SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    var not = new Notification { Contract = SelectedContractsDataSet, Type = "Contract", Creator = user, Status = false };
                    unitOfWork.Notifications.Insert(not);
                }
                else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                    || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                {
                    SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Active;

                }
                unitOfWork.ContractsDatasetRepository.Update(SelectedContractsDataSet);
                unitOfWork.Save();
                
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
        }

        private void GenerateVo(object p)
        {
            if (SelectedVoDataset == null)
                return;

            var messageResultContract = MessageBox.Show("Do you want to save the VO", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageResultContract == MessageBoxResult.Yes)
            {
                Word w = new Word();
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedVoDataset.ContractsDataset.ContractNumber +"-"+ SelectedVoDataset.VoNumber;
                var contract = unitOfWork.VoDatasetRepository.Get(
                            x => x.Id == SelectedVoDataset.Id,
                            includeProperties: "Contract,BoqAtt").FirstOrDefault();

                IsBusy = true;
                if (sf.ShowDialog() == true)
                {
                    string filePath;
                    w.GenerateVoContract(contract, _fileOutPath: out filePath, filePath: sf.FileName+".docx");
                    contract.ContractFile = new ContractFile();
                    contract.ContractFile.Content = ReadFile(sf.FileName + ".docx");

                    CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                    if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                    {
                        contract.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                        var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                        var not = new Notification { VoDataset = contract, Type = "VoDataset", Creator = user, Status = false };
                        unitOfWork.Notifications.Insert(not);
                    }
                    else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                        || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                    {
                        contract.ContractDatasetStatus = ContractDatasetStatus.Active;

                    }
                    unitOfWork.VoDatasetRepository.Update(contract);
                    unitOfWork.Save();
                }
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
            else
            {
                Word w = new Word();
                string FileName = Path.GetTempFileName();
                var contract = unitOfWork.VoDatasetRepository.Get(
                            x => x.Id == SelectedVoDataset.Id,
                            includeProperties: "Contract,BoqAtt").FirstOrDefault();

                IsBusy = true;
                string filePath;
                w.GenerateVoContract(contract, out filePath, FileName+".docx");
                contract.ContractFile = new ContractFile();
                contract.ContractFile.Content = ReadFile(FileName + ".docx");

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                {
                    contract.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    var not = new Notification { VoDataset = contract, Type = "VoDataset", Creator = user, Status = false };
                    unitOfWork.Notifications.Insert(not);
                }
                else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                    || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                {
                    contract.ContractDatasetStatus = ContractDatasetStatus.Active;

                }
                unitOfWork.VoDatasetRepository.Update(contract);
                unitOfWork.Save();
                
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
        }
        private void GenerateVoAfterPreview(object p)
        {
            if (SelectedVoDataset == null)
                return;

            var messageResultContract = MessageBox.Show("Do you want to save the VO", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageResultContract == MessageBoxResult.Yes)
            { 
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedVoDataset.ContractsDataset.ContractNumber + "-" + SelectedVoDataset.VoNumber;
              
                if (sf.ShowDialog() == true)
                {
                    System.IO.File.Copy(FilePath, sf.FileName + ".docx", true);
                    SelectedVoDataset.ContractFile = new ContractFile();
                    SelectedVoDataset.ContractFile.Content = ReadFile(sf.FileName + ".docx");

                    CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                    if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                    {
                        SelectedVoDataset.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                        var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                        var not = new Notification { VoDataset = SelectedVoDataset, Type = "VoDataset", Creator = user, Status = false };
                        unitOfWork.Notifications.Insert(not);
                    }
                    else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                        || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                    {
                        SelectedVoDataset.ContractDatasetStatus = ContractDatasetStatus.Active;

                    }
                    unitOfWork.VoDatasetRepository.Update(SelectedVoDataset);
                    unitOfWork.Save();
                }
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
            else
            {
                SelectedVoDataset.ContractFile = new ContractFile();
                SelectedVoDataset.ContractFile.Content = ReadFile(FilePath);

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                {
                    SelectedVoDataset.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;

                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    var not = new Notification { VoDataset = SelectedVoDataset, Type = "VoDataset", Creator = user, Status = false };
                    unitOfWork.Notifications.Insert(not);
                }
                else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                    || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                {
                    SelectedVoDataset.ContractDatasetStatus = ContractDatasetStatus.Active;

                }
                unitOfWork.VoDatasetRepository.Update(SelectedVoDataset);
                unitOfWork.Save();
                
                IsBusy = false;
                var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                if (win != null)
                    win.Close();
            }
        }

        private void PreviewTerminated(object p)
        {
            IsBusy = true;
             

                Task.Factory.StartNew(() =>
                {
                    string fileName = Path.GetTempFileName();
                    var contract = unitOfWork.ContractsDatasetRepository.Get(
                        x => x.Id == SelectedTerminatedContractsDataset.Id, includeProperties: "TerminatedFile").FirstOrDefault();
                    if(contract.TerminatedFile == null)
                    {

                        MessageBox.Show("The termination file is missed. Please contact with your local administrator for more information", "File Is Not Exist", MessageBoxButton.OK, MessageBoxImage.Error);
                        IsBusy = false; 
                        return;
                    }
                    SelectedTerminatedContractsDataset.TerminatedFile = contract.TerminatedFile;
                    using (FileStream fs = new FileStream(fileName + ".doc", FileMode.Create))
                    {
                        fs.Write(SelectedTerminatedContractsDataset.TerminatedFile.Content, 0, SelectedTerminatedContractsDataset.TerminatedFile.Content.Length);
                        fs.Close();
                    }
                    var word = new Word();
                    var doc = word.OpenContractDoc(contract, fileName);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsGenerateHidden = Visibility.Collapsed;
                        Document = doc.GetFixedDocumentSequence();
                        PreviewWord pw = new PreviewWord { DataContext = this };
                        pw.Show();
                        IsBusy = false;
                    }));
                    File.Delete(fileName);
                    FilePath = fileName + ".doc";
                });

            
        }
        private void Preview(object p)
        {
            IsBusy = true;
            if (SelectedContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Editable)
            {
              
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var contract = unitOfWork.ContractsDatasetRepository.Get(
                            x => x.Id == SelectedContractsDataSet.Id, includeProperties: "Contract,Planning,UnitePrice,BoqAtt,PrescriptionTechniques,Plans,Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();

                        Word w = new Word();
                        string filePath;
                        var doc = w.GenerateContract(contract, _fileOutPath: out filePath);
                        FilePath = filePath;
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            IsGenerateHidden = Visibility.Visible;
                            Document = doc.GetFixedDocumentSequence();
                            PreviewWord pw = new PreviewWord { DataContext = this };
                            pw.Show();
                            IsBusy = false;
                        }));
                        }
                          catch (Exception e)
                        {
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            MessageBox.Show(e.Message);
                            IsBusy = false;
                        }));
                    }
                   
                });

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    string fileName = Path.GetTempFileName();
                    var contract = unitOfWork.ContractsDatasetRepository.Get(
                        x => x.Id == SelectedContractsDataSet.Id, includeProperties: "ContractFile,Planning,UnitePrice,BoqAtt,PrescriptionTechniques,Plans,Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();
                    SelectedContractsDataSet.ContractFile = contract.ContractFile;
                    using (FileStream fs = new FileStream(fileName + ".docx", FileMode.Create))
                    {
                        fs.Write(SelectedContractsDataSet.ContractFile.Content, 0, SelectedContractsDataSet.ContractFile.Content.Length);
                        fs.Close();
                    }
                    var word = new Word();
                    var doc = word.OpenContract(contract, fileName);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsGenerateHidden = Visibility.Collapsed;
                        Document = doc.GetFixedDocumentSequence();
                        PreviewWord pw = new PreviewWord { DataContext = this };
                        pw.Show();
                        IsBusy = false;
                    }));
                    File.Delete(fileName);
                    File.Delete(fileName + ".docx");
                });
            }


        }

        private void PreviewVo(object p)
        {
            IsBusy = true;
            if (SelectedVoDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
            {
              
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var contract = unitOfWork.VoDatasetRepository.Get(
                         x => x.Id == SelectedVoDataset.Id, includeProperties: "Contract,BOQAtt").FirstOrDefault();
                        Word w = new Word();
                        string filePath;
                        var doc = w.GenerateVoContract(contract, _fileOutPath: out filePath);
                        FilePath = filePath;

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            IsGenerateHidden = Visibility.Visible;
                            Document = doc.GetFixedDocumentSequence();
                            PreviewVo pw = new PreviewVo { DataContext = this };
                            pw.Show();
                            IsBusy = false;
                        }));
                        }
                          catch (Exception e)
                        {
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            MessageBox.Show(e.Message);
                            IsBusy = false;
                        }));
                    }
                   
                });

            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    string fileName = Path.GetTempFileName();
                    var contract = unitOfWork.VoDatasetRepository.Get(
                        x => x.Id == SelectedVoDataset.Id, includeProperties: "ContractFile").FirstOrDefault();
                    SelectedVoDataset.ContractFile = contract.ContractFile;
                    using (FileStream fs = new FileStream(fileName + ".docx", FileMode.Create))
                    {
                        fs.Write(SelectedVoDataset.ContractFile.Content, 0, SelectedVoDataset.ContractFile.Content.Length);
                        fs.Close();
                    }
                    var word = new Word();
                    var doc = word.OpenVo(contract, fileName);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsGenerateHidden = Visibility.Collapsed;
                        Document = doc.GetFixedDocumentSequence();
                        PreviewVo pw = new PreviewVo { DataContext = this };
                        pw.Show();
                        IsBusy = false;
                    }));
                    File.Delete(fileName);
                    File.Delete(fileName + ".docx");
                });
            }


        }
        private void LoadContracts(object p)
        {
            Skip += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {
                
                var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending( y => y.Created), skip: Skip, paginate: true).ToList();
                
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractDatabases.AddRange(c);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in c)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void LoadTerminatedContracts(object p)
        {
            SkipTerminate += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {
                
                var c = unitOfWork.ContractsDatasetRepository.Get(x => x.ContractDatasetStatus == ContractDatasetStatus.Terminated, includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending( y => y.Created), skip: SkipTerminate, paginate: true).ToList();
                
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          TerminatedContractDatabases.AddRange(c);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in c)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void LoadVo(object p)
        {
            SkipVo += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {

                var vos = unitOfWork.VoDatasetRepository.Get(orderBy: x => x.OrderByDescending(y => y.Created), skip: SkipVo, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          VoDatasets.AddRange(vos);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in vos)
                              {
                                  item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        public ContractsDatabaseViewModel()
        {
            Skip = 0;
            SkipVo = 0;
            IsBusy = true;
            LoadMoreButton = false;
            IsContractChecked = true;
            IsVoChecked = false;
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal.Identity.UserType == UserType.QuantitySurveyor || customPrincipal.Identity.UserType == UserType.ContractsManager
                || customPrincipal.Identity.UserType == UserType.OperationsManager)
            {
                IsPreview = true;
            }
            else
                IsPreview = false;

            Task.Factory.StartNew(() =>
            {
                if (ContractDatabases == null)
                    ContractDatabases = new ObservableCollection<ContractsDataset>();
                if (TerminatedContractDatabases == null)
                    TerminatedContractDatabases = new ObservableCollection<ContractsDataset>();
                if (VoDatasets == null)
                    VoDatasets = new ObservableCollection<VoDataset>();
                var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();
                var tc = unitOfWork.ContractsDatasetRepository.Get(x => x.ContractDatasetStatus == ContractDatasetStatus.Terminated , includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();

                var vos = unitOfWork.VoDatasetRepository.Get(includeProperties: "ContractsDataset", orderBy: x => x.OrderByDescending(y => y.Created), skip: SkipVo, paginate: true).ToList();
                var sheets = unitOfWork.SheetsRepository.Get();
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                if (Sheets == null)
                    Sheets = new ObservableCollection<Sheet>();
                var b = unitOfWork.ProjectRepository.Get();
                if (Subcontractors == null)
                    Subcontractors = new ObservableCollection<Subcontractor>();
                var s = unitOfWork.SubcontractorRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractDatabases.AddRange(c);
                          TerminatedContractDatabases.AddRange(tc);
                          VoDatasets.AddRange(vos);
                          Projects.AddRange(b);
                          Sheets.AddRange(sheets);
                          Subcontractors.AddRange(s);
                          IsBusy = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in ContractDatabases)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                              foreach (var item in TerminatedContractDatabases)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                              foreach (var item in VoDatasets)
                              {
                                  item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                              }
                          });
                      }));
            });
         
        }

        private void SubcontractorSeleced(object p)
        {
            if (SelectedSubcontractor == null)
                return;
            var sc = unitOfWork.ContractsDatasetRepository.Get( x => x.Subcontractor.Id == SelectedSubcontractor.Id, includeProperties: "ContractBoqItem").ToList();
            ContractDatabases.Clear();
            ContractDatabases.AddRange(sc);
            var sheets = ContractDatabases.Select(x => x.BoqSheet).Distinct().ToList();
            Skip = 0;
            foreach (var item in ContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            //Sheets = new ObservableCollection<BoqSheet>();
            //Sheets.AddRange(sheets);
        }

        private void ProjectSeleced(object p)
        {
            if (SelectedProject == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            var cd = unitOfWork.ContractsDatasetRepository.Get(x => x.Project.Id == SelectedProject.Id, includeProperties: "ContractBoqItem").ToList();
            ContractDatabases.Clear();
            ContractDatabases.AddRange(cd);
            foreach (var item in ContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            Skip = 0;

        }
        private void SheetSeleced(object p)
        {
            if (SelectedSheet == null)
                return;
            var sc = unitOfWork.ContractsDatasetRepository.Get(x => x.BoqSheet.Name == SelectedSheet.Name, includeProperties: "ContractBoqItem").ToList();
            ContractDatabases.Clear();
            ContractDatabases.AddRange(sc);
            foreach (var item in ContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            Skip = 0;

        }
        
        private void SubcontractorSeleced3(object p)
        {
            if (SelectedSubcontractor3 == null)
                return;
            var sc = unitOfWork.ContractsDatasetRepository.Get( x => x.Subcontractor.Id == SelectedSubcontractor3.Id, includeProperties: "ContractBoqItem").ToList();
            TerminatedContractDatabases.Clear();
            TerminatedContractDatabases.AddRange(sc);
            var sheets = TerminatedContractDatabases.Select(x => x.BoqSheet).Distinct().ToList();
            Skip = 0;
            foreach (var item in TerminatedContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            //Sheets = new ObservableCollection<BoqSheet>();
            //Sheets.AddRange(sheets);
        }

        private void ProjectSeleced3(object p)
        {
            if (SelectedProject3 == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            var cd = unitOfWork.ContractsDatasetRepository.Get(x => x.Project.Id == SelectedProject3.Id, includeProperties: "ContractBoqItem").ToList();
            TerminatedContractDatabases.Clear();
            TerminatedContractDatabases.AddRange(cd);
            foreach (var item in TerminatedContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            Skip = 0;

        }
        private void SheetSeleced3(object p)
        {
            if (SelectedSheet3 == null)
                return;
            var sc = unitOfWork.ContractsDatasetRepository.Get(x => x.BoqSheet.Name == SelectedSheet3.Name, includeProperties: "ContractBoqItem").ToList();
            TerminatedContractDatabases.Clear();
            TerminatedContractDatabases.AddRange(sc);
            foreach (var item in TerminatedContractDatabases)
            {
                item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            }
            Skip = 0;

        }

        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }
    }
}
