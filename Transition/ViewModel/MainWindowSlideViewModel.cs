﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class MainWindowSlideViewModel : ContractsManager.Windows.ViewModel
    {

        private string _text;
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                NotifyPropertyChanged("Text");
            }
        }
        private string _body;
        public string Body
        {
            get { return _body; }
            set
            {
                _body = value;
                NotifyPropertyChanged("Body");
            }
        }
        private string _username;

        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("Username");
            }
        }
        public ICommand LoginCommand => new ActionCommand(p => Login(p));
        public ICommand HelpDialogCommand => new ActionCommand(p => Help(p));

        private async void Help(object p)
        {
            var helpDialog = new HelpDialog();

            //show the dialog
            var result = await DialogHost.Show(helpDialog, "RootDialog");

            if ((string)result == "Send")
            {
                //Outlook.SendEmail(Body);
                var url = "mailto:cmsupport@seglb.com?subject=Help&body=" + Body;
                Process.Start(url);


            }
        }

        private void Login(object param)
        {
            Text = Thread.CurrentPrincipal.Identity.Name + " " + ((CustomIdentity)(Thread.CurrentPrincipal.Identity)).LastName;
        }
    }
}
