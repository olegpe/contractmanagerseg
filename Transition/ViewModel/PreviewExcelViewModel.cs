﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class PreviewExcelViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private bool _isGenerateHidden;
        public bool IsGenerateHidden
        {
            get { return _isGenerateHidden; }
            set {
                _isGenerateHidden = value;
                NotifyPropertyChanged("IsGenerateHidden");
            }
        }
        private bool _isApproveVisible;

        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }
        public string Body { get; set; }

        private bool _isRejectVisible;

        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }
      

        public Notification Not { get; set; }
        public Notification Notification { get; set; }
        public Ipc Ipc { get; set; }
        public PreviewExcelViewModel(bool status, Ipc ipc, Notification not)
        {

            Task.Factory.StartNew(() =>
            {
                IsGenerateHidden = false;
                Ipc = unitOfWork.IpcRepository.Get(x => x.Id == ipc.Id, includeProperties:"IpcFile").FirstOrDefault();
                Notification = unitOfWork.Notifications.Get(x => x.Id == not.Id).FirstOrDefault();
                Not = not;
                string fileName = System.IO.Path.GetTempFileName();
                //try
                //{
                    using (FileStream fs = new FileStream(fileName + ".xlsx", FileMode.Create))
                    {
                        fs.Write(Ipc.IpcFile.Content, 0, Ipc.IpcFile.Content.Length);
                        fs.Close();
                    }
                    var excel = new Excel(fileName + ".xlsx");
                    var doc = excel.PreviewExcel();
               // }
               // catch
               // {
               //
               // }
                
              
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        if (status == false)
                        {
                            IsApproveVisible = false;
                            IsRejectVisible = false;
                        }
                        else
                        {
                            IsApproveVisible = true;
                            IsRejectVisible = true;
                        }
                        DocumentPreview = doc.GetFixedDocumentSequence();

                        File.Delete(fileName);
                        File.Delete(fileName + ".xlsx");

                    }));
            });
        }

        private IDocumentPaginatorSource _documentPreview;
        public IDocumentPaginatorSource DocumentPreview
        {
            get { return _documentPreview; }
            set
            {
                _documentPreview = value;
                NotifyPropertyChanged("DocumentPreview");
            }
        }

        public ICommand ApproveCommand => new ActionCommand (p => Approve(p));
        public ICommand RejectCommand => new ActionCommand(p => Reject(p));
        public ICommand SaveExcelFileCommand => new ActionCommand(p => SaveExcelFile(p));

        private void SaveExcelFile(object p)
        {
           
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = Ipc.ContractsDataset.Subcontractor.Name + "-" + Ipc.ContractsDataset.ContractNumber + "-" + "IPC#" + Ipc.Number.ToString("000");
                if (sf.ShowDialog() == true)
                {

                    using (FileStream fs = new FileStream(sf.FileName + ".xlsx", FileMode.Create))
                    {
                        fs.Write(Ipc.IpcFile.Content, 0, Ipc.IpcFile.Content.Length);
                        fs.Close();
                    }
                }
              
            var win = Application.Current.Windows.OfType<PreviewExcel>().FirstOrDefault();
            if (win != null)
                win.Close();
        }
        private void Approve(object p)
        {
            //Notification.Status = true;
            //Notification.Read = true;
            IsApproveVisible = false;
            IsRejectVisible = false;
            if (Ipc.IpcStatus == IpcStatus.PendingApproval)
                Ipc.IpcStatus = IpcStatus.Issued;
            if(Ipc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.PendingApproval)
            {
                Ipc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Active;
                var conNot = unitOfWork.Notifications.Get(x => x.Contract.Id == Ipc.ContractsDataset.Id).LastOrDefault();
                unitOfWork.Notifications.Delete(conNot);
            }
            unitOfWork.IpcRepository.Update(Ipc);
            unitOfWork.Notifications.Update(Notification);
            unitOfWork.Save();

        }
        private async void Reject(object p)
        {
            var reasonDialog = new ResonDialog { DataContext = this };

            //show the dialog
            var result = await DialogHost.Show(reasonDialog, "RootDialog");

            if ((string)result == "Send")
            {
                Not.Status = true;
                Not.Read = true;
                IsApproveVisible = false;
                IsRejectVisible = false;
                Ipc.IpcStatus = IpcStatus.Editable;
                if(Ipc.ContractActivated == true)
                {
                    Ipc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Editable;
                    Ipc.ContractsDataset.IsGenerated = false;
                    Ipc.ContractsDataset.ContractFile = null;
                    var conNot = unitOfWork.Notifications.Get(x => x.Contract.Id == Ipc.ContractsDataset.Id).LastOrDefault();
                    unitOfWork.Notifications.Delete(conNot);
                }
                Ipc.ContractActivated = false;
                Ipc.IpcFile = null;
                Ipc.IsGenerated = false;

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                var not = new Notification { Type = "Response", Creator = user, Getter = Notification.Creator, Ipc = Ipc, Body = Body, Status = false };


                var venteLocal = unitOfWork.VenteLocalRepository.Get(x => x.Contract == Ipc.ContractsDataset.ContractNumber).ToList();
                var labor = Ipc.ContractsDataset.Labors;
                var machine = Ipc.ContractsDataset.Machines;

                foreach(var item in venteLocal)
                {
                    item.PrecedentAmount = item.PrecedentAmountOld;
                    unitOfWork.VenteLocalRepository.Update(item);
                }
                foreach (var item in labor)
                {
                    item.PrecedentAmount = item.PrecedentAmountOld;
                    unitOfWork.LaborRepository.Update(item);
                }
                foreach (var item in machine)
                {
                    item.PrecedentAmount = item.PrecedentAmountOld;
                    unitOfWork.MachineRepository.Update(item);
                }

                unitOfWork.Notifications.Insert(not);
                unitOfWork.IpcRepository.Update(Ipc);
                //unitOfWork.Notifications.Update(Notification);
                unitOfWork.Save();

                var win = Application.Current.Windows.OfType<PreviewExcel>().FirstOrDefault();
                win.DialogResult = false;
            }

          
            //unitOfWork.IpcRepository.Update(Ipc);
            //unitOfWork.Notifications.Update(Notification);
        }
    }
}
