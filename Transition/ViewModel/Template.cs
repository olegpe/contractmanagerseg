﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.ViewModel
{
    class Template : INotifyPropertyChanged
    {
        private string _template;
        private string _templateName;
        private string _type;

        public string Templates
        {
            get
            {
                return _template;
            }
            set
            {
                if (_template == value) return;
                _template = value;
                OnPropertyChanged("Templates");
            }
        }
        public string TemplateName
        {
            get
            {
                return _templateName;
            }
            set
            {
                if (_templateName == value) return;
                _templateName = value;
                OnPropertyChanged("TemplateName");
            }
        }
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged("Type");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
