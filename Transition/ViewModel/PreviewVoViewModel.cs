﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class PreviewVoViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private bool _isGenerateHidden;
        public bool IsGenerateHidden
        {
            get { return _isGenerateHidden; }
            set
            {
                _isGenerateHidden = value;
                NotifyPropertyChanged("IsGenerateHidden");
            }
        }
        public string Body { get; set; }
        private bool _isApproveVisible;

        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set
            {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }

        private bool _isRejectVisible;

        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set
            {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }


        public Notification Notification { get; set; }
        public Notification Not { get; set; }
        public VoDataset Contract { get; set; }
        public PreviewVoViewModel(bool status, VoDataset contract, Notification not)
        {
            IsGenerateHidden = false;
            Task.Factory.StartNew(() =>
            {
                Contract = unitOfWork.VoDatasetRepository.Get(x => x.Id == contract.Id, includeProperties: "BoqAtt, ContractFile").FirstOrDefault();
                Notification = unitOfWork.Notifications.Get(x => x.Id == not.Id).FirstOrDefault();
                Not = not;

                string fileName = Path.GetTempFileName();

                using (FileStream fs = new FileStream(fileName + ".docx", FileMode.Create))
                {
                    fs.Write(Contract.ContractFile.Content, 0, Contract.ContractFile.Content.Length);
                    fs.Close();
                }

                Word w = new Word();
                var doc = w.PreviewVoDataset(fileName);


                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        if (status == false)
                        {
                            IsApproveVisible = false;
                            IsRejectVisible = false;
                        }
                        else
                        {
                            IsApproveVisible = true;
                            IsRejectVisible = true;
                        }

                        Document = doc.GetFixedDocumentSequence();
                    }));
            });
           
          
           
           
            //var excel = new Excel(fileName + ".xlsx");
            //var doc = excel.PreviewExcel();
            //DocumentPreview = doc.GetFixedDocumentSequence();
            //File.Delete(fileName);
            //File.Delete(fileName + ".xlsx");
        }

        private IDocumentPaginatorSource _documentPreview;
        public IDocumentPaginatorSource Document
        {
            get { return _documentPreview; }
            set
            {
                _documentPreview = value;
                NotifyPropertyChanged("Document");
            }
        }

        public ICommand ApproveCommand => new ActionCommand(p => Approve(p));
        public ICommand RejectCommand => new ActionCommand(p => Reject(p));
        public ICommand SaveVOCommand => new ActionCommand(p => Save(p));

        private void Save(object p)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = Contract.VoNumber;
            if (sf.ShowDialog() == true)
            {
                Task.Factory.StartNew(() =>
                {
                       using (FileStream fs = new FileStream(sf.FileName + ".docx", FileMode.Create))
                    {
                        fs.Write(Contract.ContractFile.Content, 0, Contract.ContractFile.Content.Length);
                        fs.Close();
                    }

                     
                });
            }
            var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
            if (win != null)
                win.Close();
        }

        private void Approve(object p)
        {
            //Notification.Status = true;
            //Notification.Read = true;

            IsApproveVisible = false;
            IsRejectVisible = false;
            //ConcurrentExclusiveSchedulerPair
            if (Contract.ContractDatasetStatus == ContractDatasetStatus.PendingApproval)
                Contract.ContractDatasetStatus = ContractDatasetStatus.Active;
            unitOfWork.VoDatasetRepository.Update(Contract);
            unitOfWork.Notifications.Update(Notification);
            unitOfWork.Save();

        }
        public async Task Reject(object p)
        {
            var reasonDialog = new ResonDialog { DataContext = this };

            //show the dialog
            var result = await DialogHost.Show(reasonDialog, "RootDialog");

            if ((string)result == "Send")
            {
                Not.Status = true;
                Not.Read = true;
                IsApproveVisible = false;
                IsRejectVisible = false;
                Contract.ContractDatasetStatus = ContractDatasetStatus.Editable;
                Contract.ContractFile = null;
               
                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                var not = new Notification { Type = "Response", Creator = user, Getter = Notification.Creator, VoDataset = Contract, Body = Body, Status = false };
                unitOfWork.Notifications.Insert(not);
                //unitOfWork.ContractsDatasetRepository.Update(Contract);
                //unitOfWork.Notifications.Update(Notification);
                unitOfWork.Save();

                //var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
                //win.DialogResult = false;
                
            }

         
        }
    }
}
