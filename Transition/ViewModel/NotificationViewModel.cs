﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class NotificationViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public List<Notification> Items
        {
            get
            {
                return new List<Notification>()
                {
                    new Notification { Type = "IPC", Getter = new User { Name = "Test" },
                    Contract = new ContractsDataset { ContractNumber = "CS-Test-001" } }
                };
            }
        }

        private ObservableCollection<Data.Notification> _notifications;
        public ObservableCollection<Data.Notification> Notifications
        {
            get { return _notifications; }
            set
            {
                _notifications = value;
                NotifyPropertyChanged("Notifications");
            }
        }
        public NotificationViewModel()
        {

            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal.Identity.UserType == UserType.Admin || customPrincipal.Identity.UserType == UserType.GeneralManager)
            {
                if (Notifications == null)
                    Notifications = new ObservableCollection<Data.Notification>();
                Notifications.Clear();
                var not = unitOfWork.Notifications.Get();

                Notifications.AddRange(not.ToList());
            }
        }


        public ICommand PreviewCommand => new ActionCommand(p => Preview(p));
        //public ICommand NotifyCommand => new ActionCommand(p => Notify(p));

      

        private void Preview(object p)
        {
            var not = Notifications.Where(x => x.Id == p as int?).FirstOrDefault();
            if (not != null)
            {
                if (not.Type == "IPC")
                {

                    PreviewExcel pw = new PreviewExcel { DataContext = new PreviewExcelViewModel(not.Status, not.Ipc, not) };
                    if (pw.ShowDialog() == true)
                    {
                        unitOfWork.IpcRepository.Update(not.Ipc);
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();

                    }
                    else
                    {
                        unitOfWork.IpcRepository.Update(not.Ipc);
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();
                    }
                  
                }
                else
                {
                    PreviewWord pw = new PreviewWord { DataContext = new PreviewWordViewModel(not.Status, not.Contract, not) };
                    if (pw.ShowDialog() == true)
                    {
                        unitOfWork.ContractsDatasetRepository.Update(not.Contract);
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();

                    }
                    else
                    {
                        unitOfWork.ContractsDatasetRepository.Update(not.Contract);
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();
                    }
                   
                }
            }
        }

    }
}
