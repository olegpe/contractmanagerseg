﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.utils;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ConnectionViewModel : ContractsManager.Windows.ViewModel
    {

        #region floating Save button 


        private bool _isConnecting;
        public bool IsConnecting
        {
            get { return _isConnecting; }
            set
            {
                _isConnecting = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsConnecting");
            }
        }
        private bool _isVisible;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsVisible");
            }
        }

        private bool _isConnectionComplete;
        public bool IsConnectionComplete
        {
            get { return _isConnectionComplete; }
            set
            {
                _isConnectionComplete = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsConnectingComplete");
            }
        }

        private double _connectionProgress;
        public double ConnectionProgress
        {
            get { return _connectionProgress; }
            set
            {
                _connectionProgress = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("ConnectionProgress");
            }
        }

        #endregion

        private ObservableCollection<ConnectionModel> _connections;
        public ObservableCollection<ConnectionModel> Connections
        {
            get
            {
                return _connections;
            }
            set
            {
                _connections = value;
                NotifyPropertyChanged("Connections");
            }

        }

        private ConnectionModel _selectedConnection;
        public ConnectionModel SelectedConnection
        {
            get
            {
                return _selectedConnection;
            }
            set
            {
                _selectedConnection = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedConnection");
            }
        }
        private ConnectionModel _connect;
        public ConnectionModel Connect
        {
            get
            {
                return _connect;
            }
            set
            {
                _connect = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("Connect");
            }
        }

        private ConnectionModel _selectedCountry;
        public ConnectionModel SelectedCountry
        {
            get
            {
                return _selectedCountry;
            }
            set
            {
                _selectedCountry = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedCountry");
            }
        }
        private ObservableCollection<Data.Currency> _currencies;
        public ObservableCollection<Data.Currency> Currencies
        {
            get { return _currencies; }
            private set
            {
                _currencies = value;

                NotifyPropertyChanged("Currencies");
            }
        }
        private UnitOfWork unitOfWork = new UnitOfWork(); 
        public ICommand ConnectCommand => new ActionCommand(p => ConnectToSql(p));
        public ICommand AddConnectionCommand => new ActionCommand(p => AddConnection(p));
        public ICommand SaveCommand => new ActionCommand(p => Save(p));
        public ICommand DeleteCommand => new ActionCommand(p => Delete(p));
        public ICommand DeleteKeyCommand => new ActionCommand(p => DeleteKey(p));
        public ICommand RowEditEndingCommand => new ActionCommand(p => RowEditEnding(p));
 
        private void RowEditEnding(object p)
        {
            Connection.SetConnectionString(Connections);
        }
        private void Save(object p)
        {
            Connection.SetConnectionString(Connections);
        }

        private async void DeleteKey(object p)
        {
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {     
                var view = new ConfirmDialog();
                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                {
                    if (SelectedConnection != null)
                    {
                        if (SelectedConnection.IsCurrent == true)
                        {
                            MessageBox.Show("This is the current connection of the software. To delete this connection, connect to another one.", "Error. This is the current connection", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        Connections.Remove(SelectedConnection);
                        updateNumbering(Connections.ToList());
                        Connection.SetConnectionString(Connections);
                        var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                        win.mainMenu.DataContext = new MainMenuViewModel();
                    }
                }
            }
        }
        
        private void Delete(object p)
        {
            if (SelectedConnection != null)
            {
                if(SelectedConnection.IsCurrent == true)
                {
                    MessageBox.Show("This is the current connection for the application. To delete this connection, connect to another one.", "Error. This is the current connection", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                Connections.Remove(SelectedConnection);
                updateNumbering(Connections.ToList());
                Connection.SetConnectionString(Connections);
                var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                win.mainMenu.DataContext = new MainMenuViewModel();
            }
        }

        private void AddConnection(object p)
        {
            PasswordBox pw = p as PasswordBox;

            
            string code = "";
            if (Connections.Count <= 0)
            {
                code = "DB1";
            }
            else
                code = "DB" + (Convert.ToInt32(Regex.Match(Connections.Last().Code, @"\d+").Value) + 1).ToString();
            Connect.Code = code;
            Connect.Password = pw.Password;
            Connections.Add(Connect);
            updateNumbering(Connections.ToList());
            Connect = new ConnectionModel();
            pw.Password = "";
            Connection.SetConnectionString(Connections);
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DataContext = new MainMenuViewModel();
        }

        public ConnectionViewModel()
        {
            Connect = new ConnectionModel();
            Connections = Connection.GetConnectionStrings();
            SelectedCountry = Connections.Where(x => x.IsCurrent == true).FirstOrDefault();
            if (SelectedCountry == null)
                return;
            var curr = unitOfWork.CurrenciesRepository.Get();
            Currencies = new ObservableCollection<Currency>();
            if(curr != null)
                Currencies.AddRange(curr);

            IsVisible = false;
            
        }

        private void ConnectToSql(object param)
        {

            //Connections.Add(Connect);
            Connections.Select(c => { c.IsCurrent = false; return c; }).ToList();
            SelectedCountry.IsCurrent = true;
            IsVisible = true;
            Task.Factory.StartNew(() =>
            {
                using (var context = new DataContext(Connection.BuildConnectionString(SelectedCountry)))
                {
                    DbConnection conn = context.Database.Connection;
                    try
                    {
                        conn.Open();   // check the database connection
                        Connection.SetConnectionString(Connections);
                        IsVisible = false;
                        MessageBox.Show("The software is successfully connected now to "+SelectedCountry.Name+"." , "Connection Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch
                    {
                        MessageBox.Show("The connection attempt was unsuccessful. Please check that the Country's database is online", "Connection Failure", MessageBoxButton.OK, MessageBoxImage.Error);
                        IsVisible = false;
                    }      
                }
            });
        }

        private void updateNumbering(List<ConnectionModel> con)
        {
            for (int i = 0; i < con.Count; i++)
            {
                con[i].Code = "DB" + i + 1;
            }
        }
    }
}
