﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;

using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace ContractsManager.ViewModel
{
    public class UserViewModel : ContractsManager.Windows.ViewModel
    {
        
        private UnitOfWork unitOfWork = new UnitOfWork();

        public ObservableCollection<User> Users { get; set; }
        private ObservableCollection<object> _dataSource;
        public ObservableCollection<object> DataSource
        {
            get { return _dataSource; }
            set
            {
                _dataSource = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("DataSource");
            }
        }

        public UserViewModel()
        {
            if (Connection.GetConnectionStrings().Count > 0)
            {
                var users = unitOfWork.UsersRepository.Get().ToList();
                if (Users == null)
                    Users = new ObservableCollection<User>();
                Users.AddRange(users);
                Users.Add(new User { Name = "New User" });
            }
            /*DataSource = new ObservableCollection<object>();
            DataSource.AddRange(Enum.GetValues(typeof(UserType))
                            .Cast<Enum>()
                            .Select(value => new
                            {
                                (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                                value
                            })
                            .OrderBy(item => item.value)
                            .ToList());
                            */

        }
        public ICommand SaveCommand => new ActionCommand(p => SaveUser(p));
        public ICommand DeleteCommand => new ActionCommand(p => DeleteUser(p));
        public ICommand UploadFileCommand => new ActionCommand(p => UploadFile(p));

        private async void DeleteUser(object p)
        {
            User user = p as User;
            if (user.Id == 1)
                return;
            //if(user.Name == Thread.CurrentPrincipal.Identity.Name);

            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                
                unitOfWork.UsersRepository.Delete(user);
                Users.Remove(user);
                unitOfWork.Save();
            }
            //Flipper.FlipCommand.Execute()

        }
        private void SaveUser(object p)
        {
            User user = p as User;
            if(user.Id == 0)
            {
                unitOfWork.UsersRepository.Insert(user);
                Users.Add(new User { Name = "New User" });
            }
            else
            {
                 unitOfWork.UsersRepository.Update(user);
            }
           
            unitOfWork.Save();
        }

        private void UploadFile(object p)
        {
            User user = p as User;
            if (user == null) return;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() ?? false)
            {
                byte[] bytes = ReadFile(openFileDialog.FileName);
                user.Signature = bytes;
 
            }
            //fd.Title = dlgOpen.FileName;
            //context.FileDatas.InsertOnSubmit(fd);
            //context.SubmitChanges();

        }
        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }
    }
}
