﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using CurrencyConverter.Enums;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class CurrencyViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private CurrencyConverter.Converter convert = new CurrencyConverter.Converter("cc556a7f9d34f553957c");
        private ObservableCollection<ContractsManager.Data.Currency> _currencies;
        public ObservableCollection<ContractsManager.Data.Currency> Currencies
        {
            get { return _currencies; }
            set
            {
                _currencies = value;
                NotifyPropertyChanged("Currencies");
            }
        }
        public List<Configuration> Configurations { get; set; }
        private ContractsManager.Data.Currency  _selectedCurrency { get; set; }
        public ContractsManager.Data.Currency SelectedCurrency
        {
            get { return _selectedCurrency; }
            set
            {
                _selectedCurrency = value;
                NotifyPropertyChanged("SelectedCurrency");
            }
        }

        private string _confCurrency { get; set; }
        public string ConfCurrency
        {
            get { return _confCurrency; }
            set
            {
                _confCurrency = value;
                NotifyPropertyChanged("ConfCurrency");
            }
        }

        private double _confTax { get; set; }
        public double ConfTax
        {
            get { return _confTax; }
            set
            {
                _confTax = value;
                NotifyPropertyChanged("ConfTax");
            }
        }

        public CurrencyViewModel()
        {
            Task.Factory.StartNew(() =>
            {
                if (Currencies == null)
                    Currencies = new ObservableCollection<ContractsManager.Data.Currency>();
                if (Configurations == null)
                    Configurations = new List<Configuration>();
                var conts = unitOfWork.CurrenciesRepository.Get();
                var confgs = unitOfWork.ConfigurationRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Currencies.AddRange(conts);
                          Configurations.AddRange(confgs);
                          var cur = Configurations.Where(x => x.Key == "Currency").FirstOrDefault();
                          var tax = Configurations.Where(x => x.Key == "Tax").FirstOrDefault();
                          if (cur != null)
                              ConfCurrency = cur.Value;
                          if (tax != null)
                              ConfTax = (double)Convert.ToDouble(tax.Value);
                      }));
            });
        }

        public ICommand SaveCommand => new ActionCommand(p => Save(p));
        public ICommand SaveConfigCommand => new ActionCommand(p => SaveConfig(p));
        public ICommand DeleteCommand => new ActionCommand(p => Delete(p));
        public ICommand UpdateCurrencyCommand => new ActionCommand(p => UpdateCurrency(p));
        public ICommand RowEditEndingCommand => new ActionCommand(p => RowEditEnding(p));
        public ICommand DeleteKeyCommand => new ActionCommand(p => DeleteKey(p));

        private void UpdateCurrency(object p)
        {
            //var convert = new CurrencyConverter.Converter("cc556a7f9d34f553957c");
            foreach (var item in Currencies)
            {
                Enum.TryParse(item.Currencies, out CurrencyType myStatus);
                item.ConversionRate = (double)convert.Convert(1, CurrencyType.USD, myStatus);
            }
            unitOfWork.Save();
        }

        private void RowEditEnding(object p)
        {
            // MessageBox.Show("These changes will effect all BOQs and VOs in project", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);


            var e = p as RoutedEventArgs;
            if (SelectedCurrency == null) return;
            var currency = convert.GetAllCurrencies();
            var currencyName = currency.Where(x => x.Id == SelectedCurrency.Currencies).FirstOrDefault();
            if (currencyName == null)
                MessageBox.Show("The Currency Code Entered is illegible, Please enter it correctly.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                SelectedCurrency.Name = currencyName.CurrencyName;

            Enum.TryParse(SelectedCurrency.Currencies, out CurrencyType myStatus);
            SelectedCurrency.ConversionRate = (double)convert.Convert(1, CurrencyType.USD, myStatus);
            if (SelectedCurrency.Id > 0)
                unitOfWork.CurrenciesRepository.Update(SelectedCurrency);
            else
                unitOfWork.CurrenciesRepository.Insert(SelectedCurrency);
            unitOfWork.Save();
        }

        private void SaveConfig(object p)
        {
            if(Configurations.Count == 0)
            {
                unitOfWork.ConfigurationRepository.Insert(new Configuration { Key = "Tax", Value = ConfTax.ToString() });
                unitOfWork.ConfigurationRepository.Insert(new Configuration { Key = "Currency", Value = ConfCurrency });
            }
            else
            {
                foreach (var item in Configurations)
                {
                    if (item.Key == "Currency")
                        item.Value = ConfCurrency;
                    else if (item.Key == "Tax")
                        item.Value = ConfTax.ToString();
                    unitOfWork.ConfigurationRepository.Update(item);
                }
            }
            
            unitOfWork.Save();

        }
        private void Save(object p)
        {
           
            foreach (var item in Currencies)
            {
                if (item.Id == 0)
                {
                    unitOfWork.CurrenciesRepository.Insert(item);
                }
                else
                {
                    unitOfWork.CurrenciesRepository.Update(item);
                }
            }
            unitOfWork.Save();
        }
        private async void DeleteKey(object p)
        {
            if (SelectedCurrency == null)
                return;
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {
                var view = new ConfirmDialog();

                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                {
                    //if(SelectedProject.Buildings != null)
                    //{
                    //    foreach(var build in SelectedProject.Buildings)
                    //    {
                    //        if(build.Boq !=)
                    //    }
                    //}

                    try
                    {
                        unitOfWork.CurrenciesRepository.Delete(SelectedCurrency);
                        Currencies.Remove(SelectedCurrency);
                        unitOfWork.Save();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.InnerException != null ? ex.InnerException.Message : ex.Message, "Delete Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        //Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                    }
                }
            }
        }
        private async void Delete(object p)
        {
            var view = new ConfirmDialog();

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                //if(SelectedProject.Buildings != null)
                //{
                //    foreach(var build in SelectedProject.Buildings)
                //    {
                //        if(build.Boq !=)
                //    }
                //}

                try
                {
                    unitOfWork.CurrenciesRepository.Delete(SelectedCurrency);
                    Currencies.Remove(SelectedCurrency);
                    unitOfWork.Save();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.InnerException != null ? ex.InnerException.Message : ex.Message, "Delete Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                }
            }
        }

    }
}
