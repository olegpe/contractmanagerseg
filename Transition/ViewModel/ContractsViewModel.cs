﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ContractsViewModel : ContractsManager.Windows.ViewModel
    {

        UnitOfWork unitOfWork = new UnitOfWork(); 
        #region floating Save button 
       
        private bool _isVisible;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsVisible");
            }
        }
         private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged("IsBusy");
            }
        }
        
        private bool _isVOBusy;
        public bool IsVOBusy
        {
            get { return _isVOBusy; }
            set
            {
                _isVOBusy = value;
                NotifyPropertyChanged("IsVOBusy");
            }
        }

        private Visibility _isUploadVisible;
        public Visibility IsUploadVisible
        {
            get { return _isUploadVisible; }
            set
            {
                _isUploadVisible = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsUploadVisible");
            }
        }

        public IEnumerable<string> ContractsTypes
        {
            get
            {
                yield return "Lump Sum";
                yield return "Remeasured";
                yield return "Cost Plus";
            }
        }
        public IEnumerable<string> ContractsSecondTypes
        {
            get
            {
                yield return "Apply";
                yield return "Supply Apply";
            }
        }

        public IEnumerable<string> ContractsLanguage
        {
            get
            {
                yield return "EN";
                yield return "FR";
                yield return "AR";
            }
        }
        #endregion
        private bool _isApproveVisible;

        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set
            {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }

        private bool _isRejectVisible;

        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set
            {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }
        private Contract _selectedContract;
        private ObservableCollection<Contract> _contracts;
        public ObservableCollection<Contract> Contracts
        {
            get
            {
                return _contracts;
            }
            set
            {
                _contracts = value;
                NotifyPropertyChanged("Contracts");

            }
        }

        private ObservableCollection<VOContract> _voContracts;
        public ObservableCollection<VOContract> VOContracts
        {
            get
            {
                return _voContracts;
            }
            set
            {
                _voContracts = value;
                NotifyPropertyChanged("VOContracts");

            }
        }

        private VOContract _selectedVOContract;
        public VOContract SelectedVOContract
        {
            get
            {
                return _selectedVOContract;
            }
            set
            {
                _selectedVOContract = value;
                NotifyPropertyChanged("SelectedVOContract");
            }
        }

        public Contract SelectedContract
        {
            get
            {
                return _selectedContract;
            }
            set
            {
                _selectedContract = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedContract");
            }
        }
        private IDocumentPaginatorSource _document;
        public IDocumentPaginatorSource Document
        {
            get { return _document; }
            set
            {
                _document = value;
                NotifyPropertyChanged("Document");
            }
        }
        private Visibility _isGenerateHidden;
        public Visibility IsGenerateHidden
        {
            get { return _isGenerateHidden; }
            set
            {
                _isGenerateHidden = value;
                NotifyPropertyChanged("IsGenerateHidden");
            }
        }
        public ContractsViewModel()
        {
            IsBusy = true;
            IsVOBusy = true;
            Task.Factory.StartNew(() =>
            {
                
                var conts = unitOfWork.ContractRepository.Get();
                var voconts = unitOfWork.VOContractRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          if (Contracts == null)
                              Contracts = new ObservableCollection<Contract>();
                          if (VOContracts == null)
                              VOContracts = new ObservableCollection<VOContract>();
                          Contracts.AddRange(conts);
                          VOContracts.AddRange(voconts); 
                          IsBusy = false;
                          IsVOBusy = false;
                      }));
            });
        }

        public ICommand UploadFileCommand => new ActionCommand(p => UploadFile(p));
        public ICommand UploadVOFileCommand => new ActionCommand(p => UploadVOFile(p));
        public ICommand SaveCommand => new ActionCommand(p => SaveContracts(p));
        public ICommand SaveVoCommand => new ActionCommand(p => SaveVoContracts(p));
        public ICommand DownloadVoCommand => new ActionCommand(p => DownloadVoContracts());
        public ICommand DeleteCommand => new ActionCommand(p => DeleteContract(p));
        public ICommand DeleteVOCommand => new ActionCommand(p => DeleteVOContract(p));
        public ICommand AddingNewItemCommand => new ActionCommand(p => AddingNewItem(p));
        public ICommand AddingNewVOItemCommand => new ActionCommand(p => AddingNewVOItem(p));
        public ICommand EndingRowEditCommand => new ActionCommand(p => EndingRowEdit(p));
        public ICommand EndingRowVOEditCommand => new ActionCommand(p => EndingRowVOEdit(p));
        public ICommand SelectionChangedCommand => new ActionCommand(p => SelectionChanged(p));
        public ICommand SelectionChangedVOCommand => new ActionCommand(p => SelectionVOChanged(p));

        private void SelectionChanged(object p)
        {
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if (!(dataGrid.SelectedItem is Contract))
                SelectedContract = null;
        }
        private void SelectionVOChanged(object p)
        {
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if (!(dataGrid.SelectedItem is VOContract))
                SelectedVOContract = null;
        }

        void EndingRowEdit(object p)
        {
           var e = p as RoutedEventArgs;
            if (SelectedContract == null) return;
            if (!string.IsNullOrWhiteSpace(SelectedContract.Template) && SelectedContract.Content != null && !string.IsNullOrWhiteSpace(SelectedContract.Type) && !string.IsNullOrWhiteSpace(SelectedContract.SecondType))

                SaveContracts(null);
         
            // 
        }

        void EndingRowVOEdit(object p)
        {
           var e = p as RoutedEventArgs;
            if (SelectedVOContract == null) return;
            if (!string.IsNullOrWhiteSpace(SelectedVOContract.Name) && SelectedVOContract.Content != null)

                SaveVoContracts(null);
         
            // 
        }
        private void AddingNewItem(object p)
        {
            string code = "";
            if (Contracts.Count <= 0)
            {
                code = "C001";
            }
            else
                code = "C" + (Convert.ToInt32(Regex.Match(Contracts.Last().TemplateNumber, @"\d+").Value) + 1).ToString("000");



            (p as AddingNewItemEventArgs).NewItem = new Data.Contract
            {
                TemplateNumber = code //whatever you put here will be the default value
            };
        }
         private void AddingNewVOItem(object p)
        {
            string code = "";
            if (VOContracts.Count <= 0)
            {
                code = "V001";
            }
            else
                code = "V" + (Convert.ToInt32(Regex.Match(VOContracts.Last().TemplateNumber, @"\d+").Value) + 1).ToString("000");



            (p as AddingNewItemEventArgs).NewItem = new Data.VOContract
            {
                TemplateNumber = code //whatever you put here will be the default value
            };
        }

        private async void DeleteContract(object p)
        {
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                unitOfWork.ContractRepository.Delete(SelectedContract);
                Contracts.Remove(SelectedContract);
                unitOfWork.Save();
            }
        }

        private async void DeleteVOContract(object p)
        {
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                unitOfWork.VOContractRepository.Delete(SelectedVOContract);
                VOContracts.Remove(SelectedVOContract);
                unitOfWork.Save();
            }
        }

        private void SaveContracts(object p)
        {

            foreach (var con in Contracts)
            {
                if (con.Id == 0)
                {
                    unitOfWork.ContractRepository.Insert(con);
                }
                else
                {
                    unitOfWork.ContractRepository.Update(con);
                }
            }
         
            unitOfWork.Save();
        }

        private void DownloadVoContracts()
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Template";
            if (sf.ShowDialog() == true)
            {
                Task.Factory.StartNew(() =>
                {
                    using (FileStream fs = new FileStream(sf.FileName + ".docx", FileMode.Create))
                    {
                        fs.Write(SelectedVOContract.Content, 0, SelectedVOContract.Content.Length);
                        fs.Close();
                    }


                });
            }
            var win = Application.Current.Windows.OfType<PreviewWord>().FirstOrDefault();
            if (win != null)
                win.Close();
        }

        private void SaveVoContracts(object p)
        {
            foreach (var con in VOContracts)
            {
                if (con.Id == 0)
                {
                    unitOfWork.VOContractRepository.Insert(con);
                }
                else
                {
                    unitOfWork.VOContractRepository.Update(con);
                }
            }
            unitOfWork.Save();
        }

        private void UploadFile(object p) 
        {
            Contract cont = Contracts.Where(x => x == SelectedContract).FirstOrDefault();
            if (cont == null) return;
            if(cont.Content != null)
            {
                IsBusy = true;
                Task.Factory.StartNew(() =>
                {
                    Word w = new Word();
                    var doc = w.PreviewContract(cont);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsGenerateHidden = Visibility.Collapsed;
                        Document = doc.GetFixedDocumentSequence();
                        PreviewWord pw = new PreviewWord { DataContext = this };
                        pw.Show();
                        IsBusy = false;
                    }));
                });
            }
            else
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Word Documents|*.doc;*.docx";
                if (openFileDialog.ShowDialog() ?? false)
                {
                    byte[] bytes = ReadFile(openFileDialog.FileName);
                    cont.Content = bytes;
                    //cont.Template = openFileDialog.SafeFileName;
                    SaveContracts(null);
                }
            }
           
            //fd.Title = dlgOpen.FileName;
            //context.FileDatas.InsertOnSubmit(fd);
            //context.SubmitChanges();

        }

        private void UploadVOFile(object p)
        {
            VOContract cont = VOContracts.Where(x => x == SelectedVOContract).FirstOrDefault();
            if (cont == null) return;
            if (cont.Content != null)
            {
                IsBusy = true;
                Task.Factory.StartNew(() =>
                {
                    Word w = new Word();
                    var doc = w.PreviewContract(cont);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsGenerateHidden = Visibility.Collapsed;
                        Document = doc.GetFixedDocumentSequence();
                        PreviewWord pw = new PreviewWord { DataContext = this };
                        pw.Show();
                        IsBusy = false;
                    }));
                });
            }
            else
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Word Documents|*.doc;*.docx";
                if (openFileDialog.ShowDialog() ?? false)
                {
                    byte[] bytes = ReadFile(openFileDialog.FileName);
                    cont.Content = bytes;
                    //cont.Template = openFileDialog.SafeFileName;
                    SaveVoContracts(null);
                }
            }

            //fd.Title = dlgOpen.FileName;
            //context.FileDatas.InsertOnSubmit(fd);
            //context.SubmitChanges();

        }
        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }
    }
}
