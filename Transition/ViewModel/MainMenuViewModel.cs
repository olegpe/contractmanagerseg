﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class MainMenuViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private int _count;
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                NotifyPropertyChanged("Count");
            }
        }
         private Visibility _isAdminVisible;
        public Visibility IsAdminVisible
        {
            get { return _isAdminVisible; }
            set
            {
                _isAdminVisible = value;
                NotifyPropertyChanged("IsAdminVisible");
            }
        }

        private bool _isNotifClicked;
        public bool IsNotifClicked
        {
            get { return _isNotifClicked; }
            set
            {
                _isNotifClicked = value;
                NotifyPropertyChanged("IsNotifClicked");
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged("IsBusy");
            }
        }
        private bool _isSubcontractorEnabled;
        public bool IsSubcontractorEnabled
        {
            get { return _isSubcontractorEnabled; }
            set
            {
                _isSubcontractorEnabled = value;
                NotifyPropertyChanged("IsSubcontractorEnabled");
            }
        }
        private bool _isBudgetEnabled;
        public bool IsBudgetEnabled
        {
            get { return _isBudgetEnabled; }
            set
            {
                _isBudgetEnabled = value;
                NotifyPropertyChanged("IsBudgetEnabled");
            }
        }
        private bool _isContractEnabled;
        public bool IsContractEnabled
        {
            get { return _isContractEnabled; }
            set
            {
                _isContractEnabled = value;
                NotifyPropertyChanged("IsContractEnabled");
            }
        }
        private bool _isDeductionEnabled;
        public bool IsDeductionEnabled
        {
            get { return _isDeductionEnabled; }
            set
            {
                _isDeductionEnabled = value;
                NotifyPropertyChanged("IsDeductionEnabled");
            }
        }
        private bool _isReportEnabled;
        public bool IsReportEnabled
        {
            get { return _isReportEnabled; }
            set
            {
                _isReportEnabled = value;
                NotifyPropertyChanged("IsReportEnabled");
            }
        }

        private ObservableCollection<Data.Notification> _notifications;
        public ObservableCollection<Data.Notification> Notifications
        {
            get { return _notifications; }
            set
            {
                _notifications = value;
                NotifyPropertyChanged("Notifications");
            }
        }

        private ObservableCollection<ConnectionModel> _connections;
        public ObservableCollection<ConnectionModel> Connections
        {
            get
            {
                return _connections;
            }
            set
            {
                _connections = value;
                NotifyPropertyChanged("Connections");
            }

        }
 private Visibility _buttons;
        public Visibility Buttons
        {
            get { return _buttons; }
            set
            {
                _buttons = value;
                NotifyPropertyChanged("Buttons");
            }
        }
        private Visibility _adminTools;
        public Visibility AdminTools
        {
            get { return _adminTools; }
            set
            {
                _adminTools = value;
                NotifyPropertyChanged("AdminTools");
            }
        }
        private ConnectionModel _selectedConnection;
        public ConnectionModel SelectedConnection
        {
            get
            {
                return _selectedConnection;
            }
            set
            {
                _selectedConnection = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedConnection");
            }
        }
        public int Skip { get; set; }
        public MainMenuViewModel()
        {
            Skip = 0;
            if (Notifications == null)
                Notifications = new ObservableCollection<Data.Notification>();
            AdminTools = Visibility.Collapsed;
            Notifications.Clear();

            Connections = Connection.GetConnectionStrings();
            SelectedConnection = Connections.Where(x => x.IsCurrent == true).FirstOrDefault();

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
            {
                Task.Factory.StartNew(() =>
                {
                    var not = unitOfWork.Notifications.Get(x => x.Read == false && (x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset"),
                        orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            
                            Notifications.Clear();
                            Notifications.AddRange(not.ToList());
                            Count = Notifications.Where(x => x.Read == false).Count();
                        }));
                });
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    if(user != null)
                    {
                        var not = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id,
                            orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true);
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {

                                Notifications.Clear();
                                Notifications.AddRange(not.ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    }
                  
                });
            }
            if (customPrincipal.Identity.UserType != UserType.GeneralManager && customPrincipal.Identity.UserType != UserType.Admin)
            {
                IsAdminVisible = Visibility.Collapsed;
            }
            else
                IsAdminVisible = Visibility.Visible;

            if (customPrincipal.Identity.UserType == UserType.QuantitySurveyor)
            {
                IsSubcontractorEnabled = false;
                IsContractEnabled = true;
                IsDeductionEnabled = true;
                IsReportEnabled = true;
                IsBudgetEnabled = true;
            }
            else if (customPrincipal.Identity.UserType == UserType.Accountant)
            {
                IsSubcontractorEnabled = false;
                IsContractEnabled = false;
                IsDeductionEnabled = false;
                IsReportEnabled = false;
                IsBudgetEnabled = false;
            }
            else
            {
                IsSubcontractorEnabled = true;
                IsContractEnabled = true;
                IsDeductionEnabled = true;
                IsReportEnabled = true;
                IsBudgetEnabled = true;
            }
        }
         
        public ICommand LoadMoreNotificationCommand => new ActionCommand(p => LoadMoreNotification(p));
        public ICommand PreviewCommand => new ActionCommand(p => Preview(p));
        public ICommand NotifyCommand => new ActionCommand(p => Notify(p));
        public ICommand AdminCommand => new ActionCommand(p => Admin(p));
        public ICommand HomeCommand => new ActionCommand(p => Home(p));
        public ICommand CountrySelectedCommand => new ActionCommand(p => ConnectToSql(p));
         
        private void LoadMoreNotification(object param)
        {
            Skip += 10;
            IsBusy = true;
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
            {
                Task.Factory.StartNew(() =>
                {
                    var not = unitOfWork.Notifications.Get(x => x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset",
                        orderBy: y => y.OrderByDescending(x => x.Read == false), skip: Skip, paginate: true);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            IsBusy = false;
                            Notifications.AddRange(not.ToList());
                            Count = Notifications.Where(x => x.Read == false).Count();
                        }));
                });
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    if (user != null)
                    {
                        var not = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id,
                            orderBy: y => y.OrderByDescending(x => x.Read == false), skip: Skip, paginate: true);
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {

                                IsBusy = false;
                                Notifications.AddRange(not.ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    }

                });
            }
        }
        private void ConnectToSql(object param)
        {

            var current = Connections.Where(x => x.IsCurrent == true).FirstOrDefault();
            if (current == SelectedConnection)
                return;
            Connections.Select(c => { c.IsCurrent = false; return c; }).ToList();
            SelectedConnection.IsCurrent = true;
            Task.Factory.StartNew(() =>
            {
                using (var context = new DataContext(Connection.BuildConnectionString(SelectedConnection)))
                {
                    DbConnection conn = context.Database.Connection;
                    try
                    {
                        conn.Open();   // check the database connection
                        Connection.SetConnectionString(Connections);
                        MessageBox.Show("The software is successfully connected now to " + SelectedConnection.Name + ".", "Connection Success", MessageBoxButton.OK, MessageBoxImage.Information);
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            MainWindow.TransiSlider.SelectedIndex = 0;
                            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                            win.AdminToolsSlide.adminTrans.SelectedIndex = 0;
                            win.mainWindowSlide.LogoutBtn_Click(null, null);
                        }));
                    }
                    catch
                    {
                        MessageBox.Show("The connection attempt was unsuccessful. Please check that the Country's database is online", "Connection Failure", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
            });
          
       
        }
        private void Home(object p)
        {
            AdminTools = Visibility.Collapsed;
            Buttons = Visibility.Visible;
        }

        private void Admin(object p)
        {
            AdminTools = Visibility.Visible;
            Buttons = Visibility.Collapsed;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.rbMainMenu2.IsChecked = true;
        }

        private async void Notify(object p)
        {
            //IsNotifClicked = !IsNotifClicked;
            var notificationDialog = new NotificationDialog { DataContext = this };

            //show the dialog
            var result = await DialogHost.Show(notificationDialog, "RootDialog");

            if(result is string)
            {
                if ((string)result == "Ok")
                {
                }
            }
            
        }

        private async void Preview(object p)
        {
            DialogHost.CloseDialogCommand.Execute(new object(), null);

            var not = Notifications.Where(x => x.Id == p as int?).FirstOrDefault();
            if (not != null)
            {
                var userCanApprove = true;
                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal.Identity.UserType == UserType.Admin || customPrincipal.Identity.UserType == UserType.GeneralManager
                    || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                    userCanApprove = false;

                if (not.Type == "IPC")
                {
                    IsBusy = true;
                   
                    PreviewExcel pw = new PreviewExcel { DataContext = new PreviewExcelViewModel(userCanApprove, not.Ipc, not) };
                    if (pw.ShowDialog() == true)
                    {
                        not.Status = true;
                        not.Read = true;
                        unitOfWork.Save();
                    }
                    else
                    {
                        
                        unitOfWork.Save();
                    }
                    Task.Factory.StartNew(() =>
                    {
                        List<Notification> newNot;
                        if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                            || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
                        {
                            newNot = unitOfWork.Notifications.Get(x => x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset").ToList();
                        }
                        else
                        {
                            var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                            newNot = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id).ToList();
                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {
                                if (Notifications == null)
                                    Notifications = new ObservableCollection<Data.Notification>();
                                Notifications.Clear();
                                Notifications.AddRange(newNot.OrderByDescending(x => x.Created).ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    });
                }
                else if (not.Type == "Response")
                {
                    var reasonDialog = new ResponseDialog { DataContext = not };

                    //show the dialog
                    var result = await DialogHost.Show(reasonDialog, "RootDialog");

                    if ((string)result == "Ok")
                    {
                        not.Read = true;
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();
                    }
                    Task.Factory.StartNew(() =>
                    {
                        List<Notification> newNot;
                        if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                            || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
                        {
                            newNot = unitOfWork.Notifications.Get(x => x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset").ToList();
                        }
                        else
                        {
                            var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                            newNot = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id).ToList();
                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {
                                if (Notifications == null)
                                    Notifications = new ObservableCollection<Data.Notification>();
                                Notifications.Clear();

                                Notifications.AddRange(newNot.OrderByDescending(x => x.Created).ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    });
                }
                else if (not.Type == "Contract")
                {
                    IsBusy = true;
                     
                    PreviewWord pw = new PreviewWord { DataContext = new PreviewWordViewModel(userCanApprove, not.Contract, not) };
                    
                    if (pw.ShowDialog() == true)
                    {
                        not.Status = true;
                        not.Read = true;
                        unitOfWork.Notifications.Update(not);
                    
                        unitOfWork.Save();
                    
                    }
                    else
                    {
                        //not.Status = true;
                        //not.Read = true;
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();
                    }
                            
                   
                    Task.Factory.StartNew(() =>
                    {
                        List<Notification> newNot;
                        if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                            || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
                        {
                            newNot = unitOfWork.Notifications.Get(x => x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset").ToList();
                        }
                        else
                        {
                            var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                            newNot = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id).ToList();

                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {
                                if (Notifications == null)
                                    Notifications = new ObservableCollection<Data.Notification>();
                                Notifications.Clear();
                                Notifications.AddRange(newNot.OrderByDescending(x => x.Created).ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    });
                }
                else if (not.Type == "VoDataset")
                {
                    IsBusy = true;
                     
                    PreviewVo pw = new PreviewVo { DataContext = new PreviewVoViewModel(userCanApprove, not.VoDataset, not) };
                    
                    if (pw.ShowDialog() == true)
                    {
                        not.Status = true;
                        not.Read = true;
                        unitOfWork.Notifications.Update(not);
                    
                        unitOfWork.Save();
                    
                    }
                    else
                    {
                        //not.Status = true;
                        //not.Read = true;
                        unitOfWork.Notifications.Update(not);
                        unitOfWork.Save();
                    }
                            
                   
                    Task.Factory.StartNew(() =>
                    {
                        var newNot = unitOfWork.Notifications.Get(x => x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset").ToList();

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {
                                if (Notifications == null)
                                    Notifications = new ObservableCollection<Data.Notification>();
                                Notifications.Clear();
                                Notifications.AddRange(newNot.OrderByDescending(x => x.Created).ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    });
                }
                IsNotifClicked = false;
                IsBusy = false;

            }
        }

        BackgroundWorker worker;
        List<Notification> not = new List<Notification>();
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal.Identity.UserType == UserType.OperationsManager || customPrincipal.Identity.UserType == UserType.GeneralManager
                || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager || customPrincipal.Identity.UserType == UserType.Admin)
            {
                Task.Factory.StartNew(() =>
                {
                    var firstNot = Notifications.FirstOrDefault();
                    not.Clear();
                    if (firstNot != null)
                    {
                        not = unitOfWork.Notifications.Get(x => (x.Type == "IPC" || x.Type == "Contract" || x.Type == "VoDataset")
                        && x.Created > Notifications.First().Created ,
                            orderBy: y => y.OrderByDescending(x => x.Created)).ToList();
                    }
                    
                    
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            foreach (var item in not)
                            {
                                Notifications.Insert(0, item);
                            }
                            Count = Notifications.Where(x => x.Read == false).Count();
                        }));
                });
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();
                    if (user != null)
                    {
                        var not = unitOfWork.Notifications.Get(x => x.Getter.Id == user.Id,
                            orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true);
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new ThreadStart(() =>
                            {

                                Notifications.Clear();
                                Notifications.AddRange(not.ToList());
                                Count = Notifications.Where(x => x.Read == false).Count();
                            }));
                    }

                });
            }
        }

        private void worker_RunWorkerCompleted(object sender,
                                                   RunWorkerCompletedEventArgs e)
        {
            //update ui once worker complete his work
        }
    }
}
