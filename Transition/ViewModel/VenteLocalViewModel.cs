﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class VenteLocalViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        private bool _isUploading;
        public bool IsUploading
        {
            get
            {
                return _isUploading;
            }
            set
            {
                _isUploading = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsUploading");
            }
        }

        private int _transIndex;

        public int TransIndex
        {
            get { return _transIndex; }
            set { _transIndex = value; NotifyPropertyChanged("TransIndex"); }
        }
        public IEnumerable<string> Units
        {
            get
            {
                yield return "ft";
                yield return "U";
                yield return "D";
                yield return "ens";
                yield return "ml";
                yield return "m2";
                yield return "m3";
                yield return "kg";
                yield return "T";
            }
        }
        private string currency;
        public string Currency
        {
            get { return currency; }
            set
            {
                currency = value;
                NotifyPropertyChanged("Currency");
            }
        }
        private ObservableCollection<Po> _pos;
        public ObservableCollection<Po> Pos
        {
            get { return _pos; }
            set
            {
                _pos = value;
                NotifyPropertyChanged("Pos");
            }
        }
        private ObservableCollection<LaborDatabase> _laborDatabases;
        public ObservableCollection<LaborDatabase> LaborDatabases
        {
            get { return _laborDatabases; }
            set
            {
                _laborDatabases = value;
                NotifyPropertyChanged("LaborDatabases");
            }
        }
        private ObservableCollection<MachineDatabase> _machineDatabases;
        public ObservableCollection<MachineDatabase> MachineDatabases
        {
            get { return _machineDatabases; }
            set
            {
                _machineDatabases = value;
                NotifyPropertyChanged("MachineDatabases");
            }
        }

        private LaborDatabase _selectedLabor;
        public LaborDatabase SelectedLabor
        {
            get
            {
                return _selectedLabor;
            }
            set
            {
                _selectedLabor = value;
                NotifyPropertyChanged("SelectedLabor");
            }
        }

        private MachineDatabase _selectedMachine;
        public MachineDatabase SelectedMachine
        {
            get
            {
                return _selectedMachine;
            }
            set
            {
                _selectedMachine = value;
                NotifyPropertyChanged("SelectedMachine");
            }
        }
        private Po _selectedPo;
        public Po SelectedPo
        {
            get
            {
                return _selectedPo;
            }
            set
            {
                _selectedPo = value;
                NotifyPropertyChanged("SelectedPo");
            }
        }
        public VenteLocalViewModel()
        {

            Pos = new ObservableCollection<Po>();
            LaborDatabases = new ObservableCollection<LaborDatabase>();
            MachineDatabases = new ObservableCollection<MachineDatabase>();
            Task.Factory.StartNew(() =>
            {
                string cur = "";
                if (Connection.GetConnectionStrings().Count > 0)
                {
                    var con = unitOfWork.ConfigurationRepository.Get(x => x.Key == "Currency").FirstOrDefault();
                    cur = con.Value;
                }

                var pos = unitOfWork.PoRepository.Get();
                var labor = unitOfWork.LaborDatabaseRepository.Get().ToList();
                var machine = unitOfWork.MachineDatabaseRepository.Get().ToList();
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Currency = cur;
                          Pos.AddRange(pos);
                          LaborDatabases.AddRange(labor);
                          MachineDatabases.AddRange(machine);
                      }));
            });
        }

        public ICommand UploadVenteLocalCommand => new ActionCommand(p => UploadVenteLocal(p));
        public ICommand DeleteCommand => new ActionCommand(p => DeleteRecord(p));
        public ICommand DeleteLaborsCommand => new ActionCommand(p => DeleteLabors(p));
        public ICommand ClearCommand => new ActionCommand(p => Clear(p));
        public ICommand SaveCommand => new ActionCommand(p => SaveRecords(p));
        public ICommand PosCommand => new ActionCommand(p => Post());
        public ICommand LaborCommand => new ActionCommand(p => Labor());
        public ICommand MachinesCommand => new ActionCommand(p => Machine());
        public ICommand RowEditEndingPoCommand => new ActionCommand(p => RowEditEndingPo(p));
        public ICommand RowEditEndingLaborCommand => new ActionCommand(p => RowEditEndingLabor(p));
        public ICommand RowEditEndingMachineCommand => new ActionCommand(p => RowEditEndingMachine(p));

        private void Clear(object p)
        {
            foreach (var item in Pos.ToList())
            {
                unitOfWork.PoRepository.Delete(item);
                Pos.Remove(item);
            }
            unitOfWork.Save();
        }
        private void RowEditEndingPo(object p)
        {
            var e = p as RoutedEventArgs;
            if (SelectedPo == null) return;
            if (SelectedPo.Id > 0)
                unitOfWork.PoRepository.Update(SelectedPo);
            else
                unitOfWork.PoRepository.Insert(SelectedPo);
            unitOfWork.Save();
        }
        private void RowEditEndingLabor(object p)
        {
            var e = p as RoutedEventArgs;
            if (SelectedLabor == null) return;
            if (SelectedLabor.Id > 0)
                unitOfWork.LaborDatabaseRepository.Update(SelectedLabor);
            else
                unitOfWork.LaborDatabaseRepository.Insert(SelectedLabor);
            unitOfWork.Save();
        }
        private void RowEditEndingMachine(object p)
        {
            var e = p as RoutedEventArgs;
            if (SelectedMachine == null) return;
            if (SelectedMachine.Id > 0)
                unitOfWork.MachineDatabaseRepository.Update(SelectedMachine);
            else
                unitOfWork.MachineDatabaseRepository.Insert(SelectedMachine);
            unitOfWork.Save();
        }

        private void Post()
        {
            TransIndex = 0;
        }

        private void Machine()
        {
            TransIndex = 1;
        }

        private void Labor()
        {
            TransIndex = 2;
        }
        private void SaveRecords(object p)
        {
            try
            {
                foreach (var vlocal in Pos)
                {
                    if (vlocal.Id == 0)
                    {
                        unitOfWork.PoRepository.Insert(vlocal);
                    }
                    else
                    {
                        unitOfWork.PoRepository.Update(vlocal);
                    }
                }
                unitOfWork.Save();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
        }

        private async void DeleteRecord(object p)
        {
            if (p != null)
            {
                System.Collections.IList items = (System.Collections.IList)p;
                List<Po> selection = items?.Cast<Po>().ToList(); 
                var view = new ConfirmDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                { 
                   foreach (var item in selection)
                   {
                       unitOfWork.PoRepository.Delete(item);
                       Pos.Remove(item);
                   }
                   unitOfWork.Save();
                } 
            }
        }
        
        private async void DeleteLabors(object p)
        {
            if (p != null)
            {
                System.Collections.IList items = (System.Collections.IList)p;
                List<LaborDatabase> selection = items?.Cast<LaborDatabase>().ToList(); 
                var view = new ConfirmDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                { 
                   foreach (var item in selection)
                   {
                       unitOfWork.LaborDatabaseRepository.Delete(item);
                       LaborDatabases.Remove(item);
                   }
                   unitOfWork.Save();
                } 
            }
        }
        private void UploadVenteLocal(object p)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() ?? false)
            {
                IsUploading = true;
                Task.Factory.StartNew(() =>
                {
                    var excel = new Excel(openFileDialog.FileName);
                    var ventes = excel.GetVenteLocal();



                    excel.Close();
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                  new ThreadStart(() =>
                  {
                      IsUploading = false;
                      Pos.AddRange(ventes);
                      foreach (var item in ventes)
                      {
                          unitOfWork.PoRepository.Insert(item);
                      }
                      unitOfWork.Save();
                  }));
                });
                //byte[] bytes = ReadFile(openFileDialog.FileName);
                //SelectedBuilding.Boq.Content = bytes;
            }
        }
    }
}
