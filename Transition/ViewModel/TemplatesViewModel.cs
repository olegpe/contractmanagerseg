﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.ViewModel
{
    class TemplatesViewModel : INotifyPropertyChanged
    {
        private Template selectedTemplate;
        public ObservableCollection<Template> Templates { get; set; }

        public Template SelectedTemplate
        {
            get { return selectedTemplate; }
            set
            {
                selectedTemplate = value;
                OnPropertyChanged("SelectedTemplate");
            }
        }
        public TemplatesViewModel()
        {
            Templates = new ObservableCollection<Template>
            {
                new Template { Templates="Code1", TemplateName="Lebanon", Type="Acronym1" },
                new Template { Templates="Code2", TemplateName="Italy", Type ="Acronym2" },
                new Template { Templates="Code3", TemplateName="Germany", Type="Acronym3" },
                new Template { Templates="Code4", TemplateName="Ukraine", Type="Acronym4" }
            };
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
