﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MahApps.Metro.Controls.Dialogs;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ReportViewModel : ContractsManager.Windows.ViewModel
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        private double _tax;
        public double Tax
        {
            get { return _tax; }
            set
            {
                _tax = value;
                NotifyPropertyChanged("Tax");
            }
        }
        private double _usd;
        public double USD
        {
            get { return _usd; }
            set
            {
                _usd = value;
                NotifyPropertyChanged("USD");
            }
        }
        private double _eur;
        public double EUR
        {
            get { return _eur; }
            set
            {
                _eur = value;
                NotifyPropertyChanged("EUR");
            }
        }

        private int _reportTrans;
        public int ReportTrans
        {
            get { return _reportTrans; }
            set
            {
                _reportTrans = value;
                NotifyPropertyChanged("ReportTrans");
            }
        }

        public Project SelectedExportProject { get; set; }
        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject");
            }
        }
        private Project _selectedSiteProject;
        public Project SelectedSiteProject
        {
            get { return _selectedSiteProject; }
            set
            {
                _selectedSiteProject = value;
                NotifyPropertyChanged("SelectedSiteProject");
            }
        }
        public List<Contract> Contracts { get; set; }
        private ObservableCollection<Project> _projects;
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Projects");
            }
        }

        private ObservableCollection<ContractsDataset> _contractsDataset;
        public ObservableCollection<ContractsDataset> ContractsDatasets
        {
            get { return _contractsDataset; }
            set
            {
                _contractsDataset = value;
                NotifyPropertyChanged("ContractsDatasets");
            }
        }
        private ObservableCollection<ContractsDataset> _contractsDataset2;
        public ObservableCollection<ContractsDataset> ContractsDatasets2
        {
            get { return _contractsDataset2; }
            set
            {
                _contractsDataset2 = value;
                NotifyPropertyChanged("ContractsDatasets2");
            }
        }
        private ObservableCollection<dynamic> _contractReports;
        public ObservableCollection<dynamic> ContractReports
        {
            get { return _contractReports; }
            set
            {
                _contractReports = value;
                NotifyPropertyChanged("ContractReports");
            }
        }
        List<Currency> Currencies { get; set; }
        private ContractsDataset _selectedContract;

        public ContractsDataset SelectedContract
        {
            get
            {
                return _selectedContract;
            }
            set
            {
                _selectedContract = value;
                NotifyPropertyChanged("SelectedContract");

            }
        }
        private double _totalBoq;
        public double TotalBoq
        {
            get { return _totalBoq; }
            set
            {
                _totalBoq = value;
                NotifyPropertyChanged("TotalBoq");
            }
        }
        private bool _isFirstChecked;
        public bool IsFirstChecked
        {
            get { return _isFirstChecked; }
            set
            {
                _isFirstChecked = value;
                NotifyPropertyChanged("IsFirstChecked");
            }
        }
        private double _totalBoq1;
        public double TotalBoq1
        {
            get { return _totalBoq1; }
            set
            {
                _totalBoq1 = value;
                NotifyPropertyChanged("TotalBoq1");
            }
        }

        private double _ommissions1;
        public double Ommissions1
        {
            get { return _ommissions1; }
            set
            {
                _ommissions1 = value;
                NotifyPropertyChanged("Ommissions1");
            }
        }
        private double _additions1;
        public double Additions1
        {
            get { return _additions1; }
            set
            {
                _additions1 = value;
                NotifyPropertyChanged("Additions1");
            }
        }
        private double _ommissions;
        public double Ommissions
        {
            get { return _ommissions; }
            set
            {
                _ommissions = value;
                NotifyPropertyChanged("Ommissions");
            }
        }
        private double _additions;
        public double Additions
        {
            get { return _additions; }
            set
            {
                _additions = value;
                NotifyPropertyChanged("Additions");
            }
        }
        private double _totalBaq;
        public double TotalBAO
        {
            get { return _totalBaq; }
            set
            {
                _totalBaq = value;
                NotifyPropertyChanged("TotalBAO");
            }
        }
        private double _totalBaq1;
        public double TotalBAO1
        {
            get { return _totalBaq1; }
            set
            {
                _totalBaq1 = value;
                NotifyPropertyChanged("TotalBAO1");
            }
        }
        private double _advancePayment;
        public double AdvancePayment
        {
            get { return _advancePayment; }
            set
            {
                _advancePayment = value;
                NotifyPropertyChanged("AdvancePayment");
            }
        }
        private double _advancePayment1;
        public double AdvancePayment1
        {
            get { return _advancePayment1; }
            set
            {
                _advancePayment1 = value;
                NotifyPropertyChanged("AdvancePayment1");
            }
        }
        private double _totalCumul;
        public double TotalCumul
        {
            get { return _totalCumul; }
            set
            {
                _totalCumul = value;
                NotifyPropertyChanged("TotalCumul");
            }
        }
        private double _totalCumul1;
        public double TotalCumul1
        {
            get { return _totalCumul1; }
            set
            {
                _totalCumul1 = value;
                NotifyPropertyChanged("TotalCumul1");
            }
        }
        private double _totalProrataCumul;
        public double TotalProrataCumul
        {
            get { return _totalProrataCumul; }
            set
            {
                _totalProrataCumul = value;
                NotifyPropertyChanged("TotalProrataCumul");
            }
        }
        private double _totalCumulDeduction;
        public double TotalCumulDeduction
        {
            get { return _totalCumulDeduction; }
            set
            {
                _totalCumulDeduction = value;
                NotifyPropertyChanged("TotalCumulDeduction");
            }
        }
        private double _retention;
        public double Retention
        {
            get { return _retention; }
            set
            {
                _retention = value;
                NotifyPropertyChanged("Retention");
            }
        }
        private double _apRecovery;
        public double ApRecovery
        {
            get { return _apRecovery; }
            set
            {
                _apRecovery = value;
                NotifyPropertyChanged("ApRecovery");
            }
        }
        private double _perceived;
        public double Perceived
        {
            get { return _perceived; }
            set
            {
                _perceived = value;
                NotifyPropertyChanged("Perceived");
            }
        }
        private double _perceivedTth;
        public double PerceivedTth
        {
            get { return _perceivedTth; }
            set
            {
                _perceivedTth = value;
                NotifyPropertyChanged("PerceivedTth");
            }
        }

        private double _totalProrataCumul1;
        public double TotalProrataCumul1
        {
            get { return _totalProrataCumul1; }
            set
            {
                _totalProrataCumul1 = value;
                NotifyPropertyChanged("TotalProrataCumul1");
            }
        }
        private double _totalCumulDeduction1;
        public double TotalCumulDeduction1
        {
            get { return _totalCumulDeduction1; }
            set
            {
                _totalCumulDeduction1 = value;
                NotifyPropertyChanged("TotalCumulDeduction1");
            }
        }
        private double _retention1;
        public double Retention1
        {
            get { return _retention1; }
            set
            {
                _retention1 = value;
                NotifyPropertyChanged("Retention1");
            }
        }
        private double _apRecovery1;
        public double ApRecovery1
        {
            get { return _apRecovery1; }
            set
            {
                _apRecovery1 = value;
                NotifyPropertyChanged("ApRecovery1");
            }
        }
        private double _perceived1;
        public double Perceived1
        {
            get { return _perceived1; }
            set
            {
                _perceived1 = value;
                NotifyPropertyChanged("Perceived1");
            }
        }
        private double _perceivedTth1;
        public double PerceivedTth1
        {
            get { return _perceivedTth1; }
            set
            {
                _perceivedTth1 = value;
                NotifyPropertyChanged("PerceivedTth1");
            }
        }

        private double _TotalNetEx;
        public double TotalNetEx
        {
            get { return _TotalNetEx; }
            set
            {
                _TotalNetEx = value;
                NotifyPropertyChanged("TotalNetEx");
            }
        }

        private IDialogCoordinator dialogCoordinator;
        public ReportViewModel(IDialogCoordinator instance)
        {
            dialogCoordinator = instance;
            IsFirstChecked = true;
            Task.Factory.StartNew(() =>
            {
                Currencies = unitOfWork.CurrenciesRepository.Get().ToList();
                var tax = unitOfWork.ConfigurationRepository.Get(x => x.Key == "Tax").FirstOrDefault();
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var b = unitOfWork.ProjectRepository.Get();


                Contracts = unitOfWork.Contracts.Get().ToList();
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Projects.AddRange(b);
                          Tax = (double)Convert.ToDouble(tax.Value);
                          Site(null);
                      }));
            });
        }

        public ICommand ProjectSiteSelectedCommand => new ActionCommand(p => SiteProjectSeleced(p));
        public ICommand ExportReportsCommand => new ActionCommand(p => ExportReports(p));

        public ICommand ProjectSelectedCommand => new ActionCommand(p => ProjectSeleced(p));
        public ICommand SiteCommand => new ActionCommand(p => Site(p));
        public ICommand RecapSiteCommand => new ActionCommand(p => RecapSite(p));
        public ICommand ComparisionSiteCommand => new ActionCommand(p => ComparisionSite(p));

        private async void ExportReports(object p)
        {
            var view = new ExportDialog { DataContext = this };

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Choose")
            {
                if (SelectedExportProject == null)
                    return;
                double count = (double)((40.0/SelectedExportProject.ContractDatasets.Count)/100.0);
                double progress = 0.1;
                USD = Currencies.FirstOrDefault(x => x.Currencies == SelectedExportProject.Currency.Currencies).ConversionRate;
                EUR = USD / Currencies.FirstOrDefault(x => x.Currencies == "EUR").ConversionRate;
                List<ContractsDataset> contracts = new List<ContractsDataset>();
                ProgressDialogController controller = await dialogCoordinator.ShowProgressAsync(this, "Exporting Reports", "Generating Reports ...");
             
                controller.SetProgress(0.05);
                {
                    await Task.Factory.StartNew(() =>
                    {
                        var allVos = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.Project.Id == SelectedExportProject.Id).Where(x => x.VoItems != null).SelectMany(x => x.VoItems).ToList();
                        var allVente = unitOfWork.VenteLocalRepository.Get();
                        
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            controller.SetProgress(progress);
                            controller.SetMessage("Calculating Data ...");
                        
                        }));

                        foreach (var item in SelectedExportProject.ContractDatasets.Where(x => x.ContractDatasetStatus == ContractDatasetStatus.Active).ToList())
                        {
                            var lastIpc = item.Ipcs.Where(x => x.IpcStatus == IpcStatus.Issued).OrderByDescending(x => x.Number).FirstOrDefault();
                            item.Contract = Contracts.Where(x => x.Id == item.ContractId).FirstOrDefault();
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                              new ThreadStart(() =>
                              {
                                  progress += count;

                                  controller.SetProgress(progress);
                                  contracts.Add(item);
                                  contracts = new List<ContractsDataset>(contracts.OrderBy(x => x.ContractNumber));

                              }));
                            item.SubName = item.Subcontractor.Name;
                            item.TotalBoq = 0;
                            item.Ommissions = 0;
                            item.Additions = 0;
                            item.TotalBAO = 0;
                            item.AdvancePayment = 0;
                            item.TotalCumul = 0;
                            item.TotalProrataCumul = 0;
                            item.TotalCumulDeduction = 0;
                            item.Retention = 0;
                            item.ApRecovery = 0;
                            item.Perceived = 0;
                            var voTotalCumul = 0.0;
                            var LastvoTotalCumul = 0.0;
                            var trade = item.BoqSheet;
                            if (trade != null)
                                item.Activity = trade.Name;
                            item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);

                            var vos = allVos.Where(x => x.VoDataset.ContractsDataset.Id == item.Id).ToList();
                            if (item.Buildings == null)
                                return;
                            foreach (var vo in item.VoDatasets)
                            {


                                var sum = vo.VoItems.Sum(z => z.Pt);
                                if (vo.Type == "Omission")
                                {
                                    voTotalCumul -= vo.VoItems.Sum(x => x.CumulQte * x.Pu);
                                    item.Ommissions += sum;
                                }
                                else
                                {
                                    voTotalCumul += vo.VoItems.Sum(x => x.CumulQte * x.Pu);
                                    item.Additions += sum;
                                }


                            }

                            item.TotalBAO = item.TotalBoq + item.Additions + item.Ommissions;
                            // advance perventage = from net amount
                            double retention, prorata, apRecovery;
                            if (lastIpc != null)
                                item.AdvancePayment = lastIpc.AdvancePaymentAmountCumul;
                            if (item.Ipcs.Where(x => x.IpcStatus != IpcStatus.Issued).Count() > 0)
                            {
                                item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.LastCumul * x.Pu);
                                item.TotalCumul -= vos.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.LastCumul * x.Pu);
                                item.TotalCumul += vos.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.LastCumul * x.Pu);

                            }
                            else
                            {
                                item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.CumulQte * x.Pu);
                                item.TotalCumul += voTotalCumul;

                            }

                            if (item.Currency.Currencies == SelectedExportProject.Currency.Currencies)
                            {
                                item.Rate = 1;
                            }
                            else
                            {
                                item.Rate = item.Currency.ConversionRate * SelectedSiteProject.Currency.ConversionRate;
                            }
                            if (item.TotalBAO != 0)
                                item.TotalCumulPercentage = item.TotalCumul / item.TotalBAO;

                            if (lastIpc != null)
                            {
                                item.Retention = lastIpc.Retention; // cumul retention of last ipc
                                if (item.TotalCumul != 0)
                                    item.RetentionPercentage = item.Retention / item.TotalCumul; // cumul retention of last ipc

                                item.TotalProrataCumul = lastIpc.Prorata; // cumul last ipc of prorata
                                if (item.TotalCumul != 0)
                                    item.TotalProrataCumulPercentage = item.TotalProrataCumul / item.TotalCumul;

                                item.ApRecovery = lastIpc.ApRecovery; // Last value of cumul advanc recovery
                                if (item.TotalCumul != 0)
                                    item.ApRecoveryPercentage = item.ApRecovery / item.TotalCumul; // Last value of cumul advanc recovery
                            }

                            //Retentio perce = rete / executed
                            //Net Executed = exec - prorata - ded
                            //Perec = / net amount
                            if (item.TotalBAO != 0)
                                item.AdvancePaymentPercentage = (item.AdvancePayment / item.TotalBAO) * 100;
                            if (lastIpc != null)
                            {
                                var sumMachine = item.Machines.Sum(x => x.UnitPrice * (x.Deduction / 100));
                                var sumLabor = item.Labors.Sum(x => x.UnitPrice * (x.Deduction / 100));
                                var sumVente = allVente.Where(x => x.Contract == item.ContractNumber).Sum(x => x.SaleUnit * (x.Deduction / 100));
                                item.TotalCumulDeduction = sumVente + sumMachine + sumLabor;
                            }

                            if (item.TotalCumul != 0)
                                item.TotalCumulDeductionPercentage = item.TotalCumulDeduction / item.TotalCumul;
                            item.NetExecuted = item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction;
                            if (item.TotalBAO != 0)
                                item.NetExecutedPercentage = item.NetExecuted / item.TotalBAO;
                            item.Perceived = item.AdvancePayment + item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction
                            - item.Retention - item.ApRecovery;
                            item.PerceivedTTC = item.Perceived * (1 + (Tax / 100));

 
                           
                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            progress = 0.5;
                           controller.SetProgress(progress);
                           controller.SetMessage("Generating Excel File ...");

                        }));
                        var excel = new Excel(new FileInfo("Reports.xlsx").FullName);
                        excel.ExportSiteReports(contracts, SelectedExportProject, controller, progress, Tax, Currencies);
                        SelectedExportProject = null;
                    });
                }
               //var excel = new Excel(new FileInfo("reports.xlsx").FullName);
               //excel.ExportSiteReports();
               //controller.SetMessage("Exporting Recap Site ...");
               //excel.ExportRecapReports();
              
            }
            else
            {
                return;
            }
        }
        private void SiteProjectSeleced(object p)
        {
            if (SelectedSiteProject == null)
                return;
            if (ContractsDatasets == null)
                ContractsDatasets = new ObservableCollection<ContractsDataset>();
            ContractsDatasets.Clear();

            TotalBoq = 0;
            Ommissions = 0;
            Additions = 0;
            TotalBAO = 0;
            AdvancePayment = 0;
            TotalCumul = 0;
            TotalProrataCumul = 0;
            TotalCumulDeduction = 0;
            Retention = 0;
            ApRecovery = 0;
            Perceived = 0;
            PerceivedTth = 0;
            TotalNetEx = 0;
            var allVos = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.Project.Id == SelectedSiteProject.Id).Where(x => x.VoItems != null).SelectMany(x => x.VoItems).ToList();
            var allVente = unitOfWork.VenteLocalRepository.Get();
            Task.Factory.StartNew(() =>
            {
                foreach (var item in SelectedSiteProject.ContractDatasets.Where(x => x.ContractDatasetStatus == ContractDatasetStatus.Active).ToList())
                {
                    var lastIpc = item.Ipcs.Where(x => x.IpcStatus == IpcStatus.Issued).OrderByDescending(x => x.Number).FirstOrDefault();
                    item.Contract = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == item.Id, includeProperties: "Contract").First().Contract;

                    item.SubName = item.Subcontractor.Name;
                    item.TotalBoq = 0;
                    item.Ommissions = 0;
                    item.Additions = 0;
                    item.TotalBAO = 0;
                    item.AdvancePayment = 0;
                    item.TotalCumul = 0;
                    item.TotalProrataCumul = 0;
                    item.TotalCumulDeduction = 0;
                    item.Retention = 0;
                    item.ApRecovery = 0;
                    item.Perceived = 0;
                    var voTotalCumul = 0.0;
                    var trade = item.BoqSheet;
                    if (trade != null)
                        item.Activity = trade.Name;
                    item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);

                    var vos = allVos.Where(x => x.VoDataset.ContractsDataset.Id == item.Id).ToList();
                    if (item.Buildings == null)
                        return;
                    foreach (var vo in item.VoDatasets)
                    {
                        var sum = vo.VoItems.Sum(z => z.Pt);
                        if (vo.Type == "Omission")
                        {
                            voTotalCumul -= vo.VoItems.Sum(x => x.CumulQte * x.Pu);
                            item.Ommissions += sum;
                        }
                        else
                        {
                            voTotalCumul += vo.VoItems.Sum(x => x.CumulQte * x.Pu);
                            item.Additions += sum;
                        }

                    }

                    item.TotalBAO = item.TotalBoq + item.Additions + item.Ommissions;
                    // advance perventage = from net amount
                    double retention, prorata, apRecovery;
                    if (lastIpc != null)
                        item.AdvancePayment = lastIpc.AdvancePaymentAmountCumul;
                    if (item.Ipcs.Where(x => x.IpcStatus != IpcStatus.Issued).Count() > 0)
                    {
                        item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.LastCumul * x.Pu);
                        item.TotalCumul -= vos.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.LastCumul * x.Pu);
                        item.TotalCumul += vos.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.LastCumul * x.Pu);

                    }
                    else
                    {
                        item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.CumulQte * x.Pu);
                        item.TotalCumul += voTotalCumul;

                    }

                    if (item.Currency.Currencies == SelectedSiteProject.Currency.Currencies)
                    {
                        item.Rate = 1;
                    }
                    else
                    {
                        item.Rate = item.Currency.ConversionRate * SelectedSiteProject.Currency.ConversionRate;
                    }
                    if (item.TotalBAO != 0)
                        item.TotalCumulPercentage = item.TotalCumul / item.TotalBAO;

                    if (lastIpc != null)
                    {
                        item.Retention = lastIpc.Retention; // cumul retention of last ipc
                        if (item.TotalCumul != 0)
                            item.RetentionPercentage = item.Retention / item.TotalCumul; // cumul retention of last ipc
                    
                        item.TotalProrataCumul = lastIpc.Prorata; // cumul last ipc of prorata
                        if (item.TotalCumul != 0)
                            item.TotalProrataCumulPercentage = item.TotalProrataCumul / item.TotalCumul;
               
                        item.ApRecovery = lastIpc.ApRecovery; // Last value of cumul advanc recovery
                        if (item.TotalCumul != 0)
                            item.ApRecoveryPercentage = item.ApRecovery / item.TotalCumul; // Last value of cumul advanc recovery
                    }

                    //Retentio perce = rete / executed
                    //Net Executed = exec - prorata - ded
                    //Perec = / net amount
                    if (item.TotalBAO != 0)
                        item.AdvancePaymentPercentage = (item.AdvancePayment / item.TotalBAO) * 100;
                    if(lastIpc != null)
                    {
                        var sumMachine = item.Machines.Sum(x => x.UnitPrice * (x.Deduction / 100));
                        var sumLabor = item.Labors.Sum(x => x.UnitPrice * (x.Deduction / 100));
                        var sumVente = allVente.Where(x => x.Contract == item.ContractNumber).Sum(x => x.SaleUnit * (x.Deduction / 100));
                        item.TotalCumulDeduction = sumVente + sumMachine + sumLabor;
                    }
                  
                    if (item.TotalCumul != 0)
                        item.TotalCumulDeductionPercentage = item.TotalCumulDeduction / item.TotalCumul;
                    item.NetExecuted = item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction;
                    if (item.TotalBAO != 0)
                        item.NetExecutedPercentage = item.NetExecuted / item.TotalBAO;
                    item.Perceived = item.AdvancePayment + item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction
                    - item.Retention - item.ApRecovery;
                    item.PerceivedTTC = item.Perceived * (1 + (Tax / 100));


                    TotalBoq += item.TotalBoq = (item.TotalBoq / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    Ommissions += item.Ommissions = (item.Ommissions / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    Additions += item.Additions = (item.Additions / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    TotalBAO += item.TotalBAO = (item.TotalBAO / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    AdvancePayment += item.AdvancePayment = (item.AdvancePayment / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    TotalCumul += item.TotalCumul = (item.TotalCumul / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    TotalProrataCumul += item.TotalProrataCumul = (item.TotalProrataCumul / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    TotalCumulDeduction += item.TotalCumulDeduction = (item.TotalCumulDeduction / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    Retention += item.Retention = (item.Retention / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    ApRecovery += item.ApRecovery = (item.ApRecovery / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    Perceived += item.Perceived = (item.Perceived / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    PerceivedTth += item.PerceivedTTC = (item.PerceivedTTC / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    TotalNetEx += item.NetExecuted = (item.NetExecuted / item.Currency.ConversionRate) * SelectedSiteProject.Currency.ConversionRate;
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new ThreadStart(() =>
                       {
                           ContractsDatasets.Add(item);
                           ContractsDatasets = new ObservableCollection<ContractsDataset>(ContractsDatasets.OrderBy(x => x.ContractNumber));

                       }));
                }
            });
        }
        private void ContractSelected(object p)
        {

        }
        private void Site(object p)
        {
            ReportTrans = 0;

        }
        private void RecapSite(object p)
        {
            ReportTrans = 1;
        }
        private void ComparisionSite(object p)
        {
            ReportTrans = 2;
        }

        private void ProjectSeleced(object p)
        {
            if (SelectedProject == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            if (ContractsDatasets2 == null)
                ContractsDatasets2 = new ObservableCollection<ContractsDataset>();
            else ContractsDatasets2.Clear();


            TotalBoq1 = 0;
            Ommissions1 = 0;
            Additions1 = 0;
            TotalBAO1 = 0;
            AdvancePayment1 = 0;
            TotalCumul1 = 0;
            TotalProrataCumul1 = 0;
            TotalCumulDeduction1 = 0;
            Retention1 = 0;
            ApRecovery1 = 0;
            Perceived1 = 0;
            PerceivedTth1 = 0;
            var sheets = unitOfWork.SheetsRepository.Get();
            USD = Currencies.FirstOrDefault(x => x.Currencies == SelectedProject.Currency.Currencies).ConversionRate;
            EUR = USD / Currencies.FirstOrDefault(x => x.Currencies == "EUR").ConversionRate;

            Task.Factory.StartNew(() =>
            {
                foreach (var sheet in sheets)
                {
                    if (sheet.Name == "Prelims")
                        continue;
                    var group = SelectedProject.ContractDatasets.Where(x => x.ContractDatasetStatus == ContractDatasetStatus.Active).Where(x => x.BoqSheet != null && x.BoqSheet.Name == sheet.Name).ToList();
                  
                    //var allVos = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.Project.Id == SelectedSiteProject.Id).Where(x => x.VoItems != null).SelectMany(x => x.VoItems).ToList();
                    var allVente = unitOfWork.VenteLocalRepository.Get();

                    var itemTotal = new ContractsDataset();
                    itemTotal.Activity = sheet.Name;
                    if (group.Count > 0)
                    {
                        foreach (var item in group)
                        {
                            var lastIpc = item.Ipcs.Where(x => x.IpcStatus == IpcStatus.Issued).OrderByDescending(x => x.Number).FirstOrDefault();
                            //item.Contract = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == item.Id, includeProperties: "Contract").First().Contract;

                            // item.SubName = item.Subcontractor.Name;
                            item.TotalBoq = 0;
                            item.Ommissions = 0;
                            item.Additions = 0;
                            item.TotalBAO = 0;
                            item.AdvancePayment = 0;
                            item.TotalCumul = 0;
                            item.TotalProrataCumul = 0;
                            item.TotalCumulDeduction = 0;
                            item.Retention = 0;
                            item.ApRecovery = 0;
                            item.Perceived = 0;
                            var voTotalCumul = 0.0;
                            var trade = item.BoqSheet;
                            if (trade != null)
                                item.Activity = trade.Name;
                            item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);

                            var vos = item.VoDatasets.SelectMany(x => x.VoItems).ToList();
                            if (item.Buildings == null)
                                return;
                            foreach (var vo in item.VoDatasets)
                            {


                                var sum = vo.VoItems.Sum(z => z.Pt);
                                voTotalCumul += vo.VoItems.Sum(x => x.CumulQte * x.Pu);
                                if (vo.Type == "Ommissions")
                                    item.Ommissions += sum;
                                else
                                    item.Additions += sum;


                            }

                            item.TotalBAO = item.TotalBoq + item.Additions + item.Ommissions;
                            // advance perventage = from net amount
                            double retention, prorata, apRecovery;
                            if (lastIpc != null)
                                item.AdvancePayment = lastIpc.AdvancePaymentAmountCumul;
                            if (item.Ipcs.Where(x => x.IpcStatus != IpcStatus.Issued).Count() > 0)
                            {
                                item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.LastCumul * x.Pu);
                                item.TotalCumul += vos.Sum(x => x.LastCumul * x.Pu);

                            }
                            else
                            {
                                item.TotalCumul = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.CumulQte * x.Pu);
                                item.TotalCumul += voTotalCumul;

                            }

                            //if (item.Currency.Currencies == SelectedSiteProject.Currency.Currencies)
                            //{
                            //    item.Rate = 1;
                            //}
                            //else
                            //{
                            //    item.Rate = item.Currency.ConversionRate * SelectedSiteProject.Currency.ConversionRate;
                            //}
                            if (item.TotalBAO != 0)
                                item.TotalCumulPercentage = item.TotalCumul / item.TotalBAO;

                            if (lastIpc != null)
                            {
                                item.Retention = lastIpc.Retention; // cumul retention of last ipc
                                if (item.TotalCumul != 0)
                                    item.RetentionPercentage = item.Retention / item.TotalCumul; // cumul retention of last ipc

                                item.TotalProrataCumul = lastIpc.Prorata; // cumul last ipc of prorata
                                if (item.TotalCumul != 0)
                                    item.TotalProrataCumulPercentage = item.TotalProrataCumul / item.TotalCumul;

                                item.ApRecovery = lastIpc.ApRecovery; // Last value of cumul advanc recovery
                                if (item.TotalCumul != 0)
                                    item.ApRecoveryPercentage = item.ApRecovery / item.TotalCumul; // Last value of cumul advanc recovery
                            }

                            //Retentio perce = rete / executed
                            //Net Executed = exec - prorata - ded
                            //Perec = / net amount
                            if (item.TotalBAO != 0)
                                item.AdvancePaymentPercentage = (item.AdvancePayment / item.TotalBAO) * 100;
                            if (lastIpc != null)
                            {
                                var sumMachine = item.Machines.Sum(x => x.UnitPrice * (x.Deduction / 100));
                                var sumLabor = item.Labors.Sum(x => x.UnitPrice * (x.Deduction / 100));
                                var sumVente = allVente.Where(x => x.Contract == item.ContractNumber).Sum(x => x.SaleUnit * (x.Deduction / 100));
                                item.TotalCumulDeduction = sumVente + sumMachine + sumLabor;
                            }

                            if (item.TotalCumul != 0)
                                item.TotalCumulDeductionPercentage = item.TotalCumulDeduction / item.TotalCumul;
                            item.NetExecuted = item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction;
                            if (item.TotalBAO != 0)
                                item.NetExecutedPercentage = item.NetExecuted / item.TotalBAO;
                            item.Perceived = item.AdvancePayment + item.TotalCumul - item.TotalProrataCumul - item.TotalCumulDeduction
                            - item.Retention - item.ApRecovery;
                            item.PerceivedTTC = item.Perceived * (1 + (Tax / 100));

                            TotalBoq1 += item.TotalBoq;
                            Ommissions1 += item.Ommissions;
                            Additions1 += item.Additions;
                            TotalBAO1 += item.TotalBAO;
                            AdvancePayment1 += item.AdvancePayment;
                            TotalCumul1 += item.TotalCumul;
                            TotalProrataCumul1 += item.TotalProrataCumul;
                            TotalCumulDeduction1 += item.TotalCumulDeduction;
                            Retention1 += item.Retention;
                            ApRecovery1 += item.ApRecovery;
                            Perceived1 += item.Perceived;
                            PerceivedTth1 += item.PerceivedTTC;

                            itemTotal.TotalBoq += item.TotalBoq;
                            itemTotal.Ommissions += item.Ommissions;
                            itemTotal.Additions += item.Additions;
                            itemTotal.TotalBAO += item.TotalBAO;
                            itemTotal.AdvancePayment += item.AdvancePayment;
                            itemTotal.TotalCumul += item.TotalCumul;
                            itemTotal.TotalProrataCumul += item.TotalProrataCumul;
                            itemTotal.TotalCumulDeduction += item.TotalCumulDeduction;
                            itemTotal.Retention += item.Retention;
                            itemTotal.ApRecovery += item.ApRecovery;
                            itemTotal.Perceived += item.Perceived;
                            itemTotal.PerceivedTTC += item.PerceivedTTC;

                            itemTotal.TotalCumulPercentage += item.TotalCumulPercentage;
                            itemTotal.RetentionPercentage += item.RetentionPercentage;
                            itemTotal.TotalProrataCumulPercentage += item.TotalProrataCumulPercentage;
                            itemTotal.ApRecoveryPercentage += item.ApRecoveryPercentage;
                            itemTotal.AdvancePaymentPercentage += item.AdvancePaymentPercentage;

                        }
                        if(itemTotal.TotalBAO != 0)
                        {
                            itemTotal.TotalCumulPercentage = itemTotal.TotalCumul / itemTotal.TotalBAO;
                            itemTotal.AdvancePaymentPercentage = (itemTotal.AdvancePayment / itemTotal.TotalBAO) * 100;
                        }
                        if (itemTotal.TotalCumul != 0)
                        {
                            itemTotal.RetentionPercentage = itemTotal.Retention / itemTotal.TotalCumul;
                            itemTotal.TotalProrataCumulPercentage = itemTotal.TotalProrataCumul / itemTotal.TotalCumul;
                            itemTotal.ApRecoveryPercentage = itemTotal.ApRecovery / itemTotal.TotalCumul;
                        }
                        


                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                              new ThreadStart(() =>
                              {
                                  ContractsDatasets2.Add(itemTotal);
                              }));

                    }
                    else
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            ContractsDatasets2.Add(new ContractsDataset { Activity = sheet.Name });
                        }));
                    }
                }
            });
             
        }

    }
}
