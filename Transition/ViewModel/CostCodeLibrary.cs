﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManager.ViewModel
{
    class CostCodeLibrary : INotifyPropertyChanged
    {

        private string _code;
        private string _en;
        private string _fr;

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (_code == value) return;
                _code = value;
                OnPropertyChanged("Code");
            }
        }
        public string En
        {
            get
            {
                return _en;
            }
            set
            {
                if (_en == value) return;
                _en = value;
                OnPropertyChanged("En");
            }
        }
        public string Fr
        {
            get
            {
                return _fr;
            }
            set
            {
                if (_fr == value) return;
                _fr = value;
                OnPropertyChanged("Fr");
            }
        }
      

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
