﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.MainSlides;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity.Core.Common.EntitySql;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class IpcViewModel : ContractsManager.Windows.ViewModel
    {
        #region Properties

        private DateTime? _fromDate;
        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                NotifyPropertyChanged("FromDate");
            }
        }
        
        private DateTime? _dateIpc;
        public DateTime? DateIpc
        {
            get { return _dateIpc; }
            set
            {
                _dateIpc = value;
                NotifyPropertyChanged("DateIpc");
            }
        }
        private DateTime? _toDate;
        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                NotifyPropertyChanged("ToDate");
            }
        }

        private UnitOfWork unitOfWork = new UnitOfWork();

        public int BuildingIndex { get; set; }

        public int IpcNumber { get; set; }
        private double _penaltyPercentage;
        public double PenaltyPercentage
        {
            get { return _penaltyPercentage; }
            set
            {
                _penaltyPercentage = value;
                _penaltyResult = (double)((_penaltyPercentage/100f) * _penalty);
                NotifyPropertyChanged("PenaltyPercentage");
                NotifyPropertyChanged("PenaltyResult");

            }
        }
        private int _diffrents;
        public int Diffrents
        {
            get { return _diffrents; }
            set {
                _diffrents = value;
                NotifyPropertyChanged("Diffrents");
                NotifyPropertyChanged("PenaltyResult");

            }
        }
        private double _penaltyResult;
        public double PenaltyResult
        {
            get { return _penaltyResult; }
            set {
                _penaltyResult = value;
                NotifyPropertyChanged("PenaltyResult");
            }
        }
        private double _penalty;
        public double Penalty
        {
            get { return _penalty; }
            set {
                _penalty = value;
                _penaltyResult = (double)((_penaltyPercentage / 100f) * _penalty);
                NotifyPropertyChanged("Penalty");
                NotifyPropertyChanged("PenaltyResult");

            }
        }
        public double RetentionIpc { get; set; }
        public double PreviousIpcAvancePayment { get; set; }
        private int _ipcTrans;
        public int IpcTrans
        {
            get { return _ipcTrans; }
            set
            {
                _ipcTrans = value;
                NotifyPropertyChanged("IpcTrans");
            }
        }
        private double _totalAmounts;
        public double TotalAmounts
        {
            get { return _totalAmounts; }
            set
            {
                _totalAmounts = value;
                NotifyPropertyChanged("TotalAmounts");
            }
        }
        private int _deductionTrans;
        public int DeductionTrans
        {
            get { return _deductionTrans; }
            set
            {
                _deductionTrans = value;
                NotifyPropertyChanged("DeductionTrans");
            }
        }
        private int _progress1Trans;
        public int Progress1Trans
        {
            get { return _progress1Trans; }
            set
            {
                _progress1Trans = value;
                NotifyPropertyChanged("Progress1Trans");
            }
        }
        private int _progressTrans;
        public int ProgressTrans
        {
            get { return _progressTrans; }
            set
            {
                _progressTrans = value;
                NotifyPropertyChanged("ProgressTrans");
            }
        }
        private double _laborSum;
        public double LaborSum
        {
            get { return _laborSum; }
             set
            {
                _laborSum = value;
                NotifyPropertyChanged("LaborSum");
            }
        }
        private double _laborConsSum;
        public double LaborConsSum
        {
            get { return _laborConsSum; }
             set
            {
                _laborConsSum = value;
                NotifyPropertyChanged("LaborConsSum");
            }
        }
        private double _venteSum;
        public double VenteSum
        {
            get { return _venteSum; }
             set
            {
                _venteSum = value;
                NotifyPropertyChanged("VenteSum");
            }
        }
        private double _venteConsSum;
        public double VenteConsSum
        {
            get { return _venteConsSum; }
             set
            {
                _venteConsSum = value;
                NotifyPropertyChanged("VenteConsSum");
            }
        }
        private double _machineSum;
        public double MachineSum
        {
            get { return _machineSum; }
             set
            {
                _machineSum = value;
                NotifyPropertyChanged("MachineSum");
            }
        }
        private double _machineConsSum;
        public double MachineConsSum
        {
            get { return _machineConsSum; }
             set
            {
                _machineConsSum = value;
                NotifyPropertyChanged("MachineConsSum");
            }
        }

        private double _boqSum;
        public double BoqSum
        {
            get { return _boqSum; }
             set
            {
                _boqSum = value;

                NotifyPropertyChanged("BoqSum");
            }
        }
        private double _boqCumulSum;
        public double BoqCumulSum
        {
            get { return _boqCumulSum; }
             set
            {
                _boqCumulSum = value;

                NotifyPropertyChanged("BoqCumulSum");
            }
        }
        private double _boqPrecedSum;
        public double BoqPrecedSum
        {
            get { return _boqPrecedSum; }
             set
            {
                _boqPrecedSum = value;

                NotifyPropertyChanged("BoqPrecedSum");
            }
        }
        private double _boqActualSum;
        public double BoqActualSum
        {
            get { return _boqActualSum; }
             set
            {
                _boqActualSum = value;
                NotifyPropertyChanged("BoqActualSum");
            }
        }
        private double _voCumulSum;
        public double VoCumulSum
        {
            get { return _voCumulSum; }
             set
            {
                _voCumulSum = value;

                NotifyPropertyChanged("VoCumulSum");
            }
        }
        private double _voSum;
        public double VoSum
        {
            get { return _voSum; }
             set
            {
                _voSum = value;
                NotifyPropertyChanged("VoSum");
            }
        }
        private double _voPrecedSum;
        public double VoPrecedSum
        {
            get { return _voPrecedSum; }
             set
            {
                _voPrecedSum = value;

                NotifyPropertyChanged("VoPrecedSum");
            }
        }
        private double _voActualSum;
        public double VoActualSum
        {
            get { return _voActualSum; }
             set
            {
                _voActualSum = value;
                NotifyPropertyChanged("VoActualSum");
            }
        }
        private int _laborTrans;
        public int LaborTrans
        {
            get { return _laborTrans; }
            set
            {
                _laborTrans = value;
                NotifyPropertyChanged("LaborTrans");
            }
        }
        private double _retentionPercentage;
        public double RetentionPercentage
        {
            get { return _retentionPercentage; }
            set
            {
                _retentionPercentage = value;
                ResultRetention = TotalBoqCum * (_retentionPercentage / 100);
                NotifyPropertyChanged("RetentionPercentage");
                NotifyPropertyChanged("ResultRetention");
            }
        }
        private double _advancePaymentPercentage;
        public double AdvancePaymentPercentage
        {
            get { return _advancePaymentPercentage; }
            set
            {
                _advancePaymentPercentage = value;
                _result = TotalBoqPt * (_advancePaymentPercentage / 100);

                NotifyPropertyChanged("AdvancePaymentPercentage");
                NotifyPropertyChanged("Result");
            }
        }
        private bool _isPreviousVoEnabled;
        public bool IsPreviousVoEnabled
        {
            get
            {
                return _isPreviousVoEnabled;
            }
            set
            {
                _isPreviousVoEnabled = value;
 
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsPreviousVoEnabled");
            }
        }
        private bool _isDownloadButtonEnabled;
        public bool IsDownloadButtonEnabled
        {
            get
            {
                return _isDownloadButtonEnabled;
            }
            set
            {
                _isDownloadButtonEnabled = value;
 
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsDownloadButtonEnabled");
            }
        }
        private bool _isNextVoEnabled;
        public bool IsNextVoEnabled
        {
            get
            {
                return _isNextVoEnabled;
            }
            set
            {
                _isNextVoEnabled = value;
                NotifyPropertyChanged("IsNextVoEnabled");
            }
        }
        private bool _interimDisabled;
        public bool InterimDisabled
        {
            get
            {
                return _interimDisabled;
            }
            set
            {
                _interimDisabled = value;
                NotifyPropertyChanged("InterimDisabled");
            }
        }
        private bool _finalDisabled;
        public bool FinalDisabled
        {
            get
            {
                return _finalDisabled;
            }
            set
            {
                _finalDisabled = value;
                NotifyPropertyChanged("FinalDisabled");
            }
        }
        private double _totalBoqPt;
        public double TotalBoqPt
        {
            get { return _totalBoqPt; }
            set
            {
                _totalBoqPt = value;
                NotifyPropertyChanged("TotalBoqPt");
            }
        }
        private double TotalBoqCumApRecovery;
        private double _totalBoqCum;
        public double TotalBoqCum
        {
            get { return _totalBoqCum; }
            set
            {
                _totalBoqCum = value;
                NotifyPropertyChanged("TotalBoqCum");
            }
        }
        private double _sum;
        public double Sum
        {
            get { return _sum; }
            private set
            {
                _sum = value;
                NotifyPropertyChanged("Sum");
            }
        }
        public string BuildingName { get; set; }
        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                NotifyPropertyChanged("SelectedProject");
            }
        }
        private Project _selectedProjectf;
        public Project SelectedProjectf
        {
            get { return _selectedProjectf; }
            set
            {
                _selectedProjectf = value;
                NotifyPropertyChanged("SelectedProjectf");
            }
        }

        private Subcontractor _selectedSubcontractor;
        public Subcontractor SelectedSubcontractor
        {
            get { return _selectedSubcontractor; }
            set
            {
                _selectedSubcontractor = value;
                NotifyPropertyChanged("SelectedSubcontractor");
            }
        }

        private Subcontractor _selectedSubcontractorf;
        public Subcontractor SelectedSubcontractorf
        {
            get { return _selectedSubcontractorf; }
            set
            {
                _selectedSubcontractorf = value;
                NotifyPropertyChanged("SelectedSubcontractorf");
            }
        }

        private ContractsDataset _selectedContractsDataset;
        public ContractsDataset SelectedContractsDataset
        {
            get { return _selectedContractsDataset; }
            set
            {
                _selectedContractsDataset = value;
                NotifyPropertyChanged("SelectedContractsDataset");
            }
        }
        private ContractsDataset _selectedContractsDatasetf;
        public ContractsDataset SelectedContractsDatasetf
        {
            get { return _selectedContractsDatasetf; }
            set
            {
                _selectedContractsDatasetf = value;
                NotifyPropertyChanged("SelectedContractsDatasetf");
            }
        }
        private Ipc _selectedIpc;
        public Ipc SelectedIpc
        {
            get { return _selectedIpc; }
            set
            {
                _selectedIpc = value;
                NotifyPropertyChanged("SelectedIpc");
            }
        }
        public Ipc Ipc { get; set; }

        private ObservableCollection<Project> _projects;
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
 
                NotifyPropertyChanged("Projects");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors;
        public ObservableCollection<Subcontractor> Subcontractors
        {
            get { return _subcontractors; }
            set
            {
                _subcontractors = value;
 
                NotifyPropertyChanged("Subcontractors");
            }
        }

        private ObservableCollection<Subcontractor> _subcontractorsf;
        public ObservableCollection<Subcontractor> Subcontractorsf
        {
            get { return _subcontractorsf; }
            set
            {
                _subcontractorsf = value;
                NotifyPropertyChanged("Subcontractorsf");
            }
        }
        private ObservableCollection<ContractBoqItem> _contBoqItem;
        public ObservableCollection<ContractBoqItem> ContBoqItems
        {
            get { return _contBoqItem; }
            set
            {
                _contBoqItem = value;
                //_contBoqItem.AddRange(value.OrderBy(x => x.BoqItem.OrderBoq));
               
                if (_contBoqItem != null)
                {
                    _contBoqItem.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (ContractBoqItem item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSale;
                            }
                        } 
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (ContractBoqItem item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotalSale;
                                item.PropertyChanged += UpdateTotalSale;
                            }
                        }
                    };
                }
                NotifyPropertyChanged("ContBoqItems");
            }
        }
        private VoDataset _selectedVoLevel;

        public VoDataset SelectedVoLevel
        {
            get { return _selectedVoLevel; }
            set {
                _selectedVoLevel = value;
                NotifyPropertyChanged("SelectedVoLevel");
            }
        }

        private ObservableCollection<VoDataset> _vos;
        public ObservableCollection<VoDataset> Vos
        {
            get { return _vos; }
            set
            {
                _vos = value;
 
                NotifyPropertyChanged("Vos");
            }
        }
        private dynamic _selectedIpcType;
        public dynamic SelectedIpcType
        {
            get { return _selectedIpcType; }
            set
            {
                _selectedIpcType = value;
                NotifyPropertyChanged("SelectedIpcType");
            }
        }
        private ObservableCollection<dynamic> _ipcTypes;
        public ObservableCollection<dynamic> IpcTypes
        {
            get { return _ipcTypes; }
            set
            {
                _ipcTypes = value;
                NotifyPropertyChanged("IpcTypes");
            }
        }

        private ObservableCollection<ContractVo> _contVo;
        public ObservableCollection<ContractVo> ContVo
        {
            get { return _contVo; }
            set
            {
                _contVo = value;
                //_contBoqItem.AddRange(value.OrderBy(x => x.BoqItem.OrderBoq));

                if (_contVo != null)
                {
                    _contVo.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (ContractVo item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotaVolSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (ContractVo item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotaVolSale;
                                item.PropertyChanged += UpdateTotaVolSale;
                            }
                        }
                    };
                }

                NotifyPropertyChanged("ContVo");
            }
        }

        public ObservableCollection<ContractBoqItem> AllContBoqItems
        {
            get; set;
        }

        public ObservableCollection<ContractVo> AllContVo
        {
            get; set;
        }

        private void UpdateTotalSale(object sender, PropertyChangedEventArgs e)
        {
            if (ContBoqItems.Count > 0)
            {
                BoqSum = ContBoqItems.Sum(x => x.Pt);
                BoqCumulSum = ContBoqItems.Sum(x => x.CumulAmount);
                BoqPrecedSum = ContBoqItems.Sum(x => x.PrecedAmount);
                BoqActualSum = ContBoqItems.Sum(x => x.ActualAmount);
            }
            if(SelectedContractsDataset != null)
            {
                TotalBoqCum = AllContBoqItems.Sum(x => x.CumulAmount);
                TotalBoqCum += AllContVo.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.CumulAmount);
                TotalBoqCum -= AllContVo.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.CumulAmount);
                TotalBoqCum *= ((double)Convert.ToDouble(SelectedContractsDataset.HoldWarranty) / 100);

                TotalBoqCumApRecovery = AllContBoqItems.Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery += AllContVo.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery -= AllContVo.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery *= ((double)Convert.ToDouble(SelectedContractsDataset.RecoverAdvance) / 100);

                var ipc = Ipcs.Where(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();
                if (ipc.Count > 0)
                    TotalBoqCum -= ipc.OrderBy(x => x.Id).Last().RetentionAmountCumul;

            }

        }
        private void UpdateTotaVolSale(object sender, PropertyChangedEventArgs e)
        {
            if (ContVo.Count > 0)
            {
                VoSum = ContVo.Sum(x => x.Pt);
                VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                VoActualSum = ContVo.Sum(x => x.ActualAmount);
            }
            if (SelectedContractsDataset != null)
            {
                TotalBoqCum = AllContBoqItems.Sum(x => x.CumulAmount);
                TotalBoqCum += AllContVo.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.CumulAmount);
                TotalBoqCum -= AllContVo.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.CumulAmount);
                TotalBoqCum *= ((double)Convert.ToDouble(SelectedContractsDataset.HoldWarranty) / 100);

                TotalBoqCumApRecovery = AllContBoqItems.Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery += AllContVo.Where(x => x.VoDataset.Type == "Addition").Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery -= AllContVo.Where(x => x.VoDataset.Type == "Omission").Sum(x => x.CumulAmount);
                TotalBoqCumApRecovery *= ((double)Convert.ToDouble(SelectedContractsDataset.RecoverAdvance) / 100);

                var ipc = Ipcs.Where(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();
                if (ipc.Count > 0)
                    TotalBoqCum -= ipc.OrderBy(x => x.Id).Last().RetentionAmountCumul;
            }
                
        }

        private ObservableCollection<Labor> _labors;
        public ObservableCollection<Labor> Labors
        {
            get { return _labors; }
            set
            {
                _labors = value;
                if (_labors != null)
                {
                    _labors.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (Labor item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateLaborTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (Labor item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateLaborTotalSale;
                                item.PropertyChanged += UpdateLaborTotalSale;
                            }
                        }
                    };
                }


                LaborConsSum = (double)Labors.Sum(x => x.Amount);
                LaborSum = (double)Labors.Sum(x => x.ActualAmount);

                NotifyPropertyChanged("Labors");
      
            }
        }
        private void UpdateLaborTotalSale(object sender, PropertyChangedEventArgs e)
        {

            if (Labors.Count == 0) return;
            LaborConsSum = (double)Labors.Sum(x => x.Amount);
            LaborSum = (double)Labors.Sum(x => x.ActualAmount);
        }

        private ObservableCollection<VenteLocal> _venteLocals;
        public ObservableCollection<VenteLocal> VenteLocals
        {
            get { return _venteLocals; }
            set
            {
                _venteLocals = value;
                if (_venteLocals != null)
                {
                    _venteLocals.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (VenteLocal item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateVentTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (VenteLocal item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateVentTotalSale;
                                item.PropertyChanged += UpdateVentTotalSale;
                            }
                        }
                    };
                }
                VenteConsSum = VenteLocals.Sum(x => x.ConsumedAmount);
                VenteSum = VenteLocals.Sum(x => x.ActualAmount);
                NotifyPropertyChanged("VenteLocals");
      
            }
        }

        private void UpdateVentTotalSale(object sender, PropertyChangedEventArgs e)
        {
            if (VenteLocals.Count == 0) return;

            VenteConsSum = VenteLocals.Sum(x => x.ConsumedAmount);
            VenteSum = VenteLocals.Sum(x => x.ActualAmount);
        }

        private ObservableCollection<Machine> _machines;
        public ObservableCollection<Machine> Machines
        {
            get { return _machines; }
            set
            {
                _machines = value;
                if (_machines != null)
                {
                    _machines.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (Machine item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateMachineTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (Machine item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateMachineTotalSale;
                                item.PropertyChanged += UpdateMachineTotalSale;
                            }
                        }
                    };
                }
                MachineConsSum = (double)Machines.Sum(x => x.Amount);
                MachineSum = (double)Machines.Sum(x => x.ActualAmount);
                NotifyPropertyChanged("Machines");
      
            }
        }

        private ObservableCollection<Sheet> _boqSheets;
        public ObservableCollection<Sheet> BoqSheets
        {
            get { return _boqSheets; }
            set
            {
                _boqSheets = value;
                NotifyPropertyChanged("BoqSheets");
            }
        }
        private ObservableCollection<BoqSheet> _voSheets;
        public ObservableCollection<BoqSheet> VOSheets
        {
            get { return _voSheets; }
            set
            {
                _voSheets = value;
                NotifyPropertyChanged("VOSheets");
            }
        }

        private void UpdateMachineTotalSale(object sender, PropertyChangedEventArgs e)
        {
            if (Machines.Count == 0) return;

            MachineConsSum = (double)Machines.Sum(x => x.Amount);
            MachineSum = (double)Machines.Sum(x => x.ActualAmount);
        }

        private ObservableCollection<Ipc> _ipcs;
        public ObservableCollection<Ipc> Ipcs
        {
            get { return _ipcs; }
            set
            {
                _ipcs = value;
 
                NotifyPropertyChanged("Ipcs");
      
            }
        }

        private ObservableCollection<ContractsDataset> _contractDatasets;
        public ObservableCollection<ContractsDataset> ContractDatasets
        {
            get { return _contractDatasets; }
            set
            {
                _contractDatasets = value;
 
                NotifyPropertyChanged("ContractDatasets");
            }
        }private ObservableCollection<Building> _buildings;
        public ObservableCollection<Building> Buildings
        {
            get { return _buildings; }
            set
            {
                _buildings = value;
 
                NotifyPropertyChanged("Buildings");
            }
        }

        private Building _selectedbuidling;
        public Building SelectedBuilding
        {
            get { return _selectedbuidling; }
            set
            {
                _selectedbuidling = value;
 
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedBuilding");
            }
        }

        private BoqSheet _selectedVoSheet;
        public BoqSheet SelectedVoSheet
        {
            get { return _selectedVoSheet; }
            set
            {
                _selectedVoSheet = value;
                NotifyPropertyChanged("SelectedVoSheet");
            }
        }
        private Sheet _selectedBoqSheet;
        public Sheet SelectedBoqSheet
        {
            get { return _selectedBoqSheet; }
            set
            {
                _selectedBoqSheet = value;
                NotifyPropertyChanged("SelectedBoqSheet");
            }
        }

        private bool _isPreviousEnabled;
        public bool IsPreviousEnabled
        {
            get
            {
                return _isPreviousEnabled;
            }
            set
            {
                _isPreviousEnabled = value;
                NotifyPropertyChanged("IsPreviousEnabled");
            }
        }
        private bool _isNextEnabled;
        public bool IsNextEnabled
        {
            get
            {
                return _isNextEnabled;
            }
            set
            {
                _isNextEnabled = value;
                NotifyPropertyChanged("IsNextEnabled");
            }
        }
        
        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged("IsBusy");
            }
        }

        private bool _isEdit;
        public bool IsEdit
        {
            get
            {
                return _isEdit;
            }
            set
            {
                _isEdit = value;
                NotifyPropertyChanged("IsEdit");
            }
        }

        private bool _isSecondIpc;
        public bool IsSecondIpc
        {
            get { return _isSecondIpc; }
            set {
                _isSecondIpc = value;
                NotifyPropertyChanged("IsSecondIpc");
            }
        }

        private bool _isPreviewBusyLast;
        public bool IsPreviewBusyLast
        {
            get
            {
                return _isPreviewBusyLast;
            }
            set
            {
                _isPreviewBusyLast = value;
                NotifyPropertyChanged("IsPreviewBusyLast");
            }
        }

        private bool _isPreviewBusy;
        public bool IsPreviewBusy
        {
            get
            {
                return _isPreviewBusy;
            }
            set
            {
                _isPreviewBusy = value;
                NotifyPropertyChanged("IsPreviewBusy");
            }
        }

        private bool _retention;
        public bool Retention
        {
            get
            {
                return _retention;
            }
            set
            {
                _retention = value;
                NotifyPropertyChanged("Retention");
            }
        }

        private bool _advancePaymentRb;
        public bool AdvancePaymentRb
        {
            get
            {
                return _advancePaymentRb;
            }
            set
            {
                _advancePaymentRb = value;
                NotifyPropertyChanged("AdvancePaymentRb");
            }
        }

        private bool _interim;
        public bool Interim
        {
            get
            {
                return _interim;
            }
            set
            {
                _interim = value;
                NotifyPropertyChanged("Interim");
            }
        }

        private bool _isAccountent;
        public bool IsAccountent
        {
            get
            {
                return _isAccountent;
            }
            set
            {
                _isAccountent = value;
                NotifyPropertyChanged("IsAccountent");
            }
        }

        private bool _isPreview;
        public bool IsPreview
        {
            get
            {
                return _isPreview;
            }
            set
            {
                _isPreview = value;
                NotifyPropertyChanged("IsPreview");
            }
        }

        private bool _isEditable;
        public bool IsEditable
        {
            get
            {
                return _isEditable;
            }
            set
            {
                _isEditable = value;
                NotifyPropertyChanged("IsEditable");
            }
        }

        private bool _isTypeSelected;
        public bool IsTypeSelected
        {
            get
            {
                return _isTypeSelected;
            }
            set
            {
                _isTypeSelected = value;
                NotifyPropertyChanged("IsTypeSelected");
            }
        }

        private bool _final;
        public bool Final
        {
            get
            {
                return _final;
            }
            set
            {
                _final = value;
                NotifyPropertyChanged("Final");
            }
        }

        private bool _isApproveVisible;
        public bool IsApproveVisible
        {
            get { return _isApproveVisible; }
            set
            {
                _isApproveVisible = value;
                NotifyPropertyChanged("IsApproveVisible");
            }
        }

        private bool _isRejectVisible;
        public bool IsRejectVisible
        {
            get { return _isRejectVisible; }
            set
            {
                _isRejectVisible = value;
                NotifyPropertyChanged("IsRejectVisible");
            }
        }

        private int _voLevel;
        public int VoLevel
        {
            get { return _voLevel; }
            set { _voLevel = value; NotifyPropertyChanged("VoLevel"); }
        }
        private double _result;
        public double Result
        {
            get { return _result; }
            set { _result = value; NotifyPropertyChanged("Result"); }
        }
        private double _resultRetention;
        public double ResultRetention
        {
            get { return _resultRetention; }
            set { _resultRetention = value; NotifyPropertyChanged("ResultRetention"); }
        }
        private ContractBoqItem _selectedContBoq;

        public ContractBoqItem SelectedContBoq
        {
            get { return _selectedContBoq; }
            set {
                _selectedContBoq = value;
                NotifyPropertyChanged("SelectedContBoq");
            }
        }

        private IDocumentPaginatorSource _document;
        public IDocumentPaginatorSource Document
        {
            get { return _document; }
            set
            {
                _document = value;
                NotifyPropertyChanged("Document");
            }
        }

        private IDocumentPaginatorSource _documentPreview;
        public IDocumentPaginatorSource DocumentPreview
        {
            get { return _documentPreview; }
            set
            {
                _documentPreview = value;
                NotifyPropertyChanged("DocumentPreview");
            }
        }

        private IDocumentPaginatorSource _documentPreviewLast;
        public IDocumentPaginatorSource DocumentPreviewLast
        {
            get { return _documentPreviewLast; }
            set
            {
                _documentPreviewLast = value;
                NotifyPropertyChanged("DocumentPreviewLast");
            }
        }
        public int Skip { get; set; }
        public int SkipVo { get; set; }
        public string FilePath { get; set; }
        private bool _loadMoreButton;
        public bool LoadMoreButton
        {
            get
            {
                return _loadMoreButton;
            }
            set
            {
                _loadMoreButton = value;
                NotifyPropertyChanged("LoadMoreButton");
            }
        }
        #endregion
        public IpcViewModel()
        {
            IpcTrans = 0;
            IpcNumber = 1;
            IsApproveVisible = false;
            IsRejectVisible = false;
            SelectedContractsDataset = null;
            SelectedProject = null;
            SelectedSubcontractor = null;
            SelectedIpc = null;
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            var userType = customPrincipal.Identity.UserType;
            if (userType == UserType.Accountant || userType == UserType.Admin)
                IsAccountent = true;
            else
                IsAccountent = false;

            if (userType == UserType.QuantitySurveyor || userType == UserType.ContractsManager
               || userType == UserType.OperationsManager)
            {
                IsPreview = true;
            }
            else
                IsPreview = false;

            if (userType == UserType.ContractsManager || userType == UserType.OperationsManager)
            {
                IsEditable = true;
            }
            else
                IsEditable = false;
            Task.Factory.StartNew(() =>
            {
                //if (ContractDatasets == null)
                //    ContractDatasets = new ObservableCollection<ContractsDataset>();
                if (Ipcs == null)
                    Ipcs = new ObservableCollection<Ipc>();
                var ipc = unitOfWork.IpcRepository.Get(orderBy: y => y.OrderByDescending(x => x.Created), skip:Skip, paginate:true).ToList();
                var confgs = unitOfWork.ConfigurationRepository.Get(x => x.Key == "Tax").FirstOrDefault();
                foreach (var item in ipc)
                {
                    item.TotalAmountHT = item.TotalAmount / (1 + (double)Convert.ToDouble(confgs.Value)/100);
                }

                if (ContractDatasets == null)
                    ContractDatasets = new ObservableCollection<ContractsDataset>();
                var c = unitOfWork.ContractsDatasetRepository.Get();

                if (Subcontractorsf == null)
                    Subcontractorsf = new ObservableCollection<Subcontractor>();
                var sub = unitOfWork.SubcontractorRepository.Get().ToList();

                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var b = unitOfWork.ProjectRepository.Get();
                if (BoqSheets == null)
                    BoqSheets = new ObservableCollection<Sheet>();
                var sheets = unitOfWork.SheetsRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractDatasets.AddRange(c);
                          Subcontractorsf.AddRange(sub);
                          Projects.AddRange(b);
                          Ipcs.AddRange(ipc);
                          BoqSheets.AddRange(sheets);
                      }));
            });
        }
        public ICommand SheetSelectedCommand => new ActionCommand(p => SheetSeleced(p));
        public ICommand NextBuildingCommand => new ActionCommand(p => NextBuilding(p));
        public ICommand PreviousBuildingCommand => new ActionCommand(p => PreviousBuilding(p));
        public ICommand ProjectSelectedCommand => new ActionCommand(p => ProjectSeleced(p));
        public ICommand ContractSelectedCommand => new ActionCommand(p => ContractSeleced(p));
        public ICommand SubcontractorCommand => new ActionCommand(p => SubcontractorSeleced(p));
        public ICommand DeleteCommand => new ActionCommand(p => DeleteRecord(p));
        public ICommand EditCommand => new ActionCommand(p => EditRecord(p));
        public ICommand CellEditedCommand => new ActionCommand(p => CellEditedEvent(p));
        public ICommand CellEditedVoCommand => new ActionCommand(p => CellEditedEventVo(p));
        public ICommand SaveCommand => new ActionCommand(p => SaveRecords(p));
        public ICommand PreviewCommand => new ActionCommand(p => Preview(p));
        public ICommand PreviewExcelCommand => new ActionCommand(p => PreviewExcel(p));
        public ICommand DownloadInCreateCommand => new ActionCommand(p => DownloadInCreate(p));
        public ICommand NextCommand => new ActionCommand(p => Next());
        public ICommand PreviousCommand => new ActionCommand(p => Previous());
        public ICommand GenerateExcelCommand => new ActionCommand(p => GenerateExcel(p));
        public ICommand GenerateSelectedExcelCommand => new ActionCommand(p => GenerateSelectedExcel(p));
        public ICommand SaveExcelFileCommand => new ActionCommand(p => SaveExcelFile(p));
        public ICommand AdvanceUnCheckedCommand => new ActionCommand(p => AdvanceUnChecked(p));
        public ICommand NextVoCommand => new ActionCommand(p => NextVo());
        public ICommand PreviousVoCommand => new ActionCommand(p => PreviousVo());
        public ICommand SheetSelectedVoCommand => new ActionCommand(p => SheetVoSelected(p));
        public ICommand VoSelectedCommand => new ActionCommand(p => VoSelected(p));
        public ICommand BackToIpcCommand => new ActionCommand(p => BackToIpc(p));
        public ICommand NewIpcCommand => new ActionCommand(p => NewIpc(p));
        public ICommand IPCTypeSelectedCommand => new ActionCommand(p => IPCTypeSelected(p));
        public ICommand CreateFinalIpcCommand => new ActionCommand(p => CreateFinalIpc(p));
        public ICommand FilterProjectSelectedCommand => new ActionCommand(p => FilterProject(p));
        public ICommand FilterSubcontractorSelectedCommand => new ActionCommand(p => FilterSubcontractor(p));
        public ICommand FilterTradeSelectedCommand => new ActionCommand(p => FilterTrade(p));
        public ICommand FilterContractSelectedCommand => new ActionCommand(p => FilterContract(p));

        public ICommand CellEditEndingIpcCommand => new ActionCommand(p => CellEditEndingIpc(p));
        public ICommand LoadIpcCommand => new ActionCommand(p => LoadIpc(p));
        public ICommand ClearFilterCommand => new ActionCommand(p => ClearFilter(p));

        private Visibility _isLumpSum;
        public Visibility IsLumpSum
        {
            get { return _isLumpSum; }
            set
            {
                _isLumpSum = value;
                NotifyPropertyChanged("IsLumpSum");
            }
        }

        private void ClearFilter(object p)
        {
            SelectedProjectf = null;
            SelectedSubcontractorf = null;
            SelectedBoqSheet = null;
            SelectedContractsDatasetf = null;
            Skip = 0;
            Task.Factory.StartNew(() =>
            {
                var ipc = unitOfWork.IpcRepository.Get(orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Ipcs = new ObservableCollection<Ipc>();
                          Ipcs.AddRange(ipc);
                          LoadMoreButton = false;
                      }));
            });
        }
        private void LoadIpc(object p)
        {
            Skip += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {
                var ipc = unitOfWork.IpcRepository.Get(orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Ipcs.AddRange(ipc);
                          LoadMoreButton = false;
                      }));
            });
        }
        private void CellEditEndingIpc(object p)
        {
            unitOfWork.IpcRepository.Update(SelectedIpc);
            unitOfWork.Save();
        }
        private void FilterProject(object p)
        {
            if (SelectedProjectf == null)
                return;
            Ipcs.Clear();
            Ipcs.AddRange(unitOfWork.IpcRepository.Get(x => x.ContractsDataset.Project.Id == SelectedProjectf.Id).OrderByDescending(x => x.Created).ToList());
        }
        private void FilterSubcontractor(object p)
        {
            if (SelectedSubcontractorf == null)
                return;
            Ipcs.Clear();
            Ipcs.AddRange(unitOfWork.IpcRepository.Get(x => x.ContractsDataset.Subcontractor.Id == SelectedSubcontractorf.Id).OrderByDescending(x => x.Created).ToList());
        }
        private void FilterTrade(object p)
        {
            if (SelectedBoqSheet == null)
                return;
            Ipcs.Clear();
            Ipcs.AddRange(unitOfWork.IpcRepository.Get(x => x.ContractsDataset.BoqSheet.Name == SelectedBoqSheet.Name).OrderByDescending(x => x.Created).ToList());
        }
        private void FilterContract(object p)
        {
            if (SelectedContractsDatasetf == null)
                return;
            Ipcs.Clear();
            Ipcs.AddRange(unitOfWork.IpcRepository.Get(x => x.ContractsDataset.Id == SelectedContractsDatasetf.Id).OrderByDescending(x => x.Created).ToList());
        }
        private void CreateFinalIpc(object p)
        {
            var contract = p as ContractsDataset;
            IpcTrans = 2;
            DeductionTrans = 0;
            Progress1Trans = 0;
            AdvancePaymentPercentage = 0;
            RetentionPercentage = 0;
            SelectedProject = unitOfWork.ProjectRepository.Get(x => x.Id == contract.Project.Id).FirstOrDefault();
            //ProjectSeleced(null);
            SelectedSubcontractor = unitOfWork.SubcontractorRepository.Get(x => x.Id == contract.Subcontractor.Id).FirstOrDefault();
            //SubcontractorSeleced(null);
            SelectedContractsDataset = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == contract.Id).FirstOrDefault();
            //ContractSeleced(null);
            InterimDisabled = true;
            SelectedIpcType = IpcTypes[3];

        }
        private async void IPCTypeSelected(object p)
        {
            if (SelectedContractsDataset == null  || SelectedProject == null || SelectedSubcontractor == null)
                return;
            if (SelectedIpcType == null)
            {
                IsTypeSelected = false;
                return;
            }
            if (IsEdit == true)
                return;
            if (SelectedIpcType.Type == "Advance Payment")
            {
                Retention = false;
                Interim = false;
                Final = false;
                AdvancePaymentRb = true;
                IsTypeSelected = true;

                var view = new PaymentDialog
                {
                    DataContext = this
                };

                //show the dialog
                
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Ok")
                {

                }
                else if ((string)result == "Cancel")
                {
                    //SelectedIpcType = null;
                    AdvancePaymentPercentage = 0;
                    return;
                }

            }
            else if (SelectedIpcType.Type == "Retention Release")
            {
                AdvancePaymentRb = false;
                Retention = true;
                Final = false;
                Interim = false;
                IsTypeSelected = true;

                var view = new PaymentDialog
                {
                    DataContext = this
                };

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Ok")
                {

                }
                else if ((string)result == "Cancel")
                {
                    //SelectedIpcType = null;
                    RetentionPercentage = 0;
                    return;
                }

            }
            else if (SelectedIpcType.Type == "Interim")
            {
                Interim = true;
                Retention = false;
                AdvancePaymentRb = false;
                Final = false;
                IsTypeSelected = true;
            }
            else if (SelectedIpcType.Type == "Final")
            {
                Final = true;
                Interim = false;
                Retention = false;
                AdvancePaymentRb = false;
                IsTypeSelected = true;
            }
            var ipc = Ipcs.Where(x => x.ContractsDataset.Id == SelectedContractsDataset.Id && x.IpcStatus == IpcStatus.Issued).ToList();
            if(ipc.Count >= 1)
            {
                var lastIpc = ipc.First();
                var ipcFile = unitOfWork.IpcRepository.Get(x => x.Id == lastIpc.Id, includeProperties: "IpcFile").FirstOrDefault();
                lastIpc.IpcFile = ipcFile.IpcFile;
                string fileName = Path.GetTempFileName();
                IsPreviewBusyLast = true;
                if (lastIpc.IpcFile != null)
                {
                    using (FileStream fs = new FileStream(fileName + ".xlsx", FileMode.Create))
                    {
                        fs.Write(lastIpc.IpcFile.Content, 0, lastIpc.IpcFile.Content.Length);
                        fs.Close();
                    }

                    Task.Factory.StartNew(() =>
                    {
                        var excel = new Excel(fileName + ".xlsx");
                        var doc = excel.PreviewExcel();
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                           new ThreadStart(() =>
                           {
                               DocumentPreviewLast = doc.GetFixedDocumentSequence();
                               IsPreviewBusyLast = false;
                           }));
                        File.Delete(fileName);
                        File.Delete(fileName + ".xlsx");
                    });
                }
                    
            }
            else
            {
                DeductionTrans = 1;
            }
            IpcTrans = 2;
        }
        private void NewIpc(object p)
        {
            SelectedIpc = null;
            IpcTrans = 1;
            DeductionTrans = 0;
            Progress1Trans = 0;
            AdvancePaymentPercentage = 0;
            RetentionPercentage = 0;
            SelectedProject = null;
            SelectedSubcontractor = null;
            SelectedContractsDataset = null;
            InterimDisabled = false;
            IsDownloadButtonEnabled = false;
            FinalDisabled = false;
            ContBoqItems = null;
            ContVo = null;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
        }
        private void BackToIpc(object p)
        {
            IpcTrans = 0;
            SelectedProject = null;
            SelectedSubcontractor = null;
            SelectedContractsDataset = null;
            SelectedIpcType = null;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
            win.IpcDatabaseSlide.DataContext = new IpcViewModel();
        }
        private void SheetSeleced(object p)
        {
            if (SelectedBuilding == null)
                return;
            BuildingName = SelectedBuilding.Name;
            if (AllContBoqItems == null) return; //TODO::ERROR MESSAGE
            var boqItems = AllContBoqItems.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id).OrderBy(x => x.OrderBoq).ToList();

            if (ContBoqItems == null)
                ContBoqItems = new ObservableCollection<ContractBoqItem>();
            ContBoqItems.Clear();
            ContBoqItems.AddRange(boqItems);
            if (ContBoqItems.Count > 0)
            {
                BoqSum = ContBoqItems.Sum(x => x.Pt);
                BoqCumulSum = ContBoqItems.Sum(x => x.CumulAmount);
                BoqPrecedSum = ContBoqItems.Sum(x => x.PrecedAmount);
                BoqActualSum = ContBoqItems.Sum(x => x.ActualAmount);
            }
        }
        private void SheetVoSelected(object p)
        {
            if (SelectedVoSheet != null)
            {
                //if (ContVo == null)
                //    ContVo = new ObservableCollection<ContractVo>();
                //ContVo.Clear();
                //var vos = AllContVo.Where(x => x.BoqSheet.Boq.Building.Id == SelectedVoLevel.BuildingId && x.BoqSheet.Id == SelectedVoSheet.Id && x.Level == SelectedVoLevel.Tag).ToList();
                //ContVo.AddRange(vos);
                //if (ContVo.Count > 0)
                //{
                //    VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                //    VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                //    VoActualSum = ContVo.Sum(x => x.ActualAmount);
                //    VoSum = ContVo.Sum(x => x.Pt);
                //}
            }
        }
        private void NextVo()
        {
            VoLevel++;
            if (ContVo == null)
                ContVo = new ObservableCollection<ContractVo>();
            ContVo.Clear();
           
            if (VoLevel == SelectedContractsDataset.VoLevel)
                IsNextVoEnabled = false;
            if (VoLevel > 1)
                IsPreviousVoEnabled = true;
            var vos = AllContVo.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id && x.Level == VoLevel).ToList();
            var votrades = vos.Select(x => x.BoqSheet).Distinct();
            if (VOSheets == null)
                VOSheets = new ObservableCollection<BoqSheet>();
            VOSheets.Clear();
            VOSheets.AddRange(votrades);

            ContVo.AddRange(vos);
            if (ContVo.Count > 0)
            {
                VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                VoActualSum = ContVo.Sum(x => x.ActualAmount);
                VoSum = ContVo.Sum(x => x.Pt);
            }
        }
        private void PreviousVo()
        {
            VoLevel--;
            if (ContVo == null)
                ContVo = new ObservableCollection<ContractVo>();
            ContVo.Clear();
         
            if (VoLevel == 1)
                IsPreviousVoEnabled = false;
            if (SelectedContractsDataset.VoLevel > 1)
                IsNextVoEnabled = true;
            var vos = AllContVo.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id && x.Level == VoLevel).ToList();
            var votrades = vos.Select(x => x.BoqSheet).Distinct();
            if (VOSheets == null)
                VOSheets = new ObservableCollection<BoqSheet>();
            VOSheets.Clear();
            VOSheets.AddRange(votrades);
            ContVo.Clear();
            ContVo.AddRange(vos);
            if (ContVo.Count > 0)
            {
                VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                VoActualSum = ContVo.Sum(x => x.ActualAmount);
                VoSum = ContVo.Sum(x => x.Pt);
            }
        }
        private void AdvanceUnChecked(object p)
        {

        }
        private void Next()
        {
            if (DeductionTrans == 0)
            {
                DeductionTrans = 1;
            }
            else if (DeductionTrans == 1)
            {
                if (ProgressTrans == 0)
                    ProgressTrans = 1;
                else
                 DeductionTrans = 2;
            }else if (DeductionTrans == 2)
            {
                DeductionTrans = 3;
            }
            else if (DeductionTrans == 3)
            {
                if (LaborTrans < 2)
                    LaborTrans++;
                else
                {
                    DeductionTrans = 4;
                    PreviewExcel(null);
                }
            }
        }
        private void Previous()
        {
        }
        private void SaveExcelFile(object p)
        {
            if (SelectedIpc.IpcStatus == IpcStatus.Editable)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedIpc.ContractsDataset.Subcontractor.Name + "-" + SelectedIpc.ContractsDataset.ContractNumber + "-" + "IPC#" + SelectedIpc.Number.ToString("000");
                if (sf.ShowDialog() == true)
                {
                    System.IO.File.Copy(FilePath, sf.FileName + ".xlsx", true);
                }

            }
            else
            {

                var ipcFile = unitOfWork.IpcRepository.Get(x => x.Id ==SelectedIpc.Id, includeProperties: "IpcFile").FirstOrDefault();
                SelectedIpc.IpcFile = ipcFile.IpcFile;

                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = SelectedIpc.ContractsDataset.Subcontractor.Name + "-" + SelectedIpc.ContractsDataset.ContractNumber + "-" + "IPC#" + SelectedIpc.Number.ToString("000");
                if (sf.ShowDialog() == true)
                {

                    using (FileStream fs = new FileStream(sf.FileName + ".xlsx", FileMode.Create))
                    {
                        fs.Write(SelectedIpc.IpcFile.Content, 0, SelectedIpc.IpcFile.Content.Length);
                        fs.Close();
                    }
                }
                //File.Delete(fileName + ".xps");
            }
            var win = Application.Current.Windows.OfType<PreviewExcel>().FirstOrDefault();
            if (win != null)
                win.Close();
        }
        private void Preview(object p)
        {
            IsPreviewBusy = true; // {IFAO} présentep[ewe32we2 {IFAOS} 

            if (SelectedIpc.IpcStatus == IpcStatus.Editable)
            {
                var name = "";
                
                Task.Factory.StartNew(() =>
                {
                    var contractType = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == SelectedIpc.ContractsDataset.Id, includeProperties: "Contract").FirstOrDefault();
                    SelectedIpc.ContractsDataset.Contract = contractType.Contract;

                    if (SelectedIpc.ContractsDataset.Contract.Type == "Lump Sum")
                        name = new FileInfo("ipc4.xlsx").FullName;
                    else
                        name = new FileInfo("ipc3.xlsx").FullName;

                    double total;
                    string filePath;
                    var excel = new Excel(name);
                    var doc = excel.CreateIpc(SelectedIpc, true, out total, unitOfWork, null, out filePath);
                    FilePath = filePath;
                    TotalAmounts = total;
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new ThreadStart(() =>
                       {
                           DocumentPreview = doc.GetFixedDocumentSequence();
                           PreviewExcel pw = new PreviewExcel { DataContext = this };
                           pw.Show();
                           IsPreviewBusy = false;
                       }));
                });
            }
            else
            {
                string fileName = Path.GetTempFileName();
                var ipcFile = unitOfWork.IpcRepository.Get(x => x.Id == SelectedIpc.Id, includeProperties: "IpcFile").FirstOrDefault();
                SelectedIpc.IpcFile = ipcFile.IpcFile;
                using (FileStream fs = new FileStream(fileName + ".xlsx", FileMode.Create))
                {
                    fs.Write(SelectedIpc.IpcFile.Content, 0, SelectedIpc.IpcFile.Content.Length);
                    fs.Close();
                }
                var excel = new Excel(fileName + ".xlsx");
                var doc = excel.PreviewExcel();
                DocumentPreview = doc.GetFixedDocumentSequence();
                PreviewExcel pw = new PreviewExcel { DataContext = this };
                pw.Show();
                IsPreviewBusy = false;
                File.Delete(fileName);
                File.Delete(fileName + ".xlsx");
                //File.Delete(fileName + ".xps");
            }
        }
        public void PreviewExcel(object p)
        {
            if (DeductionTrans < 3)
                return;
            IsBusy = true;

            
            Ipc ipc;

            if (SelectedIpc != null)
                ipc = SelectedIpc;
            else
            {
                ipc = new Ipc();
                ipc.ContractsDataset = SelectedContractsDataset;
                ipc.IpcStatus = IpcStatus.Editable;
                ipc.AdvancePayment = TotalBoqPt;

                ipc.Penalty = PenaltyResult;

                if (AdvancePaymentRb == true)
                {
                    ipc.Type = "Avance / Advance Payment";
                    ipc.AdvancePaymentAmount = Result;
                    ipc.AdvancePaymentAmountCumul = Result;
                    ipc.AdvancePaymentPercentage = AdvancePaymentPercentage;
                }
                else if (Retention == true)
                {
                    ipc.Type = "RG / Retention";
                    ipc.RetentionAmount = ResultRetention;
                    ipc.RetentionAmountCumul = ResultRetention;
                    ipc.RetentionPercentage = RetentionPercentage;
                }
                else if (Interim == true)
                    ipc.Type = "Provisoire / Interim";
                else if (Final == true)
                    ipc.Type = "Final / Final";
                else ipc.Type = "";
                ipc.Number = IpcNumber;
            }
            ipc.Retention = TotalBoqCum;  
            ipc.ApRecovery = TotalBoqCumApRecovery;

            ipc.FromDate = FromDate;
            ipc.ToDate = ToDate;
            ipc.DateIpc = DateIpc;

            Ipc = ipc;//For Download Button
            var name = "";
            if (ipc.ContractsDataset.Contract.Type == "Lump Sum")
                name = new FileInfo("ipc4.xlsx").FullName;
            else
                name = new FileInfo("ipc3.xlsx").FullName;
            //SaveDeduction();
            //unitOfWork.Save();
            Task.Factory.StartNew(() =>
            {
                double total;
                string filePath;
                var excel = new Excel(name);
                var doc = excel.CreateIpc(ipc, true, out total, unitOfWork, VenteLocals.ToList(), out filePath);
                FilePath = filePath;
                TotalAmounts = total;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                     new ThreadStart(() =>
                     {
                         Document = doc.GetFixedDocumentSequence();
                         IsBusy = false;
                         IsDownloadButtonEnabled = true;
                     }));

            });
            UpdateLaborTotalSale(null, null);
            UpdateMachineTotalSale(null, null);
            UpdateVentTotalSale(null, null);
        }
        private void DownloadInCreate(object p)
        {

            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = Ipc.ContractsDataset.Subcontractor.Name + "-" + Ipc.ContractsDataset.ContractNumber + "-" + "IPC#" + Ipc.Number.ToString("000");
            if (sf.ShowDialog() == true)
            {
                File.Copy(FilePath, sf.FileName+".xlsx");
            }
        }
        private void GenerateExcel(object p)
        {
            Ipc ipc;

            if (SelectedIpc != null)
                ipc = SelectedIpc;
            else
            {
                ipc = new Ipc();

                ipc.ContractsDataset = SelectedContractsDataset;
                ipc.IpcStatus = IpcStatus.Issued;

                ipc.AdvancePayment = TotalBoqPt;
                ipc.Penalty = PenaltyResult;

                if (AdvancePaymentRb == true)
                {
                    ipc.Type = "Avance / Advance Payment";
                    ipc.AdvancePaymentAmount = Result;
                    ipc.AdvancePaymentAmountCumul = Result;
                    ipc.AdvancePaymentPercentage = AdvancePaymentPercentage;

                }
                else if (Retention == true)
                {
                    ipc.Type = "RG / Retention";

                    ipc.RetentionAmount = ResultRetention;
                    ipc.RetentionAmountCumul = ResultRetention;
                    ipc.RetentionPercentage = RetentionPercentage;
                }
                else if (Interim == true)
                    ipc.Type = "Provisoire / Interim";
                else if (Final == true)
                    ipc.Type = "Final / Final";

                else if (Final == true)
                {
                    ipc.Type = "Final / Final";
                    //SelectedContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Terminated;
                }
                else ipc.Type = "";

                unitOfWork.IpcRepository.Insert(ipc);
                Ipcs.Add(ipc);
                var orderedIpc = Ipcs.OrderByDescending(x => x.Created);
                Ipcs = new ObservableCollection<Ipc>();
                Ipcs.AddRange(orderedIpc);
                ipc.Number = IpcNumber;
            }
            ipc.Retention = TotalBoqCum;
            ipc.ApRecovery = TotalBoqCumApRecovery;

            var name = "";
           

            IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                var contractType = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == ipc.ContractsDataset.Id, includeProperties: "Contract").FirstOrDefault();
                ipc.ContractsDataset.Contract = contractType.Contract;

                if (ipc.ContractsDataset.Contract.Type == "Lump Sum")
                    name = new FileInfo("ipc4.xlsx").FullName;
                else
                    name = new FileInfo("ipc3.xlsx").FullName;
                double total;
                var excel = new Excel(name);
                string filePath = "";
                var doc = excel.CreateIpc(ipc, false, out total, unitOfWork, null, out filePath);
                //if (doc == null)
                //    return;
                TotalAmounts = total;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        //Document = doc.GetFixedDocumentSequence();
                        IsBusy = false;

                        ipc.IsGenerated = true;
                       
                        CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                        if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                        {
                            var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();

                            if (ipc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                            {
                                ipc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;
                                ipc.ContractActivated = true;
                                var notCon = new Notification { Contract = ipc.ContractsDataset, Type = "Contract", Creator = user, Status = false };
                                unitOfWork.Notifications.Insert(notCon);
                            }
                            else
                                ipc.ContractActivated = false;
                            ipc.IpcStatus = IpcStatus.PendingApproval;

                            var not = new Notification { Ipc = ipc, Type = "IPC", Creator = user, Status = false, Created = DateTime.Now };
                            unitOfWork.Notifications.Insert(not);
                        }
                        else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                                || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                        {
                            ipc.IpcStatus = IpcStatus.Issued;
                            if (ipc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                                ipc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Active;
                        }
                        ipc.Created = DateTime.Now;
                        SaveDeduction();
                        unitOfWork.Save();
                        IpcTrans = 0;
                        var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                        win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;

                    }));
            });
           
        }
        private void GenerateSelectedExcel(object p)
        {
            if (SelectedIpc == null)
                return;

            var name = "";
           

            IsBusy = true;
            IsPreviewBusy = true;
            var win = Application.Current.Windows.OfType<PreviewExcel>().FirstOrDefault();
            if(win != null)
            win.Close();
            Task.Factory.StartNew(() =>
            {
                var contractType = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == SelectedIpc.ContractsDataset.Id, includeProperties: "Contract").FirstOrDefault();
                SelectedIpc.ContractsDataset.Contract = contractType.Contract;

                if (SelectedIpc.ContractsDataset.Contract.Type == "Lump Sum")
                    name = new FileInfo("ipc4.xlsx").FullName;
                else
                    name = new FileInfo("ipc3.xlsx").FullName;

                double total;
                var excel = new Excel(name);
                string filePath;
                var doc = excel.CreateIpc(SelectedIpc, false, out total, unitOfWork, null, out filePath);
                //if (doc == null)
                //    return;
                TotalAmounts = total;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsBusy = false;
                        IsPreviewBusy = false;

                        CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                        if (customPrincipal.Identity.UserType != UserType.OperationsManager && customPrincipal.Identity.UserType != UserType.RegionalOperationsManager)
                        {
                            SelectedIpc.IpcStatus = IpcStatus.PendingApproval;

                            var user = unitOfWork.UsersRepository.Get(x => x.Email == customPrincipal.Identity.Email).FirstOrDefault();

                            if (SelectedIpc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                            {
                                SelectedIpc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.PendingApproval;
                                SelectedIpc.ContractActivated = true;
                                var notCon = new Notification { Contract = SelectedIpc.ContractsDataset, Type = "Contract", Creator = user, Status = false };
                                unitOfWork.Notifications.Insert(notCon);
                            }
                            else
                                SelectedIpc.ContractActivated = false;

                            var not = new Notification { Ipc = SelectedIpc, Type = "IPC", Creator = user, Status = false, Created = DateTime.Now };
                            unitOfWork.Notifications.Insert(not);
                        }
                        else if (customPrincipal.Identity.UserType == UserType.OperationsManager
                               || customPrincipal.Identity.UserType == UserType.RegionalOperationsManager)
                        {
                            SelectedIpc.IpcStatus = IpcStatus.Issued;
                            if (SelectedIpc.ContractsDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                                SelectedIpc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Active;

                        }
                        SelectedIpc.IsGenerated = true;
                        if (SelectedIpc.Type == "Final / Final")
                        {
                            //SelectedIpc.ContractsDataset.ContractDatasetStatus = ContractDatasetStatus.Terminated;
                        }
                        else
                        {
                           
                        }
                        SelectedIpc.Created = DateTime.Now;

                        unitOfWork.IpcRepository.Update(SelectedIpc);
                        unitOfWork.Save();
                    }));
            });
            
        }
   
        private void CellEditedEvent(object p)
        {
            var e = p as DataGridCellEditEndingEventArgs;
            if((string)e.Column.Header == Properties.Langs.Lang.cumulQte || (string)e.Column.Header == Properties.Langs.Lang.cumulPerc
                || (string)e.Column.Header == Properties.Langs.Lang.actualQte)
            {
                if (SelectedContBoq == null)
                    return;
                if(SelectedContractsDataset.Contract.Type == "Lump Sum")
                {
                    if(SelectedContBoq.Qte < SelectedContBoq.CumulQte)
                    {
                        System.Windows.MessageBox.Show("The \"" + Properties.Langs.Lang.cumulQte + "\" shouldn't exceed the initial BOQ quantities", "Exceeded quantity limit!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        SelectedContBoq.CumulQte = 0;
                    }
                }
            }
        }

        private void CellEditedEventVo(object p)
        {
            try
            {
                var e = p as DataGridCellEditEndingEventArgs;
                if (e == null) return;
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    var column = e.Column as DataGridBoundColumn;
                    if (column != null)
                    {
                        //   var contract = e.Row.DataContext as ContractBoqItem;

                        var bindingPath = (column.Binding as Binding).Path.Path;
                        if ((string)column.Header == "Unit Price")
                        {
                            int rowIndex = e.Row.GetIndex();
                            var el = e.EditingElement as TextBox;
                            var pr = Convert.ToInt32(el.Text);
                            var elBoq = ContVo.ElementAt(rowIndex);
                            elBoq.CumulAmount = pr * elBoq.CumulQte;
                            elBoq.ActualAmount = pr * elBoq.ActualQte;
                            elBoq.PrecedAmount = pr * elBoq.PrecedQte;

                            // rowIndex has the row index
                            // bindingPath has the column's binding
                            // el.Text has the new, user-entered value
                        }
                        else if ((string)column.Header == "Cumul Qtes")
                        {
                            int rowIndex = e.Row.GetIndex();
                            var el = e.EditingElement as TextBox;
                            var pr = Convert.ToInt32(el.Text);
                            var elBoq = ContVo.ElementAt(rowIndex);

                            //  elBoq.CumulAmount = pr * elBoq.BoqItem.Pu;
                        }
                        else if ((string)column.Header == "Actual Qtes")
                        {
                            int rowIndex = e.Row.GetIndex();
                            var el = e.EditingElement as TextBox;
                            var pr = Convert.ToInt32(el.Text);
                            var elBoq = ContVo.ElementAt(rowIndex);

                            //   elBoq.ActualAmount = pr * elBoq.BoqItem.Pu;
                        }
                        else if ((string)column.Header == "Preced Qtes")
                        {
                            int rowIndex = e.Row.GetIndex();
                            var el = e.EditingElement as TextBox;
                            var pr = Convert.ToInt32(el.Text);
                            var elBoq = ContVo.ElementAt(rowIndex);

                            //   elBoq.PrecedAmount = pr * elBoq.BoqItem.Pu;
                        }
                    }
                }
            }
            catch(FormatException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Format Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Format Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        //public ICommand DeleteCommand => new ActionCommand(p => DeleteRecord(p));      
        private async void DeleteRecord(object p)
        {
            if (SelectedIpc == null) return;
          //  Ipc = p as Ipc;

            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {

                if (AllContBoqItems == null)
                    AllContBoqItems = new ObservableCollection<ContractBoqItem>();

                if (AllContVo == null)
                    AllContVo = new ObservableCollection<ContractVo>();

                AllContVo.Clear();
                AllContVo.AddRange(unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.Id == SelectedIpc.ContractsDataset.Id).SelectMany(x => x.VoItems).ToList());
                AllContBoqItems.Clear();
                AllContBoqItems.AddRange(unitOfWork.ContractBoqItemRepository.Get(x => x.ContractsDatasetId == SelectedIpc.ContractsDataset.Id)
                                                                        .ToList());

                foreach (var item in AllContBoqItems)
                {
                    item.CumulQte = item.LastCumul;
                    item.PrecedQte = item.LastPreced;
                    item.ActualQte = item.LastActual;
                }
                foreach (var item in AllContVo)
                {
                    item.CumulQte = item.LastCumul;
                    item.PrecedQte = item.LastPreced;
                    item.ActualQte = item.LastActual;
                }

                unitOfWork.IpcRepository.Delete(SelectedIpc);
                Ipcs.Remove(SelectedIpc);
                unitOfWork.Save();
            }

        }
        private void EditRecord(object p)
        {
            if (SelectedIpc == null)
                return;
            IsEdit = true;

            AdvancePaymentRb = false;
            Retention = false;
            Interim = false;
            Final = false;

            //AdvancePayment = SelectedIpc.AdvancePayment;
            SelectedProject = SelectedIpc.ContractsDataset.Project;
            SelectedSubcontractor = SelectedIpc.ContractsDataset.Subcontractor;
            SelectedContractsDataset = SelectedIpc.ContractsDataset;
            IpcTrans = 2;
            DeductionTrans = 1;
            FromDate = SelectedIpc.FromDate;
            ToDate = SelectedIpc.ToDate;
            DateIpc = SelectedIpc.DateIpc;

            //IPCTypeSelected(null);
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
        }
 
        private void PreviousBuilding(object p)
        {
            if (SelectedContractsDataset == null) return;
            BuildingIndex--;
            if (BuildingIndex == 0)
                IsPreviousEnabled = false;
            if (BuildingIndex < SelectedProject.Buildings.Count - 1)
                IsNextEnabled = true;
            SelectedBuilding = SelectedContractsDataset.Project.Buildings.ElementAt(BuildingIndex);
            ChangeBuilding();
        }
        private void NextBuilding(object p)
        {
            if (SelectedContractsDataset == null) return;
            BuildingIndex++;
            if (BuildingIndex == SelectedProject.Buildings.Count - 1)
                IsNextEnabled = false;
            if (BuildingIndex > 0)
                IsPreviousEnabled = true;
            SelectedBuilding = SelectedContractsDataset.Project.Buildings.ElementAt(BuildingIndex);
            ChangeBuilding();
        }
        private void ChangeBuilding()
        {
            BuildingName = SelectedBuilding.Name;
            if (AllContBoqItems == null) return; //TODO::ERROR MESSAGE
            var boqItems = AllContBoqItems.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id).OrderBy(x => x.OrderBoq).ToList();

          

            if (ContBoqItems == null)
                ContBoqItems = new ObservableCollection<ContractBoqItem>();
            ContBoqItems.Clear();
            ContBoqItems.AddRange(boqItems);
            if (ContBoqItems.Count > 0)
            {
                BoqSum = ContBoqItems.Sum(x => x.Pt);
                BoqCumulSum= ContBoqItems.Sum(x => x.CumulAmount);
                BoqPrecedSum = ContBoqItems.Sum(x => x.PrecedAmount);
                BoqActualSum = ContBoqItems.Sum(x => x.ActualAmount);
            }

            if (AllContVo == null) return;

            var vos = AllContVo.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id && x.Level == VoLevel).ToList();

            var votrades = vos.Select(x => x.BoqSheet).Distinct();
            if (VOSheets == null)
                VOSheets = new ObservableCollection<BoqSheet>();
            VOSheets.Clear();
            VOSheets.AddRange(votrades);

            if (ContVo == null)
                ContVo = new ObservableCollection<ContractVo>();
            ContVo.Clear();

            ContVo.AddRange(vos);
            if (ContVo.Count > 0)
            {
                VoSum = ContVo.Sum(x => x.Pt);
                VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                VoActualSum = ContVo.Sum(x => x.ActualAmount);
            }


        }
        private void SaveRecords(object p)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

            try
            {
                if (SelectedContractsDataset == null)
                    return; // TODO: ERROR SHOULD CHOSE CONTRACTSDDATASET
                if(SelectedIpc != null)
                {
                    SelectedIpc.ContractsDataset = SelectedContractsDataset;
                    SelectedIpc.IpcStatus = IpcStatus.Editable;
                    SelectedIpc.AdvancePayment = TotalBoqPt;
                    SelectedIpc.Retention = TotalBoqCum;
                    SelectedIpc.AdvancePaymentAmount = Result;
                    SelectedIpc.AdvancePaymentAmountCumul = Result;
                    SelectedIpc.RetentionAmount = ResultRetention;
                    SelectedIpc.RetentionAmountCumul = ResultRetention;
                    SelectedIpc.RetentionPercentage = RetentionPercentage;
                    SelectedIpc.RetentionPercentage = AdvancePaymentPercentage;
                    if (TotalAmounts != 0)
                        SelectedIpc.TotalAmount = TotalAmounts;

                    //if (AdvancePaymentRb == true)
                    //{
                    //    SelectedIpc.Type = "Avance / Advance Payment";
                    //    SelectedIpc.AdvancePaymentPercentage = AdvancePaymentPercentage;
                    //}
                    //else if (Retention == true)
                    //{
                    //    SelectedIpc.Type = "RG / Retention";
                    //
                    //    SelectedIpc.RetentionPercentage = RetentionPercentage;
                    //}
                    //else if (Interim == true)
                    //    SelectedIpc.Type = "Provisoire / Interim";
                    //else if (Final == true)
                    //{
                    //    SelectedIpc.Type = "Final / Final";
                    //}
                    //else SelectedIpc.Type = "";
                    //SelectedIpc.Number = IpcNumber;
                    SelectedIpc.FromDate = FromDate;
                    SelectedIpc.ToDate = ToDate;
                    SelectedIpc.DateIpc = DateIpc;
                    unitOfWork.IpcRepository.Update(SelectedIpc);
                    unitOfWork.ContractsDatasetRepository.Update(SelectedContractsDataset);
                }
                else
                {
                    var ipc = new Ipc();

                    ipc.ContractsDataset = SelectedContractsDataset;
                    ipc.IpcStatus = IpcStatus.Editable;
                    ipc.AdvancePayment = TotalBoqPt;
                    ipc.AdvancePaymentAmount = Result;
                    ipc.AdvancePaymentAmountCumul = Result;
                    ipc.RetentionAmount = ResultRetention;
                    ipc.RetentionAmountCumul = ResultRetention;
                    ipc.Retention = TotalBoqCum;
                    ipc.RetentionPercentage = RetentionPercentage;
                    if (TotalAmounts > 0)
                        ipc.TotalAmount = TotalAmounts;
                    if (AdvancePaymentRb == true)
                    {
                        ipc.Type = "Avance / Advance Payment";
                        ipc.AdvancePaymentPercentage = AdvancePaymentPercentage;
                    }
                    else if (Retention == true)
                    {
                        ipc.Type = "RG / Retention";
                        ipc.RetentionPercentage = RetentionPercentage;
                    }
                    else if (Interim == true)
                        ipc.Type = "Provisoire / Interim";
                    else if (Final == true)
                    {
                        ipc.Type = "Final / Final";
                    }
                    else ipc.Type = "";
                    ipc.FromDate = FromDate;
                    ipc.ToDate = ToDate;
                    ipc.DateIpc = DateIpc;

                    // unitOfWork.IpcRepository.Update(ipc);
                    unitOfWork.ContractsDatasetRepository.Update(SelectedContractsDataset);

                    ipc.Number = IpcNumber;
                    unitOfWork.IpcRepository.Insert(ipc);
                    Ipcs.Add(ipc);
                }
             


                SaveDeduction();
                unitOfWork.Save();
                IpcTrans = 0;
                SelectedProject = null;
                SelectedSubcontractor = null;
                SelectedContractsDataset = null;
                SelectedIpcType = null;
                 
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            finally
            {
                win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
                win.IpcDatabaseSlide.DataContext = new IpcViewModel();

            }
            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
            win.IpcDatabaseSlide.DataContext = new IpcViewModel();

        }
        private void SaveDeduction()
        {
            
            foreach (var mat in Labors)
            {
            
                if (mat.Id == 0)
                {
                    mat.ContractsDatasetId = SelectedContractsDataset.Id;
                    unitOfWork.LaborRepository.Insert(mat);
                }
                else
                    unitOfWork.LaborRepository.Update(mat);
            }
            foreach (var mat in Machines)
            {
                if (mat.Id == 0)
                {
                    mat.ContractsDatasetId = SelectedContractsDataset.Id;
                    unitOfWork.MachineRepository.Insert(mat);
                }
                else
                    unitOfWork.MachineRepository.Update(mat);
            }
            //foreach (var mat in VenteLocals)
            //{
            //    if (mat.Id == 0)
            //    {
            //        mat.Contract = SelectedContractsDataset.ContractNumber;
            //        unitOfWork.VenteLocalRepository.Insert(mat);
            //    }
            //    else
            //        unitOfWork.VenteLocalRepository.Update(mat);
            //}
            foreach (var contract in AllContBoqItems)
            {
                unitOfWork.ContractBoqItemRepository.Update(contract);
            }

            foreach (var contract in AllContVo)
            {
                unitOfWork.ContractVoRepository.Update(contract);
            }
        }
        private void ProjectSeleced(object p)
        {
            if (SelectedProject == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            List<Subcontractor> cd;
            //if (SelectedContractsDataset != null)
            //{
                //SaveRecords(null);
                //SelectedSubcontractor = null;
               // SelectedContractsDataset = null;
            //}
            try
            {
                cd = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject.Id).Select(x => x.Subcontractor).Distinct().ToList();

            }
            catch (Exception ex)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(ex.Message, "Error");
                return;
            }
             if (Subcontractors == null)
                Subcontractors = new ObservableCollection<Subcontractor>();
            Subcontractors.Clear();
            Subcontractors.AddRange(cd);
        }
        private void SubcontractorSeleced(object p)
        {
            if (SelectedSubcontractor == null)
                return;
            //if (SelectedContractsDataset != null)
            //{
            //    SaveRecords(null);
                SelectedContractsDataset = null;
            //}
            var sc = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject.Id && x.SubcontractorId == SelectedSubcontractor.Id).ToList();
            if (ContractDatasets == null)
                ContractDatasets = new ObservableCollection<ContractsDataset>();
            ContractDatasets.Clear();
            ContractDatasets.AddRange(sc);
        }
        private async void ContractSeleced(object p)
        {
            if (SelectedContractsDataset == null)
                return;
        
                var contractType = unitOfWork.ContractsDatasetRepository.Get(x => x.Id == SelectedContractsDataset.Id, includeProperties: "Contract").FirstOrDefault();
            
                        SelectedContractsDataset.Contract = contractType.Contract;
                        if (SelectedContractsDataset.Contract.Type == "Lump Sum")
                            IsLumpSum = Visibility.Collapsed;
        
           
            var ipc = unitOfWork.IpcRepository.Get(x => x.ContractsDataset.Id == SelectedContractsDataset.Id).ToList();
            var ipcEdit = unitOfWork.IpcRepository.Get(x => x.ContractsDataset.Id == SelectedContractsDataset.Id && (x.IpcStatus == IpcStatus.Editable || x.IpcStatus == IpcStatus.PendingApproval)).ToList();
            var ipcPending = ipcEdit.Where(x => x.IpcStatus == IpcStatus.PendingApproval).ToList();

            if (ipc.Where(x => x.Type == "Final / Final" && x.IpcStatus == IpcStatus.Issued).Count() > 0)
            {
                InterimDisabled = true;
                FinalDisabled = true;
            }
            if (ipcPending.Count > 0)
            {
                System.Windows.MessageBox.Show("Please note that this contract number has an IPC needs to be approved before creating a new one.", "Cannot create IPC", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                return;
            }
            else if (ipcEdit.Count == 1)
            {
                if(SelectedIpc != null)
                {
                    if (SelectedIpc.Id == ipcEdit.FirstOrDefault().Id)
                    {

                    }
                    else
                    {
                        var result = System.Windows.MessageBox.Show("Please note that this contract number has an editable IPC, do you wish to edit its corresponding editable IPC?", "Cannot create IPC", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                        if (result == MessageBoxResult.OK)
                        {
                            SelectedIpc = ipcEdit.FirstOrDefault();
                            EditRecord(null);
                        }
                        else
                        {
                            SelectedContractsDataset = null;
                            return;
                        }
                    }
                }
                else
                {
                    var result = System.Windows.MessageBox.Show("Please note that this contract number has an editable IPC, do you wish to edit its corresponding editable IPC?", "Cannot create IPC", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.OK)
                    {
                        SelectedIpc = ipcEdit.FirstOrDefault();
                        EditRecord(null);
                    }
                    else
                    {
                        SelectedContractsDataset = null;
                        return;
                    }
                }

            }
            else if (ipcEdit.Count > 1)
            {

                var result = System.Windows.MessageBox.Show("Please note that this contract number has an editable IPC, do you wish to edit its last editable IPC?", "Cannot create IPC", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK)
                {
                    SelectedIpc = ipcEdit.FirstOrDefault();
                    EditRecord(null);
                }
                else
                {
                    SelectedContractsDataset = null;
                    return;
                }
            }

            if (Buildings == null)
                Buildings = new ObservableCollection<Building>();
            Buildings.Clear();

            var buildings = SelectedContractsDataset.Buildings.ToList();
            Buildings.AddRange(buildings);

            SelectedBuilding = Buildings.FirstOrDefault();

            if (SelectedBuilding == null) return;

            if (ContBoqItems == null)
                ContBoqItems = new ObservableCollection<ContractBoqItem>();

            if (ContVo == null)
                ContVo = new ObservableCollection<ContractVo>();

            if (AllContBoqItems == null)
                AllContBoqItems = new ObservableCollection<ContractBoqItem>();

            if (AllContVo == null)
                AllContVo = new ObservableCollection<ContractVo>();

            VoLevel = SelectedContractsDataset.VoLevel;
            if (VoLevel > 1)
            {
                IsNextVoEnabled = false;
                IsPreviousVoEnabled = true;
            }
            else
            {
                IsNextVoEnabled = false;
                IsPreviousVoEnabled = false;
            }
            var ex = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset_Id == SelectedContractsDataset.Id);
            AllContVo.Clear();
            AllContVo.AddRange(ex.Where(x=>x.VoItems != null).SelectMany(x => x.VoItems).ToList());
            AllContBoqItems.Clear();
            AllContBoqItems.AddRange(unitOfWork.ContractBoqItemRepository.Get(x => x.ContractsDatasetId == SelectedContractsDataset.Id && x.BoqSheet != null)
                                                                   .ToList());

            TotalBoqPt = AllContBoqItems.Sum(x => x.Pt) * ((double)Convert.ToDouble(SelectedContractsDataset.SubcontractorAdvancePayee) / 100);
            //TotalBoqPt = SelectedContractsDataset.AdvancePayment * ((double)Convert.ToDouble(SelectedContractsDataset.SubcontractorAdvancePayee) / 100);
            TotalBoqCum = AllContBoqItems.Sum(x => x.CumulAmount);
            TotalBoqCum += AllContVo.Where( x=> x.VoDataset.Type == "Addition").Sum(x => x.CumulAmount);
            TotalBoqCum -= AllContVo.Where( x=> x.VoDataset.Type == "Omission").Sum(x => x.CumulAmount);
            TotalBoqCumApRecovery = TotalBoqCum;
            TotalBoqCumApRecovery = TotalBoqCumApRecovery * ((double)Convert.ToDouble(SelectedContractsDataset.RecoverAdvance) / 100);
            TotalBoqCum = TotalBoqCum * ((double)Convert.ToDouble(SelectedContractsDataset.HoldWarranty) / 100);
            IpcNumber = 1;

            if(ipc.Count > 0)
            {
                RetentionPercentage = ipc.Last().RetentionPercentage;
                AdvancePaymentPercentage = ipc.Last().AdvancePaymentPercentage;

                TotalBoqPt -= ipc.OrderBy(x => x.Id).Last().AdvancePaymentAmountCumul;
                TotalBoqCum -= ipc.OrderBy(x => x.Id).Last().RetentionAmountCumul;

            }

            if (ipc.Count() > 0 && IsEdit == false)
            {
               // PreviousIpcAvancePayment = ipc.FirstOrDefault().AdvancePayment;
                AdvancePaymentRb = false;
                // AdvancePayment = PreviousIpcAvancePayment;

                foreach (var item in AllContBoqItems)
                {
                    item.LastActual = item.ActualQte;
                    item.LastCumul = item.CumulQte;
                    item.LastPreced = item.PrecedQte;

                    item.PrecedQte = item.CumulQte;
                }
                foreach (var item in AllContVo)
                {
                    item.LastActual = item.ActualQte;
                    item.LastCumul = item.CumulQte;
                    item.LastPreced = item.PrecedQte;

                    item.PrecedQte = item.CumulQte;
                }
                
                //AdvancePayment = ipc.Last().AdvancePayment;
                IpcNumber = ipc.Count() + 1;


                IsSecondIpc = false;//To check if advancepayment enabled or not
            }
            else
            {
               
                IsSecondIpc = true;
            }
            //EventWaitHandle
            IpcTypes = new ObservableCollection<dynamic>();
            dynamic ipcType1 = new System.Dynamic.ExpandoObject();
            ipcType1.Type = "Advance Payment";
            ipcType1.Amount = SelectedContractsDataset.AdvancePayment * ((double)Convert.ToDouble(SelectedContractsDataset.SubcontractorAdvancePayee)/100);
            ipcType1.PreviousPaid = ipc.Sum(x=>x.AdvancePaymentAmount);
            ipcType1.Remaining = TotalBoqPt;
            dynamic ipcType2 = new System.Dynamic.ExpandoObject();
            ipcType2.Type = "Retention Release";
            ipcType2.Amount = AllContBoqItems.Sum(x => x.CumulAmount) + AllContVo.Sum(x => x.CumulAmount);
            ipcType2.PreviousPaid = ipc.Sum(x => x.RetentionAmount);
            ipcType2.Remaining = TotalBoqCum;
            dynamic ipcType3 = new System.Dynamic.ExpandoObject();
            ipcType3.Type = "Interim";
            ipcType3.Amount = AllContBoqItems.Sum(x => x.Pt) + AllContVo.Sum(x => x.Pt);
            ipcType3.PreviousPaid = "";
            ipcType3.Remaining = "";
            dynamic ipcType4 = new System.Dynamic.ExpandoObject();
            ipcType4.Type = "Final";
            ipcType4.Amount = "";
            ipcType4.PreviousPaid = "";
            ipcType4.Remaining = "";
            IpcTypes.Add(ipcType1);
            IpcTypes.Add(ipcType3);
            IpcTypes.Add(ipcType2);
            IpcTypes.Add(ipcType4);

           

            var boqItems = AllContBoqItems.Where(x => x.BoqSheet.Boq.Building.Id == SelectedBuilding.Id).ToList();
            ContBoqItems.AddRange(boqItems.OrderBy(x => x.OrderBoq).ToList());
            if (ContBoqItems.Count > 0)
            {
                BoqSum = ContBoqItems.Sum(x => x.Pt);
                BoqCumulSum = ContBoqItems.Sum(x => x.CumulAmount);
                BoqPrecedSum = ContBoqItems.Sum(x => x.PrecedAmount);
                BoqActualSum = ContBoqItems.Sum(x => x.ActualAmount);
            }

            Vos = new ObservableCollection<VoDataset>();
            Vos.AddRange(unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.Id == SelectedContractsDataset.Id).ToList());

            var venteLocal = unitOfWork.VenteLocalRepository.Get(x => x.Contract == SelectedContractsDataset.ContractNumber).ToList();
            //var labor = unitOfWork.LaborRepository.Get(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();
            //var machine = unitOfWork.MachineRepository.Get(x => x.ContractsDatasetId == SelectedContractsDataset.Id).ToList();

            if (VenteLocals == null)
                VenteLocals = new ObservableCollection<VenteLocal>();
            if (Labors == null)
                Labors = new ObservableCollection<Labor>(); 
            if (Machines == null)
                Machines = new ObservableCollection<Machine>();

            VenteLocals.Clear();
            Labors.Clear();
            Machines.Clear();

            VenteLocals.AddRange(venteLocal);
            if(SelectedContractsDataset.Labors != null)
            Labors.AddRange(SelectedContractsDataset.Labors);
            if(SelectedContractsDataset.Machines != null)
            Machines.AddRange(SelectedContractsDataset.Machines);


            if (Labors.Count != 0)
            {
                LaborConsSum = (double)Labors.Sum(x => x.Amount);
                LaborSum = (double)Labors.Sum(x => x.ActualAmount);
            }

            if (VenteLocals.Count != 0)
            {
                VenteConsSum = VenteLocals.Sum(x => x.ConsumedAmount);
                VenteSum = VenteLocals.Sum(x => x.ActualAmount);
            }
            if (Machines.Count != 0)
            {
                MachineConsSum = (double)Machines.Sum(x => x.Amount);
                MachineSum = (double)Machines.Sum(x => x.ActualAmount);
            }
            if (Buildings.Count > 1)
            {
                BuildingIndex = 0;
                IsNextEnabled = true;
                IsPreviousEnabled = false;
            }

            #region Penalty
            var dif = DateTime.Now - SelectedContractsDataset.CompletionDate;
            Diffrents = dif.Value.Days;
            if (Diffrents > 0)
            {
                var minPenalty = ((double)Convert.ToDouble(SelectedContractsDataset.LatePenalties) / 1000f) * AllContBoqItems.Sum(x => x.Pt) * Diffrents;
                var maxPenalty = ((double)Convert.ToDouble(SelectedContractsDataset.LatePenaliteCeiling) / 100f) * AllContBoqItems.Sum(x => x.Pt) * Diffrents;
                Penalty = minPenalty > maxPenalty ? maxPenalty : minPenalty;
                var view = new PenaltyDialog
                {
                    DataContext = this
                };

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Apply")
                {

                }
                else if ((string)result == "Cancel")
                {
                    PenaltyPercentage = 0;
                }
            }
            else if (IsEdit == true)
            {
                if (SelectedIpc.Type == "Avance / Advance Payment")
                {

                    Retention = false;
                    Interim = false;
                    Final = false;
                    AdvancePaymentRb = true;
                    IsTypeSelected = true;
                    var viewPayment = new PaymentDialog
                    {
                        DataContext = this
                    };

                    //show the dialog

                    var resultPayment = await DialogHost.Show(viewPayment, "RootDialog");

                    if ((string)resultPayment == "Ok")
                    {

                    }
                    else if ((string)resultPayment == "Cancel")
                    {
                        AdvancePaymentPercentage = 0;
                        BackToIpc(null);
                        return;
                    }
                }
                else if (SelectedIpc.Type == "RG / Retention")
                {
                    AdvancePaymentRb = false;
                    Retention = true;
                    Final = false;
                    Interim = false;
                    IsTypeSelected = true;
                    var viewPayment = new PaymentDialog
                    {
                        DataContext = this
                    };

                    //show the dialog

                    var resultPayment = await DialogHost.Show(viewPayment, "RootDialog");

                    if ((string)resultPayment == "Ok")
                    {

                    }
                    else if ((string)resultPayment == "Cancel")
                    {
                        RetentionPercentage = 0;
                        BackToIpc(null);

                        return;
                    }
                }
            }
            #endregion


            IsEdit = false; // We use IsEdit to check if we are editing an ipc or creating a new one


        }
        private void VoSelected(object p)
        {
            if (SelectedVoLevel == null)
                return;
            if (ContVo == null)
                ContVo = new ObservableCollection<ContractVo>();
            ContVo.Clear();
            //SelectedVoLevel
           
            ContVo.AddRange(SelectedVoLevel.VoItems.ToList());
            if (ContVo.Count > 0)
            {
                VoCumulSum = ContVo.Sum(x => x.CumulAmount);
                VoPrecedSum = ContVo.Sum(x => x.PrecedAmount);
                VoActualSum = ContVo.Sum(x => x.ActualAmount);
                VoSum = ContVo.Sum(x => x.Pt);
            }
        }
    }
}
 