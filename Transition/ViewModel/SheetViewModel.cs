﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class SheetViewModel : ContractsManager.Windows.ViewModel
    {
        public UnitOfWork unitOfWork { get; set; }
        private ObservableCollection<ContractsManager.Data.Sheet> _sheets;
        public ObservableCollection<ContractsManager.Data.Sheet> Sheets
        {
            get { return _sheets; }
            set
            {
                _sheets = value;
                NotifyPropertyChanged("Sheets");
            }
        }
        private ContractsManager.Data.Sheet _selectedSheet { get; set; }
        public ContractsManager.Data.Sheet SelectedSheet
        {
            get { return _selectedSheet; }
            set
            {
                _selectedSheet = value;
                NotifyPropertyChanged("SelectedSheet");
            }
        }
        private Sheet _oldSheet;

        public SheetViewModel()
        {
            unitOfWork = new UnitOfWork();

            Task.Factory.StartNew(() =>
            {
                if (Sheets == null)
                    Sheets = new ObservableCollection<ContractsManager.Data.Sheet>();
                var sheets = unitOfWork.SheetsRepository.Get();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Sheets.AddRange(sheets);
                          //Currencies.Add(new Data.Currency { Currencies = "Test", ConversionRate = 0.0f });
                          // Contracts.Add(new Contract());
                      }));
            });
        }

        public ICommand SaveCommand => new ActionCommand(p => Save(p));
        public ICommand DeleteCommand => new ActionCommand(p => Delete(p));
        public ICommand SelectedCellsChangedCommand => new ActionCommand(p => SelectedCellsChanged(p));
        public ICommand RowEditEndingCommand => new ActionCommand(p => RowEditEnding(p));
        public ICommand SelectionChangedCommand => new ActionCommand(p => SelectionChanged(p));

        private void SelectionChanged(object p)
        {
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if (!(dataGrid.SelectedItem is Sheet))
                SelectedSheet = null;
        }

        private void RowEditEnding(object p)
        {
          //  MessageBox.Show("These changes will effect all BOQs and VOs in project", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);

           var e = p as RoutedEventArgs;
           if (SelectedSheet == null) return;
           if (SelectedSheet.Id > 0)
               unitOfWork.SheetsRepository.Update(SelectedSheet);
            else
            {
                if(!string.IsNullOrWhiteSpace(SelectedSheet.Name))
                    unitOfWork.SheetsRepository.Insert(SelectedSheet);
            }
            unitOfWork.Save();
            _oldSheet = new Sheet { Name = SelectedSheet.Name, NameFr = SelectedSheet.NameFr, Id = SelectedSheet.Id };

        }

        private async void SelectedCellsChanged(object p)
        {
            var p1 = p as MouseButtonEventArgs;
            var item = p1.Source as DataGrid;

            if (item.CurrentColumn == null)
                return;
            var col = item.CurrentColumn.Header as string;
            if (col != "Cost Code")
                return;

            Sheet s = new Sheet();
            item.CommitEdit();
            item.CancelEdit();

            //Sheet sheet = item.SelectedItem as Sheet;
            var view = new CostCodeDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if (result is string)
            {
                unitOfWork.Save();
            }
            else
            {
                var costCode = unitOfWork.CostCodeLibraryRepository.Get(x => x.Id == ((CostCodeLibrary)result).Id).FirstOrDefault();
                if(SelectedSheet == null)
                {
                    SelectedSheet = unitOfWork.SheetsRepository.Get(x => x.Id == _oldSheet.Id).FirstOrDefault();
                    Sheets.Add(SelectedSheet);
                }
                SelectedSheet.CostCode = costCode;
                item.CommitEdit();
            }
            if (SelectedSheet == null)
                return;
            if (SelectedSheet.Id > 0)
                unitOfWork.SheetsRepository.Update(SelectedSheet);
            else
                unitOfWork.SheetsRepository.Insert(SelectedSheet);

            unitOfWork.Save();

        }

        private void Save(object p)
        {
            foreach (var item in Sheets)
            {
                if (item.Id == 0)
                {
                    unitOfWork.SheetsRepository.Insert(item);
                }
                else
                {
                    unitOfWork.SheetsRepository.Update(item);
                }
            }
            unitOfWork.Save();
        }

        private async void Delete(object p)
        {

            var view = new ConfirmDialog();

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                try
                {
                    unitOfWork.SheetsRepository.Delete(SelectedSheet);
                    Sheets.Remove(SelectedSheet);
                    unitOfWork.Save();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.InnerException != null ? ex.InnerException.Message : ex.Message, "Delete Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                }
            }
        }

    }
}
