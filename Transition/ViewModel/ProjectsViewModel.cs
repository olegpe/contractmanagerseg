﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ProjectsViewModel : ContractsManager.Windows.ViewModel
    {

        public UnitOfWork unitOfWork { get; set; }

        private Visibility _isNextVoVisibil;

        public Visibility IsNextVoVisibil
        {
            get { return _isNextVoVisibil; }
            set {
                _isNextVoVisibil = value;
                NotifyPropertyChanged("IsNextVoVisibil");
            }
        }
        private Visibility _isPreviousVoVisibil;

        public Visibility IsPreviousVoVisibil
        {
            get { return _isPreviousVoVisibil; }
            set {
                _isPreviousVoVisibil = value;
                NotifyPropertyChanged("IsPreviousVoVisibil");
            }
        }
        public IEnumerable<string> Units
        {
            get
            {
                yield return "ft";
                yield return "U";
                yield return "D";
                yield return "dm";
                yield return "ens";
                yield return "ml";
                yield return "m2";
                yield return "m3";
                yield return "kg";
                yield return "T";
            }
        }
        public bool AllB { get; set; }
        public bool AllT { get; set; }
        public bool OnlyThis { get; set; }
        private bool _isPreviousVoEnabled;
        public bool IsPreviousVoEnabled
        {
            get {
                return _isPreviousVoEnabled;
            }
            set
            {
                _isPreviousVoEnabled = value;
                NotifyPropertyChanged("IsPreviousVoEnabled");
            }
        }
        private bool _isPreview;
        public bool IsPreview
        {
            get {
                return _isPreview;
            }
            set
            {
                _isPreview = value;
                NotifyPropertyChanged("IsPreview");
            }
        }
        private bool _isNextVoEnabled;
        public bool IsNextVoEnabled
        {
            get {
                return _isNextVoEnabled;
                }
            set
            {
                _isNextVoEnabled = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsNextVoEnabled");
            }
        }
        private int _voLevel;
        public int VoLevel
        {
            get { return _voLevel; }
            set { _voLevel = value; NotifyPropertyChanged("VoLevel"); NotifyPropertyChanged("EnabledVoGrid"); }
        }
        private bool _isUploading;
        public bool IsUploading
        {
            get {
                return _isUploading;
                }
            set
            {
                _isUploading = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsUploading");
            }
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get {
                return _isBusy;
                }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsBusy");
            }
        }
        private bool _enabledVoGrid;
        public bool EnabledVoGrid
        {
            get {
                if (SelectedProject != null)
                {
                    return VoLevel == SelectedBuilding.ProjectLevel;
                }
                else return false;
                }
            set
            {
                _enabledVoGrid = value;
                NotifyPropertyChanged("EnabledVoGrid");
            }
        }
        private double _sum;
        public double Sum
        {
            get { return _sum; }
            private set
            {
                _sum = value;
                NotifyPropertyChanged("Sum");
            }
        }
        private string _previousBuilding;
        public string PreviousBuilding
        {
            get { return _previousBuilding; }
            set {
                _previousBuilding = value;
                NotifyPropertyChanged("PreviousBuilding");
            }
        }
        private string _nextBuilding;
        public string NextBuilding
        {
            get { return _nextBuilding; }
            set {
                _nextBuilding = value;
                NotifyPropertyChanged("NextBuilding");
            }
        }
        private string _previousVo;
        public string PreviousVoString
        {
            get { return _previousVo; }
            set {
                _previousVo = value;
                NotifyPropertyChanged("PreviousVoString");
            }
        }
        private string _nextVo;
        public string NextVoString
        {
            get { return _nextVo; }
            set {
                _nextVo = value;
                NotifyPropertyChanged("NextVoString");
            }
        }

        private ObservableCollection<dynamic> _vos;
        public ObservableCollection<dynamic> Vos
        {
            get { return _vos; }
            set
            {
                _vos = value;
                NotifyPropertyChanged("Vos");
            }
        }

        private dynamic _selectedVoLevel;
        public dynamic SelectedVoLevel
        {
            get { return _selectedVoLevel; }
            set
            {
                _selectedVoLevel = value;

                NotifyPropertyChanged("SelectedVoLevel");
            }
        }

        private Project _selectedProject;
        public Project SelectedProject {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                if(Sheets != null)
                    Sheets.Clear();
                if (BoqItems != null)
                    BoqItems.Clear();
                if (VoItems != null)
                    VoItems.Clear();
                NotifyPropertyChanged("EnabledVoGrid");
                NotifyPropertyChanged("SelectedProject");
            }
        }
        public ObservableCollection<Project> Projects
        {
            get;
            private set;
        }
        private ObservableCollection<BoqSheet> _sheets;
        public ObservableCollection<BoqSheet> Sheets
        {
            get { return _sheets; }
            private set
            {
                _sheets = value;
                NotifyPropertyChanged("Sheets");
            }
        }
        private ObservableCollection<BoqSheet> _voSheets;
        public ObservableCollection<BoqSheet> VOSheets
        {
            get { return _voSheets; }
            private set
            {
                _voSheets = value;
                NotifyPropertyChanged("VOSheets");
            }
        }
        private ObservableCollection<Building> _buildings;
        public ObservableCollection<Building> Buildings
        {
            get { return _buildings; }
            private set
            {
                _buildings = value;
                NotifyPropertyChanged("Buildings");
            }
        }
        private ObservableCollection<BoqItem> _boqItems;
        public ObservableCollection<BoqItem> BoqItems
        {
            get { return _boqItems; }
            private set
            {
                _boqItems = value;
                if(_boqItems != null)
                {
                    _boqItems.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (BoqItem item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (BoqItem item in args.NewItems)
                            {
                                //var x = item.PropertyChanged.GetInvocationList();
                                item.PropertyChanged -= UpdateTotalSale;
                                item.PropertyChanged += UpdateTotalSale;
                            }
                        }
                    };
                }
                NotifyPropertyChanged("BoqItems");
            }
        }

        private double  _boqSum;
        public double BoqSum
        {
            get { return _boqSum; }
            set
            {
                _boqSum = value;
               
                NotifyPropertyChanged("BoqSum");
            }
        }
        private double _voSum;
        public double VoSum
        {
            get { return _voSum; }
            set
            {
                _voSum = value;
                NotifyPropertyChanged("VoSum");
            }
        }

        private ObservableCollection<Vo> _voItems;
        public ObservableCollection<Vo> VoItems
        {
            get { return _voItems; }
            private set
            {
                _voItems = value;
                if(_voItems != null)
                {
                    _voItems.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (Vo item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (Vo item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                                item.PropertyChanged += UpdateTotalSaleVo;
                            }
                        }
                    };
                }
                NotifyPropertyChanged("VoItems");
            }
        }
        private ObservableCollection<Sheet> _orginalSheets;
        public ObservableCollection<Sheet> OrginalSheets
        {
            get { return _orginalSheets; }
            private set
            {
                _orginalSheets = value;
                NotifyPropertyChanged("OrginalSheets");
            }
        }
        private Sheet _orginalSelectedSheet;

        public Sheet OrginalSelectedSheet
        {
            get { return _orginalSelectedSheet; }
            set { _orginalSelectedSheet = value; }
        }

        private ObservableCollection<Data.Currency> _currencies;
        public ObservableCollection<Data.Currency> Currencies
        {
            get { return _currencies; }
            private set
            {
                _currencies = value;
            
                NotifyPropertyChanged("Currencies");
            }
        }

        public string BuildingName { get; set; }
        public int BuildingNumber { get; set; }
        private int _trans;
        public int Trans
        {
            get { return _trans; }
            set
            {
                _trans = value;
                NotifyPropertyChanged("Trans");
            }
        }
        private int _transBoq;
        public int TransBoq
        {
            get { return _transBoq; }
            set
            {
                _transBoq = value;
                NotifyPropertyChanged("TransBoq");
            }
        }

        private ContractsManager.Data.Currency _currency;
        public ContractsManager.Data.Currency Currency
        {
            get { return _currency; }
            set
            {
                _currency = value;
             
                NotifyPropertyChanged("Currency");
            }
        }
        private int _buildingIndex;
        public int BuildingIndex
        {
            get { return _buildingIndex; }
            set
            {
                _buildingIndex = value;
                NotifyPropertyChanged("BuildingIndex");
            }
        }

        public BoqItem CopyBoq { get; set; }
        public Vo CopyVo { get; set; }
        private BoqItem _selectedBoq;
        public BoqItem SelectedBoq
        {
            get { return _selectedBoq; }
            set
            {
                _selectedBoq = value;
                NotifyPropertyChanged("SelectedBoq");
            }
        }
        private Vo _selectedVo;
        public Vo SelectedVo
        {
            get { return _selectedVo; }
            set
            {
                _selectedVo = value;
                NotifyPropertyChanged("SelectedVo");
            }
        }

        private BoqSheet _selecedSheet;
        public BoqSheet SelectedSheet
        {
            get { return _selecedSheet; }
            set
            {
                _selecedSheet = value;
                if(_selecedSheet != null && _selecedSheet.BoqItems != null && _selecedSheet.BoqItems.Count > 0)
                {
                    if(BoqItems == null)
                    {
                        BoqItems = new ObservableCollection<BoqItem>();
                    }
                    BoqItems.Clear();
                    BoqItems.AddRange(_selecedSheet.BoqItems.OrderBy(x => x.OrderBoq));
                    if(BoqItems.Count > 0)
                    {
                        BoqSum = BoqItems.Sum(x => x.Pt);
                    }
                }
                else
                    BoqItems = new ObservableCollection<BoqItem>();

                NotifyPropertyChanged("SelectedSheet");
            }
        }

        private BoqSheet _selecedSheetVo;
        public BoqSheet SelectedSheetVo
        {
            get { return _selecedSheetVo; }
            set
            {
                _selecedSheetVo = value;
                if (_selecedSheetVo == null)
                    return;
                if (_selecedSheetVo.Vos == null)
                    return;
                VoItems = new ObservableCollection<Vo>();
                VoItems.AddRange(_selecedSheetVo.Vos.Where(x => x.Level == VoLevel && x.IsContractVo == false).OrderBy(x => x.OrderVo).ToList());
            
                if (VoItems.Count > 0)
                {
                    VoSum = VoItems.Sum(x => x.Pt);
                }
                NotifyPropertyChanged("SelectedSheetVo");
            }
        }
        private Building _selectedbuidling;
        public Building SelectedBuilding
        {
            get { return _selectedbuidling; }
            set
            {
                _selectedbuidling = value ;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("SelectedBuilding");
            }
        }

        private void UpdateTotalSale(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Pt")
            {
                if(e.PropertyName == "OrderBoq")
                {
                    BoqItems = new ObservableCollection<BoqItem>(
                        BoqItems.OrderBy(x => x.OrderBoq));
                }
                return;
            }
                
            updateBoq();
        }
        private void updateBoq()
        {
            BoqSum = Sum = BoqItems.Sum(x => x.Pt);
        }
        private void UpdateTotalSaleVo(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Pt")
            {
                if (e.PropertyName == "OrderVo")
                {
                    VoItems = new ObservableCollection<Vo>(
                        VoItems.OrderBy(x => x.OrderVo));
                }
                return;
            }
            //Sum = VoItems.Sum(x => x.Pt);
            updateVo();
        }
        private void updateVo()
        { 
            if (VoItems.Count == 0) return;

            VoSum = VoItems.Sum(x => x.Pt);
             

        }

        private Visibility _isOrderVisible;
        public Visibility IsOrderVisible
        {
            get { return _isOrderVisible; }
            set
            {
                _isOrderVisible = value;
                NotifyPropertyChanged("IsOrderVisible");
            }
        }

        public ProjectsViewModel()
        {
            Trans = 0;
            IsBusy = true;
            unitOfWork = new UnitOfWork();

            
            Task.Factory.StartNew(() =>
            {
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var projects = unitOfWork.ProjectRepository.Get();
                if (Currencies == null)
                    Currencies = new ObservableCollection<Data.Currency>();
                var curs = unitOfWork.CurrenciesRepository.Get();
                if (OrginalSheets == null)
                    OrginalSheets = new ObservableCollection<Sheet>();
                var sheet = unitOfWork.SheetsRepository.Get().OrderBy( x => x.Id).ToList();
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          Projects.AddRange(projects.OrderBy(x => x.Code).ToList());
                          Currencies.AddRange(curs);
                          OrginalSheets.AddRange(sheet);

                          CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                          if (customPrincipal.Identity.UserType == UserType.QuantitySurveyor)
                          {
                              IsPreview = false;
                          }
                          else
                              IsPreview = true;

                          if (customPrincipal.Identity.UserType == UserType.Admin)
                          {
                              IsOrderVisible = Visibility.Visible;
                          }
                          else
                              IsOrderVisible = Visibility.Collapsed;

                          IsBusy = false;
                      }));
            });

            Sum = 0;
        }

        public Project Project { get; set; }
        public ICommand SaveCommand => new ActionCommand(p => SaveProject(p));
        public ICommand DeleteProjectCommand => new ActionCommand(p => DeleteProject(p));
        public ICommand CellEditedCommand => new ActionCommand(p => CellEditedEvent(p));
        public ICommand SaveBoqCommand => new ActionCommand(p => SaveBoq(p));
        public ICommand SaveVoCommand => new ActionCommand(p => SaveVo(p));
        public ICommand OpenBoqCommand => new ActionCommand(p => OpenBoq(p));
        public ICommand VoCommand => new ActionCommand(p => OpenVo(p));
        public ICommand UploadBoqCommand => new ActionCommand(p => UploadBoq(p));
        public ICommand AddBuildingCommand => new ActionCommand(p => AddBuilding(p));
        public ICommand NextBuildingCommand => new ActionCommand(p => GoNextBuilding(p));
        public ICommand NextVoCommand => new ActionCommand(p => NextVo());
        public ICommand PreviousBuildingCommand => new ActionCommand(p => GoPreviousBuilding(p));
        public ICommand PreviousVoCommand => new ActionCommand(p => PreviousVo());
        public ICommand AddingNewItemCommand => new ActionCommand(p => AddingNewItem(p));
        public ICommand RowEditEndingCommand => new ActionCommand(p => RowEditEnding(p));
        public ICommand AddingNewVoCommand => new ActionCommand(p => AddingNewVo(p));
        public ICommand AddingNewBoqCommand => new ActionCommand(p => AddingNewBoq(p));
        public ICommand DeleteBoqItemsCommand => new ActionCommand(p => DeleteBoqItems(p));
        public ICommand DeleteKeyCommand => new ActionCommand(p => DeleteBoqKey(p));
        public ICommand DeleteVoKeyCommand => new ActionCommand(p => DeleteVoKey(p));
        public ICommand DeleteSelectedBoqItemsCommand => new ActionCommand(p => DeleteSelectedBoqItems(p));
        public ICommand CopyBoqItemsCommand => new ActionCommand(p => CopyBoqItems(p));
        public ICommand PasteBoqItemsCommand => new ActionCommand(p => PasteBoqItems(p));
        public ICommand CopyVoCommand => new ActionCommand(p => CopyVoItem(p));
        public ICommand PasteVoCommand => new ActionCommand(p => PasteVo(p));
        public ICommand InsertBoqItemsCommand => new ActionCommand(p => InsertBoqItems(p));
        public ICommand DeleteTradeCommand => new ActionCommand(p => DeleteTrade(p));
        public ICommand InsertTradeCommand => new ActionCommand(p => InsertTrade(p));
        public ICommand ClearBoqCommand => new ActionCommand(p => ClearBoq(p));
        public ICommand ClearVoCommand => new ActionCommand(p => ClearVo(p));
        public ICommand SelectedCellsChangedCommand => new ActionCommand(p => SelectedCellsChanged(p));
        public ICommand SelectedCellsChangedVoCommand => new ActionCommand(p => SelectedCellsChangedView(p));
        public ICommand DeleteVoItemsCommand => new ActionCommand(p => DeleteVoItems(p));
        public ICommand InsertVoItemsCommand => new ActionCommand(p => InsertVoItems(p));
        public ICommand RowEditEndingBoqCommand => new ActionCommand(p => RowEditEndingBoq(p));
        public ICommand RowEditEndingVoCommand => new ActionCommand(p => RowEditEndingVo(p));
        public ICommand SelectionChangedCommand => new ActionCommand(p => SelectionChanged(p));
        public ICommand BackToProjectCommand => new ActionCommand(p => BackToProject(p));
        public ICommand AddBuildingsCommand => new ActionCommand(p => AddBuildings(p));
      //  public ICommand BuildingSelectedDialogCommand => new ActionCommand(p => BuildingSelectedDialog(p));
        public ICommand BuildingFilterdDialogCommand => new ActionCommand(p => BuildingFilterDialog(p));
        public ICommand UploadVoCommand => new ActionCommand(p => UploadVo(p));

        private async void BuildingFilterDialog(object p)
        {
            if (SelectedProject == null)
                return;
            var view = new BuildingFilterDialog
            {
                DataContext = this
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Chose")
            {
                BuildingIndex = Buildings.IndexOf(SelectedBuilding);
                if (Buildings.Count > BuildingIndex + 1)
                    NextBuilding = Buildings[BuildingIndex + 1].Name;
                else
                    NextBuilding = "New Building";
                if (BuildingIndex > 0)
                    PreviousBuilding = Buildings[BuildingIndex - 1].Name;
                ChangeBuilding();
            }
        }
        private async Task AddBuildings(object p)
        {
            if (SelectedProject == null)
                return;
            var view = new BuildingDialog
            {
                DataContext = this
            };
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());

            //show the dialog
            try{
                var result = await DialogHost.Show(view, "RootDialog");
                if (SelectedProject.Buildings == null)
                    SelectedProject.Buildings = new List<Building>();
                if ((string)result == "Create")
                {
                    for (int i = 1; i <= BuildingNumber; i++)
                    {
                        var building = new Building { Name = BuildingName +" "+ i, Type = GuidString, ProjectLevel = 1, SubContractorLevel = 1, Boq = new Boq { BoqSheets = new List<BoqSheet>() } };
                        SelectedProject.Buildings.Add(building);
                        Buildings.Add(building);

                        Sheets = new ObservableCollection<BoqSheet>();
                        var sheets = OrginalSheets.Select(x => new BoqSheet { Name = x.Name, Boq = building.Boq, CostCode = x.CostCode }).ToList();
                        Sheets.AddRange(sheets);

                        building.Boq.BoqSheets = sheets;
                        foreach (var item in sheets)
                        {
                            unitOfWork.BoqSheetRepository.Insert(item);
                        }
                    }
                    BuildingIndex = 0;
                    SelectedBuilding = Buildings[BuildingIndex];
                    //PreviousBuilding = Buildings[BuildingIndex - 1].Name;

                    if (Buildings.Count > BuildingIndex + 1)
                        NextBuilding = Buildings[BuildingIndex + 1].Name;
                    else
                        NextBuilding = "New Building";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        private void SelectionChanged(object p)
        {
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if (!(dataGrid.SelectedItem is Project))
                SelectedProject = null;
        }

        private void RowEditEndingBoq(object p)
        {   
            updateBoq();
        }
          private void RowEditEndingVo(object p)
        {
            updateVo();
        }

        private void DeleteTrade(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (ListBox)contextMenu.PlacementTarget;
            foreach (BoqSheet sheet in item.SelectedItems.OfType<BoqSheet>().ToList())
            {
                //boq.IsHide = true;
                Sheets.Remove(sheet);
                SelectedBuilding.Boq.BoqSheets.Remove(sheet);
                BoqItems.Clear();
            }
        }
        private async void InsertTrade(object p)
        {
            var sheet = new BoqSheet();
            var view = new SheetDialog
            {
                DataContext = sheet
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Save")
            {
                if (Sheets == null) Sheets = new ObservableCollection<BoqSheet>();
                sheet.BoqItems = new List<BoqItem>();
                SelectedBuilding.Boq.BoqSheets.Add(sheet);
                Sheets.Clear();
                Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy( x=>x.Created).ToList());
                unitOfWork.Save();
            }
        }
        private async void ClearVo(object p)
        {
            var view = new ClearDialogVo
            {
                DataContext = this
            };

            var result = await DialogHost.Show(view, "RootDialog");
            if (SelectedSheetVo == null)
            {
                MessageBox.Show("Select a building before deleting VOs", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if ((string)result == "Clear")
            {     
                if (AllT == true)
                {
                    if (SelectedBuilding == null)
                    {
                        MessageBox.Show("Select a building clearing its BOQ", "Warning. Building Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    foreach (var sheet in SelectedBuilding.Boq.BoqSheets.ToList())
                    {
                        if (sheet.Vos == null)
                            continue;
                        foreach (var vo in sheet.Vos.ToList())
                        {
                            SelectedSheetVo.Vos.Remove(vo);
                            //unitOfWork.ContractVoRepository.Delete(vo.Id);
                        }
                        VoItems.Clear();
                        VoSum = 0;
                    }
                }
                else if (OnlyThis == true)
                {
                    if (SelectedSheet == null)
                    {
                        MessageBox.Show("Select a trade clearing its BOQ", "Warning. trade Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    if (SelectedSheetVo.Vos != null)
                    {
                        foreach (var vo in SelectedSheetVo.Vos.ToList())
                        {
                            SelectedSheetVo.Vos.Remove(vo);
                            //unitOfWork.ContractVoRepository.Delete(vo.Id);
                        }
                        VoItems.Clear();
                        VoSum = 0;
                    }
                  
                }

                VoSum = 0;
            }
        }
        private async void ClearBoq(object p)
        {
            var view = new ClearDialog
            {
                DataContext = this
            };

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Clear")
            {
                if(AllB == true)
                {
                    foreach (var building in SelectedProject.Buildings.ToList())
                    {
                        if (building == null)
                            continue;
                        foreach (var sheet in building.Boq.BoqSheets.ToList())
                        {
                            if (sheet == null)
                                continue;
                            if (sheet.BoqItems == null)
                                continue;

                            foreach (var boq in sheet.BoqItems.ToList())
                            {
                                sheet.BoqItems.Remove(boq);
                            }
                            BoqItems.Clear();
                            BoqSum = 0;
                        }
                    }
                }
                else if(AllT == true)
                {
                    if(SelectedBuilding == null)
                    {
                        MessageBox.Show("Select a building before clearing its BOQ", "Warning. Building Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    foreach (var sheet in SelectedBuilding.Boq.BoqSheets.ToList())
                    {
                        if (sheet.BoqItems == null)
                            continue;
                        foreach (var boq in sheet.BoqItems.ToList())
                        {
                            sheet.BoqItems.Remove(boq);
                        }
                        BoqItems.Clear();
                        BoqSum = 0;
                    }
                }
                else if(OnlyThis == true)
                {
                    if (SelectedSheet == null)
                    {
                        MessageBox.Show("Select a trade before clearing its BOQ", "Warning. trade Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    if (SelectedSheet.BoqItems != null)
                    {
                        foreach (var boq in SelectedSheet.BoqItems.ToList())
                        {
                            SelectedSheet.BoqItems.Remove(boq);
                            unitOfWork.BoqItemRepository.Delete(boq);
                        }
                        SelectedSheet.IsActive = false;
                        BoqItems.Clear();
                        BoqSum = 0;
                        unitOfWork.Save();
                    }
                    
                }
            }
        }
        private async void SelectedCellsChanged(object p)
        {
            var p1 = p as MouseButtonEventArgs;
            var item = p1.Source as DataGrid;
            if (item.CurrentColumn == null) return;
            if (SelectedBoq == null) return;
     
            item.CommitEdit();
            item.CancelEdit();
            var col = item.CurrentColumn.Header as string;
           
            if (col == "Cost Code")
            {
                IsBusy = true;

                var view = new CostCodeDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if (result is string)
                {
                    //unitOfWork.Save();
                    if (BoqItems != null)
                        BoqItems.Clear();
                    else
                        BoqItems = new ObservableCollection<BoqItem>();
                    BoqItems.AddRange(SelectedSheet.BoqItems.OrderBy(x => x.No).ToList());
                    IsBusy = false;
                }
                else
                {
                    var costCode = unitOfWork.CostCodeLibraryRepository.Get(x => x.Id == ((CostCodeLibrary)result).Id).FirstOrDefault();
                    if(SelectedBoq != null)
                        SelectedBoq.CostCode = costCode;
                    if (BoqItems != null)
                        BoqItems.Clear();
                    else
                        BoqItems = new ObservableCollection<BoqItem>();
                    BoqItems.AddRange(SelectedSheet.BoqItems.OrderBy(x => x.OrderBoq).ToList());
                    IsBusy = false;
                }
               
            }
           
        }
        private async void SelectedCellsChangedView(object p)
        {
            var p1 = p as MouseButtonEventArgs;
            var item = p1.Source as DataGrid;
            if (item.CurrentColumn == null) return;
            if (SelectedVo == null) return;

            item.CommitEdit();
            item.CancelEdit();

            var col = item.CurrentColumn.Header as string;
            if (col == "Cost Code")
            {
                IsBusy = true;

                var view = new CostCodeDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if (result is string)
                {
                    if (VoItems != null)
                        VoItems.Clear();
                    else
                        VoItems = new ObservableCollection<Vo>();
                    VoItems.AddRange(SelectedSheetVo.Vos.ToList());
                    IsBusy = false;
                }
                else
                {
                    if (VoItems != null)
                        VoItems.Clear();
                    else
                        VoItems = new ObservableCollection<Vo>();
                    VoItems.AddRange(SelectedSheetVo.Vos.OrderBy(x=>x.OrderVo).ToList());

                    var costCode = unitOfWork.CostCodeLibraryRepository.Get(x => x.Id == ((CostCodeLibrary)result).Id).FirstOrDefault();
                    if(SelectedVo != null)
                        SelectedVo.CostCode = costCode;

                    IsBusy = false;
                }
            }
        }
        private async void DeleteSelectedBoqItems(object p)
        {
            if (SelectedBoq == null) return;
            var view = new ConfirmDialog();
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                SelectedSheet.BoqItems.Remove(SelectedBoq);
                BoqItems.Remove(SelectedBoq);
                updateBoq();
            }

        }
        private async void DeleteBoqKey(object p)
        {
            if (SelectedBoq == null)
                return;
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {
                var view = new ConfirmDialog();
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                {
                    SelectedSheet.BoqItems.Remove(SelectedBoq);
                    BoqItems.Remove(SelectedBoq);
                    updateBoq();
                }
            }
        }
        private async void DeleteVoKey(object p)
        {
            if (SelectedVo == null)
                return;
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {

                var view = new ConfirmDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Delete")
                {
                    VoItems.Remove(SelectedVo);
                    SelectedSheetVo.Vos = VoItems;

                    unitOfWork.Save();
                    updateVo();
                }
            }
        }
        private async void DeleteBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                foreach (BoqItem boq in itemg.SelectedItems.OfType<BoqItem>().ToList())
                {
                    SelectedSheet.BoqItems.Remove(boq);
                    BoqItems.Remove(boq);
                }
                updateBoq();
            }
       
        }
        private void CopyBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            BoqItem boq = itemg.SelectedItem as BoqItem;
            CopyBoq = boq;
        }
        private void PasteBoqItems(object p)
        {
            if (CopyBoq == null)
                return;
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            BoqItem boq = itemg.SelectedItem as BoqItem;
            double order;
            var newBoq = new BoqItem
            {
                BoqSheet = CopyBoq.BoqSheet,
                BOQType = CopyBoq.BOQType,
                CostCode = CopyBoq.CostCode,
                Key = CopyBoq.Key,
                No = CopyBoq.No,
                Pu = CopyBoq.Pu,
                Pt = CopyBoq.Pt,
                Unite = CopyBoq.Unite,
                Qte = CopyBoq.Qte
            };

            var index = BoqItems.IndexOf(boq);
            if (index == -1)
            {
                if(BoqItems.Count == 0)
                {
                    order = 1.0f;
                    newBoq.OrderBoq = order;
                    BoqItems.Add(newBoq);
                }
                else
                {
                    order = BoqItems.Last().OrderBoq+0.1f;
                    newBoq.OrderBoq = order;
                    BoqItems.Add(newBoq);
                }
            }
            else
            {
                order = boq.OrderBoq + 0.1f;
                newBoq.OrderBoq = order;
                BoqItems.Insert(index, newBoq);
            }
            SelectedSheet.BoqItems.Add(newBoq);
            //unitOfWork.BoqItemRepository.Insert(CopyBoq);
            unitOfWork.Save();
            updateBoq();

        }
        private void InsertBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            BoqItem boq = itemg.SelectedItem as BoqItem;
            if (boq == null) return;
            if (boq.BOQType != BOQType.Line)
            {
                return;
            }
            var index = BoqItems.IndexOf(boq);
            var newBoq = new BoqItem {  BoqSheet = boq.BoqSheet, BOQType = BOQType.Line, OrderBoq = boq.OrderBoq+0.01f };
            BoqItems.Insert(index, newBoq);
            SelectedSheet.BoqItems.Add(newBoq);
            updateBoq();
        }
        private async void DeleteVoItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                foreach (Vo vo in itemg.SelectedItems.OfType<Vo>().ToList())
                {
                    //boq.IsHide = true;
                    VoItems.Remove(vo);
                    SelectedSheetVo.Vos = VoItems;
                }

                //unitOfWork.Save();
                updateVo();
            }
        }

        private void CopyVoItem(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            Vo vo = itemg.SelectedItem as Vo;
            CopyVo = vo;
        }
        private void PasteVo(object p)
        {
            if (CopyVo == null)
                return;
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            Vo vo = itemg.SelectedItem as Vo;
            double order = 1.0f;
            var index = VoItems.IndexOf(vo);
            var newVo = new Vo
            {
                BoqSheet = SelectedSheetVo,
                BOQType = CopyVo.BOQType,
                CostCode = CopyVo.CostCode,
                Key = CopyVo.Key,
                No = CopyVo.No,
                OrderVo = order,
                Level = CopyVo.Level,
                Pu = CopyVo.Pu,
                Pt = CopyVo.Pt,
                Unite = CopyVo.Unite,
                Qte = CopyVo.Qte
            };

            if (index == -1)
            {
                if (VoItems.Count == 0)
                {
                    order = 1.0f;
                    VoItems.Add(newVo);
                }
                else
                {
                    order = VoItems.Last().OrderVo + 0.1f;
                    newVo.OrderVo = order;
                    VoItems.Add(newVo);

                }
            }
            else
            {
                order = vo.OrderVo + 0.1f;
                newVo.OrderVo = order;
                VoItems.Insert(index, newVo);
            }
            SelectedSheetVo.Vos.Add(newVo);
            unitOfWork.VoRepository.Insert(newVo);
            unitOfWork.Save();
            updateVo();
        }
        private void InsertVoItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            Vo vo = itemg.SelectedItem as Vo;
            if (vo.BOQType != BOQType.Line)
            {
                return;
            }
            var index = VoItems.IndexOf(vo);
            var newBoq = new Vo { BoqSheet = vo.BoqSheet, BOQType = BOQType.Line, Level = vo.Level, OrderVo = vo.OrderVo+0.01f };
            VoItems.Insert(index, newBoq);
            SelectedSheetVo.Vos.Add(newBoq);

            updateVo();
        }
        private void CellEditedEvent(object p)
        {

        }
        private void DeleteProject(object p)
        {
            var cont = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject.Id).ToList();
            if (cont.Count > 0) {
                MessageBox.Show("Can't delete this project", "Warning", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            unitOfWork.ProjectRepository.Delete(SelectedProject.Id);
            unitOfWork.Save();
            Projects.Remove(SelectedProject);
        }
        private void SaveProject(object p)
        {
            try
            {
                foreach (var project in Projects)
                {
                    if (project.Id == 0)
                    {
                        unitOfWork.ProjectRepository.Insert(project);
                    }
                    else
                    {
                        unitOfWork.ProjectRepository.Update(project);
                    }
                }
                unitOfWork.Save();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
        }
        private void RowEditEnding(object p)
        {
            SaveCommand.Execute(null);

        }
        private void AddingNewItem(object p)
        {
            string code = "";
            if (Projects.Count <= 0)
            {
                code = "A01";
            }
            else
                code = "A" + (Convert.ToInt32(Regex.Match(Projects.Last().Code, @"\d+").Value) + 1).ToString("00");



            (p as AddingNewItemEventArgs).NewItem = new Data.Project
            {
                Code = code, //whatever you put here will be the default value
                Name = "",
                Acronym = "",
                City = "",
            };

            unitOfWork.ProjectRepository.Insert(((Project)(p as AddingNewItemEventArgs).NewItem));
            unitOfWork.Save();

        }
        private void AddingNewVo(object p)
        {
            if (SelectedSheetVo == null)
            {
                MessageBox.Show("Select a building before adding new BOQ", "Warning. Building Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            double order;
            if(SelectedSheetVo.Vos == null)
            {
                SelectedSheetVo.Vos = new List<Vo>();
                order = 1.0f;
            }
            if (SelectedSheetVo.Vos.Count == 0)
            {
                order = 1.0f;
                SelectedSheetVo.IsActive = true;

            }
            else
            {
                order = SelectedSheetVo.Vos.ElementAt(SelectedSheetVo.Vos.Count - 1).OrderVo;

            }
            (p as AddingNewItemEventArgs).NewItem = new Vo
            {
                BOQType = BOQType.Line,
                Level = SelectedBuilding.ProjectLevel,
                BoqSheet = SelectedSheetVo,
                OrderVo = order + 1.0f
            };
            SelectedSheetVo.Vos.Add((Vo)(p as AddingNewItemEventArgs).NewItem);
            updateVo();
        }
        private void AddingNewBoq(object p)
        {
            if (SelectedSheet == null)
            {
                MessageBox.Show("Select a trade before adding new BOQ", "Warning. Trade Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            double order;
            if (SelectedSheet.BoqItems == null)
            {
                SelectedSheet.BoqItems = new List<BoqItem>();
                SelectedSheet.IsActive = true;
            }


            if (SelectedSheet.BoqItems.Count == 0)
            {
                order = 1.0f;
                SelectedSheet.IsActive = true;

            }
            else
            {
                order = SelectedSheet.BoqItems.ElementAt(SelectedSheet.BoqItems.Count - 1).OrderBoq;

            }
              (p as AddingNewItemEventArgs).NewItem = new Data.BoqItem
            {
                BOQType = BOQType.Line,
                BoqSheet = SelectedSheetVo,
                OrderBoq = order + 1.0f
            };
            SelectedSheet.BoqItems.Add((BoqItem)(p as AddingNewItemEventArgs).NewItem);
            updateBoq();
        }
        private async void OpenBoq(object p)
        {
            if (SelectedProject == null)
            {
                MessageBox.Show("Select a project before opening Project BOQ", "Warning. Project Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                
                SelectedProject = unitOfWork.ProjectRepository.Get(x => x.Id == SelectedProject.Id).FirstOrDefault();
                Trans = 1;
                
                Buildings = new ObservableCollection<Building>();
                if (SelectedProject.Buildings == null)
                {
                    SelectedProject.Buildings = new List<Building>();
                    Buildings = new ObservableCollection<Building>();
                }
                else
                {
                    Buildings.AddRange(SelectedProject.Buildings);
                }
                SelectedBuilding = SelectedProject.Buildings.FirstOrDefault();
                if (SelectedBuilding == null)
                {
                    await AddBuildings(null);
                }
                else if (SelectedBuilding != null)
                {
                    if (SelectedBuilding.Boq == null)
                        SelectedBuilding.Boq = new Boq();
                    //if(SelectedBuilding.Boq.Currency != null)
                    //    Currency = Currencies.Where( x=> x.Currencies == SelectedProject.Currency).FirstOrDefault() ;
                    if (SelectedBuilding.Boq.BoqSheets != null)
                    {
                        Sheets = new ObservableCollection<BoqSheet>();
                        var sheets = SelectedBuilding.Boq.BoqSheets.OrderBy(x=>x.CostCodeId).ToList();
                        Sheets.AddRange(sheets);
                        SelectedSheet = Sheets.FirstOrDefault();
                    }
                }
                VoLevel = SelectedBuilding.ProjectLevel;
                if(VoLevel == 1)
                {
                    IsNextVoVisibil = Visibility.Collapsed;
                    IsPreviousVoVisibil = Visibility.Collapsed;
                }
                else if(VoLevel > 1)
                {
                    IsNextVoVisibil = Visibility.Collapsed;
                    IsPreviousVoVisibil = Visibility.Visible;
                    PreviousVoString = "Vo#" + (VoLevel - 1).ToString();

                }
              
                BuildingIndex = 0;
                if (Buildings.Count > 1)
                {
                    NextBuilding = Buildings[1].Name;
                }
                else
                {
                    NextBuilding = "New Building";
                }
                TransBoq = 0;
                var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
        private void BackToProject(object p)
        {
            Trans = 0;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
        }
        private void OpenVo(object p)
        {
            if(Sheets != null)
                SelectedSheetVo = Sheets.FirstOrDefault();
            Trans = 2;
        }
        private void GoPreviousBuilding(object p)
        {
            BuildingIndex--;
            SelectedBuilding = Buildings[BuildingIndex];

            if (BuildingIndex == 0)
            {
                NextBuilding = Buildings[BuildingIndex + 1].Name;
            }
            else
            {
                NextBuilding = Buildings[BuildingIndex + 1].Name;
                PreviousBuilding = Buildings[BuildingIndex - 1].Name;
            }
            
            ChangeBuilding();
        }
        private async void GoNextBuilding(object p)
        {
          
            if(NextBuilding == "New Building")
            {
                var view = new ConfirmNewBuildingDialog();

                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result != "Yes")
                {
                    return;
                }

                BuildingIndex++;
                SelectedBuilding = new Building { Name = "", ProjectLevel =1, SubContractorLevel=1, Boq = new Boq { BoqSheets = new List<BoqSheet>() } };
                SelectedProject.Buildings.Add(SelectedBuilding);
                Buildings.Add(SelectedBuilding);
                NextBuilding = "New Building";

                Sheets = new ObservableCollection<BoqSheet>();
                var sheets = OrginalSheets.Select(x => new BoqSheet { Name = x.Name, Boq = SelectedBuilding.Boq, CostCode = x.CostCode }).ToList();
                Sheets.AddRange(sheets.OrderBy(x => x.Created).ToList());
                SelectedBuilding.Boq.BoqSheets = sheets;
                foreach (var item in sheets)
                {
                    unitOfWork.BoqSheetRepository.Insert(item);
                }


                PreviousBuilding = Buildings[BuildingIndex - 1].Name;
                unitOfWork.Save();
            }
            else
            {
                BuildingIndex++;
                SelectedBuilding = Buildings[BuildingIndex];
                PreviousBuilding = Buildings[BuildingIndex - 1].Name;

                if (Buildings.Count > BuildingIndex + 1)
                    NextBuilding = Buildings[BuildingIndex + 1].Name;
                else
                    NextBuilding = "New Building";
            }
            ChangeBuilding();
        }

        private void NextVo()
        {
            VoLevel++;

            VoItems = new ObservableCollection<Vo>();
      
            if (VoLevel == SelectedBuilding.ProjectLevel - 1)
                NextVoString = "New Vo";
            else
                NextVoString = "VO#" + (VoLevel + 1).ToString();

            PreviousVoString = "VO#" + (VoLevel - 1).ToString();
            if (VoLevel == SelectedBuilding.ProjectLevel)
                IsNextVoVisibil = Visibility.Collapsed;
            if (VoLevel > 1)
            {
                IsPreviousVoVisibil = Visibility.Visible;
            }
            if (Sheets != null)
                SelectedSheetVo = Sheets.FirstOrDefault();
        }
        private void PreviousVo()
        {
            VoLevel--;
            VoItems = new ObservableCollection<Vo>();
     
            if (VoLevel == SelectedBuilding.ProjectLevel - 1)
                NextVoString = "New Vo";
            else
                NextVoString = "VO#" + (VoLevel + 1).ToString();

            PreviousVoString = "VO#" + (VoLevel - 1).ToString();
            if (VoLevel == 1)
                IsPreviousVoVisibil = Visibility.Collapsed;

            if (SelectedBuilding.ProjectLevel > 1)
                IsNextVoVisibil = Visibility.Visible;
            if (Sheets != null)
                SelectedSheetVo = Sheets.FirstOrDefault();
        }

        private void AddBuilding(object p)
        {
            ////if (SelectedProject.Buildings == null)
            ////SelectedProject.Buildings = new Collection<Building>();
            //unitOfWork.Save();
            //
            //SelectedProject.Buildings.Add(new Building { Name = "Building Title" });
            //SelectedBuilding = SelectedProject.Buildings.Last();
            //
            //ChangeBuilding();
            //
            //BuildingIndex = SelectedProject.Buildings.Count - 1;
            //IsNextEnabled = false;
            //IsPreviousEnabled = true;
        }
        private void ChangeBuilding()
        {
            if (SelectedBuilding.Boq != null)
            {
                if(SelectedBuilding.Boq.BoqSheets != null)
                {
                    Sheets = new ObservableCollection<BoqSheet>();
                    var sheets = SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Created).ToList();
                    Sheets.AddRange(sheets);
                    SelectedSheet = SelectedBuilding.Boq.BoqSheets.FirstOrDefault();
                }
            }
            else
            {
                if (Sheets != null) Sheets.Clear();
                if (BoqItems != null) BoqItems.Clear();
                if (VoItems != null) VoItems.Clear();
            }
            VoLevel = SelectedBuilding.ProjectLevel;
            if (VoLevel == 1)
            {
                IsNextVoVisibil = Visibility.Collapsed;
                IsPreviousVoVisibil = Visibility.Collapsed;
            }
            else if (VoLevel > 1)
            {
                IsNextVoVisibil = Visibility.Collapsed;
                IsPreviousVoVisibil = Visibility.Visible;
                PreviousVoString = "Vo#" + (VoLevel - 1).ToString();

            }
        }
        private async void UploadVo(object p)
        {
            if (SelectedBuilding == null)
                return;

            var view1 = new ConfirmUploadBoq
            {
                DataContext = this
            };

            var result1 = await DialogHost.Show(view1, "RootDialog");
            if ((string)result1 == "Import")
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

                if (openFileDialog.ShowDialog() ?? false)
                {
                    IsUploading = true;
                    Task.Factory.StartNew(() =>
                    {
                        var excel = new Excel(openFileDialog.FileName);

                        if (SelectedBuilding.Boq == null)
                            SelectedBuilding.Boq = new Boq();
                        var buildings = Buildings.Where(x => x.Type == SelectedBuilding.Type).ToList();

                  
                       // var test = excel.GetSheets().ToList();
                        foreach (var building in buildings)
                        {
                            foreach (var sheet in building.Boq.BoqSheets)
                            {
                                double orderBoq = 1.0f;
                                //if(sheet.BoqItems == null)
                                var result = excel.GetVoItems(sheet, ref orderBoq, unitOfWork, SelectedBuilding.ProjectLevel);
                                if (result != null)
                                {
                                    foreach (var item in result)
                                    {
                                        sheet.Vos.Add(item);

                                    }
                                }
                                    
                            }
                        }

                        unitOfWork.Save();
                        excel.Close();
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          IsUploading = false;

                          if (Sheets == null)
                          {

                              Sheets = new ObservableCollection<BoqSheet>();
                              Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Created).ToList());
                              SelectedSheetVo = SelectedBuilding.Boq.BoqSheets.FirstOrDefault();
                          }
                          else
                          {
                              Sheets.Clear();
                              Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Created).ToList());
                              SelectedSheetVo = Sheets.Where(x => x.IsActive == true).FirstOrDefault();
                          }
                          VoItems = new ObservableCollection<Vo>();
                          VoItems.AddRange(SelectedSheetVo.Vos);
                          if (VoItems.Count > 0)
                          {
                              VoSum = VoItems.Sum(x => x.Pt);
                          }
                      }));
                    });
                }
                return;
            }

            else
            {
                if (SelectedBuilding == null)
                    return;
               
                if (VoLevel == SelectedBuilding.ProjectLevel)
                {
                    var cvo = new List<Vo>();
                    if (SelectedSheetVo.BoqItems == null) return;
                    foreach (var item in SelectedSheetVo.BoqItems.ToList())
                    {
                        Vo voParent = new Vo
                        {
                            No = item.No,
                            Key = item.Key,
                            Unite = item.Unite,
                            CostCode = item.CostCode,
                            Qte = item.Qte,
                            Pu = item.Pu,
                            Pt = item.Pt,
                            OrderVo = item.OrderBoq,
                            BoqSheet = item.BoqSheet,
                            BOQType = BOQType.Line,
                            Level = SelectedBuilding.ProjectLevel
                        };
                        cvo.Add(voParent);
                    }
                    VoItems.AddRange(cvo.OrderBy(x => x.OrderVo));
                    foreach (var item in VoItems)
                    {
                        SelectedSheetVo.Vos.Add(item);
                
                    }
                    
                    SelectedSheetVo.HasVo = true;
                    unitOfWork.Save();
                }
                
                else
                {
                    VoItems.AddRange(_selecedSheetVo.Vos.Where(x => x.Level == VoLevel && x.IsContractVo == false).OrderBy(x => x.OrderVo).ToList());
                }
                if (VoItems.Count > 0)
                {
                    VoSum = VoItems.Sum(x => x.Pt);
                }
            }
        }
        private void UploadBoq(object p)
        {
            //TODO:: SELECTED BUILDING MUST NOT BE CREATED ANOTHER TIME AND ADDED TO BUILDINGS OC<>
            if (SelectedBuilding == null)
                return;
            var boqCount = SelectedProject.Buildings.Where(x => x.Boq.BoqSheets != null).SelectMany(x => x.Boq.BoqSheets).Where(x => x.Vos != null).SelectMany(x => x.Vos).Count();
            if (boqCount > 0)
            {
                MessageBox.Show("Importing new BOQ will clear the previous data", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
               // return;
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() ?? false)
            {
                IsUploading = true;
                Task.Factory.StartNew(() =>
                    {
                        var excel = new Excel(openFileDialog.FileName);

                        if (SelectedBuilding.Boq == null)
                            SelectedBuilding.Boq = new Boq();
                        var buildings = Buildings.Where(x => x.Type == SelectedBuilding.Type).ToList();
                        
                        double orderBoq = 1.0f;
                        var test = excel.GetSheets().ToList();
                        foreach (var building in buildings)
                        {
                            foreach (var sheet in building.Boq.BoqSheets)
                            {
                                //if(sheet.BoqItems == null)
                                var result = excel.GetItems(sheet, ref orderBoq, unitOfWork);
                                if (result != null)
                                    sheet.BoqItems = result;
                            }
                        }
                        
                        unitOfWork.Save();
                        excel.Close();
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          IsUploading = false;

                          if (Sheets == null)
                          {

                              Sheets = new ObservableCollection<BoqSheet>();
                              Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Created).ToList());
                              SelectedSheet = SelectedBuilding.Boq.BoqSheets.FirstOrDefault();
                          }
                          else
                          {
                              Sheets.Clear();
                              Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Created).ToList());
                              SelectedSheet = Sheets.Where( x => x.IsActive == true).FirstOrDefault();
                          }
                              
                      }));
                    });
             
                
         //TODO:: SAVE FILE TO Boq
               
                //byte[] bytes = ReadFile(openFileDialog.FileName);
                //SelectedBuilding.Boq.Content = bytes;
            }
        }
        private async void SaveVo(object p)
        {
            unitOfWork.Save();

            try
            {
                //var sheet = Sheets.Where(x => x.Vos != null).SelectMany(x => x.Vos).Where(x => x.Level == SelectedProject.VoLevel).ToList();
                //if (sheet.Count != 0)
                //{
                    var view1 = new SaveVoDialog();

                    var result1 = await DialogHost.Show(view1, "RootDialog");

                    if ((string)result1 == "Yes")
                    {

                        SelectedBuilding.ProjectLevel++;
                        VoLevel = SelectedBuilding.ProjectLevel;
                        if (VoLevel > 1)
                        {
                            IsPreviousVoEnabled = true;
                            IsNextVoEnabled = false;
                        }
                        Vos = new ObservableCollection<dynamic>();
                        for (int i = 1; i < Buildings.Count; i++)
                        {
                            for (int j = 0; j < Buildings[i].ProjectLevel; j++)
                            {
                                dynamic vsd = new System.Dynamic.ExpandoObject();
                                vsd.Name = Buildings[i].Name + " - VO" + j;
                                vsd.Level = i;
                                vsd.BuildingName = Buildings[i].Name;
                                Vos.Add(vsd);
                            }
                        
                        }
                        
                        if (VoItems != null)
                                VoItems.Clear();
                            if (BoqItems != null)
                                BoqItems.Clear();
                            Trans = 0;
                    SelectedSheetVo = SelectedBuilding.Boq.BoqSheets.FirstOrDefault();
                    SelectedSheet = SelectedBuilding.Boq.BoqSheets.FirstOrDefault();
                }
                else
                    {
                        return;
                    }
                //}

                unitOfWork.ProjectRepository.Update(SelectedProject);
                unitOfWork.Save();

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

           // SelectedSheet = null;
           // SelectedSheetVo = null;

            Trans = 1;
        }
        private async void SaveBoq(object p)
        {
            if (SelectedProject.Currency == null)
            {
                var view = new ErrorDialog();

                var result = await DialogHost.Show(view, "RootDialog");

                return;
            }
            else
            {
                var view = new SaveDialog();

                var result = await DialogHost.Show(view, "RootDialog");

                if ((string)result == "Yes")
                {
                    Trans = 0;
                    TransBoq = 0;
                    var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                    win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
                }
                else
                {
                    return;
                }
            }
            unitOfWork.ProjectRepository.Update(SelectedProject);
            unitOfWork.Save();
        }
        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);

            //Close BinaryReader
            br.Close();

            //Close FileStream
            fStream.Close();

            return data;
        }
    }
}
