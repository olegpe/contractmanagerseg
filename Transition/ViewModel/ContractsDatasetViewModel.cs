﻿using ContractsManager.Data;
using ContractsManager.Data.Excel;
using ContractsManager.Data.Repository;
using ContractsManager.Data.utils;
using ContractsManager.Dialogs;
using ContractsManager.Windows;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ContractsManager.ViewModel
{
    public class ContractsDatasetViewModel : ContractsManager.Windows.ViewModel
    {
        public UnitOfWork unitOfWork { get; set; }

        public bool AllT { get; set; }
        public bool OnlyThis { get; set; }
        private bool _isDeleteEnabled;
        public bool IsDeleteEnabled
        {
            get
            {
                return _isDeleteEnabled;
            }
            set
            {
                _isDeleteEnabled = value;
                NotifyPropertyChanged("IsDeleteEnabled");
            }
        }
        private bool _isVoDeleteEnabled;
        public bool IsVoDeleteEnabled
        {
            get
            {
                return _isVoDeleteEnabled;
            }
            set
            {
                _isVoDeleteEnabled = value;
                NotifyPropertyChanged("IsVoDeleteEnabled");
            }
        }

        public IEnumerable<string> VoTypes
        {
            get
            {
                yield return "Addition";
                yield return "Omission";
            }
        }
        public IEnumerable<string> PaymentsTerms
        {
            get
            {
                yield return "Effet";
                yield return "Chèque ou effet";
                yield return "Chèque ou virement";
            }
        }

        private bool _isVoTerminateEnabled;
        public bool IsVoTerminateEnabled
        {
            get
            {
                return _isVoTerminateEnabled;
            }
            set
            {
                _isVoTerminateEnabled = value;
                NotifyPropertyChanged("IsVoTerminateEnabled");
            }
        }

        private bool _isTerminateEnabled;
        public bool IsTerminateEnabled
        {
            get
            {
                return _isTerminateEnabled;
            }
            set
            {
                _isTerminateEnabled = value;
                NotifyPropertyChanged("IsTerminateEnabled");
            }
        }

        private bool _isEditableEnabled;
        public bool IsEditableEnabled
        {
            get
            {
                return _isEditableEnabled;
            }
            set
            {
                _isEditableEnabled = value;
                NotifyPropertyChanged("IsEditableEnabled");
            }
        }


        private bool _isVoEditableEnabled;
        public bool IsVoEditableEnabled
        {
            get
            {
                return _isVoEditableEnabled;
            }
            set
            {
                _isVoEditableEnabled = value;
                NotifyPropertyChanged("IsVoEditableEnabled");
            }
        }

        private bool _isPreviousEnabled;
        public bool IsPreviousEnabled
        {
            get
            {
                return _isPreviousEnabled;
            }
            set
            {
                _isPreviousEnabled = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsPreviousEnabled");
            }
        }
        private bool _isUploading;
        public bool IsUploading
        {
            get
            {
                return _isUploading;
            }
            set
            {
                _isUploading = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsUploading");
            }
        }
        private bool _isVoGridEnabled;
        public bool IsVoGridEnabled
        {
            get
            {
                return _isVoGridEnabled;
            }
            set
            {
                _isVoGridEnabled = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsVoGridEnabled");
            }
        }
        private bool _contractTypeLumpSum;
        public bool ContractTypeLumpSum
        {
            get
            {
                return _contractTypeLumpSum;
            }
            set
            {
                _contractTypeLumpSum = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ContractTypeLumpSum");
            }
        }
        private bool _contractTypeRemasured;
        public bool ContractTypeRemasured
        {
            get
            {
                return _contractTypeRemasured;
            }
            set
            {
                _contractTypeRemasured = value;
                NotifyPropertyChanged("ContractTypeRemasured");
            }
        }
        private bool _contractTypeCostPlus;
        public bool ContractTypeCostPlus
        {
            get
            {
                return _contractTypeCostPlus;
            }
            set
            {
                _contractTypeCostPlus = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ContractTypeCostPlus");
            }
        }
        private bool _isChecked;
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsChecked");
            }
        }
        private bool _isCheckedEx;
        public bool IsCheckedEx
        {
            get
            {
                return _isCheckedEx;
            }
            set
            {
                _isCheckedEx = value;
                NotifyPropertyChanged("IsCheckedEx");
            }
        }
        private bool _isCheckedVo;
        public bool IsCheckedVo
        {
            get
            {
                return _isCheckedVo;
            }
            set
            {
                _isCheckedVo = value;
                NotifyPropertyChanged("IsCheckedVo");
            }
        }
        private int _newCDTrans;
        public int NewCDTrans
        {
            get
            {
                return _newCDTrans;
            }
            set
            {
                _newCDTrans = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("NewCDTrans");
            }
        }
        private int _cdTransIndex;
        public int CdTransIndex
        {
            get
            {
                return _cdTransIndex;
            }
            set
            {
                _cdTransIndex = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("CdTransIndex");
            }
        }
        private int _newDatasetTrans;
        public int NewDatasetTrans
        {
            get
            {
                return _newDatasetTrans;
            }
            set
            {
                _newDatasetTrans = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("NewDatasetTrans");
            }
        }
        private bool _isNextEnabled;
        public bool IsNextEnabled
        {
            get
            {
                return _isNextEnabled;
            }
            set
            {
                _isNextEnabled = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsNextEnabled");
            }
        }
        public int BuildingIndex { get; set; }
        private string _buildingName;
        public string BuildingName
        {
            get { return _buildingName; }
            set
            {
                _buildingName = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("BuildingName");
            }
        }
        private bool _isPreviousVoEnabled;
        public bool IsPreviousVoEnabled
        {
            get
            {
                return _isPreviousVoEnabled;
            }
            set
            {
                _isPreviousVoEnabled = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsPreviousVoEnabled");
            }
        }
        private bool _isNextVoEnabled;
        public bool IsNextVoEnabled
        {
            get
            {
                return _isNextVoEnabled;
            }
            set
            {
                _isNextVoEnabled = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsNextVoEnabled");
            }
        }
        private int _voLevel;
        public int VoLevel
        {
            get { return _voLevel; }
            set { _voLevel = value; NotifyPropertyChanged("VoLevel"); NotifyPropertyChanged("EnabledVoGrid"); }
        }
        private bool _enabledVoGrid;
        public bool EnabledVoGrid
        {
            get
            {
                if (SelectedBuilding != null)
                {
                    return VoLevel == SelectedBuilding.SubContractorLevel;
                }
                else return false;
            }
            set
            {
                _enabledVoGrid = value;
                NotifyPropertyChanged("EnabledVoGrid");
            }
        }
        private double _boqSum;
        public double BoqSum
        {
            get { return _boqSum; }
            set
            {
                _boqSum = value;
                NotifyPropertyChanged("BoqSum");
            }
        }
        private double _voSum;
        public double VoSum
        {
            get { return _voSum; }
            set
            {
                _voSum = value;
                NotifyPropertyChanged("VoSum");
            }
        }
        public IEnumerable<string> Units
        {
            get
            {
                yield return "ft";
                yield return "U";
                yield return "D";
                yield return "dm";
                yield return "ens";
                yield return "ml";
                yield return "m2";
                yield return "m3";
                yield return "kg";
                yield return "T";
            }
        }
        private Building _selectedbuidling;
        public Building SelectedBuilding
        {
            get { return _selectedbuidling; }
            set
            {
                _selectedbuidling = value;
                NotifyPropertyChanged("SelectedBuilding");
            }
        }
        private Building _selectedVoBuilding;
        public Building SelectedVoBuilding
        {
            get { return _selectedVoBuilding; }
            set
            {
                _selectedVoBuilding = value;
                NotifyPropertyChanged("SelectedVoBuilding");
            }
        }

        private BoqSheet _selectedSheetVo;
        public BoqSheet SelectedSheetVo
        {
            get { return _selectedSheetVo; }
            set
            {
                _selectedSheetVo = value;
                NotifyPropertyChanged("SelectedSheetVo");
            }
        }

        private BoqSheet _selectedBoqSheet;
        public BoqSheet SelectedBoqSheet
        {
            get { return _selectedBoqSheet; }
            set
            {
                _selectedBoqSheet = value;
                NotifyPropertyChanged("SelectedBoqSheet");
            }
        }

        private BoqSheet _selectedVoSheet;
        public BoqSheet SelectedVoSheet
        {
            get { return _selectedVoSheet; }
            set
            {
                _selectedVoSheet = value;
                NotifyPropertyChanged("SelectedVoSheet");
            }
        }

        private Contract _selectedContract;
        public Contract SelectedContract
        {
            get { return _selectedContract; }
            set
            {
                _selectedContract = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedContract");
            }
        }

        private BoqSheet _selectedSheet;
        public BoqSheet SelectedSheet
        {
            get { return _selectedSheet; }
            set
            {
                _selectedSheet = value;
                NotifyPropertyChanged("SelectedSheet");
            }
        }

        private ObservableCollection<BoqSheet> _boqSheets;
        public ObservableCollection<BoqSheet> BoqSheets
        {
            get { return _boqSheets; }
            set
            {
                _boqSheets = value;
                NotifyPropertyChanged("BoqSheets");
            }
        }

        private ObservableCollection<Building> _selectedBuildings;
        public ObservableCollection<Building> SelectedBuildings
        {
            get { return _selectedBuildings; }
            set
            {
                _selectedBuildings = value;
                NotifyPropertyChanged("SelectedBuildings");
            }
        }

        private ObservableCollection<BoqSheet> _voSheets;
        public ObservableCollection<BoqSheet> VoSheets
        {
            get { return _voSheets; }
            set
            {
                _voSheets = value;
                NotifyPropertyChanged("VoSheets");
            }
        }

        private ContractVo _selectedVo;
        public ContractVo SelectedVo
        {
            get { return _selectedVo; }
            set
            {
                _selectedVo = value;
                NotifyPropertyChanged("SelectedVo");
            }
        }
        public ContractVo CopyVo;
        public ContractBoqItem CopyBoq;

        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject");
            }
        }
        
        private Project _selectedProject1;
        public Project SelectedProject1
        {
            get { return _selectedProject1; }
            set
            {
                _selectedProject1 = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProject1");
            }
        }

        private Subcontractor _selectedSubcontractor;
        public Subcontractor SelectedSubcontractor
        {
            get { return _selectedSubcontractor; }
            set
            {
                _selectedSubcontractor = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedSubcontractor");
            }
        }
        private Subcontractor _selectedVOSubcontractor;
        public Subcontractor SelectedVOSubcontractor
        {
            get { return _selectedVOSubcontractor; }
            set
            {
                _selectedVOSubcontractor = value;
                NotifyPropertyChanged("SelectedVOSubcontractor");
            }
        }

        public ContractsDataset ContractsDataSet
             {
                 get { return _selectedContractsDataSet; }
                 set
                 {
                     _selectedContractsDataSet = value;
                     NotifyPropertyChanged();
                     NotifyPropertyChanged("SelectedContractsDataSet");
                 }
             }

        private ContractsDataset _selectedContractsDataSet;
        public ContractsDataset SelectedContractsDataSet
        {
            get { return _selectedContractsDataSet; }
            set
            {
                _selectedContractsDataSet = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedContractsDataSet");
            }
        }
        private ContractsDataset _selectedVOContractsDataset;
        public ContractsDataset SelectedVOContractsDataset
        {
            get { return _selectedVOContractsDataset; }
            set
            {
                _selectedVOContractsDataset = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedVOContractsDataset");
            }
        }

        private VoDataset _voDataset;
        public VoDataset VoDataset
        {
            get { return _voDataset; }
            set
            {
                _voDataset = value;
                NotifyPropertyChanged("VoDataset");

            }
        }
        private ObservableCollection<VoDataset> _voDatasets;
        public ObservableCollection<VoDataset> VoDatasets
        {
            get { return _voDatasets; }
            set
            {
                _voDatasets = value;
                NotifyPropertyChanged("VoDatasets");
            }
        }

        private ObservableCollection<ContractsDataset> _contractsDatasets;
        public ObservableCollection<ContractsDataset> ContractsDatasets
        {
            get { return _contractsDatasets; }
            set
            {
                _contractsDatasets = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ContractsDatasets");
            }
        }
        private ObservableCollection<ContractsDataset> _voContractsDatasets;
        public ObservableCollection<ContractsDataset> VOContractDatasets
        {
            get { return _voContractsDatasets; }
            set
            {
                _voContractsDatasets = value;
                NotifyPropertyChanged("VOContractDatasets");
            }
        }

        private ObservableCollection<dynamic> _projectVos;
        public ObservableCollection<dynamic> ProjectVos
        {
            get { return _projectVos; }
            set
            {
                _projectVos = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ProjectVos");
            }
        }

        private dynamic _selectedProjectVo;
        public dynamic SelectedProjectVo
        {
            get { return _selectedProjectVo; }
            set
            {
                _selectedProjectVo = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SelectedProjectVo");
            }
        }

        private ObservableCollection<Contract> _contracts;
        public ObservableCollection<Contract> Contracts
        {
            get { return _contracts; }
            set
            {
                _contracts = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Contracts");
            }
        }

        private ObservableCollection<Building> _buildings;
        public ObservableCollection<Building> Buildings
        {
            get { return _buildings; }
            set
            {
                _buildings = value;
                NotifyPropertyChanged("Buildings");
            }
        }

        private ObservableCollection<Contract> _scontracts;
        public ObservableCollection<Contract> SContracts
        {
            get { return _scontracts; }
            set
            {
                _scontracts = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("SContracts");
            }
        }

        private ObservableCollection<VOContract> _voContracts;
        public ObservableCollection<VOContract> VoContracts
        {
            get { return _voContracts; }
            set
            {
                _voContracts = value;
                NotifyPropertyChanged("VoContracts");
            }
        }

        private ObservableCollection<Project> _projects { get; set; }
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set {
                _projects = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Projects");
            }
        }

        private ObservableCollection<Subcontractor> _subcontractors;
        public ObservableCollection<Subcontractor> Subcontractors
        { 
            get { return _subcontractors; }
            set
            {
                _subcontractors = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Subcontractors");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors1;
        public ObservableCollection<Subcontractor> Subcontractors1
        { 
            get { return _subcontractors1; }
            set
            {
                _subcontractors1 = value;
                NotifyPropertyChanged("Subcontractors1");
            }
        }
        private ObservableCollection<Subcontractor> _subcontractors2;
        public ObservableCollection<Subcontractor> Subcontractors2
        { 
            get { return _subcontractors2; }
            set
            {
                _subcontractors2 = value;
                NotifyPropertyChanged("Subcontractors2");
            }
        }
        public Subcontractor SelectedSubcontractor1 { get; set; }
        public Subcontractor SelectedSubcontractor2 { get; set; }
        private ObservableCollection<Subcontractor> _voSubcontractors;
        public ObservableCollection<Subcontractor> VOSubcontractors
        { 
            get { return _voSubcontractors; }
            set
            {
                _voSubcontractors = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("VOSubcontractors");
            }
        }

        private ObservableCollection<BoqSheet> _sheets;
        public ObservableCollection<BoqSheet> Sheets
        {
            get { return _sheets; }
            set
            {
                _sheets = value;
                NotifyPropertyChanged("Sheets");
            }
        }
        private ObservableCollection<BoqSheet> _sheetsVo;
        public ObservableCollection<BoqSheet> SheetsVo
        {
            get { return _sheetsVo; }
            set
            {
                _sheetsVo = value;
                NotifyPropertyChanged("SheetsVo");
            }
        }

        private ObservableCollection<BoqSheet> _sheets1;
        public ObservableCollection<BoqSheet> Sheets1
        {
            get { return _sheets1; }
            set
            {
                _sheets1 = value;
                NotifyPropertyChanged("Sheets1");
            }
        }

        private ObservableCollection<BoqSheet> _sheets2;
        public ObservableCollection<BoqSheet> Sheets2
        {
            get { return _sheets2; }
            set
            {
                _sheets2 = value;
                NotifyPropertyChanged("Sheets2");
            }
        }
        public ContractBoqItem SelectedBoqItem { get; set; }

        private ObservableCollection<ContractBoqItem> _boqItems;
        public ObservableCollection<ContractBoqItem> BoqItems
        {
            get { return _boqItems; }
            set
            {
                _boqItems = value;
                if (_boqItems != null)
                {
                    _boqItems.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (ContractBoqItem item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSale;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (ContractBoqItem item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotalSale;
                                item.PropertyChanged += UpdateTotalSale;
                            }
                        }
                    };
                }

                NotifyPropertyChanged("BoqItems");
            }
        }

        private ObservableCollection<ContractVo> contractVos;
        public ObservableCollection<ContractVo> ContractVoVo
        {
            get { return contractVos; }
            set
            {
                contractVos = value;
                if (contractVos != null)
                {
                    contractVos.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (ContractVo item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (ContractVo item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                                item.PropertyChanged += UpdateTotalSaleVo;
                            }
                        }
                    };
                }
                NotifyPropertyChanged("ContractVoVo");
            }
        }

        private ObservableCollection<ContractVo> _voItems { get; set; }
        public ObservableCollection<ContractVo> VoItems
        {
            get { return _voItems; }
            set
            {
                _voItems = value;
                if (_voItems != null)
                {
                    _voItems.CollectionChanged += (s, args) =>
                    {
                        if (args.Action == NotifyCollectionChangedAction.Remove)
                        {
                            foreach (ContractVo item in args.OldItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                            }
                        }
                        else if (args.Action == NotifyCollectionChangedAction.Add)
                        {
                            foreach (ContractVo item in args.NewItems)
                            {
                                item.PropertyChanged -= UpdateTotalSaleVo;
                                item.PropertyChanged += UpdateTotalSaleVo;
                            }
                        }
                    };
                }
                NotifyPropertyChanged("VoItems");
            }
        }
 
        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged();
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged("IsBusy");
            }
        }
        private Visibility _isOrderVisible;
        public Visibility IsOrderVisible
        {
            get { return _isOrderVisible; }
            set
            {
                _isOrderVisible = value;
                NotifyPropertyChanged("IsOrderVisible");
            }
        }

        private int _mainTrans;
        public int mainTrans
        {
            get
            {
                return _mainTrans;
            }
            set
            {
                _mainTrans = value;
                NotifyPropertyChanged("mainTrans");
            }
        }

        private double _sum;
        public double Sum
        {
            get { return _sum; }
            private set
            {
                _sum = value;
                NotifyPropertyChanged("Sum");
            }
        }

        private VOContract voContract;
        public VOContract VoContract
        {
            get { return voContract; }
            set
            {
                voContract = value;
                NotifyPropertyChanged("VoContract");
            }
        }
        private Project _SelectedProjectFilter;
        public Project SelectedProjectFilter
        {
            get { return _SelectedProjectFilter; }
            set {
                _SelectedProjectFilter = value;
                NotifyPropertyChanged("SelectedProjectFilter");
            }
        }
        private Project _SelectedProjectFilterVo;
        public Project SelectedProjectFilterVo
        {
            get { return _SelectedProjectFilterVo; }
            set {
                _SelectedProjectFilterVo = value;
                NotifyPropertyChanged("SelectedProjectFilterVo");
            }
        }

        private ObservableCollection<Data.Currency> _currencies;
        public ObservableCollection<Data.Currency> Currencies
        {
            get { return _currencies; }
            private set
            {
                _currencies = value;

                NotifyPropertyChanged("Currencies");
            }
        }
        public BoqSheet SelectedSheet1 { get; set; }
        public BoqSheet SelectedSheet2 { get; set; }
    

        private void UpdateTotalSale(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Pt")
            {
                if (e.PropertyName == "OrderBoq")
                {
                    BoqItems = new ObservableCollection<ContractBoqItem>(
                        BoqItems.OrderBy(x => x.OrderBoq));
                }
                return;
            }
            update();
        }
        private void UpdateTotalSaleVo(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Pt")
            {
                if (e.PropertyName == "OrderVo")
                {
                    ContractVoVo = new ObservableCollection<ContractVo>(
                        ContractVoVo.OrderBy(x => x.OrderVo));
                }
                return;
            }
            updateVo();
        }
        private void updateVo()
        {
            if (ContractVoVo.Count == 0) return;
            VoSum = ContractVoVo.Sum(x => x.Pt);
        }
        private void update()
        {
            BoqSum = BoqItems.Sum(x => x.Pt);
        }
        public int Skip { get; set; }
        public int SkipVo { get; set; }
        private bool _loadMoreButton;
        public bool LoadMoreButton
        {
            get
            {
                return _loadMoreButton;
            }
            set
            {
                _loadMoreButton = value;
                NotifyPropertyChanged("LoadMoreButton");
            }
        }
        public ContractsDatasetViewModel()
        {
            Skip = 0;
            SkipVo= 0;
            LoadMoreButton = false;
            mainTrans = 2;
            unitOfWork = new UnitOfWork();
            CdTransIndex = 1;
            NewDatasetTrans = 0;
            IsChecked = true;
            IsCheckedEx = false;
            IsCheckedVo = false;
            IsBusy = true;
            IsOrderVisible = Visibility.Collapsed;
            VoDataset = new VoDataset();

            SelectedVo = new ContractVo();
            if (SelectedProject != null)
                SelectedProject = null;
            if (SelectedSubcontractor != null)
                SelectedSubcontractor = null;
            Task.Factory.StartNew(() =>
            {
              //  Sheets1 = new ObservableCollection<BoqSheet>();
                if (ContractsDatasets == null)
                    ContractsDatasets = new ObservableCollection<ContractsDataset>();
                if (VoDatasets == null)
                    VoDatasets = new ObservableCollection<VoDataset>();
                if (VoContracts == null)
                    VoContracts = new ObservableCollection<VOContract>();
                var cdatasets = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();
                var voatasets = unitOfWork.VoDatasetRepository.Get(includeProperties: "ContractsDataset", orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();
                var voContracts = unitOfWork.VOContractRepository.Get().OrderByDescending(x => x.Created).ToList();
             
                if (Currencies == null)
                    Currencies = new ObservableCollection<Data.Currency>();
                var curs = unitOfWork.CurrenciesRepository.Get();
             //   var sheets = unitOfWork.SheetsRepository.Get();
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var projects = unitOfWork.ProjectRepository
                .Get(includeProperties: "Buildings, Buildings.Boq, Buildings.Boq.BoqSheets");
                if (Subcontractors == null)
                    Subcontractors = new ObservableCollection<Subcontractor>();
                var subcontractors = unitOfWork.SubcontractorRepository.Get().ToList();
                if (Contracts == null)
                    Contracts = new ObservableCollection<Contract>();
                var contracts = unitOfWork.ContractRepository.Get().ToList();

                if (ContractsDataSet == null)
                    ContractsDataSet = new ContractsDataset();
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractsDatasets.AddRange(cdatasets);
                          VoDatasets.AddRange(voatasets);
                          VoContracts.AddRange(voContracts);
                          Currencies.AddRange(curs);
                          Projects.Clear();
                          Projects.AddRange(projects);
                          Subcontractors.Clear();
                          Subcontractors.AddRange(subcontractors);
                          Contracts.Clear();
                        //  Sheets1.AddRange(sheets);
                          Contracts.AddRange(contracts);

                          CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                          if (customPrincipal.Identity.UserType == UserType.Admin)
                          {
                              IsOrderVisible = Visibility.Visible;
                          }
                          else
                              IsOrderVisible = Visibility.Collapsed;



                          IsBusy = false;


                      Task.Factory.StartNew(() =>
                      {
                          foreach (var item in ContractsDatasets)
                          {
                              item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                          }
                          foreach (var item in VoDatasets)
                          {
                              item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                          }
                      });
                      }));
            });
        }
        public ICommand OpenContractDatasetCommand => new ActionCommand(p => OpenContent(p));
        public ICommand DeleteCommand => new ActionCommand(p => DeleteRecord(p));
        public ICommand RemarkCommand => new ActionCommand(p => Remark(p));
        public ICommand RemarkCPCommand => new ActionCommand(p => RemarkCP(p));
        public ICommand EditCommand => new ActionCommand(p => EditRecord(p));
        public ICommand TerminateCommand => new ActionCommand(p => TerminateRecord(p));
        public ICommand SaveCommand => new ActionCommand(p => SaveRecords(p));
        public ICommand SaveOnCommand => new ActionCommand(p => SaveOnRecords(p));
        public ICommand OpenExistCommand => new ActionCommand(p => OpenExist(p));
        //public ICommand CheckCommand => new ActionCommand(p => CheckBoxChecked(p));
        //public ICommand UncheckedCommand => new ActionCommand(p => CheckBoxUnchecked(p));
        public ICommand ProjectSelectedCommand => new ActionCommand(p => ProjectSeleced(p));
        public ICommand ProjectVoSelectedCommand => new ActionCommand(p => ProjectVoSelected(p));
        public ICommand BuildingSelectedVOCommand => new ActionCommand(p => BuildingSelectedVO(p));
        public ICommand SubcontractorVoCommand => new ActionCommand(p => SubcontractorSelected(p));
        public ICommand ContractSelectedVOCommand => new ActionCommand(p => ContractSelectedVO(p));
        public ICommand BuildingSelectedCommand => new ActionCommand(p => BuildingSeleced(p));
        public ICommand SheetSelectedDialogCommand => new ActionCommand(p => SheetSelectedDialog(p));
        public ICommand BuildingsSelectedDialogCommand => new ActionCommand(p => BuildingSelectedDialog(p));
        public ICommand SheetVoSelectedCommand => new ActionCommand(p => SheetVoSeleced(p));
        public ICommand SheetSelectedVoCommand => new ActionCommand(p => SheetVoSelected(p));
        public ICommand AttachDocCommand => new ActionCommand(p => AttachDoc(p));
        public ICommand AttachDocVoCommand => new ActionCommand(p => AttachDocVo(p));
        public ICommand AddingSubcontratorCommand => new ActionCommand(p => AddingSubcontrator(p));
        public ICommand SelectedCellsChangedCommand => new ActionCommand(p => SelectedCellsChanged(p));
        public ICommand SelectedCellsChangedVoCommand => new ActionCommand(p => SelectedCellsChangedVo(p));
        public ICommand SelectedCurrencyChangedCommand => new ActionCommand(p => SelectedCurrencyChanged(p));
        public ICommand CopyBoqItemsCommand => new ActionCommand(p => CopyBoqItems(p));
        public ICommand PasteBoqItemsCommand => new ActionCommand(p => PasteBoqItems(p));
        public ICommand DeleteBoqItemsCommand => new ActionCommand(p => DeleteBoqItems(p));
        public ICommand DeleteKeyCommand => new ActionCommand(p => DeleteKeyBoqItems(p));
        //public ICommand DeleteVoKeyCommand => new ActionCommand(p => DeleteVoKey(p));

        public ICommand InsertBoqItemsCommand => new ActionCommand(p => InsertBoqItems(p));
        public ICommand RefreshCommand => new ActionCommand(p => RefreshBoq(p));
        public ICommand NextVoCommand => new ActionCommand(p => NextVo());
        public ICommand PreviousVoCommand => new ActionCommand(p => PreviousVo());
        public ICommand DeleteVoItemsCommand => new ActionCommand(p => DeleteVoItems(p));
        public ICommand InsertVoItemsCommand => new ActionCommand(p => InsertVoItems(p));
        public ICommand VoCommand => new ActionCommand(p => OpenVo(p));
        public ICommand BackToBoqCommand => new ActionCommand(p => BackToBoq(p));
        public ICommand ClearVoCommand => new ActionCommand(p => ClearVo(p));
        public ICommand AddingNewVoCommand => new ActionCommand(p => AddingNewVo(p));
        public ICommand AddingNewBoqCommand => new ActionCommand(p => AddingNewBoq(p));
        public ICommand RowEditEndingVoCommand => new ActionCommand(p => RowEditEndingVo(p));
        public ICommand RowEditEndingBoqCommand => new ActionCommand(p => RowEditEndingBoq(p));
        public ICommand OpenTradeCommand => new ActionCommand(p => OpenTrade(p));
        public ICommand SelectProjectVoCommand => new ActionCommand(p => SelectProjectVo(p));
        public ICommand SaveParticularCommand => new ActionCommand(p => SaveParticular(p));
        public ICommand UploadBoqCommand => new ActionCommand(p => UploadBoq(p));
        public ICommand UploadVoCommand => new ActionCommand(p => UploadVo(p));
        public ICommand ContractTypeCommand => new ActionCommand(p => ContractType(p));
        public ICommand SelectionChangedCommand => new ActionCommand(p => SelectionChanged(p));
        public ICommand SelectionChangedVoCommand => new ActionCommand(p => SelectionChangedVo(p));
        public ICommand NextBuildingCommand => new ActionCommand(p => NextBuilding(p));
        public ICommand PreviousBuildingCommand => new ActionCommand(p => PreviousBuilding(p));
        public ICommand DateChangedCommand => new ActionCommand(p => DateChanged(p));
        public ICommand ProjectSelected1Command => new ActionCommand(p => ProjectSelected1(p));
        public ICommand ProjectSelected2Command => new ActionCommand(p => ProjectSelected2(p));
        public ICommand SubcontractorSelected1Command => new ActionCommand(p => SubcontractorSelected1(p));
        public ICommand SubcontractorSelected2Command => new ActionCommand(p => SubcontractorSelected2(p));
        public ICommand SheetSelected1Command => new ActionCommand(p => SheetSelected1(p));
        public ICommand SheetSelected2Command => new ActionCommand(p => SheetSelected2(p));
        public ICommand ContractNumberLostFocusCommand => new ActionCommand(p => ContractNumberLostFocus(p));
        public ICommand EditVoCommand => new ActionCommand(p => EditVo(p));
        public ICommand DeleteVoCommand => new ActionCommand(p => DeleteVoRecord(p));
        public ICommand TerminateVoCommand => new ActionCommand(p => TerminateVoRecord(p));
        public ICommand SaveVoCommand => new ActionCommand(p => SaveVo(p));
        public ICommand NewVoCommand => new ActionCommand(p => NewVo(p));
        public ICommand ContractVoChangedCommand => new ActionCommand(p => ContractVoChanged(p));
        public ICommand LoadContractsCommand => new ActionCommand(p => LoadContracts(p));
        public ICommand LoadVoCommand => new ActionCommand(p => LoadVo());
        public ICommand ClearVoFilterCommand => new ActionCommand(p => ClearVoFilter(p));
        public ICommand ClearFilterCommand => new ActionCommand(p => ClearFilter(p));
        
        private void ClearFilter(object p)
        {
            Skip = 0;
            SelectedProjectFilter = null;
            SelectedSubcontractor1 = null;
            SelectedSheet1 = null;
            Task.Factory.StartNew(() =>
            {

                //  var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();
                var cdatasets = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem,Project,Project.Buildings", orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractsDatasets.AddRange(cdatasets);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in cdatasets)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void ClearVoFilter(object p)
        {
            SkipVo = 0;
            SelectedProjectFilterVo = null;
            SelectedSubcontractor2 = null;
            SelectedSheet2 = null;
            Task.Factory.StartNew(() =>
            {

                var vos = unitOfWork.VoDatasetRepository.Get(orderBy: x => x.OrderByDescending(y => y.Created), skip: SkipVo, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          VoDatasets.AddRange(vos);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in vos)
                              {
                                  item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }

        private void LoadContracts(object p)
        {
            Skip += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {

              //  var c = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem", orderBy: x => x.OrderByDescending(y => y.Created), skip: Skip, paginate: true).ToList();
                var cdatasets = unitOfWork.ContractsDatasetRepository.Get(includeProperties: "ContractBoqItem,Project,Project.Buildings", orderBy: y => y.OrderByDescending(x => x.Created), skip: Skip, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          ContractsDatasets.AddRange(cdatasets);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in cdatasets)
                              {
                                  item.TotalBoq = item.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void LoadVo()
        {
            SkipVo += 10;
            LoadMoreButton = true;
            Task.Factory.StartNew(() =>
            {

                var vos = unitOfWork.VoDatasetRepository.Get(orderBy: x => x.OrderByDescending(y => y.Created), skip: SkipVo, paginate: true).ToList();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      new ThreadStart(() =>
                      {
                          VoDatasets.AddRange(vos);
                          LoadMoreButton = false;
                          Task.Factory.StartNew(() =>
                          {
                              foreach (var item in vos)
                              {
                                  item.TotalAmount = item.VoItems.Sum(x => x.Pt);
                              }
                          });
                      }));
            });
        }
        private void EditVo(object p)
        {
            if (VoDataset == null)
                return;
            mainTrans = 1;
            VoSum = 0;
            ContractVoVo = new ObservableCollection<ContractVo>();
            SelectedProject1 = VoDataset.Project;
            SelectedVOSubcontractor = VoDataset.Subcontractor;
            SelectedVOContractsDataset = VoDataset.ContractsDataset;
            SelectedBuilding = VoDataset.Building;
            VoContract = VoDataset.Contract;
            ContractVoVo.AddRange(VoDataset.VoItems.ToList());
            VoSum = ContractVoVo.Sum(x => x.Pt);
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Collapsed;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
        }
        private void NewVo(object p)
        {
            VoDataset = new VoDataset();
            ContractVoVo = new ObservableCollection<ContractVo>();

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Visible;
        }
        private void ContractVoChanged(object p)
        {
            if (VoContract == null)
                return;
            VoDataset.Contract = VoContract;
        }
        
        private void TerminateVoRecord(object p)
        {
            if (VoDataset == null)
                return;
            try
            {
                var result = MessageBox.Show("Are you sure you want to terminate this VO?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    VoDataset.ContractDatasetStatus = ContractDatasetStatus.Terminated;
                    unitOfWork.VoDatasetRepository.Update(VoDataset);
                    unitOfWork.Save();
                    IsVoTerminateEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void DeleteVoRecord(object p)
        {
            if (VoDataset == null)
                return;
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            //TODO: Contract Orders changed because of EF
            if ((string)result == "Delete")
            {

                foreach (var vo in VoDataset.VoItems.ToList())
                {
                    unitOfWork.ContractVoRepository.Delete(vo);
                }
                var nots = unitOfWork.Notifications.Get(x => x.VoDataset_Id == VoDataset.Id).ToList();
                foreach (var item in nots)
                {
                    unitOfWork.Notifications.Delete(item.Id);
                }
                unitOfWork.Save();
                unitOfWork.VoDatasetRepository.Delete(VoDataset);
                VoDatasets.Remove(VoDataset);
                unitOfWork.Save();
            }
        }
        private void ContractNumberLostFocus(object p)
        {
            var match = ContractsDatasets.Where(x => x.ContractNumber == ContractsDataSet.ContractNumber).ToList();
            if (match.Count > 0)
            {
                //ContractsDataSet.ContractNumber = "CS-" + SelectedProject.Acronym + "-";
                //MessageBox.Show("This number already exist. Please choose another number", "Contract number error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }
        private void ProjectSelected2(object p)
        {
            if (SelectedProjectFilterVo == null) return;
            var sub = unitOfWork.VoDatasetRepository.Get(x => x.Project.Id == SelectedProjectFilterVo.Id).Select(x => x.Subcontractor).Distinct().ToList();
            Subcontractors2 = new ObservableCollection<Subcontractor>();
            Subcontractors2.AddRange(sub);
            var vos = unitOfWork.VoDatasetRepository.Get(x => x.Project.Id == SelectedProjectFilterVo.Id).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(vos);
            SkipVo = 0;
        }
        private void ProjectSelected1(object p)
        {
            //VoDataset.Project = SelectedProjectFilter;
            if (SelectedProjectFilter == null) return;
            var sub = unitOfWork.ProjectRepository.Get(x => x.Id == SelectedProjectFilter.Id).SelectMany(x => x.ContractDatasets).Select(x => x.Subcontractor).ToList();
            Subcontractors1 = new ObservableCollection<Subcontractor>();
            Subcontractors1.AddRange(sub);
            var cont = unitOfWork.ContractsDatasetRepository.Get(x => x.Project.Id == SelectedProjectFilter.Id);
            ContractsDatasets = new ObservableCollection<ContractsDataset>();
            ContractsDatasets.AddRange(cont);
            Skip = 0;
        }
        private void SubcontractorSelected2(object p)
        {
            if (SelectedSubcontractor2 == null)
                return;

            var cont = unitOfWork.VoDatasetRepository.Get(x => x.Subcontractor.Id == SelectedSubcontractor2.Id).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(cont);
            var sheets = VoDatasets.Select(x => x.ContractsDataset.BoqSheet).Distinct().ToList();
            Sheets2 = new ObservableCollection<BoqSheet>();
            Sheets2.AddRange(sheets);
            SkipVo = 0;
        }
        private void SubcontractorSelected1(object p)
        {
            //VoDataset.Subcontractor = SelectedSubcontractor1;
            if (SelectedSubcontractor1 == null)
                return;
            var cont = unitOfWork.ContractsDatasetRepository.Get(x => x.Subcontractor.Id == SelectedSubcontractor1.Id);
            ContractsDatasets = new ObservableCollection<ContractsDataset>();
            ContractsDatasets.AddRange(cont);
            var sheets = ContractsDatasets.Select(x => x.BoqSheet).ToList();
            Sheets1 = new ObservableCollection<BoqSheet>();
            Sheets1.AddRange(sheets);
            Skip = 0;

        }
        private void SheetSelected2(object p)
        {
            if (SelectedSheet2 == null)
                return;
            var cont = unitOfWork.VoDatasetRepository.Get(x => x.ContractsDataset.BoqSheet.Name == SelectedSheet2.Name).Distinct();
            VoDatasets = new ObservableCollection<VoDataset>();
            VoDatasets.AddRange(cont);
            SkipVo = 0;
        }
        private void SheetSelected1(object p)
        {
            if (SelectedSheet1 == null)
                return;
            var cont = unitOfWork.ContractsDatasetRepository.Get(x => x.BoqSheet.Name == SelectedSheet1.Name);
            ContractsDatasets = new ObservableCollection<ContractsDataset>();
            ContractsDatasets.AddRange(cont);
            Skip = 0;

        }

        private void DateChanged(object p)
        {
            if (ContractsDataSet == null)
                return;
            if (ContractsDataSet.CompletionDate == null)
                return;
            if (ContractsDataSet.ContractDate == null)
            {
                MessageBox.Show("Select Contract first");
                return;
            }
            if(ContractsDataSet.CompletionDate < ContractsDataSet.ContractDate)
            {
                MessageBox.Show("Completion Date should be at leaset bigger than Start Date");
                ContractsDataSet.CompletionDate = null;
                return;
            }

        }

        private void OpenExist(object p)
        {
            NewDatasetTrans = 0;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Collapsed;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
        }
        private void SelectionChanged(object p)
        {
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if(ContractsDataSet != null)
            {
                if (ContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Active )
                {
                    IsTerminateEnabled = true;
                    IsDeleteEnabled = false;
                    IsEditableEnabled = true;
                }
                else if ( ContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.PendingApproval)
                {
                    IsTerminateEnabled = false;
                    IsDeleteEnabled = false;
                    IsEditableEnabled = false;
                }
                else if (ContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Editable)
                {
                    IsTerminateEnabled = false;
                    IsDeleteEnabled = true;
                    IsEditableEnabled = true;

                }
                else if (ContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Terminated)
                {
                    IsTerminateEnabled = false;
                    IsDeleteEnabled = false;
                    IsEditableEnabled = false;
                }

                if (customPrincipal.Identity.UserType != UserType.QuantitySurveyor 
                    && customPrincipal.Identity.UserType != UserType.ContractsManager
                    && customPrincipal.Identity.UserType != UserType.OperationsManager)
                {
                    IsTerminateEnabled = false;
                    IsDeleteEnabled = false;
                    IsEditableEnabled = false;
                }
            }

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;

        }
        private void SelectionChangedVo(object p)
        {
            var e = p as SelectionChangedEventArgs;
            var dataGrid = e.Source as DataGrid;
            if(VoDataset != null)
            {
                if (VoDataset.ContractDatasetStatus == ContractDatasetStatus.Active )
                {
                    IsVoTerminateEnabled = true;
                    IsVoDeleteEnabled = false;
                    IsVoEditableEnabled = false;
                }
                else if (VoDataset.ContractDatasetStatus == ContractDatasetStatus.PendingApproval)
                {
                    IsVoTerminateEnabled = false;
                    IsVoDeleteEnabled = false;
                    IsVoEditableEnabled = false;
                }
                else if (VoDataset.ContractDatasetStatus == ContractDatasetStatus.Editable)
                {
                    IsVoTerminateEnabled = false;
                    IsVoDeleteEnabled = true;
                    IsVoEditableEnabled = true;

                }
                else if (VoDataset.ContractDatasetStatus == ContractDatasetStatus.Terminated)
                {
                    IsVoTerminateEnabled = false;
                    IsVoDeleteEnabled = false;
                    IsVoEditableEnabled = false;
                }
            }
            //var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            //win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
        }

        private void ContractType(object p)
        {
            var type = p as string;
            if (SContracts == null)
                SContracts = new ObservableCollection<Contract>();
            SContracts.Clear();
            SContracts.AddRange(Contracts.Where(c => c.Type == type).ToList());

        }
        private void ContractSelectedVO(object p)
        {
            //VoDataset.ContractsDataset = SelectedVOContractsDataset;
            if (SelectedVOContractsDataset != null)
            {
                VoDataset.ContractsDataset = SelectedVOContractsDataset;
                IsVoGridEnabled = true;
                ContractVoVo = new ObservableCollection<ContractVo>();

                Buildings = new ObservableCollection<Building>();
                Buildings.AddRange(SelectedProject1.Buildings);
                SelectedBuilding = Buildings.FirstOrDefault();

                if (SelectedVOContractsDataset.BoqSheet == null)
                    return;
                SelectedVoSheet = SelectedVOContractsDataset.BoqSheet;
               
                //ContractVoVao.AddRange(vo);
            }
            else
                IsVoGridEnabled = false;
        }
        private void SubcontractorSelected(object p)
        {
            if (SelectedVOSubcontractor == null)
                return;
            if (SelectedVOSubcontractor == null)
                return;
            VoDataset.Subcontractor = SelectedVOSubcontractor;
            //if (SelectedContractsDataset != null)
            //{
            //    SaveRecords(null);
            SelectedVOContractsDataset = null;
            //}
            var sc = unitOfWork.ContractsDatasetRepository.Get(x => x.ProjectId == SelectedProject1.Id && x.SubcontractorId == SelectedVOSubcontractor.Id).ToList();
            if (VOContractDatasets == null)
                VOContractDatasets = new ObservableCollection<ContractsDataset>();
            VOContractDatasets.Clear();
            VOContractDatasets.AddRange(sc);
        }
        private void ProjectVoSelected(object p)
        {
            if (SelectedProject1 == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            List<Subcontractor> cd;
            VoDataset.Project = SelectedProject1;

            SelectedVOSubcontractor = null;
            SelectedVOContractsDataset = null;
            SelectedBuilding = null;

            ContractVoVo = new ObservableCollection<ContractVo>();
            ProjectVos = new ObservableCollection<dynamic>();
            for (int i = 0; i < SelectedProject1.Buildings.Count; i++)
            {
                for (int j = 1; j < SelectedProject1.Buildings.ElementAt(i).ProjectLevel; j++)
                {
                    dynamic vsd = new System.Dynamic.ExpandoObject();
                    vsd.Name = SelectedProject1.Buildings.ElementAt(i).Name + "/VO#" + j;
                    vsd.Tag = j;
                    vsd.BuildingName = SelectedProject1.Buildings.ElementAt(i).Name;
                    ProjectVos.Add(vsd);
                }
            }

            try
            {
                cd = unitOfWork.ContractsDatasetRepository.Get(x => x.Project.Id == SelectedProject1.Id).Select(x => x.Subcontractor).Distinct().ToList();
                if (VOSubcontractors == null)
                    VOSubcontractors = new ObservableCollection<Subcontractor>();
                VOSubcontractors.Clear();
                VOSubcontractors.AddRange(cd);
            }
            catch (Exception ex)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(ex.Message, "Error");
                return;
            }
       
        }
        private void BuildingSelectedVO(object p)
        {
            if (SelectedBuilding == null)
                return;
            if (SelectedVOContractsDataset == null)
                return;
            if (ContractVoVo == null)
                ContractVoVo = new ObservableCollection<ContractVo>();
            else
                ContractVoVo.Clear();

            //VoDataset.Building = SelectedBuilding;  
            //var trade = SelectedBuilding.Boq.BoqSheets.Where(x => x.ContractBoqItems.Where(y => y.ContractsDataset.Id == SelectedVOContractsDataset.Id).Count() > 0).FirstOrDefault();
            //if (trade == null)
            //    return;
         

            //var vo = SelectedVoSheet.ContractVos.Where(x => x.ContractsDataset.Id == SelectedVOContractsDataset.Id && x.Level == SelectedBuilding.SubContractorLevel )
            //    .ToList();
            if(VoDataset.VoItems != null)
            {
                //ContractVoVo.AddRange(VoDataset.VoItems.OrderBy(x => x.No).ToList());
                if (VoDataset.VoItems.Count > 0)
                {
                    VoSum = ContractVoVo.Sum(x => x.Pt);
                }
            }
              
        }
        private async void SaveVo(object p) 
        {
            var view = new SaveVoDialog();

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Yes")
            {
                SelectedBuilding.SubContractorLevel++;
                VoLevel = SelectedBuilding.SubContractorLevel;
                VoDataset.VoItems = ContractVoVo;
                VoDataset.TotalAmount = VoSum;
                VoDataset.Building = SelectedBuilding;
                VoDataset.ContractDatasetStatus = ContractDatasetStatus.Editable;
                if (VoLevel > 1)
                {
                    IsPreviousVoEnabled = true;
                    IsNextVoEnabled = false;
                }
                if(VoDataset.Id > 0)
                {
                    unitOfWork.VoDatasetRepository.Update(VoDataset);
                }
                else
                {
                    unitOfWork.VoDatasetRepository.Insert(VoDataset);
                    VoDatasets.Add(VoDataset);

                }
            }
            else
            {
                return;
            }
            VoSum = 0;
            unitOfWork.ContractsDatasetRepository.Update(SelectedVOContractsDataset);
            unitOfWork.Save();
            //ContractVoVo.Clear();
            SelectedProjectVo = null;
            mainTrans = 5;
            SelectedProjectVo = null;
            SelectedProject1 = null;
            SelectedVOSubcontractor = null;
            SelectedVOContractsDataset = null;
            SelectedBuilding = null;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Collapsed;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
        }
        private void SelectProjectVo(object pa)
        {
            if(SelectedVOContractsDataset == null)
            {
                MessageBox.Show("Select the Subcontractor's BOQ first before adding a VO", "Error. Subcontractor's BOQ Is Not Defined", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(SelectedProjectVo != null)
            {
                if (ContractVoVo == null)
                    ContractVoVo = new ObservableCollection<ContractVo>();
                ContractVoVo.Clear();
                //TODO : BUILDING MUST BE CHOOSEN
                //VoLevel = Selected
                var trade = SelectedBuilding.Boq.BoqSheets
                    .Where(x => x.ContractBoqItems.Where(y => y.ContractsDatasetId == SelectedVOContractsDataset.Id).Count() > 0)
                    .FirstOrDefault();
                if (trade == null)
                    return;
                var vo = SelectedBuilding.Boq.BoqSheets
                    .Where(x => x.Id == trade.Id).SelectMany(x => x.Vos).Where(x => x.Level == SelectedProjectVo.Tag).ToList();
         
                foreach(var item in vo)
                {
                    var lineActivity = new ContractVo
                    {
                        No = item.No,
                        Level = SelectedBuilding.SubContractorLevel,
                        Key = item.Key,
                        Unite = item.Unite,
                        CostCode = item.CostCode,
                        Qte = item.Qte,
                        Pu = item.Pu,
                        BoqSheet = item.BoqSheet,
                        BOQType = BOQType.Line,
                        OrderVo = item.OrderVo,
                        ContractsDataset = SelectedVOContractsDataset
                    };
                    unitOfWork.ContractVoRepository.Insert(lineActivity);
                    ContractVoVo.Add(lineActivity);
                }
                
                unitOfWork.Save();
                updateVo();
            }
        }
        private void SheetVoSelected(object p)
        {
            if (SelectedVoSheet != null)
            {
                if (SelectedVoBuilding == null)
                    return;
                ContractVoVo = new ObservableCollection<ContractVo>();
                var bi = SelectedVoSheet.ContractVos.Where(x => x.Level == SelectedVoBuilding.SubContractorLevel).ToList();
                //var bi = unitOfWork.ContractVoRepository.Get(x => x.BoqSheetId == SelectedVoSheet.Id && x.Level == VoLevel);
                //ContractVoVo.AddRange(bi.OrderBy(x=>x.No).ToList());
                if (ContractVoVo.Count > 0)
                {
                    VoSum = ContractVoVo.Sum(x => x.Pt);
                }
            }
        }
        private void AddingNewVo(object p)
        {
            if (SelectedVoSheet == null)
            {
                MessageBox.Show("Select a building before adding a new VO", "Trade Not Defined", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            } 
            if (SelectedVoSheet == null) return;
            (p as AddingNewItemEventArgs).NewItem = new Data.ContractVo
            {
                Level = SelectedBuilding.SubContractorLevel,
                BoqSheet = SelectedVoSheet,
                BOQType = BOQType.Line,
                VoDataset = VoDataset
            };
            SelectedVoSheet.ContractVos.Add((ContractVo)(p as AddingNewItemEventArgs).NewItem);
            

            //  ContractVoVo.Add(new ContractVo());
        }
        private void InsertVoItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;
            if (SelectedSheetVo == null)
            {
                MessageBox.Show("Select a building before adding a new VO", "Trade Not Defined", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGrid)contextMenu.PlacementTarget;
            ContractVo vo = item.SelectedItem as ContractVo;
            
            if(SelectedSheetVo == null)
            {
                SelectedSheetVo = SelectedBuilding.Boq.BoqSheets.Where(x => x.Name == SelectedVOContractsDataset.BoqSheet.Name).FirstOrDefault();

            }
            if (vo.BOQType != BOQType.Line)
            {
                return;
            }
            var index = ContractVoVo.IndexOf(vo);
            var newBoq = new ContractVo
            {
                BoqSheet = vo.BoqSheet,
                BOQType = BOQType.Line,
                Level = SelectedVoBuilding.SubContractorLevel,
                OrderVo = vo.OrderVo + 0.01f,
            };
            ContractVoVo.Insert(index, newBoq);
            SelectedSheetVo.ContractVos.Add(newBoq);
            //unitOfWork.ContractVoRepository.Insert(newBoq);
            //unitOfWork.Save();
        }
        private async void OpenTrade(object p)
        {
            if (ContractsDataSet.Id != 0)
                return;
            if (ContractsDataSet.BoqSheet != null)
                return;
            var view = new TradesDialog
            {
                DataContext = this
            };

            var result = await DialogHost.Show(view, "RootDialog");
            if (SelectedSheet == null)
                return;
            if ((string)result == "Chose")
            {
                SelectedBoqSheet = SelectedSheet;
                ContractsDataSet.BoqSheet = SelectedSheet;

                var view1 = new ConfirmUploadBoq
                {
                    DataContext = this
                };

                var result1 = await DialogHost.Show(view1, "RootDialog");
                if ((string)result1 == "Import")
                {
                    UploadBoq(null);
                    return;
                }
                
                var trade = SelectedBuilding.Boq.BoqSheets.Where(x=>x.ContractBoqItems != null)
                    .Where(x => x.ContractBoqItems.Where(y => y.ContractsDatasetId == ContractsDataSet.Id).Count() > 0).FirstOrDefault();
                if (trade != null)
                {
                    SelectedBoqSheet = trade;
                    if (SelectedSheet.Id == trade.Id)
                    {
                        return;
                    }
                    else
                    {

                        foreach (var building in Buildings)
                        {
                            var sheet = building.Boq.BoqSheets.Where(x => x.Name == SelectedBoqSheet.Name).FirstOrDefault();
                            if (sheet == null)
                                continue;
                            foreach (var item in sheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList())
                            {
                                unitOfWork.ContractBoqItemRepository.Delete(item);
                                sheet.ContractBoqItems.Remove(item);
                            }
                        }
                        unitOfWork.Save();
                    }
                    
                }

                SelectedBoqSheet = SelectedSheet;
                ContractsDataSet.BoqSheet = SelectedSheet;

                await Task.Factory.StartNew(() =>
                {
                  
                    foreach (var building in Buildings)
                    {
                        var sheet = building.Boq.BoqSheets.Where(x => x.Name == SelectedSheet.Name).FirstOrDefault();
                        if (sheet == null)
                            continue;
                        foreach (var item in sheet.BoqItems)
                        {
                            var activity = new ContractBoqItem
                            {
                                No = item.No,
                                Key = item.Key,
                                Unite = item.Unite,
                                CostCode = item.CostCode,
                                Qte = item.Qte,
                                Pu = item.Pu,
                                BoqSheet = item.BoqSheet,
                                BOQType = BOQType.Line,
                                OrderBoq = item.OrderBoq,
                                ContractsDataset = ContractsDataSet,
                            };
                            //ContractsDataSet.ContractVo.Add(activity);
                            //unitOfWork.ContractBoqItemRepository.Insert(activity);
                            sheet.ContractBoqItems.Add(activity);
                        }
                    }

                    ContractsDataSet.Currency = SelectedProject.Currency;
                    unitOfWork.Save();
                });

              
                if (BoqItems == null)
                    BoqItems = new ObservableCollection<ContractBoqItem>();
                BoqItems.Clear();
                BoqItems.AddRange(SelectedSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).OrderBy(x => x.OrderBoq).ToList());
                update();
            }
            else
            {
                //var trade = SelectedBuilding.Boq.BoqSheets.Where(x => x.ContractBoqItems.Where(y => y.ContractsDataset.Id == ContractsDataSet.Id).Count() > 0).FirstOrDefault();
                //
                //if (trade != null)
                //{
                //    var boqitems = trade.ContractBoqItems.Where(x => x.ContractsDataset.Id == ContractsDataSet.Id)
                //    ToList();
                //    BoqItems.AddRange(boqitems);
                //}
                //view.TradeLb.UnselectAll();
                //if(SelectedSheet != null)
                //    SelectedSheet.Clear();
            }
        }
        private void RowEditEndingVo(object p)
        {
            updateVo();
        }
        private void RowEditEndingBoq(object p)
        {
           
            update();
        }     
        private void AddingNewBoq(object p)
        {
            if (SelectedBoqSheet == null)
            {
                SelectedBoqSheet = SelectedBuilding.Boq.BoqSheets.Where(x => x.Name == SelectedVOContractsDataset.BoqSheet.Name).FirstOrDefault();
                if (SelectedBoqSheet == null)
                {
                    MessageBox.Show("Select a building before adding a new BOQ", "Trade Not Defined", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            double order;

            if (SelectedBoqSheet.ContractBoqItems.Where( x=>x.ContractsDatasetId == ContractsDataSet.Id).Count() == 0)
            {
                order = 1.0f;
                SelectedBoqSheet.IsActive = true;

            }
            else
            {
                order = (double)SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).ElementAt(SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).Count() - 1).OrderBoq;

            }
            (p as AddingNewItemEventArgs).NewItem = new Data.ContractBoqItem
            {
                BoqSheet = SelectedBoqSheet,
                ContractsDataset = ContractsDataSet,
                OrderBoq = order + 1.0f,
                BOQType = BOQType.Line
            };
            SelectedBoqSheet.ContractBoqItems.Add((ContractBoqItem)(p as AddingNewItemEventArgs).NewItem);
        }
        private void NextVo()
        {
            VoLevel++;
            if (ContractVoVo == null)
                ContractVoVo = new ObservableCollection<ContractVo>();
            ContractVoVo.Clear();
            SelectedSheetVo = null;
            if (VoLevel == SelectedVoBuilding.SubContractorLevel)
                IsNextVoEnabled = false;
            if (VoLevel > 1)
                IsPreviousVoEnabled = true;
        }
        private void PreviousVo()
        {
            VoLevel--;
            if (ContractVoVo == null)
                ContractVoVo = new ObservableCollection<ContractVo>();
            ContractVoVo.Clear();
            SelectedSheetVo = null;
            if (VoLevel == 1)
                IsPreviousVoEnabled = false;
            if (SelectedVoBuilding.SubContractorLevel > 1)
                IsNextVoEnabled = true;
        }
        private async void ClearVo(object p)
        {
            var view = new ConfirmBoqDialog();

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Delete")
            {
                //foreach (var sheet in SelectedBuilding.Boq.BoqSheets.ToList())
                //{
                try
                {
                    foreach (var vo in ContractVoVo)
                    {
                        VoDataset.VoItems.Remove(vo);
                        unitOfWork.ContractVoRepository.Delete(vo);
                    }
                 
                
                    ContractVoVo.Clear();
                    unitOfWork.Save();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.InnerException.Message, "Delete Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                }
            }
        }
        private void BackToBoq(object p)
        {
            NewDatasetTrans = 3;
        }
        private void OpenVo(object p)
        {
            if (SelectedSheetVo == null)
                ContractVoVo = null;
            NewDatasetTrans = 4;
        }

        private async void OpenContent(object p)
        {
            if (p.ToString() != "True")
                return;
            if (SelectedContractsDataSet != null)
                SelectedContractsDataSet = null;

            if (ContractsDataSet != null)
                ContractsDataSet = null;
            if (Buildings != null)
                Buildings = null;
            if (BoqItems != null)
                BoqItems = null;
            await Task.Factory.StartNew(() =>
            {
               
                if (Projects == null)
                    Projects = new ObservableCollection<Project>();
                var projects = unitOfWork.ProjectRepository
                .Get(includeProperties: "Buildings, Buildings.Boq, Buildings.Boq.BoqSheets, Buildings.Boq.BoqSheets.BoqItems").ToList();
                if (Subcontractors == null)
                    Subcontractors = new ObservableCollection<Subcontractor>();
                var subcontractors = unitOfWork.SubcontractorRepository.Get().ToList();
                if (Contracts == null)
                    Contracts = new ObservableCollection<Contract>();
                var contracts = unitOfWork.ContractRepository.Get().ToList();
               
                if (ContractsDataSet == null)
                    ContractsDataSet = new ContractsDataset();
          
            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new ThreadStart(() =>
                       {
                           Projects.Clear();
                           Projects.AddRange(projects);
                           Subcontractors.Clear();
                           Subcontractors.AddRange(subcontractors);
                           Contracts.Clear();
                           Contracts.AddRange(contracts);
                       }));
            });
            CdTransIndex = 1;

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
        }
        private void DeleteVoItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGrid)contextMenu.PlacementTarget;
            foreach (ContractVo vo in item.SelectedItems.OfType<ContractVo>().ToList())
            {
                //boq.IsHide = true;
                ContractVoVo.Remove(vo);
            }
            updateVo();
        }

        private async void SelectedCellsChanged(object p)
        {
            var p1 = p as MouseButtonEventArgs;
            var item = p1.Source as DataGrid;
            if (SelectedBoqItem == null) return;
            if (item.CurrentColumn == null) return;

            item.CommitEdit();
            item.CancelEdit();

            var col = item.CurrentColumn.Header as string;
            if(col == "Cost Code")
            {

                var view = new CostCodeDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if (result is string)
                {
                    //unitOfWork.Save();
                    if (BoqItems != null)
                        BoqItems.Clear();
                    else
                        BoqItems = new ObservableCollection<ContractBoqItem>();
                    BoqItems.AddRange(SelectedBoqSheet.ContractBoqItems.Where( x => x.ContractsDatasetId == ContractsDataSet.Id).OrderBy(x => x.OrderBoq).ToList());
                }
                else
                {
                    //((BoqItem)boq).CostCode = (CostCodeLibrary)result;
                    var costCode = unitOfWork.CostCodeLibraryRepository.Get(x => x.Id == ((CostCodeLibrary)result).Id).FirstOrDefault();
                    if (SelectedBoqItem == null) return;
                        SelectedBoqItem.CostCode = costCode;
                    if (BoqItems != null)
                        BoqItems.Clear();
                    else
                        BoqItems = new ObservableCollection<ContractBoqItem>();
                    BoqItems.AddRange(SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).OrderBy(x => x.OrderBoq).ToList());
                }
            }
        }
        private async void SelectedCellsChangedVo(object p)
            {
            var p1 = p as MouseButtonEventArgs;
            var item = p1.Source as DataGrid;
            if (item.CurrentColumn == null) return;
            if (SelectedVo == null) return;
            
         
            //item.CancelEdit();

            var col = item.CurrentColumn.Header as string;
            if(col == "Cost Code")
            {
                item.CommitEdit(DataGridEditingUnit.Row, false);

                var keyEventArgs = new KeyEventArgs(InputManager.Current.PrimaryKeyboardDevice,
                    PresentationSource.FromDependencyObject(item), System.Environment.ProcessorCount, Key.Return);
                keyEventArgs.RoutedEvent = UIElement.KeyDownEvent;
                item.RaiseEvent(keyEventArgs);

                var view = new CostCodeDialog();

                //show the dialog
                var result = await DialogHost.Show(view, "RootDialog");

                if (result is string)
                {
                    if (ContractVoVo != null)
                        ContractVoVo.Clear();
                    else
                        ContractVoVo = new ObservableCollection<ContractVo>();
                    ContractVoVo.AddRange(SelectedVoSheet.ContractVos.Where(x => x.Level == SelectedBuilding.SubContractorLevel).OrderBy(x => x.OrderVo).ToList());
                }
                else
                {
                    //((BoqItem)boq).CostCode = (CostCodeLibrary)result;
                    var costCode = unitOfWork.CostCodeLibraryRepository.Get(x => x.Id == ((CostCodeLibrary)result).Id).FirstOrDefault();
                    if (SelectedVo != null)
                        SelectedVo.CostCode = costCode;

                    if (ContractVoVo != null)
                        ContractVoVo.Clear();
                    else
                        ContractVoVo = new ObservableCollection<ContractVo>();
                    ContractVoVo.AddRange(SelectedVoSheet.ContractVos.Where(x => x.Level == SelectedBuilding.SubContractorLevel).OrderBy(x => x.OrderVo).ToList());
                }
            }
           // Send(Key.Enter, item);

        }
        public static void Send(Key key, Control item)
        {
            if (Keyboard.PrimaryDevice != null)
            {
                if (Keyboard.PrimaryDevice.ActiveSource != null)
                {
                    var e = new KeyEventArgs(Keyboard.PrimaryDevice, PresentationSource.FromVisual(item), 0, key)
                    {
                        RoutedEvent = Keyboard.KeyDownEvent
                    };
                    InputManager.Current.ProcessInput(e);

                    // Note: Based on your requirements you may also need to fire events for:
                    // RoutedEvent = Keyboard.PreviewKeyDownEvent
                    // RoutedEvent = Keyboard.KeyUpEvent
                    // RoutedEvent = Keyboard.PreviewKeyUpEvent
                }
            }
        }
        private void SelectedCurrencyChanged(object p)
        {
            if (ContractsDataSet.Currency == null)
                return;
            if (SelectedProject == null)
                return;
            if (ContractsDataSet.ContractBoqItem == null)
                return;
            var e = p as SelectionChangedEventArgs;
            if (e.RemovedItems.Count == 0)
                return;
            var oldCurrency = e.RemovedItems[0] as Currency;
            IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                var mt = unitOfWork.VenteLocalRepository.Get(x => x.Contract == ContractsDataSet.ContractNumber).ToList();

                var ma = unitOfWork.MachineRepository.Get(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList();

                var lb = unitOfWork.LaborRepository.Get(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList();

                var Boqs = ContractsDataSet.ContractBoqItem.ToList();

                foreach (var item in mt)
                {
                    var price = item.SaleUnit / oldCurrency.ConversionRate;
                    item.SaleUnit = price * ContractsDataSet.Currency.ConversionRate;
                    unitOfWork.VenteLocalRepository.Update(item);
                }
                foreach (var item in ma)
                {
                    var price = item.UnitPrice / oldCurrency.ConversionRate;
                    item.UnitPrice = price * ContractsDataSet.Currency.ConversionRate;
                    unitOfWork.MachineRepository.Update(item);
                }
                foreach (var item in lb)
                {
                    var price = item.UnitPrice / oldCurrency.ConversionRate;
                    item.UnitPrice = price * ContractsDataSet.Currency.ConversionRate;
                    unitOfWork.LaborRepository.Update(item);
                }
                foreach (var boq in Boqs.ToList())
                {
                    var price = boq.Pu / oldCurrency.ConversionRate;
                    boq.Pu = price * ContractsDataSet.Currency.ConversionRate;
                    unitOfWork.ContractBoqItemRepository.Update(boq);
                }
                unitOfWork.Save();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              new ThreadStart(() =>
              {
                  IsBusy = false;
              }));
            });

            
        }
        private void InsertBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGrid)contextMenu.PlacementTarget;
            ContractBoqItem boq = item.SelectedItem as ContractBoqItem;
            if(boq.BOQType != BOQType.Line)
            {
                return;
            }
            if(SelectedBoqSheet == null)
            {
                SelectedBoqSheet = SelectedBuilding.Boq.BoqSheets.Where(x => x.Name == SelectedVOContractsDataset.BoqSheet.Name).FirstOrDefault();
            }
            var index = BoqItems.IndexOf(boq);
            var newBoq = new ContractBoqItem { BoqSheet = boq.BoqSheet, BOQType = BOQType.Line, OrderBoq = boq.OrderBoq+0.01f, ContractsDataset = ContractsDataSet };
            BoqItems.Insert(index, newBoq);

            SelectedBoqSheet.ContractBoqItems.Add(newBoq);

            update();
            //BoqItemsLCV.Refresh();
        }
        private async void RefreshBoq(object p)
        {
           

            var view = new ClearDialogSubBoq
            {
                DataContext = this
            };

            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Clear")
            {
                
                if (AllT == true)
                {
                   
                    foreach (var building in Buildings)
                    {
                        var sheet = building.Boq.BoqSheets.Where(x => x.Name == ContractsDataSet.BoqSheet.Name).FirstOrDefault();
                        if (sheet == null)
                            continue;
                        foreach (var boq in sheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList())
                        {
                            sheet.ContractBoqItems.Remove(boq);
                        }
                        BoqItems.Clear();
                        BoqSum = 0;
                    }
                }
                else if (OnlyThis == true)
                {
                    if (SelectedBoqSheet == null)
                    {
                        MessageBox.Show("Select a trade before clearing its BOQ", "Warning. trade Not Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    foreach (var boq in SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList())
                    {
                        SelectedBoqSheet.ContractBoqItems.Remove(boq);
                    }
                    BoqItems.Clear();
                    BoqSum = 0;
                }
            }
        }

        private void PasteBoqItems(object p)
        {
            if (CopyBoq == null)
                return;
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            ContractBoqItem boq = itemg.SelectedItem as ContractBoqItem;
            double order;
            //TODO::CHECK CONTRACTDATASET AND SHEET 
            var newBoq = new ContractBoqItem
            {
                ContractsDataset = CopyBoq.ContractsDataset,
                BoqSheet = CopyBoq.BoqSheet,
                BOQType = CopyBoq.BOQType,
                CostCode = CopyBoq.CostCode,
                Key = CopyBoq.Key,
                No = CopyBoq.No,
                Pu = CopyBoq.Pu,
                Unite = CopyBoq.Unite,
                Qte = CopyBoq.Qte
            };
            var index = BoqItems.IndexOf(boq);
            if (index == -1)
            {
                if (BoqItems.Count == 0)
                {
                    order = 1.0f;
                    newBoq.OrderBoq = order;
                    BoqItems.Add(newBoq);
                }
                else
                {
                    order = (double)BoqItems.Last().OrderBoq + 0.1f;
                    newBoq.OrderBoq = order;
                    BoqItems.Add(newBoq);
                }
            }
            else
            {
                order = (double)boq.OrderBoq + 0.1f;
                newBoq.OrderBoq = order;

                BoqItems.Insert(index, newBoq);
            }
            SelectedBoqSheet.ContractBoqItems.Add(newBoq);
            unitOfWork.ContractBoqItemRepository.Insert(newBoq);
            unitOfWork.Save();
            update();
        }
        private void CopyBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var itemg = (DataGrid)contextMenu.PlacementTarget;
            ContractBoqItem boq = itemg.SelectedItem as ContractBoqItem;
            CopyBoq = boq;
        }
        private void DeleteBoqItems(object p)
        {
            var arg = p as RoutedEventArgs;
            var menuItem = (MenuItem)arg.Source;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGrid)contextMenu.PlacementTarget;
            foreach (ContractBoqItem boq in item.SelectedItems.OfType<ContractBoqItem>().ToList())
            {
                if (boq.Id == 0) continue;
                BoqItems.Remove(boq);
                SelectedBoqSheet.ContractBoqItems.Remove(boq);
                unitOfWork.ContractBoqItemRepository.Delete(boq);
            }
            unitOfWork.Save();
            update();
        }
        private void DeleteKeyBoqItems(object p)
        {
            if (SelectedBoqItem == null)
                return;
            KeyEventArgs e = p as KeyEventArgs;
            if (e.Key == Key.Delete)
            {
                if (SelectedBoqItem.Id == 0) return;
                BoqItems.Remove(SelectedBoqItem);
                    SelectedBoqSheet.ContractBoqItems.Remove(SelectedBoqItem);
                    unitOfWork.ContractBoqItemRepository.Delete(SelectedBoqItem);
                
                unitOfWork.Save();
                update();
            }
        }

        //private void DeleteVoKey(object p)
        //{
        //    if (SelectedVo == null)
        //        return;
        //    KeyEventArgs e = p as KeyEventArgs;
        //    if (e.Key == Key.Delete)
        //    {
        //        ContractVoVo.Remove(SelectedVo);
        //        update();
        //    }
        //}
      
        private void AddingSubcontrator(object p)
        {
            try
            {
                if (Subcontractors == null) return;
                if (SelectedSubcontractor == null)
                    return;
                //foreach (var sub in Subcontractors)
                //{
                //    if (sub.Id == 0)
                //    {
                //        unitOfWork.SubcontractorRepository.Insert(sub);
                //    }
                //    else
                //    {
                //        unitOfWork.SubcontractorRepository.Update(sub);
                //    }
                //}
                unitOfWork.Save();
                if(string.IsNullOrWhiteSpace(ContractsDataSet.ContractNumber))
                    ContractsDataSet.ContractNumber = "CS-" + SelectedProject.Acronym + "-";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
        }
        private async void RemarkCP(object p)
        {
            var view = new RemarkCPDialog
            {
                DataContext = ContractsDataSet
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Save")
            {
                unitOfWork.Save();
            }
        }
        private async void Remark(object p)
        {
        
            var view = new RemarkDialog
            {
                DataContext = ContractsDataSet
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "Save")
            {
                unitOfWork.Save();
            }
        }
        private void EditRecord(object p)
        {
             if (p == null) return;
            ResetTables();
            //ContractsDataSet = SelectedContractsDataSet;

            //ContractsDataSet = unitOfWork.ContractsDatasetRepository.Get(x=> x.Id == ContractsDataSet.Id, includeProperties: "Project, Subcontractor , Contract").FirstOrDefault();
            if (ContractsDataSet.ContractDatasetStatus == ContractDatasetStatus.Active)
            {
                NewDatasetTrans = 5;
                mainTrans = 3;
                IsChecked = true;
                IsCheckedEx = false;
                IsCheckedVo = false;
                //VoLevel = ContractsDataSet.VoLevel;
                if (ContractsDataSet.Contract != null)
                {
                    if (ContractsDataSet.Contract.Type == "Cost Plus")
                    {
                        ContractTypeCostPlus = true;
                        if (SContracts == null)
                            SContracts = new ObservableCollection<Contract>();
                        SContracts.Clear();
                        SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                        SelectedContract = ContractsDataSet.Contract;
                    }
                    else if (ContractsDataSet.Contract.Type == "Remeasured")
                    {
                        ContractTypeRemasured = true;
                        if (SContracts == null)
                            SContracts = new ObservableCollection<Contract>();
                        SContracts.Clear();
                        SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                        SelectedContract = ContractsDataSet.Contract;

                        Buildings = new ObservableCollection<Building>();
                        if (ContractsDataSet.Buildings != null)
                            Buildings.AddRange(ContractsDataSet.Buildings.ToList());
                        SelectedBuilding = Buildings.FirstOrDefault();
                    }
                    else if (ContractsDataSet.Contract.Type == "Lump Sum")
                    {
                        ContractTypeLumpSum = true;
                        if (SContracts == null)
                            SContracts = new ObservableCollection<Contract>();
                        SContracts.Clear();
                        SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                        SelectedContract = ContractsDataSet.Contract;
                    }
                }
                return;
            }

            //IsBusy = true;
            
            //Get Attachments 
           

            if (ContractsDataSet.Project != null)
            {
                Buildings = new ObservableCollection<Building>();
                if(ContractsDataSet.Buildings != null)
                    Buildings.AddRange(ContractsDataSet.Buildings.ToList());
                SelectedBuilding = Buildings.FirstOrDefault();
                SelectedProject = ContractsDataSet.Project;
                //EditProjectSelected();
            }
                
            if (ContractsDataSet.Subcontractor != null)
                SelectedSubcontractor = ContractsDataSet.Subcontractor;
            if (ContractsDataSet.Contract != null)
            {
                if (ContractsDataSet.Contract.Type == "Cost Plus")
                {
                    ContractTypeCostPlus = true;
                    if (SContracts == null)
                        SContracts = new ObservableCollection<Contract>();
                    SContracts.Clear();
                    SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                    SelectedContract = ContractsDataSet.Contract;
                }
                else if (ContractsDataSet.Contract.Type == "Remeasured")
                {
                    ContractTypeRemasured = true;
                    if (SContracts == null)
                        SContracts = new ObservableCollection<Contract>();
                    SContracts.Clear();
                    SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                    SelectedContract = ContractsDataSet.Contract;
                }
                else if (ContractsDataSet.Contract.Type == "Lump Sum")
                {
                    ContractTypeLumpSum = true;
                    if (SContracts == null)
                        SContracts = new ObservableCollection<Contract>();
                    SContracts.Clear();
                    SContracts.AddRange(Contracts.Where(c => c.Type == ContractsDataSet.Contract.Type).ToList());
                    SelectedContract = ContractsDataSet.Contract;
                }
            }
            if (SelectedProject == null)
            {
                NewDatasetTrans = 0;
                CdTransIndex = 0;
                IsChecked = true;
                IsCheckedEx = false;
            }
            else
            {
               
                IsPreviousEnabled = false;
                if (SelectedProject.Buildings.Count > 1)
                    IsNextEnabled = true;
                else IsNextEnabled = false;
                BuildingIndex = 0;
                //BuildingName = SelectedProject.Buildings.FirstOrDefault();
                CdTransIndex = 0;
                IsChecked = true;
                IsCheckedEx = false;
            }

            Task.Factory.StartNew(() =>
            {

                //var contract3 = unitOfWork.ContractsDatasetRepository.Get(
                //  x => x.Id == ContractsDataSet.Id,
                //  includeProperties: "PrescriptionTechniques,Plans").FirstOrDefault();
                //var contract4 = unitOfWork.ContractsDatasetRepository.Get(
                //      x => x.Id == ContractsDataSet.Id,
                //      includeProperties: "Other,PlansHSE,DocumentsJuridiques").FirstOrDefault();
                //var contract1 = unitOfWork.ContractsDatasetRepository.Get(
                //          x => x.Id == ContractsDataSet.Id,
                //          includeProperties: "ContractFile,Planning").FirstOrDefault();
                //var contract2 = unitOfWork.ContractsDatasetRepository.Get(
                //      x => x.Id == ContractsDataSet.Id,
                //      includeProperties: "UnitePrice,BoqAtt").FirstOrDefault();
            
                

                //ContractsDataSet.ContractFile = contract1.ContractFile;
                //ContractsDataSet.Planning = contract1.Planning;
                //ContractsDataSet.UnitePrice = contract2.UnitePrice;
                //ContractsDataSet.BoqAtt = contract2.BoqAtt;
                //ContractsDataSet.PrescriptionTechniques = contract3.PrescriptionTechniques;
                //ContractsDataSet.Plans = contract3.Plans;
                //ContractsDataSet.Other = contract4.Other;
                //ContractsDataSet.PlansHSE = contract4.PlansHSE;
                //ContractsDataSet.DocumentsJuridiques = contract4.DocumentsJuridiques;
            });

            NewDatasetTrans = 0;
            mainTrans = 3;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Visible;
        }
        private void TerminateRecord(object p)
        {
            try {
                var result = MessageBox.Show("Are you sure you want to terminate this Contract?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    //SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Terminated;
                    IsTerminateEnabled = false;
                    IsEditableEnabled = false;
                    IsBusy = true;
                    Task.Factory.StartNew(() =>
                    {
                        string name = new FileInfo("CS-HAB006-R.doc").FullName;
                        Word word = new Word();
                        var path = word.GenerateTerminationFile(name, SelectedContractsDataSet);
                        SelectedContractsDataSet.TerminatedFile = new ContractFile();
                        SelectedContractsDataSet.TerminatedFile.Content = ReadFile(path+".doc");
                        SelectedContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Terminated;
                        File.Delete(path);
                        File.Delete(path + ".doc");
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() =>
                        {
                            unitOfWork.Save();
                            IsBusy = false;

                            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                            MainWindow.TransiSlider.SelectedIndex = 5;
                            win.IpcDatabaseSlide.DataContext = new IpcViewModel();
                            ((IpcViewModel)win.IpcDatabaseSlide.DataContext).CreateFinalIpcCommand.Execute(SelectedContractsDataSet);
                            //unitOfWork.ContractsDatasetRepository.Update(SelectedContractsDataSet);
                        }));
                    });

                   

                }
                else
                    return;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private async void AttachDoc(object p)
        {
            //TODO::ATTACH DOCUMENT
            var view = new AttachDialog
            {
                DataContext = this
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "1")
            {
                if(ContractsDataSet.Planning_Id != null)
                {
                    ContractsDataSet.Planning = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.Planning_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.Planning_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {

                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.Planning = new ContractFile();
                            ContractsDataSet.Planning.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
               
            }
            else if ((string)result == "2")
            {
                if (ContractsDataSet.UnitePrice_Id != null)
                {
                    ContractsDataSet.UnitePrice = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.UnitePrice_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.UnitePrice_Id = null;
                    unitOfWork.Save();
                }
                else { 
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.UnitePrice = new ContractFile();
                            ContractsDataSet.UnitePrice.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "6")
            {
                if (ContractsDataSet.BoqAtt_Id != null)
                {
                    ContractsDataSet.BoqAtt = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.BoqAtt_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.BoqAtt_Id = null;
                    unitOfWork.Save();
                }
                else { 
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.BoqAtt = new ContractFile();
                            ContractsDataSet.BoqAtt.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "3")
            {
                if (ContractsDataSet.PrescriptionTechniques_Id != null)
                {
                    ContractsDataSet.PrescriptionTechniques = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.PrescriptionTechniques_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.PrescriptionTechniques_Id = null;
                    unitOfWork.Save();
                }
                else { 
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.PrescriptionTechniques = new ContractFile();
                            ContractsDataSet.PrescriptionTechniques.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "4")
            {
                if (ContractsDataSet.Plans_Id != null)
                {
                    ContractsDataSet.Plans = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.Plans_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.Plans_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.Plans = new ContractFile();
                            ContractsDataSet.Plans.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "5")
            {
                if (ContractsDataSet.Other_Id != null)
                {
                    ContractsDataSet.Other = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.Other_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.Other_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.Other = new ContractFile();
                            ContractsDataSet.Other.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "7")
            {
                if (ContractsDataSet.PlansHSE_Id != null)
                {
                    ContractsDataSet.PlansHSE = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.PlansHSE_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.PlansHSE_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.PlansHSE = new ContractFile();
                            ContractsDataSet.PlansHSE.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "8")
            {
                if (ContractsDataSet.DocumentsJuridiques_Id != null)
                {
                    ContractsDataSet.DocumentsJuridiques = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.DocumentsJuridiques_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.DocumentsJuridiques_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.DocumentsJuridiques = new ContractFile();
                            ContractsDataSet.DocumentsJuridiques.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
            else if ((string)result == "9")
            {
                if (ContractsDataSet.Other_Id != null)
                {
                    ContractsDataSet.Other = null;
                    ContractFile plan = new ContractFile { Id = (int)ContractsDataSet.Other_Id };
                    unitOfWork.context.ContractFiles.Attach(plan);
                    unitOfWork.context.Entry(plan).State = System.Data.Entity.EntityState.Deleted;
                    ContractsDataSet.Other_Id = null;
                    unitOfWork.Save();
                }
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {
                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            ContractsDataSet.Other = new ContractFile();
                            ContractsDataSet.Other.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }
            }
        }

        private async void AttachDocVo(object p)
        {
            //TODO::ATTACH DOCUMENT
            var view = new VoAttachDialog
            {
                DataContext = this
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if ((string)result == "BOQAtt")
            {
                if (VoDataset.BOQAtt != null)
                    VoDataset.BOQAtt = null;
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Pdf Files|*.pdf";
                    if (openFileDialog.ShowDialog() ?? false)
                    {
                        IsUploading = true;

                        await Task.Factory.StartNew(() =>
                        {

                            byte[] bytes = ReadFile(openFileDialog.FileName);
                            VoDataset.BOQAtt = new ContractFile();
                            VoDataset.BOQAtt.Content = bytes;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                          new ThreadStart(() =>
                          {
                              IsUploading = false;
                          }));
                        });
                    }
                }

            }
            
        }

        private async void ProjectSeleced(object p)
        {
            if (SelectedProject == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            if (SelectedProject.Buildings == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            if (ContractsDataSet == null)
                ContractsDataSet = new ContractsDataset();
            var e = p as SelectionChangedEventArgs;
            
            if (SelectedProject != null)
            {
                if(SelectedProject.Buildings == null)
                {
                    MessageBox.Show("This project has no buildings. Add buildings to the project before continue", "Project is Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                
                if(Buildings == null || Buildings.Count == 0 || e.RemovedItems.Count == 1)
                {
                    if (SelectedProject.Buildings != null)
                    {
                        //Buildings = new ObservableCollection<Building>();
                        //Buildings.AddRange(SelectedProject.Buildings);
                        if(SelectedProject.Buildings.Count == 1)
                        {
                            SelectedBuilding = SelectedProject.Buildings.FirstOrDefault();
                            ContractsDataSet.Buildings = SelectedProject.Buildings;
                            if (Buildings == null)
                                Buildings = new ObservableCollection<Building>();
                            Buildings.AddRange(ContractsDataSet.Buildings.ToList());
                        }
                        else
                        {
                            var view = new BuildingsDialog
                            {
                                DataContext = this
                            };

                            var result = await DialogHost.Show(view, "RootDialog");
                            if ((string)result == "Chose")
                            {
                                if (Buildings == null)
                                {
                                    SelectedProject = null;
                                    return;
                                }
                                var orderdBuildings = Buildings.OrderBy(x => x.Id).ToList();
                                Buildings.Clear();
                                Buildings.AddRange(orderdBuildings);
                                SelectedBuilding = Buildings.FirstOrDefault();
                                ContractsDataSet.Buildings = Buildings;
                            }
                            else
                            {
                                SelectedProject = null;
                                return;
                            }
                        }
                    }
                }
            }
            //SelectedBuilding = Buildings.FirstOrDefault();
            if (ContractsDataSet.Currency == null)
                ContractsDataSet.Currency = SelectedProject.Currency;
            if (SelectedBuilding ==  null)
            {
                MessageBox.Show("To complete the contract's BOQ creation, create a building for the project", "Project's buildings are Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (SelectedBuilding.Boq == null || SelectedBuilding.Boq.BoqSheets == null)
            {
                MessageBox.Show("To complete the Contract's BOQ creation, fill the sheets in the selected project first. ", "Project's trades are Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
                
            if(SelectedBuilding != null)
            {
                BuildingName = SelectedBuilding.Name;
                if (SelectedBuilding.Boq == null)
                    SelectedBuilding.Boq = new Boq();
                //TODO: CHECK IF PROJECT SHEETS IS NULL AND PRINT ERROR MESSAGE
                if (Sheets == null)
                    Sheets = new ObservableCollection<BoqSheet>();
                Sheets.Clear();
                if (SheetsVo == null)
                    SheetsVo = new ObservableCollection<BoqSheet>();
                SheetsVo.Clear();

               //var sheets = SelectedBuilding.Boq.BoqSheets.Where(x => x.IsActive == true).OrderBy(x=>x.Id).ToList();
               Sheets.AddRange(SelectedBuilding.Boq.BoqSheets.OrderBy(x => x.Id).ToList());

                if (ContractVoVo == null)
                    ContractVoVo = new ObservableCollection<ContractVo>();
                ContractVoVo.Clear();

            }
            if(SelectedProject.Buildings.Count > 1)
                IsNextEnabled = true;
        }
        private void EditProjectSelected()
        {
            if (SelectedProject == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            if (SelectedProject.Buildings == null)
                return; //TODO: ERROR MESSAGE SHOW DISPLAY TO USER
            if (SelectedProject != null)
                SelectedBuilding = SelectedProject.Buildings.FirstOrDefault();
            if (SelectedBuilding == null)
            {
                MessageBox.Show("To complete Contract dataset creation, create a building for the project", "Project's buildings are Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (SelectedBuilding.Boq == null || SelectedBuilding.Boq.BoqSheets == null)
            {
                MessageBox.Show("To complete Contract dataset creation, fill the sheets in the selected prject first. ", "Project's trades are Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (SelectedBuilding != null)
            {
                BuildingName = SelectedBuilding.Name;
                if (SelectedBuilding.Boq == null)
                    SelectedBuilding.Boq = new Boq();
                //TODO: CHECK IF PROJECT SHEETS IS NULL AND PRINT ERROR MESSAGE
                if (Sheets == null)
                    Sheets = new ObservableCollection<BoqSheet>();
                Sheets.Clear();

                var sheets = SelectedBuilding.Boq.BoqSheets.ToList();
                Sheets.AddRange(sheets);
        
                if (ContractVoVo == null)
                    ContractVoVo = new ObservableCollection<ContractVo>();
                ContractVoVo.Clear();
                
            }
            if (SelectedProject.Buildings.Count > 1)
                IsNextEnabled = true;
        }
        private void SheetSelectedDialog(object p)
        {
            //var e = p as SelectionChangedEventArgs;
            //if (SelectedSheet == null)  = new ObservableCollection<BoqSheet>();
            //
            //var test = ((ListBox)(e.Source)).SelectedItems.Cast<BoqSheet>().ToList();
            //SelectedSheet.Clear();
            //SelectedSheet.AddRange(test);
        }
        private void BuildingSelectedDialog(object p)
        {
            var e = p as SelectionChangedEventArgs;
            if (Buildings == null) Buildings = new ObservableCollection<Building>();
            
            var test = ((ListBox)(e.Source)).SelectedItems.Cast<Building>().ToList();
            Buildings.Clear();
            Buildings.AddRange(test);
        }   
        private void SheetVoSeleced(object p)
        {
            if (SelectedSheetVo != null)
            {
                if (ContractVoVo == null)
                    ContractVoVo = new ObservableCollection<ContractVo>();
                ContractVoVo.Clear();
                var bi = unitOfWork.ContractVoRepository.Get(x => x.BoqSheetId == SelectedSheetVo.Id && x.Level == VoLevel);
                ContractVoVo.AddRange(bi.OrderBy(x => x.OrderVo).ToList());
                if (ContractVoVo.Count > 0)
                {
                    VoSum = ContractVoVo.Sum(x => x.Pt);
                }
            }
        }
       
        private void SaveOnRecords(object p)
        {
            /* if (ContractTypeCostPlus == true)
                 ContractsDataSet.Contract = Contracts.Where(c => c.Type == "Cost Plus").ToList().First();
             if (ContractTypeRemasured == true)
                 ContractsDataSet.Contract = Contracts.Where(c => c.Type == "Remasured").ToList().First();
             if (ContractTypeLumpSum == true)
                 ContractsDataSet.Contract = Contracts.Where(c => c.Type == "Lump Sum").ToList().First();
            */
            ContractsDataSet.Contract = SelectedContract;
            unitOfWork.ContractsDatasetRepository.Update(ContractsDataSet);
            unitOfWork.Save();
            mainTrans = 4;
            NewDatasetTrans = 0;
            IsChecked = false;
            IsCheckedEx = true;
            ResetTables();
        }
        private void SaveParticular(object p)
        {
            if (SelectedContract == null)
            {
                MessageBox.Show("The Contract is not selected. Please select a contract then try again.", "The Contract Is Not Defined", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ContractsDataSet.Contract = SelectedContract;
       
            //TODO::Should contract change
            ContractsDataSet.Project = SelectedProject;
            ContractsDataSet.Subcontractor = SelectedSubcontractor;//TODO: CHECK CONTRACT
            //var xxx = unitOfWork.ContractsDatasetRepository.Get(x => x.Project.Id == SelectedProject.Id);
            //var sdasd = xxx.OrderBy(x => x.Order).ToList();
            //var contract = sdasd.LastOrDefault();
            //int order = 0;
            //if(ContractsDataSet.Id == 0)
            //{
            //    if (contract == null)
            //    {
            //        order = 0;
            //        ContractsDataSet.Order = order + 1;
            //    }
            //    else
            //    {
            //        order = contract.Order;
            //        ContractsDataSet.Order = contract.Order + 1;
            //    }
            //    ContractsDataSet.ContractNumber = "CS-" + SelectedProject.Acronym + "-" + (order + 1).ToString("000");
            //
            //}


            //ContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Editable;
            //if (!ContractsDatasets.Contains(ContractsDataSet))
            //    ContractsDatasets.Add(ContractsDataSet);
            //if(ContractsDataSet.Id == 0)
            //    unitOfWork.ContractsDatasetRepository.Insert(ContractsDataSet);
            //else
            //    unitOfWork.ContractsDatasetRepository.Update(ContractsDataSet);

            //var orderdContracts = ContractsDatasets.OrderByDescending(x => x.Created).ToList();
            //ContractsDatasets = new ObservableCollection<ContractsDataset>();
            //ContractsDatasets.AddRange(orderdContracts);

            //unitOfWork.Save();
            NewDatasetTrans = 3;
            OpenTrade(null);
        }
       
        private void SaveRecords(object p)
        {

            if (SelectedContract == null)
            {
                MessageBox.Show("The contract is not selected. Please select a contract then try again.", "The Contract Is Not Defined", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //TODO::Chech selectedproject if null
            ContractsDataSet.Contract = SelectedContract;
            if (ContractsDataSet.ContractBoqItem != null)
                //I saved advancePayment here but i will recalculate it in IpcViewModel
                ContractsDataSet.AdvancePayment = ContractsDataSet.ContractBoqItem.Where(x => x.BoqSheet != null).Sum(x => x.Pt);
            ContractsDataSet.Project = SelectedProject;
            ContractsDataSet.Subcontractor = SelectedSubcontractor;//TODO: CHECK CONTRACT

            ContractsDataSet.ContractDatasetStatus = ContractDatasetStatus.Editable;
            if (!ContractsDatasets.Contains(ContractsDataSet))
                ContractsDatasets.Add(ContractsDataSet);

            //Check Contract Number Identity
            var ident = unitOfWork.ContractsDatasetRepository.Get(x => x.ContractNumber == ContractsDataSet.ContractNumber && x.Id != ContractsDataSet.Id).FirstOrDefault();
            if(ident != null)
            {
                MessageBox.Show("The contract number is already exist. Change the number before continue", "Can't Save Contract", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                if (ContractsDataSet.Id == 0)
                {
                    unitOfWork.ContractsDatasetRepository.Insert(ContractsDataSet);
                }
                else
                {
                    unitOfWork.ContractsDatasetRepository.Update(ContractsDataSet);
                }
                unitOfWork.Save();


                mainTrans = 4;
                IsChecked = false;
                IsCheckedVo = false;
                IsCheckedEx = true;
                BoqSum = 0;
                ResetTables();
                var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
                win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
        }
        private void ResetTables()
        {
            //ContractsDataSet = new ContractsDataset();
            SelectedProject = null;
            SelectedBoqSheet = null;
            SelectedContract = null;
            if (SContracts != null)
                SContracts.Clear();
            ContractTypeLumpSum = false;
            ContractTypeRemasured = false;
            ContractTypeCostPlus = false;
            Sheets = null;
            SelectedSubcontractor = null;
            if (BoqItems != null)
                BoqItems.Clear();
            if (ContractVoVo != null)
                ContractVoVo.Clear();
            NewCDTrans = 0;
            NewDatasetTrans = 0;
            BuildingIndex = 0;
        }
        private async void DeleteRecord(object p)
        {
            if (ContractsDataSet == null) return;
         
            var view = new ConfirmDialog();

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            //TODO: Contract Orders changed because of EF
            if ((string)result == "Delete")
            {
                if (BoqItems != null) BoqItems.Clear();
                var not = unitOfWork.Notifications.Get(x => x.Contract.Id == ContractsDataSet.Id);
                if(not != null && not.Count() > 0)
                {
                    if (not.FirstOrDefault().Read == false)
                        unitOfWork.Notifications.Delete(not.FirstOrDefault());
                }
                unitOfWork.ContractsDatasetRepository.Delete(ContractsDataSet);
                ContractsDatasets.Remove(ContractsDataSet);
                //for (int i = 0; i < ContractsDatasets.Count; i++)
                //{
                //    ContractsDatasets[i].Order = i + 1;
                //    ContractsDatasets[i].ContractNumber = "CS-" + ContractsDatasets[i].Project.Acronym + "-" + ContractsDatasets[i].Order.ToString("000");
                //    unitOfWork.ContractsDatasetRepository.Update(ContractsDatasets[i]);
                //}
                unitOfWork.Save();
            }

        }
        private void PreviousBuilding(object p)
        {
            BuildingIndex--;
            if (BuildingIndex < 0)
                return;
            if (BuildingIndex == 0)
                IsPreviousEnabled = false;
            if (BuildingIndex < SelectedProject.Buildings.Count - 1)
                IsNextEnabled = true;
            SelectedBuilding = SelectedProject.Buildings.ElementAt(BuildingIndex);
            ChangeBuilding();
        }
        private void NextBuilding(object p)
        {
            BuildingIndex++;
            if (BuildingIndex == SelectedProject.Buildings.Count - 1)
                IsNextEnabled = false;
            if (BuildingIndex > 0)
                IsPreviousEnabled = true;
            SelectedBuilding = SelectedProject.Buildings.ElementAt(BuildingIndex);
            ChangeBuilding();
        }
        private void ChangeBuilding()
        {
            if (SelectedBuilding.Boq != null)
            {
                SelectedBoqSheet = SelectedBuilding.Boq.BoqSheets.Where(x => x.Name == SelectedBoqSheet.Name).FirstOrDefault();
            }
            else
            {
                if (Sheets != null) Sheets.Clear();
                if (BoqItems != null) BoqItems.Clear();
            }
        }
        private void BuildingSeleced(object p)
        {
            if (SelectedBuilding == null)
                return;
            if (ContractsDataSet == null)
                return;
            if (ContractsDataSet.Id == 0)
                return;

            if (BoqItems == null)
                BoqItems = new ObservableCollection<ContractBoqItem>();
            BoqItems.Clear();    
           
            var trade = SelectedBuilding.Boq.BoqSheets.Where(x => x.Name == ContractsDataSet.BoqSheet.Name).FirstOrDefault();
            SelectedBoqSheet = trade;

            if (SelectedBoqSheet == null)
                return;
         
        
            var boqitems = SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id)
               .ToList();
            if (boqitems.Count == 0)
            {
                foreach (var item in SelectedBoqSheet.BoqItems)
                {
                    var activity = new ContractBoqItem
                    {
                        No = item.No,
                        Key = item.Key,
                        Unite = item.Unite,
                        CostCode = item.CostCode,
                        Qte = item.Qte,
                        Pu = item.Pu,
                        BoqSheet = item.BoqSheet,
                        BOQType = BOQType.Line,
                        OrderBoq = item.OrderBoq,
                        ContractsDataset = ContractsDataSet,
                    };
                    //ContractsDataSet.ContractVo.Add(activity);
                    unitOfWork.ContractBoqItemRepository.Insert(activity);
                    SelectedBoqSheet.ContractBoqItems.Add(activity);
                }

                unitOfWork.Save();
            }

            boqitems = SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id ).OrderBy(x => x.OrderBoq)
                .ToList();
            BoqItems.AddRange(boqitems);
           update();
        }
        private void UploadBoq(object p)
        {
            //TODO:: SELECTED BUILDING MUST NOT BE CREATED ANOTHER TIME AND ADDED TO BUILDINGS OC<>
            if (SelectedBuilding == null || ContractsDataSet.BoqSheet == null)
            {
                MessageBox.Show("Select a building or a trade before uploading the BOQ", "Building not selected", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SelectedBoqSheet = ContractsDataSet.BoqSheet;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() ?? false)
             {
                IsUploading = true;
                Task.Factory.StartNew(() =>
                {
                    var excel = new Excel(openFileDialog.FileName);

                    if (SelectedBuilding.Boq == null)
                        SelectedBuilding.Boq = new Boq();
                    double orderBoq = 1.0f;
                    var test = excel.GetSheets().ToList();

                    var result = excel.GetContractBoqItems(SelectedBoqSheet, ContractsDataSet, ref orderBoq, unitOfWork);
                    if (result != null)
                    {
                        foreach (var sub in SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).ToList())
                        {
                            unitOfWork.ContractBoqItemRepository.Delete(sub);
                            SelectedBoqSheet.ContractBoqItems.Remove(sub);
                        }
                        foreach (var item in result)
                        {
                            SelectedBoqSheet.ContractBoqItems.Add(item);
                        }
                        unitOfWork.Save();
                        
                    }
                    excel.Close();
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsUploading = false;
                        if (BoqItems == null)
                            BoqItems = new ObservableCollection<ContractBoqItem>();
                        BoqItems.Clear();
                        var boqitems = SelectedBoqSheet.ContractBoqItems.Where(x => x.ContractsDatasetId == ContractsDataSet.Id).OrderBy(x => x.OrderBoq).ToList();
                        BoqItems.AddRange(boqitems);
                        update();
                    }));
                });
            }
        }

        private void UploadVo(object p)
        {
            //TODO:: SELECTED BUILDING MUST NOT BE CREATED ANOTHER TIME AND ADDED TO BUILDINGS OC<>
            if (SelectedBuilding == null)
            {
                MessageBox.Show("Select a building or a trade before uploading the BOQ", "Building not selected", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SelectedBoqSheet = VoDataset.ContractsDataset.BoqSheet;
            if(SelectedBoqSheet == null)
            {
                MessageBox.Show("Select a building or a trade before uploading the BOQ", "Building not selected", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() ?? false)
            {
                IsUploading = true;
                Task.Factory.StartNew(() =>
                {
                    var excel = new Excel(openFileDialog.FileName);

                    if (SelectedBuilding.Boq == null)
                        SelectedBuilding.Boq = new Boq();
                    double orderBoq = 1.0f;
                    var test = excel.GetSheets().ToList();

                    var result = excel.GetContractVoItems(SelectedBoqSheet, VoDataset, ref orderBoq, unitOfWork);
                    if (result != null)
                    {
                        if(VoDataset.VoItems != null)
                        {
                            foreach (var sub in VoDataset.VoItems)
                            {
                                unitOfWork.ContractVoRepository.Delete(sub);
                                VoDataset.VoItems.Remove(sub);
                            }
                        }
                       
                        VoDataset.VoItems = result.ToList();
                        unitOfWork.Save();

                    }
                    excel.Close();
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    new ThreadStart(() =>
                    {
                        IsUploading = false;
                        if (ContractVoVo != null)
                            ContractVoVo.Clear();
                        else
                            ContractVoVo = new ObservableCollection<ContractVo>();
                        ContractVoVo.AddRange(VoDataset.VoItems.OrderBy(x => x.OrderVo).ToList());
                        updateVo();
                    }));
                });
            }
        }
        byte[] ReadFile(string sPath)
        {
            byte[] data = null;
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fStream);
            data = br.ReadBytes((int)numBytes);
            br.Close();
            fStream.Close();

            return data;
        }
    }
}
