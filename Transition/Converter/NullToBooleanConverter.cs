﻿using ContractsManager.Data;
using System;
using System.Threading;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    class NullToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if((string)parameter == "Generate")
            {
                if (value == null)
                    return false;
                else
                    return !((Ipc)value).IsGenerated;
            }
            if (value == null)
            {
                return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
