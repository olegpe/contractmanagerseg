﻿using ContractsManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoqTypeToBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            var boqType = (BOQType)value;
            if (boqType == BOQType.Activity || boqType == BOQType.SubActivity || boqType == BOQType.TotalSum)
                return true;
            return false;

        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
