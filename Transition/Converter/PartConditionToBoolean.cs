﻿using ContractsManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContractsManager.Converter
{
 
    public class PartConditionToBoolean : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return true;
            //ContractsDataset cont = value as ContractsDataset;
            //if (cont == null) return false;
            //if (//!string.IsNullOrWhiteSpace(cont.ContractDate) && !string.IsNullOrWhiteSpace(cont.CompletionDate) &&
            //     !string.IsNullOrWhiteSpace(cont.LatePenaliteCeiling)
            //    && !string.IsNullOrWhiteSpace(cont.LatePenalties) && !string.IsNullOrWhiteSpace(cont.HoldWarranty)
            //    && !string.IsNullOrWhiteSpace(cont.MintenancePeriod) && !string.IsNullOrWhiteSpace(cont.WorkWarranty)
            //    && !string.IsNullOrWhiteSpace(cont.Termination) && !string.IsNullOrWhiteSpace(cont.DaysNumber)
            //    && !string.IsNullOrWhiteSpace(cont.Progress) && !string.IsNullOrWhiteSpace(cont.HoldBack)
            //    && !string.IsNullOrWhiteSpace(cont.SubcontractorAdvancePayee) && !string.IsNullOrWhiteSpace(cont.RecoverAdvance)
            //    && !string.IsNullOrWhiteSpace(cont.ProcurementConstruction) && !string.IsNullOrWhiteSpace(cont.ProrataAccount)
            //    && !string.IsNullOrWhiteSpace(cont.ManagementFees))
            //    return true;
            //else return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
