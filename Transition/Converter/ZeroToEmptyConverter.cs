﻿using ContractsManager.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    public class ZeroToEmptyConverter : IMultiValueConverter
    {
        public object Convert(object []value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value[0] != DependencyProperty.UnsetValue && value[1] != DependencyProperty.UnsetValue)
            if ((BOQType)value[0] == BOQType.Activity || (BOQType)value[0] == BOQType.SubActivity)
                return string.Empty;
            return String.Format("{0:n}", value[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            int val = System.Convert.ToInt32(value);
            throw new NotImplementedException();
            //return new object[] { BOQType.Line, String.Format("{0:n}", val) };
        }
    }
}
