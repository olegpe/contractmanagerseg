﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContractsManager.Converter
{
 
    public class NullToAttachmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter.ToString() == "planning")
            {

                if (value == null)
                    return "Annexe G: Planning";
                else
                {
                    return "Delete Planning";
                }
            }
            else if (parameter.ToString() == "prix")
            {
                if (value == null)
                    return "Annexe I: Prix Unitaire de résiliation";
                else
                {
                    return "Delete Termination Unit price";
                }
            }
            else if (parameter.ToString() == "Prescription")
            {
                if (value == null)
                    return "Annexe E: Cahier des Prescriptions Techniques";
                else
                {
                    return "Delete Cahier des Prescriptions Techniques";
                }
            }
            else if (parameter.ToString() == "Plans")
            {
                if (value == null)
                    return "Annexe F: Plans";
                else
                {
                    return "Delete Plans";
                }
            }
            else if (parameter.ToString() == "Other")
            {
                if (value == null)
                    return "Annexe 1";
                else
                {
                    return "Delete Annexe 1";
                }
            }
            else if (parameter.ToString() == "DocumentsJuridiques")
            {
                if (value == null)
                    return "Annexe J: Documents Juridiques";
                else
                {
                    return "Delete Documents Juridiques";
                }
            }
            else if (parameter.ToString() == "PlansHSE")
            {
                if (value == null)
                    return "Annexe H: Offre de Sous-traitant";
                else
                {
                    return "Delete Offre de Sous-traitant";
                }
            }
            else if (parameter.ToString() == "BoqAtt")
            {
                if (value == null)
                    return "Annexe D: BOQ";
                else
                {
                    return "Delete BOQ";
                }
            }
            else return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
