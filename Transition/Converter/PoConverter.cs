﻿using ContractsManager.Data;
using ContractsManager.Data.Repository;
using ContractsManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    public class PoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return ((CostCodeLibrary)value).Code;
            else return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MainWindow win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            ProjectsViewModel pvm;
            ContractsDatasetViewModel us;
            SheetViewModel svm;
            List<CostCodeLibrary> costCode = new List<CostCodeLibrary>();
            string cost = value.ToString();


            if (parameter.ToString() == "project")
            {
                pvm = win.ProjectsSlide.DataContext as ProjectsViewModel;
                costCode = pvm.unitOfWork.CostCodeLibraryRepository.Get(x => x.Code == cost).ToList();

            }
            else if (parameter.ToString() == "subcontractor")
            {
                us = win.ContractDatabaseSlide.DataContext as ContractsDatasetViewModel;
                costCode = us.unitOfWork.CostCodeLibraryRepository.Get(x => x.Code == cost).ToList();
            }
            else if (parameter.ToString() == "sheet")
            {
                svm = win.AdminToolsSlide.Sheets.DataContext as SheetViewModel;
                costCode = svm.unitOfWork.CostCodeLibraryRepository.Get(x => x.Code == cost).ToList();
            }


            if (costCode.Count() > 0)
                return costCode.First();
            else
            {
                MessageBox.Show("This Cost Code dosen't exist! Please check the Cost Code Library by double clicing the cell.", "Cost Code Doesn't Exist", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
        }
 
    }
}
