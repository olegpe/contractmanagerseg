﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    public class QteMultiplePriceConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter,
                        System.Globalization.CultureInfo culture)
        {
            if (value[0] == null || value[1] == null)
                return null;
            if (value[0] == DependencyProperty.UnsetValue)
                return DependencyProperty.UnsetValue;
            if (value[1] == DependencyProperty.UnsetValue)
                return DependencyProperty.UnsetValue;
            double p, q;
            if(double.TryParse(value[0].ToString(), out p) && double.TryParse(value[1].ToString(), out q))
                return p.ToString() + " " + q.ToString()+"%";
            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
