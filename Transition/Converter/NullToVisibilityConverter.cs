﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if((string)parameter == "ContractTemplate")
            {
                if (value == null)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            if ((string)parameter == "Response")
            {
                if ((string)value == "Response")
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            else if ((string)parameter == "IPC")
            {
                if ((string)value == "IPC")
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            else if ((string)parameter == "Contract")
            {
                if ((string)value == "Contract")
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            else if ((string)parameter == "VoDataset")
            {
                if ((string)value == "VoDataset")
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return "";
        }
    }
}
