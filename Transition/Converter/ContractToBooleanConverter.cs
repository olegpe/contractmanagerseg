﻿using ContractsManager.Data;
using System;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    class ContractToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
           if(value == null)
            {
                return false;
            }
            else
            {
                var cont = value as ContractsDataset;
                if (cont.ContractDatasetStatus == ContractDatasetStatus.Terminated)
                    return false;
                else if (cont.ContractDatasetStatus == ContractDatasetStatus.Active)
                    return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
