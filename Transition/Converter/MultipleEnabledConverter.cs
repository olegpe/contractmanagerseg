﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContractsManager.Converter
{
    public class MultipleEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                return false;

            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
                return false;

            if (values[0] == null)
            {
                return false;
            }
            else
            {
                if(values[0] is Data.Ipc)
                {
                    if (((Data.Ipc)values[0]).IpcStatus != Data.IpcStatus.Editable)
                        return false;
                    else
                    {
                        if ((bool)values[1] == true)
                            return true;
                    }
                }
                else if (values[0] is Data.ContractsDataset)
                {
                    if (((Data.ContractsDataset)values[0]).ContractDatasetStatus != Data.ContractDatasetStatus.Editable)
                        return false;
                    else
                    {
                        if ((bool)values[1] == true)
                            return true;
                    }
                }
                if ((bool)values[1] == false)
                    return false;
                return true;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
