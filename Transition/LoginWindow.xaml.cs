﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf.Transitions;
using MaterialDesignThemes.Wpf;
using ContractsManager.Dialogs;
using ContractsManager.ViewModel;
using ContractsManager.Data;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Threading;
using System.Data.Entity.Core;
using ContractsManager.utils;
using System.Text.RegularExpressions;
using System.Data.Common;
using System.Configuration;

namespace ContractsManager
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        private ConnectionModel _selectedCountry;
        public ConnectionModel SelectedCountry
        {
            get
            {
                return _selectedCountry;
            }
            set
            {
                _selectedCountry = value;
            }
        }
        private ObservableCollection<ConnectionModel> _connections;
        public ObservableCollection<ConnectionModel> Connections
        {
            get
            {
                return _connections;
            }
            set
            {
                _connections = value;
            }

        }

        public LoginWindow()
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;

        }
        public ConnectionModel Connect { get; set; }
        private async void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Connect = new ConnectionModel();
            Connections = Connection.GetConnectionStrings();


            var view = new ConnectionDialog
            {
                DataContext = this
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog", ExtendedOpenedEventHandler, ExtendedClosingEventHandler);

            //check the result...
            Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
        }


        private void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs)
        {

        }

        private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is bool)
                if ((bool)eventArgs.Parameter == false) return;

            eventArgs.Cancel();
            eventArgs.Session.UpdateContent(new ProgressDialog());


            if (eventArgs.Parameter is string)
            {
                if (SelectedCountry == null)
                    return;
                try
                {


                    Task.Factory.StartNew(() =>
                    {
                        using (var context = new DataContext(Connection.BuildConnectionString(SelectedCountry)))
                        {
                            DbConnection conn = context.Database.Connection;
                            try
                            {
                                Connections.Select(x => { x.IsCurrent = false; return x; }).ToList();
                                conn.Open();   // check the database connection
                                SelectedCountry.IsCurrent = true;

                                Connection.SetConnectionString(Connections);

                                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                    new ThreadStart(() =>
                                    {
                                        eventArgs.Session.Close(false);
                                        var win2 = new IntroWindow();
                                        win2.Show();
                                        this.Close();
                                    }));


                            }
                            catch (Exception ex)
                            {
                                var res = MessageBox.Show("The Connection string is not valid.", "Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);

                                if (res == MessageBoxResult.OK)
                                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                       new ThreadStart(() =>
                                       {
                                           eventArgs.Session.UpdateContent(new ConnectionDialog
                                           {
                                               DataContext = this
                                           });
                                       }));
                            }
                        }


                    });

                }
                catch (EntityException exception)
                {
                    // Output expected EntityExceptions.
                    Logging.Log(exception);
                    if (exception.InnerException != null)
                    {
                        // Output unexpected InnerExceptions.
                        Logging.Log(exception.InnerException, false);
                    }
                    eventArgs.Session.UpdateContent(new ConnectionDialog());
                    return;
                }
                catch (Exception exception)
                {
                    // Output unexpected Exceptions.
                    Logging.Log(exception, false);
                    eventArgs.Session.UpdateContent(new ConnectionDialog());
                    return;
                }
            }
            else
            {

                //OK, lets cancel the close...
                PasswordBox pwd = (PasswordBox)eventArgs.Parameter;
                Connect.Password = pwd.Password;
                //var c = eventArgs.Content.ConnectionPwd;
                //...now, lets update the "session" with some new content!
                //note, you can also grab the session when the dialog opens via the DialogOpenedEventHandler

                //lets run a fake operation for 3 seconds then close this baby.

                try
                {


                    Task.Factory.StartNew(() =>
                    {
                        using (var context = new DataContext(Connection.BuildConnectionString(Connect)))
                        {
                            DbConnection conn = context.Database.Connection;
                            try
                            {
                                conn.Open();   // check the database connection
   
                                ObservableCollection<ConnectionModel> c = Connection.GetConnectionStrings();
                                foreach (var connection in c)
                                {
                                    connection.IsCurrent = false;
                                }
                                string code = "";
                                if (c.Count <= 0)
                                {
                                    code = "DB1";
                                }
                                else
                                    code = "DB" + (Convert.ToInt32(Regex.Match(c.Last().Code, @"\d+").Value) + 1).ToString();
                                Connect.Code = code;
                                Connect.IsCurrent = true;
                                c.Add(Connect);
                                Connection.SetConnectionString(c);

                                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                    new ThreadStart(() =>
                                    {
                                        eventArgs.Session.Close(false);
                                        var win2 = new IntroWindow();
                                        win2.Show();
                                        this.Close();
                            }));


                            }
                            catch(Exception ex)
                            {
                                var res = MessageBox.Show("The Connection string is not valid.", "Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);
                              
                                if(res == MessageBoxResult.OK)
                                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                   new ThreadStart(() =>
                                   {
                                       eventArgs.Session.UpdateContent(new ConnectionDialog
                                       {
                                           DataContext = this
                                       });
                                   }));
                            }
                        }

                        
                    });

                }
                catch (EntityException exception)
                {
                    // Output expected EntityExceptions.
                    Logging.Log(exception);
                    if (exception.InnerException != null)
                    {
                        // Output unexpected InnerExceptions.
                        Logging.Log(exception.InnerException, false);
                    }
                    eventArgs.Session.UpdateContent(new ConnectionDialog());
                    return;
                }
                catch (Exception exception)
                {
                    // Output unexpected Exceptions.
                    Logging.Log(exception, false);
                    eventArgs.Session.UpdateContent(new ConnectionDialog());
                    return;
                }

            }

        }
    }
}