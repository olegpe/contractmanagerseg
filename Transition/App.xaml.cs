﻿using ContractsManager.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ContractsManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {

            var lang = ContractsManager.Properties.Settings.Default.Language;
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);
            CustomPrincipal customPrincipal = new CustomPrincipal();
            AppDomain.CurrentDomain.SetThreadPrincipal(customPrincipal);

            var cstring = Connection.GetConnectionStrings();
            var con = cstring.Where(x => x.IsCurrent == true).FirstOrDefault();
            if (cstring.Count <= 0)
            {
                this.StartupUri = new System.Uri("LoginWindow.xaml", System.UriKind.Relative);
            }
         
            using (var context = new DataContext(Connection.BuildConnectionString(con)))
            {
                DbConnection conn = context.Database.Connection;
                 
                try
                {
                    conn.Open();

                    if (context.Currencies.ToList() != null)
                    {
                        this.StartupUri = new System.Uri("LoginWindow.xaml", System.UriKind.Relative);

                    }
                    this.StartupUri = new System.Uri("MainWindow.xaml", System.UriKind.Relative);
                    

                }
                catch (Exception ex)
                {
                    this.StartupUri = new System.Uri("LoginWindow.xaml", System.UriKind.Relative);
                }
            }

        

            this.Dispatcher.UnhandledException += ShowUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += ShowUnhandled;

        }

        private void ShowUnhandled(object sender, UnhandledExceptionEventArgs e)
        {
            // e.ExceptionObject = true;
            //
            // string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError: {0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",
            //
            // e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
            // e.Exception.InnerException.Message : null));
            //
            // if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            // {
            //     if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            //     {
            //         Application.Current.Shutdown();
            //     }
            // }
        }

        void ShowUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            string filePath = @"Error.txt";

            Exception ex = e.Exception;

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();

                while (ex != null)
                {
                    writer.WriteLine(ex.GetType().FullName);
                    writer.WriteLine("Message : " + ex.Message);
                    writer.WriteLine("StackTrace : " + ex.StackTrace);

                    ex = ex.InnerException;
                }
            }

            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError: {0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",

            e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
            e.Exception.InnerException.Message : null));

            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}
