﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for NewContractsDataset.xaml
    /// </summary>
    public partial class NewContractsDataset : UserControl
    {
        public NewContractsDataset()
        {
            InitializeComponent();
            //FutureDatePicker.SelectedDate = DateTime.Now;
            //FutureDatePicker2.SelectedDate = DateTime.Now;
            //FutureDatePicker.BlackoutDates.AddDatesInPast();
            //FutureDatePicker2.BlackoutDates.AddDatesInPast();
        }

        private void NewDataset_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ContractDatabase.Pb != null)
            ContractDatabase.Pb.Value = 25 * (newDataset.SelectedIndex + 1);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.ContractDatabaseSlide.contractDatasetTrans.SelectedIndex = 2;

            win.mainMenu.DimmedGrid.Visibility = Visibility.Hidden;
            win.ContractDatabaseSlide.DimmedGrid.Visibility = Visibility.Collapsed;
        }

        private void boqScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(boqGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void container_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            FrameworkElement tb = e.OriginalSource as FrameworkElement;
            if (tb != null)
            {
                switch (e.Key)
                {
                    case Key.Enter:
                        e.Handled = true;
                        tb.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
