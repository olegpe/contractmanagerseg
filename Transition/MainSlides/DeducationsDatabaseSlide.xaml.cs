﻿using ContractsManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for DeducationsDatabaseSlide.xaml
    /// </summary>
    public partial class DeducationsDatabaseSlide : UserControl
    {
        public DeducationsDatabaseSlide()
        {
            InitializeComponent();
        }

        private void RbDedu1_Click(object sender, RoutedEventArgs e)
        {
            deduTrans.SelectedIndex = 0;
        }

        private void RbDedu2_Click(object sender, RoutedEventArgs e)
        {
            deduTrans.SelectedIndex = 1;
        }

        private void RbDedu3_Click(object sender, RoutedEventArgs e)
        {
            deduTrans.SelectedIndex = 2;
        }

        private void boqScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(LaborGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void VenteScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(venteGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void machinScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(machineGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void LaborSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ((DeducationsViewModel)DataContext).LaborSelecteionChangedCommand.Execute(sender);
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.VenteLocal.DataContext = new VenteLocalViewModel();
        }
    }
}
