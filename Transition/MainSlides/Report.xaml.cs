﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : UserControl
    {
        public Report()
        {
            InitializeComponent();
        }

        private void BoqScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(site, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void BoqScroll1_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(recapSite, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }
    }
}
