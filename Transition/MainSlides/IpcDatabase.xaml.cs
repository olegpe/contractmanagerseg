﻿using ContractsManager.ViewModel;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for IpcDatabase.xaml
    /// </summary>
    public partial class IpcDatabase : UserControl
    {
        public IpcDatabase()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            var data = this.DataContext as IpcViewModel;
            if(ipcProgressTrans.SelectedIndex == 0)
            {
                ipcProgressTrans.SelectedIndex = 1;
                if(data.Interim == true || data.Final == true)
                {
                    if (data.AllContBoqItems.Count == 0)
                        ipcProgressTrans.SelectedIndex = 2;
                    else
                        ProgressTrans.SelectedIndex = 1;

                }
            }
            else if (ipcProgressTrans.SelectedIndex == 1)
            {
               
                    ipcProgressTrans.SelectedIndex = 2;
                    if (data.AllContVo.Count == 0)
                        ipcProgressTrans.SelectedIndex = 3;
                
            }
            else if (ipcProgressTrans.SelectedIndex == 2)
            {
                if(data.Labors.Count == 0 && data.VenteLocals.Count == 0 && data.Machines.Count == 0)
                {
                    ipcProgressTrans.SelectedIndex = 4;
                    data.PreviewExcel(null);
                }
                else
                    ipcProgressTrans.SelectedIndex = 3;
            }
            else if (ipcProgressTrans.SelectedIndex ==3 )
            {
                /*if (ipcLaborTrans.SelectedIndex < 2)
                    ipcLaborTrans.SelectedIndex++;
                else
                    ipcProgressTrans.SelectedIndex = 4;*/
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            var data = this.DataContext as IpcViewModel;

            if (ipcProgressTrans.SelectedIndex == 0)
            {
                ipcTrans.SelectedIndex = 0;
                return;
            }
            
        
            if (ipcProgressTrans.SelectedIndex == 4)
            {
                if (data.Labors.Count != 0 || data.VenteLocals.Count != 0 || data.Machines.Count != 0)
                    ipcProgressTrans.SelectedIndex = 3;
                else
                {
                    ipcProgressTrans.SelectedIndex = 2;
                    if (data.AllContVo.Count == 0)
                        ipcProgressTrans.SelectedIndex = 1;
                }
              
                if(ipcProgressTrans.SelectedIndex == 1)
                {
                    if(data.AllContBoqItems.Count == 0)
                    {
                        ProgressTrans.SelectedIndex = 0;

                        if (data.Interim == true || data.Final == true)
                            ipcProgressTrans.SelectedIndex = 0;
                    }
                    else ProgressTrans.SelectedIndex = 1;
                    
                }
            }

            else if (ipcProgressTrans.SelectedIndex == 3)
            {
                if (ipcLaborTrans.SelectedIndex > 0)
                    ipcLaborTrans.SelectedIndex--;
                else
                {
                    if (data.AllContVo.Count == 0)
                    {
                        ipcProgressTrans.SelectedIndex =1;
                        if (ipcProgressTrans.SelectedIndex == 1)
                        {
                            if (data.AllContBoqItems.Count == 0)
                            {
                                ProgressTrans.SelectedIndex = 0;

                                if (data.Interim == true || data.Final == true)
                                    ipcProgressTrans.SelectedIndex = 0;
                            }
                            else ProgressTrans.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        ipcProgressTrans.SelectedIndex = 2;

                    }

                   
                }
            }
            else if (ipcProgressTrans.SelectedIndex == 2)
            {
                ipcProgressTrans.SelectedIndex = 1;
          
                if (data.AllContBoqItems.Count == 0)
                {
                    ProgressTrans.SelectedIndex = 0;
               
                    if (data.Interim == true || data.Final == true)
                        ipcProgressTrans.SelectedIndex = 0;
                }
                else ProgressTrans.SelectedIndex = 1;
            }
            else if(ipcProgressTrans.SelectedIndex == 1)
            {
                if (ProgressTrans.SelectedIndex == 1)
                {
                    if (data.Interim == true || data.Final == true)
                        ipcProgressTrans.SelectedIndex = 0;
                    else
                        ProgressTrans.SelectedIndex = 0;
                }
                else
                    ipcProgressTrans.SelectedIndex = 0;

            }
            else if(ipcProgressTrans.SelectedIndex == 0)
            {
                ipcProgressTrans.SelectedIndex = 0;
                ipcTrans.SelectedIndex = 0;
            }
        }
       

        private void ipcProgressTrans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ipcLaborTrans
            if (ipcProgressTrans.SelectedIndex < 3)
            {
                if(pbadmin != null)
                pbadmin.Value = 17 * (ipcProgressTrans.SelectedIndex + 1);
            }
              
            else if (ipcProgressTrans.SelectedIndex == 3)
            {
                if (pbadmin != null)
                    pbadmin.Value = 17 * (ipcProgressTrans.SelectedIndex + 1 + ipcLaborTrans.SelectedIndex);
            }
            else if (ipcProgressTrans.SelectedIndex > 3)
            {
                if (pbadmin != null)
                    pbadmin.Value = 100;
            }
        }

        private void ipcLaborTrans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ipcLaborTrans.SelectedIndex == 0 )
            {
                laborRb.IsChecked = true;
            } 
            else if (ipcLaborTrans.SelectedIndex == 1)
            {
                materialsRb.IsChecked = true;
            }
            else
            {
                machineRb.IsChecked = true;
            }
        }

        private void laborRb_Clicked(object sender, RoutedEventArgs e)
        {
            if (laborRb.IsChecked == true)
                ipcLaborTrans.SelectedIndex = 0;
        }

        private void materialsRb_Clicked(object sender, RoutedEventArgs e)
        {
            if (materialsRb.IsChecked == true)
                ipcLaborTrans.SelectedIndex = 1;
        }

        private void machineRb_Clicked(object sender, RoutedEventArgs e)
        {
            if (machineRb.IsChecked == true)
                ipcLaborTrans.SelectedIndex = 2;
        }

        private void Pbadmin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
    
        var left = pbadmin.Value * 19;
        if (left < 1870 && left > 19)
            if (myBorder != null)
                myBorder.Margin = new Thickness(left, -20, 0, 0);
        
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            ipcProgressTrans.SelectedIndex = 0;
            ipcTrans.SelectedIndex = 0;
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            
                var border = VisualTreeHelper.GetChild(boqProgress, 0) as Decorator;
                if(border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    //scroll.ScrollToVerticalOffset(e.VerticalOffset);
                    scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
                }
            
        }

        private void VoScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            
                var border = VisualTreeHelper.GetChild(voProgress, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    //scroll.ScrollToVerticalOffset(e.VerticalOffset);
                    scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
                }
            
        }

        private void LaborScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
           
                var border = VisualTreeHelper.GetChild(laborGrid, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    //scroll.ScrollToVerticalOffset(e.VerticalOffset);
                    scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
                }
            
        }

        private void MaterialScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            
                var border = VisualTreeHelper.GetChild(materialGrid, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    //scroll.ScrollToVerticalOffset(e.VerticalOffset);
                    scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
                }
            
        }

        private void MachineScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
           
                var border = VisualTreeHelper.GetChild(machineGrid, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    //scroll.ScrollToVerticalOffset(e.VerticalOffset);
                    scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
                }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
