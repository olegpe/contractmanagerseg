﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for ContractDatabase.xaml
    /// </summary>
    public partial class ContractDatabase : UserControl
    {
        public static ProgressBar Pb { get; set; }
        public ContractDatabase()
        {
            InitializeComponent();
            Pb = pbadmin;
        }

        private void PrivBtn1_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 0;
            //trans.SelectedIndex = 0;
        }

        private void RbDataset1_Click(object sender, RoutedEventArgs e)
        {
            contractDatasetTrans.SelectedIndex = 2;
        }

        private void RbDataset2_Click(object sender, RoutedEventArgs e)
        {
            contractDatasetTrans.SelectedIndex = 4;

        }

        private void RbDataset3_Click(object sender, RoutedEventArgs e)
        {
            contractDatasetTrans.SelectedIndex = 5;

        }

        private void Pbadmin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var left = pbadmin.Value * 19;
            if (left < 1870 && left > 19)
                if (myBorder != null)
                    myBorder.Margin = new Thickness(left, -20, 0, 10);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            contractDatasetTrans.SelectedIndex = 3;

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Visible;
            DimmedGrid.Visibility = Visibility.Visible;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            contractDatasetTrans.SelectedIndex = 1;
        }

        private void voScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(voGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.mainMenu.DimmedGrid.Visibility = Visibility.Collapsed;
            DimmedGrid.Visibility = Visibility.Collapsed;
        }

        private void ExistingContractsDataset_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ExistingContractsDataset_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
