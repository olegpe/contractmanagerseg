﻿using ContractsManager.Data.Excel;
using ContractsManager.Dialogs;
using ContractsManager.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for PreviewWord.xaml
    /// </summary>
    public partial class PreviewWord : Window
    {
        public PreviewWord()
        {
            InitializeComponent();
        }

        

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
          
            
        }

        private async void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            await((PreviewWordViewModel)DataContext).Reject(null);
            this.DialogResult = false;

        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

        }
    }
}
