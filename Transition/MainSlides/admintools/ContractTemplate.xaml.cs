﻿
using System.Windows.Controls;

namespace ContractsManager.MainSlides.admintools
{
    /// <summary>
    /// Interaction logic for ContractTemplate.xaml
    /// </summary>
    public partial class ContractTemplate : UserControl
    {
        public ContractTemplate()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            object ID = ((Button)sender).CommandParameter;
        }

        private void DataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            dgContracts.CommitEdit(DataGridEditingUnit.Row, false);
        }


        private void DataGridVO_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            dgVOContracts.CommitEdit(DataGridEditingUnit.Row, false);
        }
    }
}
