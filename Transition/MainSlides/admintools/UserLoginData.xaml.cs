﻿using ContractsManager.Data;
using ContractsManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides.admintools
{
    /// <summary>
    /// Interaction logic for UserLoginData.xaml
    /// </summary>
    public partial class UserLoginData : UserControl
    {
        public UserLoginData()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            var d = win.AdminToolsSlide.UserSlide.DataContext as UserViewModel;

            var button = sender as Button;

            d.DeleteCommand.Execute(button.Tag as User);

        }
        private void Button1_Click(object sender, RoutedEventArgs e)
        {

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            var d = win.AdminToolsSlide.UserSlide.DataContext as UserViewModel;

            var button = sender as Button;

            d.SaveCommand.Execute(button.Tag as User);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            var d = win.AdminToolsSlide.UserSlide.DataContext as UserViewModel;

            var button = sender as Button;

            d.UploadFileCommand.Execute(button.Tag as User);

        }
    }
}
