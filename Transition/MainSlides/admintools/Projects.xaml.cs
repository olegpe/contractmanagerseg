﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ContractsManager.ViewModel;
using MaterialDesignThemes.Wpf.Transitions;

namespace ContractsManager.MainSlides.admintools
{
    /// <summary>
    /// Interaction logic for Projects.xaml
    /// </summary>
    public partial class Projects : UserControl
    {
        public Projects()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
         //   boqTrans.SelectedIndex = 1;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void DataGrid_InitializingNewItem(object sender, InitializingNewItemEventArgs e)
        {
            var dataGride = sender as DataGrid;
         //   boqGrid.CommitEdit(DataGridEditingUnit.Row, false);
        }

        private void BoqGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            //var dataGride = sender as DataGrid;
            //dataGride.CommitEdit(DataGridEditingUnit.Row, false);
        }

        private void BoqGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //var dataGride = sender as DataGrid;
            //dataGride.CommitEdit(DataGridEditingUnit.Row, false);
        }

        private void BoqScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(boqGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void VoScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var border = VisualTreeHelper.GetChild(voGrid, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                scroll.ScrollToVerticalOffset(e.VerticalOffset);
                scroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void BoqGrid_LoadingRowDetails(object sender, DataGridRowDetailsEventArgs e)
        {
           
        }

        private void BoqGrid_Loaded_1(object sender, RoutedEventArgs e)
        {
 boqGrid.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("No", System.ComponentModel.ListSortDirection.Ascending));
        }
    }
}
