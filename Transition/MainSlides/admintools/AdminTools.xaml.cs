﻿using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ContractsManager.ViewModel;

namespace ContractsManager.MainSlides.admintools
{
    /// <summary>
    /// Interaction logic for AdminTools.xaml
    /// </summary>
    public partial class AdminTools : UserControl
    {
 

        public AdminTools()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           /* int index = int.Parse(((Button)e.Source).Uid);
            Thickness margin = new Thickness();

            //GridCursor.Margin = new Thickness(10 + (150 * index), 0, 0, 0);
            var slide = new ThicknessAnimationUsingKeyFrames()
            {
                Duration = TimeSpan.FromSeconds(0.2),
            };


            if (index == 3)
            {
                GridCursor.Width = 168;
                margin = new Thickness(10 + (150 * index), 0, 0, 0);
            }
            else if (index == 4)
            {
                margin = new Thickness(10 + (150 * index + 18), 0, 0, 0);
                GridCursor.Width = 206;
            }
            else
            {
                GridCursor.Width = 150;
                margin = new Thickness(10 + (150 * index), 0, 0, 0);
            }

            slide.KeyFrames.Add(new LinearThicknessKeyFrame(margin, TimeSpan.FromSeconds(0.2))
                );

            Storyboard.SetTarget(slide, GridCursor);
            Storyboard.SetTargetProperty(slide, new PropertyPath("Margin"));
            var sb = new Storyboard();
            sb.Children.Add(slide);
            sb.Begin();

            switch (index)
            {
                case 0:
                    adminTrans.SelectedIndex = 0;
                    break;
                case 1:
                    adminTrans.SelectedIndex = 1;
                    break;
                case 2:
                    adminTrans.SelectedIndex = 2;
                    break;
                case 3:
                    adminTrans.SelectedIndex = 3;
                    break;
                case 4:
                    adminTrans.SelectedIndex = 4;
                    break;
                case 5:
                    adminTrans.SelectedIndex = 5;
                    break;
            }*/
        }

        private void BackFromAdminBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 0;
        }


        private int currentElement = 0;

        private void Left_Click(object sender, RoutedEventArgs e)
        {
            if (currentElement < 2)
            {
                currentElement++;
                AnimateCarousel();
            }
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            if (currentElement > 0)
            {
                currentElement--;
                AnimateCarousel();
            }
        }

        private void AnimateCarousel()
        {
            Storyboard storyboard = (this.Resources["CarouselStoryboard"] as Storyboard);
            DoubleAnimation animation = storyboard.Children.First() as DoubleAnimation;
            animation.To = -356 * currentElement;

            storyboard.Begin();
        }

        private void Rb2_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 0;
        }
  
        private void Rb4_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 5;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.contractTemplate.DataContext = new ContractsViewModel();
        }

        private void Rb7_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 2;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.costCodeSlide.DataContext = new CostCodeLibraryViewModel();
        }

        private void Rb8_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 4;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.UserSlide.DataContext = new UserViewModel();
        }

        private void Rb9_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 1;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.Currencies.DataContext = new CurrencyViewModel();
        }
        private void AdminTrans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        { 
           
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Transitioner.MovePreviousCommand.Execute(null, adminTrans);
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            Transitioner.MoveNextCommand.Execute(null, adminTrans);
        }

        private void Pbadmin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Rb10_Clicked(object sender, RoutedEventArgs e)
        {
            adminTrans.SelectedIndex = 3;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.AdminToolsSlide.Sheets.DataContext = new SheetViewModel();
        }
    }
}
