﻿using ContractsManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContractsManager.MainSlides.admintools
{
    /// <summary>
    /// Interaction logic for VenteLocal.xaml
    /// </summary>
    public partial class VenteLocal : UserControl
    {
        public VenteLocal()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.DeductionDatabaseSlide.DataContext = new DeducationsViewModel();
        }
    }
}
