﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ContractsManager.Components;
using ContractsManager.Data;
using ContractsManager.Data.utils;
using ContractsManager.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace ContractsManager.MainSlides
{
    /// <summary>
    /// Interaction logic for MainWindowSlide.xaml
    /// </summary>
    public partial class MainWindowSlide : UserControl
    {

        private string _text;
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
            }
        }
        public User User { get; set; }
        private MainWindowSlideViewModel mwvm;
        public MainWindowSlide()
        {
            InitializeComponent();
            mwvm = new MainWindowSlideViewModel();
            DataContext = mwvm;
            //mwvm.RunExtendedDialogCommand.Execute("parameter");
            User = LoginUser.LoadSession();
            if (User != null)
            {
                login(User);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            loginBtn.IsEnabled = true;
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            loginTrans.SelectedIndex = 0;
        }

        public void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            login();
            
        }
      

        private void LoginTrans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(loginTrans.SelectedIndex > 0)
            {
                loginBtn.IsEnabled = true;
            }
        }

        public void LogoutBtn_Click(object sender, RoutedEventArgs e)
        {
            LoginUser.DestroySession();
            #region Logout
            var fade0 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade1 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade2 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade3 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade4 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFC7C5C5"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade5 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFE2E2E2"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            var fade6 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFE2E2E2"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
          //  var fade7 = new ColorAnimation()
           // {
            //    To = (Color)ColorConverter.ConvertFromString("#FFE2E2E2"),
            //    Duration = TimeSpan.FromSeconds(0.5),
           // };
            var fade8 = new ColorAnimation()
            {
                To = (Color)ColorConverter.ConvertFromString("#FFE2E2E2"),
                Duration = TimeSpan.FromSeconds(0.5),
            };
            
            Storyboard.SetTarget(fade0, card0);
            Storyboard.SetTargetProperty(fade0, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));
           
            Storyboard.SetTarget(fade, card);
            Storyboard.SetTargetProperty(fade, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));
            
            Storyboard.SetTarget(fade1, card1);
            Storyboard.SetTargetProperty(fade1, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));
            
            Storyboard.SetTarget(fade2, card2);
            Storyboard.SetTargetProperty(fade2, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));
            
            Storyboard.SetTarget(fade3, card3);
            Storyboard.SetTargetProperty(fade3, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));

            Storyboard.SetTarget(fade4, card4);
            Storyboard.SetTargetProperty(fade4, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));

            //Storyboard.SetTarget(fade7, card7);
            //Storyboard.SetTargetProperty(fade7, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));
            
            var sb = new Storyboard();

            sb.Children.Add(fade0);
            sb.Children.Add(fade);
            sb.Children.Add(fade1);
            sb.Children.Add(fade2);
            sb.Children.Add(fade3);
            sb.Children.Add(fade4);

            //sb.Children.Add(fade7);


            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            timer.Start();
            timer.Tick += (senders, args) =>
            {
                timer.Stop();

                pnlFlip0.IsEnabled = false;
                pnlFlip.IsEnabled = false;
                pnlFlip1.IsEnabled = false;
                pnlFlip2.IsEnabled = false;
                pnlFlip3.IsEnabled = false;
                pnlFlip4.IsEnabled = false;

               // mainMenu.notifBtn.IsEnabled = false;
               // mainMenu.adminBtn.IsEnabled = false;
               // mainMenu.language.IsEnabled = false;
               // mainMenu.notifCounter.Visibility = Visibility.Visible;
                sb.Begin();
                loginCardTrans.SelectedIndex = 0;
                loginBtnTrans.SelectedIndex = 0;
                loginTrans.SelectedIndex = 0;
                passwordBox.Password = "";
                loginTB.Text = "";
                loginBtn.IsEnabled = true;
                this.Resources["FGColor"] = Brushes.Gray;
                var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                win.mainMenu.IsEnabled = false;
            };
            #endregion
        }

        private void PnlFlip1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 1;

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            if(win.ContractDatabaseSlide.DataContext != null)
            {
                ((ContractsDatasetViewModel)win.ContractDatabaseSlide.DataContext).SelectedProject = null;
                win.ContractDatabaseSlide.DataContext = null;
            }
            
            win.ContractDatabaseSlide.DataContext = new ContractsDatasetViewModel();

            win.mainMenu.firstMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Black;

            ///win.ContractDatabaseSlide.mainMenu = new MainMenu();
            if (User.UserType != UserType.GeneralManager && User.UserType != UserType.OperationsManager && User.UserType != UserType.Admin)
            //win.ContractDatabaseSlide.mainMenu.adminBtn.Visibility = Visibility.Collapsed;
            { }
            else
            //win.ContractDatabaseSlide.mainMenu.adminBtn.Visibility = Visibility.Visible;
            { }
        } 

        private void PnlFlip_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 2;
            //MainWindow.UserControlDatabaseSlide = new ContractsDatasetSlide();
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.databaseSlide.DataContext = new ContractsDatabaseViewModel();
            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.firstMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Black;

            //win.databaseSlide.mainMenu = new MainMenu();
            if (User.UserType != UserType.GeneralManager && User.UserType != UserType.OperationsManager && User.UserType != UserType.Admin)
            //win.databaseSlide.mainMenu.adminBtn.Visibility = Visibility.Collapsed;
            { }
            else
            //win.databaseSlide.mainMenu.mainMenu.adminBtn.Visibility = Visibility.Visible;
            { }
        }

        private void PnlFlip3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 3;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            if (win.DeductionDatabaseSlide.DataContext != null)
            {
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedProject = null;
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedSubcontractor = null;
                ((DeducationsViewModel)win.DeductionDatabaseSlide.DataContext).SelectedContractsDataset = null;
            }
                
            win.DeductionDatabaseSlide.DataContext = new DeducationsViewModel();

            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.firstMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Black;
            //win.DeductionDatabaseSlide.mainMenu = new MainMenu();
            if (User.UserType != UserType.GeneralManager && User.UserType != UserType.OperationsManager && User.UserType != UserType.Admin)
            // win.DeductionDatabaseSlide.mainMenu.adminBtn.Visibility = Visibility.Collapsed;
            { }
            else
            //win.DeductionDatabaseSlide.mainMenu.adminBtn.Visibility = Visibility.Visible;
            { }
        }

        private void PnlFlip2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 5;

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            if(win.IpcDatabaseSlide.DataContext!= null)
            {
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedProject = null;
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedSubcontractor = null;
                ((IpcViewModel)win.IpcDatabaseSlide.DataContext).SelectedContractsDataset = null;
            }
            
            win.IpcDatabaseSlide.DataContext = new IpcViewModel();
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.firstMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Black;

          
        }

        private void PnlFlip4_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 6;
            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.reportSlide.DataContext = new ReportViewModel(DialogCoordinator.Instance);

            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.firstMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Black;

           
        }

        private void PnlFlip0_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.TransiSlider.SelectedIndex = 8;

            var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            win.ProjectsSlide.DataContext = new ProjectsViewModel();
            win.mainMenu.fifthMenuBtn.Foreground = Brushes.Silver;
            win.mainMenu.SixthMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.fourthtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.secondtMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.firstMenuBtn.Foreground = Brushes.Black;
            win.mainMenu.thirdtMenuBtn.Foreground = Brushes.Black;

            

        }

        private void LoginTB_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                loginTrans.SelectedIndex = 1;
                passwordBox.Focus();
            }
        }

        private void PasswordBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                loginBtn.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            }
        }

        #region Helper
        private void login(User user = null)
        {
            var pass = passwordBox.Password;
            var username = loginTB.Text;
            loginBtnTrans.SelectedIndex = 1;
            Validation.ClearInvalid(loginTB.GetBindingExpression(TextBox.TextProperty));

            Task.Factory.StartNew(() =>
            {
                if(user == null)
                {
                    var conString = Connection.GetConnectionStrings().Where(x => x.IsCurrent == true).FirstOrDefault();
                    if (conString == null)
                    {
                        return; // TODO ERROR MESSAGE
                    }
                    using (var context = new DataContext(Connection.BuildConnectionString(conString)))
                    {
                        user = context.Users.FirstOrDefault(u => u.Username == username && u.Password == pass);
                    }
                }
               
                if (user != null)
                {
                    LoginUser.SaveSession(user);
                    User = user;
                    CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                    customPrincipal.Identity = new CustomIdentity(user.Id, user.Name, user.LastName, user.Email, user.UserType);
                    this.mwvm.LoginCommand.Execute(null);
                    #region login
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new ThreadStart(() => {

                            
                  

                            var fade0 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade1 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade2 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade3 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade4 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade5 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                            var fade6 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };
                           // var fade7 = new ColorAnimation()
                           // {
                           //     To = Colors.WhiteSmoke,
                           //     Duration = TimeSpan.FromSeconds(0.5),
                           // };
                            var fade8 = new ColorAnimation()
                            {
                                To = Colors.WhiteSmoke,
                                Duration = TimeSpan.FromSeconds(0.5),
                            };

                            Storyboard.SetTarget(fade, card);
                            Storyboard.SetTargetProperty(fade, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));

                            Storyboard.SetTarget(fade0, card0);
                            Storyboard.SetTargetProperty(fade0, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));


                            Storyboard.SetTarget(fade1, card1);
                            Storyboard.SetTargetProperty(fade1, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));


                            Storyboard.SetTarget(fade2, card2);
                            Storyboard.SetTargetProperty(fade2, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));


                            Storyboard.SetTarget(fade3, card3);
                            Storyboard.SetTargetProperty(fade3, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));


                            Storyboard.SetTarget(fade4, card4);
                            Storyboard.SetTargetProperty(fade4, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));

                            //Storyboard.SetTarget(fade7, card7);
                            //Storyboard.SetTargetProperty(fade7, new PropertyPath("(Grid.Background).(SolidColorBrush.Color)"));


                            var sb = new Storyboard();

                            if (user.UserType != UserType.QuantitySurveyor && user.UserType != UserType.Accountant)
                            {
                                sb.Children.Add(fade2);
                            }

                            if(user.UserType != UserType.Accountant)
                            {
                                sb.Children.Add(fade0);
                                sb.Children.Add(fade);
                                sb.Children.Add(fade1);
                                sb.Children.Add(fade4);
                            }
                            sb.Children.Add(fade3);
                     

                            //sb.Children.Add(fade7);
                            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
                            timer.Start();
                            timer.Tick += (senders, args) =>
                            {
                                timer.Stop();

                                if (user.UserType != UserType.QuantitySurveyor && user.UserType != UserType.Accountant)
                                {
                                    pnlFlip1.IsEnabled = true;

                                }
                                if (user.UserType != UserType.Accountant)
                                {
                                    pnlFlip0.IsEnabled = true;
                                    pnlFlip.IsEnabled = true;
                                    pnlFlip3.IsEnabled = true;
                                    pnlFlip4.IsEnabled = true;
                                }
                                    pnlFlip2.IsEnabled = true;

                                sb.Begin();
                                loginCardTrans.SelectedIndex = 1;
                                loginBtnTrans.SelectedIndex = 0;
                                this.Resources["FGColor"] = Brushes.Black;
                                //this.mainMenu = new MainMenu();

                                var win = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                                //win.mainMenu = new MainMenu();
                                win.mainMenu.IsEnabled = true;
                                win.mainMenu.DataContext = new MainMenuViewModel();
                            };
                        }));
                    #endregion
                    
                }
                else
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                  new ThreadStart(() =>
                  {
                      ValidationError validationError =
                            new ValidationError(new ExceptionValidationRule(),
                            loginTB.GetBindingExpression(TextBox.TextProperty));

                      validationError.ErrorContent = "Invalid e-mail address or password";

                      Validation.MarkInvalid(
                          loginTB.GetBindingExpression(TextBox.TextProperty),
                          validationError);


                      loginBtnTrans.SelectedIndex = 0;
                  }));
                }
            });
        }
        #endregion
    }
}

/*
  <Grid Grid.Column="0"  Margin="40,0,14,1"  >
                      
                        <StackPanel Orientation="Horizontal" Margin="0,44,-0.2,92.4">
                            <ProgressBar Style="{StaticResource MaterialDesignCircularProgressBar}" Value="0" IsIndeterminate="True" />
                            <TextBlock FontFamily="../fonts/#Meticula" FontSize="14" FontWeight="SemiBold" Margin="10,8,0,-0.2">Logging in...</TextBlock>
                        </StackPanel>
                        <Grid Name="loginGrid">
                            <Grid.RenderTransform>
                                <TranslateTransform />
                            </Grid.RenderTransform>
                            <Button Panel.ZIndex="2" FontFamily="../fonts/#Meticula"  Name="loginBtn" IsEnabled="false" Foreground="White" Background="#FF7197E2" Click="LoginBtn_Click_1"
                                              Style="{StaticResource MaterialDesignRaisedLightButton}" Margin="0,33,-0.2,88.4" Height="Auto" >
                                _Login
                            </Button>
                            <Grid.Triggers>
                                <EventTrigger RoutedEvent="Grid.MouseEnter">
                                    <BeginStoryboard>
                                        <Storyboard>
                                            <DoubleAnimation Storyboard.TargetName="loginGrid"  Duration="0:0:0.2" By="-200"
                                                     Storyboard.TargetProperty="(UIElement.RenderTransform).(TranslateTransform.X)"  />
                                        </Storyboard>
                                    </BeginStoryboard>

                                </EventTrigger>
                                <EventTrigger RoutedEvent="Grid.MouseLeave">
                                    <BeginStoryboard>
                                        <Storyboard>
                                            <DoubleAnimation Storyboard.TargetName="loginGrid"  Duration="0:0:0.2" By="200"
                                                     Storyboard.TargetProperty="(UIElement.RenderTransform).(TranslateTransform.X)"  />
                                        </Storyboard>
                                    </BeginStoryboard>
                                </EventTrigger>
                            </Grid.Triggers>
                        </Grid>

                    </Grid>
     */
